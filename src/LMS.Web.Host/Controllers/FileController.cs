﻿using Abp.BackgroundJobs;
using LMS.Controllers;
using LMS.Storage;

namespace LMS.Web.Host.Controllers
{
    public class FileController : FileControllerBase
    {
        public FileController(IBinaryObjectManager binaryObjectManager, IBackgroundJobManager backgroundJobManager) : base(binaryObjectManager, backgroundJobManager)
        {
            
        }

    }
}
