﻿using Abp.UI;
using Abp.Web.Models;
using LMS.Loan.Cases;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace LMS.Web.Host.Controllers
{
    [Route("api/[controller]/[action]")]
    public class UploadFileController : Controller
    {
        protected readonly ICaseAppService _caseAppService;

        public UploadFileController(ICaseAppService caseAppService)
        {
            _caseAppService = caseAppService;
        }

        [HttpPost]
        public async Task<JsonResult> Upload(IFormFile formFile)
        {
            try
            {
                var file = ContentDispositionHeaderValue.Parse(formFile.ContentDisposition).FileName.Trim('"');
                Guid guid = Guid.NewGuid();
                var path = Path.Combine(Directory.GetCurrentDirectory(), "Resources");
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                string extinion = Path.GetExtension(file);
                path = Path.Combine(path, guid.ToString() + extinion);
                using (Stream stream = new FileStream(path, FileMode.Create))
                {
                    await formFile.CopyToAsync(stream);
                }
                return Json(path);
            }
            catch (UserFriendlyException ex)
            {
                return Json(new AjaxResponse(new ErrorInfo(ex.Message)));
            }
        }
    }
}
