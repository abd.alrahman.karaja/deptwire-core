﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace LMS.Application.Extentions
{
    public static class StringExtension
    {
        public static DateTime? GetDateByFormat(this string str, string format)
        {
            try
            {
                List<string> dateSections = GetSectionDate(str);
                List<string> formateSections = GetSectionDate(format);
                if (dateSections.Count >= 3 && formateSections.Count >= 3)
                {
                    int dayPosition = GetPositionFromSection("d", formateSections);
                    int monthPosition = GetPositionFromSection("m", formateSections);
                    int yearPosition = GetPositionFromSection("y", formateSections);

                    var day = GetDay(dateSections[dayPosition]);
                    var month = GetMonth(dateSections[monthPosition]);
                    var year = GetYear(dateSections[yearPosition]);

                    if(day != 0 && month!=0 && year != 0)
                    {
                        return new DateTime(year, month, day);
                    }
                }

                return null;
            }

            catch (Exception)
            {
                return null;
            }
        }

        private static int GetDay(string str)
        {
            try
            {
                int day = 0;
                if (int.TryParse(str, out day))
                {
                    return day;
                }

                return 0;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        private static int GetMonth(string str)
        {
            try
            {
                if (str.Length == 3)
                    return DateTime.ParseExact(str, "MMM", CultureInfo.InvariantCulture).Month;

                else
                {
                    int month = 0;
                    if (int.TryParse(str, out month))
                    {
                        return month;
                    }

                    return 0;
                }
            }
            catch (Exception)
            {
                return 0;
            }
        }
        private static int GetYear(string str)
        {
            try
            {
                int year = 0;
                if (int.TryParse(str, out year))
                {
                    return year;
                }

                return 0;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        private static int GetPositionFromSection(string ch, List<string> formateSections)
        {
            if (formateSections[0].ToLower().Contains(ch))
                return 0;
            else if (formateSections[1].ToLower().Contains(ch))
                return 1;

            return 2;
        }

        private static List<string> GetSectionDate(string str)
        {
            var sections = new List<string>();
            if (str.Contains("/"))
            {
                sections = str.Split("/").ToList();
            }
            else if (str.Contains("-"))
            {
                sections = str.Split("-").ToList();
            }
            else if (str.Contains("."))
            {
                sections = str.Split(".").ToList();
            }
            else if (str.Length == 8 && !str.Contains("/") && !str.Contains("-") & !str.Contains("."))
            {
                sections.Add(str.Substring(0, 2));
                sections.Add(str.Substring(2, 2));
                sections.Add(str.Substring(4, 4));
            }

            return HandleSections(sections);
        }

        private static List<string> HandleSections(List<string> sections)
        {
            var list = new List<string>();
            if (sections.Count >= 3)
            {
                foreach (var section in sections)
                {
                    if (section.Contains(" "))
                        list.Add(section.GetUntilOrEmpty(" "));
                    else
                    {
                        list.Add(section);
                    }
                }
            }

            return list;
        }
        private static string GetUntilOrEmpty(this string text, string stopAt)
        {
            if (!String.IsNullOrWhiteSpace(text))
            {
                int charLocation = text.IndexOf(stopAt, StringComparison.Ordinal);

                if (charLocation > 0)
                {
                    return text.Substring(0, charLocation);
                }
            }

            return String.Empty;
        }
    }
}
