﻿using Abp.Domain.Repositories;
using LMS.Loan.Customers.Dto;
using LMS.Loan.Customers.Dto.CustomerEmails;
using LMS.Loan.Customers.Dto.CustomerMobiles;
using LMS.Loan.Customers.Dto.CustomerOfficePhones;
using LMS.Loan.Customers.Dto.CustomerPhones;
using LMS.Loan.Customers.Dto.CustomerReferencePhones;
using LMS.Loan.Customers.Dto.CustomerSocialMediaAccounts;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LMS.Loan.Customers
{
    public class CustomerAppService : LMSAppServiceBase, ICustomerAppService
    {
        private readonly IRepository<Customer> _repository;
        private readonly CustomerManager _customerManager;
        public CustomerAppService(
            IRepository<Customer> repository, 
            CustomerManager customerManager)
        {
            _repository = repository;
            _customerManager = customerManager;

        }
        public async Task<IList<CustomerDto>> GetAllAsync()
        {
            var customers = await _repository.GetAllListAsync();
            return ObjectMapper.Map<List<CustomerDto>>(customers);
        }

        public async Task<IList<ReadCustomerDto>> GetAllForGridAsync()
        {
            var customers = await _repository.GetAllListAsync();
            return ObjectMapper.Map<List<ReadCustomerDto>>(customers);
        }

        public async Task<CustomerDto> GetById(int id)
        {
            var customer = await _repository.GetAsync(id);
            return ObjectMapper.Map<CustomerDto>(customer);
        }
        public UpdateCustomerInputDto GetCustomerForEdit(int id)
        {
            var dto = new UpdateCustomerInputDto();
            var customer = _customerManager.GetCustomerById(id);
            dto.Customer = ObjectMapper.Map<UpdateCustomerDto>(customer);
            dto.Phones = ObjectMapper.Map<IList<CustomerPhoneDto>>(customer.Phones);
            dto.OfficePhone = ObjectMapper.Map<IList<CustomerOfficePhoneDto>>(customer.OfficePhone);
            dto.ReferencePhones = ObjectMapper.Map<IList<CustomerReferencePhoneDto>>(customer.ReferencePhones);
            dto.Mobiles = ObjectMapper.Map<IList<CustomerMobileDto>>(customer.Mobiles);
            dto.Emails = ObjectMapper.Map<IList<CustomerEmailDto>>(customer.Emails);
            dto.SocialMediaAccounts = ObjectMapper.Map<IList<CustomerSocialMediaAccountDto>>(customer.SocialMediaAccounts);

            return dto;
        }

        public async Task<CustomerDto> CreateAsync(CreateCustomerDto input)
        {
            var customer = ObjectMapper.Map<Customer>(input.Value);

            await _repository.InsertAndGetIdAsync(customer);

            return ObjectMapper.Map<CustomerDto>(customer);
        }
        public async Task<CustomerDto> UpdateAsync(UpdateCustomerInputDto input)
        {
            var customer = await _repository.GetAsync(input.Customer.Id);

            ObjectMapper.Map<UpdateCustomerDto, Customer>(input.Customer, customer);

            var phones = ObjectMapper.Map<IList<CustomerPhone>>(input.Phones);
            var referencePhones = ObjectMapper.Map<IList<CustomerReferencePhone>>(input.ReferencePhones);
            var officePhones = ObjectMapper.Map<IList<CustomerOfficePhone>>(input.OfficePhone);
            var mobiles= ObjectMapper.Map<IList<CustomerMobile>>(input.Mobiles);
            var emails= ObjectMapper.Map<IList<CustomerEmail>>(input.Emails);
            var socialMediaAccounts= ObjectMapper.Map<IList<CustomerSocialMediaAccount>>(input.SocialMediaAccounts);

            await _customerManager.UpdateAsync(customer, phones, officePhones, referencePhones, mobiles, emails, socialMediaAccounts);
            

            //var updatedCustomer = await _repository.UpdateAsync(customer);

            //var customer = await _repository.GetAsync(input.Id);

            return ObjectMapper.Map<CustomerDto>(customer);
        }
        public async Task DeleteAsync(int id)
        {
            var customer = await _repository.GetAsync(id);
            await _repository.DeleteAsync(customer);
        }

        
    }
}
