﻿using LMS.Loan.Shared.Dto;

namespace LMS.Loan.Customers.Dto.CustomerOfficePhones
{
    public class CustomerOfficePhoneDto : PhoneBaseDto
    {
        public int CustomerId { get; set; }
    }
}
