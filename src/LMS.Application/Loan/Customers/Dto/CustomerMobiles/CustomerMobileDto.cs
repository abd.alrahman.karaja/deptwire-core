﻿using LMS.Loan.Shared.Dto;

namespace LMS.Loan.Customers.Dto.CustomerMobiles
{
    public class CustomerMobileDto : PhoneBaseDto
    {
        public int CustomerId { get; set; }
    }
}
