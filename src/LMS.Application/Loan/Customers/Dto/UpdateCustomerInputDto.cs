﻿using LMS.Loan.Customers.Dto.CustomerEmails;
using LMS.Loan.Customers.Dto.CustomerMobiles;
using LMS.Loan.Customers.Dto.CustomerOfficePhones;
using LMS.Loan.Customers.Dto.CustomerPhones;
using LMS.Loan.Customers.Dto.CustomerReferencePhones;
using LMS.Loan.Customers.Dto.CustomerSocialMediaAccounts;
using System.Collections.Generic;

namespace LMS.Loan.Customers.Dto
{
    public class UpdateCustomerInputDto
    {
        public UpdateCustomerInputDto()
        {
            ReferencePhones = new List<CustomerReferencePhoneDto>();
            Phones = new List<CustomerPhoneDto>();
            OfficePhone = new List<CustomerOfficePhoneDto>();
            Emails = new List<CustomerEmailDto>();
            Mobiles = new List<CustomerMobileDto>();
            SocialMediaAccounts = new List<CustomerSocialMediaAccountDto>();
        }

        public UpdateCustomerDto Customer { get; set; }
        /// <summary>
        /// هواتف الشخص المرجع
        /// </summary>
        public virtual IList<CustomerReferencePhoneDto> ReferencePhones { get; set; }
        /// <summary>
        /// هواتف المكتب
        /// </summary>
        public virtual IList<CustomerOfficePhoneDto> OfficePhone { get; set; }
        /// <summary>
        /// الهواتف
        /// </summary>
        public virtual IList<CustomerPhoneDto> Phones { get; set; }
        /// <summary>
        /// الايميلات
        /// </summary>
        public virtual IList<CustomerEmailDto> Emails { get; set; }

        /// <summary>
        /// ارقام الموبايلات
        /// </summary>
        public virtual IList<CustomerMobileDto> Mobiles { get; set; }

        /// <summary>
        /// الحسابات على مواقع التواصل الاجتملعي
        /// </summary>
        public virtual IList<CustomerSocialMediaAccountDto> SocialMediaAccounts { get; set; }
    }
}
