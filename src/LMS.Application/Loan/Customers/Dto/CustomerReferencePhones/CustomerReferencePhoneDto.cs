﻿using LMS.Loan.Shared.Dto;

namespace LMS.Loan.Customers.Dto.CustomerReferencePhones
{
    public class CustomerReferencePhoneDto:PhoneBaseDto
    {
        public int CustomerId { get; set; }
    }
}
