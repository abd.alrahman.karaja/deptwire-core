﻿using Abp.Application.Services.Dto;
using LMS.Loan.Customers.Dto.CustomerEmails;
using LMS.Loan.Customers.Dto.CustomerMobiles;
using LMS.Loan.Customers.Dto.CustomerOfficePhones;
using LMS.Loan.Customers.Dto.CustomerPhones;
using LMS.Loan.Customers.Dto.CustomerReferencePhones;
using LMS.Loan.Customers.Dto.CustomerSocialMediaAccounts;
using LMS.Loan.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace LMS.Loan.Customers.Dto
{
    public class CustomerDto : EntityDto
    {
        public CustomerDto()
        {
            ReferencePhones = new List<CustomerReferencePhoneDto>();
            Phones = new List<CustomerPhoneDto>();
            OfficePhone = new List<CustomerOfficePhoneDto>();
            Emails = new List<CustomerEmailDto>();
            Mobiles = new List<CustomerMobileDto>();
            SocialMediaAccounts = new List<CustomerSocialMediaAccountDto>();
        }

        #region Personal Info

        /// <summary>
        /// الاسم
        /// </summary>
        [MaxLength(LMSConsts.MinStringLength)]
        public string Name { get; set; }

        /// <summary>
        /// اسم العميل بالعربي
        /// </summary>
        [MaxLength(LMSConsts.MinStringLength)]
        public string ArabicName { get; set; }

        /// <summary>
        /// اسم الام
        /// </summary>
        [MaxLength(LMSConsts.MinStringLength)]
        public string MotherName { get; set; }

        /// <summary>
        /// الجنس
        /// </summary>
        public string Gender { get; set; }

        /// <summary>
        /// العمر
        /// </summary>
        public int Age { get; set; }

        /// <summary>
        /// اسم شركة الزبون
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// المواليد
        /// </summary>
        public DateTime? DOB { get; set; }
        #endregion

        #region Contact Info

        /// <summary>
        /// رقم موبايل الزبون كما هو مزود من قبل البنك 
        /// </summary>
        public string CustomerMobileByBank { get; set; }

        /// <summary>
        /// ايميل الزبون كما هو مزود من قبل البنك 
        /// </summary>
        public string CustomerEmailByBank { get; set; }

        /// <summary>
        /// اسم الشخص المرجعي او الصديق كما هو مزود من قبل البنك
        /// </summary>
        public string ReferenceNameByBank { get; set; }

        /// <summary>
        /// رقم موبايل الصديق المزود من قبل البنك
        /// </summary>
        public string ReferenceMobileByBank { get; set; }

        #endregion

        #region Passport Info

        /// <summary>
        /// رقم جواز السفر
        /// </summary>
        public string PassportNo { get; set; }

        /// <summary>
        /// صلاحية جواز السفر
        /// </summary>
        public DateTime? PassportExpiry { get; set; }
        #endregion

        #region Other Info

        /// <summary>
        /// رقم الإقامة
        /// </summary>
        public string ResidenceNo { get; set; }

        /// <summary>
        /// رقم التأشيرة
        /// </summary>
        public string VisaNo { get; set; }

        #endregion

        /// <summary>
        /// تواجد العميل خارج او داخل موطن البنك
        /// </summary>
        public string InsideOrOutsideBankCountry { get; set; }

        /// <summary>
        /// خارج او داخل سوريا
        /// </summary>
        public string InsideOrOutsideSyria { get; set; }

        /// <summary>
        /// مكان تواجد الزبون حاليا
        /// </summary>
        public string CurrentLocation { get; set; }

        /// <summary>
        /// مهنة العميل
        /// </summary>
        public string CustomerCareer { get; set; }

        /// <summary>
        /// جهة عمل الزبون الحالي
        /// </summary>
        public string CurrentJob { get; set; }

        /// <summary>
        /// العنوان في سوريا
        /// </summary>
        public string HomeAddress { get; set; }
        
        public int NationalityId { get; set; }
        public virtual IList<CustomerReferencePhoneDto> ReferencePhones { get; set; }
        public virtual IList<CustomerOfficePhoneDto> OfficePhone { get; set; }
        public virtual IList<CustomerPhoneDto> Phones { get; set; }
        public virtual IList<CustomerEmailDto> Emails { get; set; }
        public virtual IList<CustomerMobileDto> Mobiles { get; set; }
        public virtual IList<CustomerSocialMediaAccountDto> SocialMediaAccounts { get; set; }

    }
}
