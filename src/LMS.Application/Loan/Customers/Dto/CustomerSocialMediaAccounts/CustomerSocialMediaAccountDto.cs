﻿using Abp.Application.Services.Dto;

namespace LMS.Loan.Customers.Dto.CustomerSocialMediaAccounts
{
    public class CustomerSocialMediaAccountDto : EntityDto
    {
        public int SocialMediaSite { get; set; }
        public string Name { get; set; }
        public int CustomerId { get; set; }
    }
}
