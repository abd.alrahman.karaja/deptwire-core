﻿using System;

namespace LMS.Loan.Customers.Dto
{
    public class TempCustomerDto 
    {
        #region Personal Info

        /// <summary>
        /// الاسم
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// اسم العميل بالعربي
        /// </summary>
        public string ArabicName { get; set; }

        /// <summary>
        /// اسم الام
        /// </summary>
        public string MotherName { get; set; }

        /// <summary>
        /// الجنس
        /// </summary>
        public string Gender { get; set; }

        /// <summary>
        /// العمر
        /// </summary>
        public int Age { get; set; }

        /// <summary>
        /// المواليد
        /// </summary>
        public DateTime? DOB { get; set; }
        #endregion

        #region Contact Info

        /// <summary>
        /// رقم موبايل الزبون كما هو مزود من قبل البنك 
        /// </summary>
        public string CustomerMobileByBank { get; set; }

        /// <summary>
        /// ايميل الزبون كما هو مزود من قبل البنك 
        /// </summary>
        public string CustomerEmailByBank { get; set; }

        /// <summary>
        /// اسم الشخص المرجعي او الصديق كما هو مزود من قبل البنك
        /// </summary>
        public string ReferenceNameByBank { get; set; }

        /// <summary>
        /// رقم موبايل الصديق المزود من قبل البنك
        /// </summary>
        public string ReferenceMobileByBank { get; set; }

        #endregion

        #region Passport Info

        /// <summary>
        /// رقم جواز السفر
        /// </summary>
        public string PassportNo { get; set; }

        /// <summary>
        /// صلاحية جواز السفر
        /// </summary>
        public DateTime? PassportExpiry { get; set; }
        #endregion

        #region Other Inof

        /// <summary>
        /// رقم الإقامة
        /// </summary>
        public string ResidenceNo { get; set; }

        /// <summary>
        /// رقم التأشيرة
        /// </summary>
        public string VisaNo { get; set; }

        #endregion

        public int CustomerId { get; set; }
    }
}
