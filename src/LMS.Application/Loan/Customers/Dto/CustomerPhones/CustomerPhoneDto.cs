﻿using LMS.Loan.Shared.Dto;

namespace LMS.Loan.Customers.Dto.CustomerPhones
{
    public class CustomerPhoneDto : PhoneBaseDto
    {
        public int CustomerId { get; set; }
    }
}
