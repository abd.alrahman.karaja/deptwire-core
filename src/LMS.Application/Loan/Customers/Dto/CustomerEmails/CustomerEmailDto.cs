﻿using LMS.Loan.Shared.Dto;

namespace LMS.Loan.Customers.Dto.CustomerEmails
{
    public class CustomerEmailDto : EmailBaseDto
    {
        public int CustomerId { get; set; }
    }
}
