﻿using LMS.Loan.Customers.Dto.CustomerEmails;
using LMS.Loan.Customers.Dto.CustomerMobiles;
using LMS.Loan.Customers.Dto.CustomerOfficePhones;
using LMS.Loan.Customers.Dto.CustomerPhones;
using LMS.Loan.Customers.Dto.CustomerReferencePhones;
using LMS.Loan.Customers.Dto.CustomerSocialMediaAccounts;
using LMS.Loan.Indexes.Shared.Dto;
using System;
using System.Collections.Generic;

namespace LMS.Loan.Customers.Dto
{
    public class ReadCustomerDto
    {
        public ReadCustomerDto()
        {
            referencePhones = new List<CustomerReferencePhoneDto>();
            phones = new List<CustomerPhoneDto>();
            emails = new List<CustomerEmailDto>();
            mobiles = new List<CustomerMobileDto>();
            socialMediaAccounts = new List<CustomerSocialMediaAccountDto>();
        }
        public int id { get; set; }
        public string name { get; set; }
        public string arabicName { get; set; }
        public string motherName { get; set; }
        public string gender { get; set; }
        public int age { get; set; }
        public DateTime? dob { get; set; }
        public string customerMobileByBank { get; set; }
        public string customerEmailByBank { get; set; }
        public string referenceNameByBank { get; set; }
        public string referenceMobileByBank { get; set; }
        public string passportNo { get; set; }
        public DateTime? passportExpiry { get; set; }
        public string residenceNo { get; set; }
        public string visaNo { get; set; }
        public string insideOrOutsideBankCountry { get; set; }
        public string insideOrOutsideSyria { get; set; }
        public string currentLocation { get; set; }
        public string customerCareer { get; set; }
        public string currentJob { get; set; }
        public string homeAddress { get; set; }
        public int nationalityId { get; set; }
        public ReadIndexDto nationality { get; set; }
        public virtual IList<CustomerReferencePhoneDto> referencePhones { get; set; }
        public virtual IList<CustomerOfficePhoneDto> officePhone { get; set; }
        public virtual IList<CustomerPhoneDto> phones { get; set; }
        public virtual IList<CustomerEmailDto> emails { get; set; }
        public virtual IList<CustomerMobileDto> mobiles { get; set; }
        public virtual IList<CustomerSocialMediaAccountDto> socialMediaAccounts { get; set; }
    }
}
