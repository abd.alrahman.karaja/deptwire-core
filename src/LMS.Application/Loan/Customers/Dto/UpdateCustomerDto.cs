﻿using Abp.Application.Services.Dto;

namespace LMS.Loan.Customers.Dto
{
    public class UpdateCustomerDto : EntityDto
    {

        /// <summary>
        /// تواجد العميل خارج او داخل موطن البنك
        /// </summary>
        public string InsideOrOutsideBankCountry { get; set; }

        /// <summary>
        /// خارج او داخل سوريا
        /// </summary>
        public string InsideOrOutsideSyria { get; set; }

        /// <summary>
        /// مكان تواجد الزبون حاليا
        /// </summary>
        public string CurrentLocation { get; set; }

        /// <summary>
        /// مهنة العميل
        /// </summary>
        public string CustomerCareer { get; set; }

        /// <summary>
        /// جهة عمل الزبون الحالي
        /// </summary>
        public string CurrentJob { get; set; }

        /// <summary>
        /// العنوان في سوريا
        /// </summary>
        public string HomeAddress { get; set; }

        
    }
}
