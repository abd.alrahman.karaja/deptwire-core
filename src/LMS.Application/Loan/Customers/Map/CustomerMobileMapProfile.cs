﻿using AutoMapper;
using LMS.Loan.Customers.Dto.CustomerMobiles;

namespace LMS.Loan.Customers.Map
{
    public class CustomerMobileMapProfile : Profile
    {
        public CustomerMobileMapProfile()
        {
            CreateMap<CustomerMobile, CustomerMobileDto>();
            CreateMap<CustomerMobileDto, CustomerMobile>();
        }
    }
}
