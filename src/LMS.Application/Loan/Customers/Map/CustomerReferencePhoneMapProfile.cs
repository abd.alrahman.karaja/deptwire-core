﻿using AutoMapper;
using LMS.Loan.Customers.Dto.CustomerReferencePhones;

namespace LMS.Loan.Customers.Map
{
    public class CustomerReferencePhoneMapProfile:Profile
    {
        public CustomerReferencePhoneMapProfile()
        {
            CreateMap<CustomerReferencePhone, CustomerReferencePhoneDto>();
            CreateMap<CustomerReferencePhoneDto, CustomerReferencePhone>();
        }
    }
}
