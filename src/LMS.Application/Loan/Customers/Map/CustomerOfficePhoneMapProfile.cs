﻿using AutoMapper;
using LMS.Loan.Customers.Dto.CustomerOfficePhones;

namespace LMS.Loan.Customers.Map
{
    public class CustomerOfficePhoneMapProfile:Profile
    {
        public CustomerOfficePhoneMapProfile()
        {
            CreateMap<CustomerOfficePhone, CustomerOfficePhoneDto>();
            CreateMap<CustomerOfficePhoneDto, CustomerOfficePhone>();
        }
    }
}
