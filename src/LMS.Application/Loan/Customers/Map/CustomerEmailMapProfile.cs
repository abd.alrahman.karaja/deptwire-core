﻿using AutoMapper;
using LMS.Loan.Customers.Dto.CustomerEmails;

namespace LMS.Loan.Customers.Map
{
    public class CustomerEmailMapProfile:Profile
    {
        public CustomerEmailMapProfile()
        {
            CreateMap<CustomerEmail, CustomerEmailDto>();
            CreateMap<CustomerEmailDto, CustomerEmail>();
        }
    }
}
