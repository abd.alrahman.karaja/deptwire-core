﻿using AutoMapper;
using LMS.Loan.Customers.Dto.CustomerSocialMediaAccounts;

namespace LMS.Loan.Customers.Map
{
    public class CustomerSocialMediaAccountMapProfile : Profile
    {
        public CustomerSocialMediaAccountMapProfile()
        {
            CreateMap<CustomerSocialMediaAccount, CustomerSocialMediaAccountDto>();
            CreateMap<CustomerSocialMediaAccountDto, CustomerSocialMediaAccount>();
        }
    }
}
