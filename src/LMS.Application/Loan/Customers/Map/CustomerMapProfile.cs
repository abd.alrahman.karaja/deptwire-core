﻿using AutoMapper;
using LMS.Loan.Cases.Dto.ImportCases;
using LMS.Loan.Customers.Dto;

namespace LMS.Loan.Customers.Map
{
    public class CustomerMapProfile:Profile
    {
        public CustomerMapProfile()
        {
            CreateMap<Customer, CustomerDto>();
            CreateMap<Customer, ReadCustomerDto>();
            CreateMap<CustomerDto, Customer>();
            CreateMap<CreateCustomerDto, Customer>();
            CreateMap<UpdateCustomerDto, Customer>();
            CreateMap<Customer, UpdateCustomerDto>();
            CreateMap<ImportCaseDto, Customer>()
                .ForMember(x => x.Name, x => x.MapFrom(y => y.CustomerName))
                .ForMember(x => x.Nationality, x => x.Ignore());
        }
    }
}
