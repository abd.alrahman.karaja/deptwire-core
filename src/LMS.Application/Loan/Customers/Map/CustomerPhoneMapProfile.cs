﻿using AutoMapper;
using LMS.Loan.Customers.Dto.CustomerPhones;

namespace LMS.Loan.Customers.Map
{
    public class CustomerPhoneMapProfile : Profile
    {
        public CustomerPhoneMapProfile()
        {
            CreateMap<CustomerPhone, CustomerPhoneDto>();
            CreateMap<CustomerPhoneDto, CustomerPhone>();
        }
    }
}
