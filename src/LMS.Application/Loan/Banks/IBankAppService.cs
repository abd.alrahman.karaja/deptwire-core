﻿using Abp.Application.Services;
using LMS.Crud.Dto;
using LMS.Loan.Banks.Dto;
using Microsoft.AspNetCore.Mvc;
using Syncfusion.EJ2.Base;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LMS.Loan.Banks
{
    public interface IBankAppService : IApplicationService
    {
        Task<IList<BankDto>> GetAllAsync();
        Task<ReadGrudDto> GetForGrid([FromBody] DataManagerRequest dm);
        Task<BankDto> GetById(int id);
        Task<UpdateBankDto> GetForEdit(int id);
        Task<BankDto> CreateAsync(CreateBankDto input);
        Task<BankDto> UpdateAsync(UpdateBankDto input);
        Task DeleteAsync(int id);
    }
}
