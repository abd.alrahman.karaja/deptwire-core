﻿using Abp;
using Abp.Domain.Repositories;
using Abp.UI;
using LMS.Crud.Dto;
using LMS.Loan.Banks;
using LMS.Loan.Banks.Dto;
using Microsoft.AspNetCore.Mvc;
using Syncfusion.EJ2.Base;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Loan.Banks
{
    public class BankAppService : AbpServiceBase, IBankAppService
    {
        private readonly IRepository<Bank> _repository;

        public BankAppService(IRepository<Bank> repository)
        {
            _repository = repository;
        }

        public async Task<IList<BankDto>> GetAllAsync()
        {
            var banks = await _repository.GetAllListAsync();
            return ObjectMapper.Map<List<BankDto>>(banks);
        }

        [HttpPost]
        public async Task<ReadGrudDto> GetForGrid([FromBody] DataManagerRequest dm)
        {
            var data = await _repository.GetAllListAsync();
            IEnumerable<ReadBankDto> banks = ObjectMapper.Map<List<ReadBankDto>>(data);

            var operations = new DataOperations();
            if (dm.Where != null && dm.Where.Count > 0)
            {
                banks = operations.PerformFiltering(banks, dm.Where, "and");
            }

            if (dm.Sorted != null && dm.Sorted.Count > 0)
            {
                banks = operations.PerformSorting(banks, dm.Sorted);
            }

            var count = banks.Count();

            if (dm.Skip != 0)
            {
                banks = operations.PerformSkip(banks, dm.Skip);
            }

            if (dm.Take != 0)
            {
                banks = operations.PerformTake(banks, dm.Take);
            }

            return new ReadGrudDto() { result = banks, count = count };
        }

        public async Task<UpdateBankDto> GetForEdit(int id)
        {
            var bank = await _repository.GetAsync(id);
            return ObjectMapper.Map<UpdateBankDto>(bank);
        }

        public async Task<BankDto> GetById(int id)
        {
            var bank = await _repository.GetAsync(id);
            return ObjectMapper.Map<BankDto>(bank);
        }

        public async Task<BankDto> CreateAsync(CreateBankDto input)
        {
            CheckBeforeCreate(input);

            var bank = ObjectMapper.Map<Bank>(input);

            await _repository.InsertAsync(bank);

            return ObjectMapper.Map<BankDto>(bank);
        }

        public async Task<BankDto> UpdateAsync(UpdateBankDto input)
        {
            CheckBeforeUpdate(input);
            var bank = await _repository.GetAsync(input.Id);
            ObjectMapper.Map<UpdateBankDto, Bank>(input, bank);

            var updatedBank = await _repository.UpdateAsync(bank);

            return ObjectMapper.Map<BankDto>(updatedBank);
        }

        public async Task DeleteAsync(int id)
        {
            var bank = await _repository.GetAsync(id);
            await _repository.DeleteAsync(bank);
        }

        #region Helper methods
        private void CheckBeforeCreate(CreateBankDto input)
        {
            var validationResultMessage = string.Empty;

            if (_repository.FirstOrDefault(x => x.Name == input.Name) != null)
            {
                validationResultMessage = $"{L(ValidationResultMessage.NameAleadyExist)} \n";
            }

            if (_repository.FirstOrDefault(x => x.ShortName == input.ShortName) != null)
            {
                validationResultMessage = $"{L(ValidationResultMessage.ShortNameAleadyExist)} \n";
            }

            if (!string.IsNullOrEmpty(validationResultMessage))
                throw new UserFriendlyException(validationResultMessage);
        }

        private void CheckBeforeUpdate(UpdateBankDto input)
        {
            var validationResultMessage = string.Empty;

            if (_repository.FirstOrDefault(x => x.Name == input.Name && x.Id != input.Id) != null)
            {
                validationResultMessage = $"{L(ValidationResultMessage.NameAleadyExist)} \n";
            }

            if (_repository.FirstOrDefault(x => x.ShortName == input.ShortName && x.Id != input.Id) != null)
            {
                validationResultMessage = $"{L(ValidationResultMessage.ShortNameAleadyExist)} \n";
            }

            if (!string.IsNullOrEmpty(validationResultMessage))
                throw new UserFriendlyException(validationResultMessage);
        }

        #endregion
    }
}
