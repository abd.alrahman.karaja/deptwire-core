﻿using Abp.Application.Services;
using LMS.Crud.Dto;
using LMS.Loan.Banks.Dto;
using Microsoft.AspNetCore.Mvc;
using Syncfusion.EJ2.Base;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LMS.Loan.Banks
{
    public interface IBankCodeAppService : IApplicationService
    {
        Task<IList<BankCodeDto>> GetAllAsync();
        Task<ReadGrudDto> GetForGrid([FromBody] DataManagerRequest dm);
        Task<BankCodeDto> GetById(int id);
        Task<UpdateBankCodeDto> GetForEdit(int id);
        Task<BankCodeDto> CreateAsync(CreateBankCodeDto input);
        Task<BankCodeDto> UpdateAsync(UpdateBankCodeDto input);
        Task DeleteAsync(int id);
    }
}
