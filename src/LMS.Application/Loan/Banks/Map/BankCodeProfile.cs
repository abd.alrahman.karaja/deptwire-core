﻿using AutoMapper;
using LMS.Loan.Banks;
using LMS.Loan.Banks.Dto;

namespace LMS.Loan.BankCodes.Map
{
    public class BankCodeProfile : Profile
    {
        public BankCodeProfile()
        {
            CreateMap<BankCode, BankCodeDto>();
            CreateMap<BankCode, ReadBankCodeDto>();
            CreateMap<BankCode, UpdateBankCodeDto>();
            CreateMap<BankCodeDto, BankCode>();
            CreateMap<CreateBankCodeDto, BankCode>();
            CreateMap<UpdateBankCodeDto, BankCode>();
        }
    }
}
