﻿using AutoMapper;
using LMS.Loan.Banks.Dto;

namespace LMS.Loan.Banks.Map
{
    public class BankProfile : Profile
    {
        public BankProfile()
        {
            CreateMap<Bank, BankDto>();
            CreateMap<Bank, ReadBankDto>();
            CreateMap<Bank, UpdateBankDto>();
            CreateMap<BankDto, Bank>();
            CreateMap<CreateBankDto, Bank>();
            CreateMap<UpdateBankDto, Bank>();
        }
    }
}
