﻿using Abp;
using Abp.Domain.Repositories;
using Abp.UI;
using LMS.Crud.Dto;
using LMS.Loan.Banks.Dto;
using LMS.Loan.Cases;
using Microsoft.AspNetCore.Mvc;
using Syncfusion.EJ2.Base;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Loan.Banks
{
    public class BankCodeAppService : AbpServiceBase, IBankCodeAppService
    {
        private readonly IRepository<BankCode> _repository;
        public BankCodeAppService(IRepository<BankCode> repository)
        {
            _repository = repository;
        }
        public async Task<IList<BankCodeDto>> GetAllAsync()
        {
            var banks = await _repository.GetAllListAsync();
            return ObjectMapper.Map<List<BankCodeDto>>(banks);
        }

        [HttpPost]
        public async Task<ReadGrudDto> GetForGrid([FromBody] DataManagerRequest dm)
        {
            var data = await _repository.GetAllListAsync();
            IEnumerable<ReadBankCodeDto> banks = ObjectMapper.Map<List<ReadBankCodeDto>>(data);

            var operations = new DataOperations();
            if (dm.Where != null && dm.Where.Count > 0)
            {
                banks = operations.PerformFiltering(banks, dm.Where, "and");
            }

            if (dm.Sorted != null && dm.Sorted.Count > 0)
            {
                banks = operations.PerformSorting(banks, dm.Sorted);
            }

            var count = banks.Count();

            if (dm.Skip != 0)
            {
                banks = operations.PerformSkip(banks, dm.Skip);
            }

            if (dm.Take != 0)
            {
                banks = operations.PerformTake(banks, dm.Take);
            }

            banks = banks.OrderByDescending(x => x.id);

            return new ReadGrudDto() { result = banks, count = count };
        }

        public async Task<UpdateBankCodeDto> GetForEdit(int id)
        {
            var bank = await _repository.GetAsync(id);
            return ObjectMapper.Map<UpdateBankCodeDto>(bank);
        }

        public async Task<BankCodeDto> GetById(int id)
        {
            var bank = await _repository.GetAsync(id);
            return ObjectMapper.Map<BankCodeDto>(bank);
        }

        public async Task<BankCodeDto> CreateAsync(CreateBankCodeDto input)
        {
            CheckBeforeCreate(input);
            var bank = ObjectMapper.Map<BankCode>(input);
            
            await _repository.InsertAndGetIdAsync(bank);

            return ObjectMapper.Map<BankCodeDto>(bank);
        }
        public async Task<BankCodeDto> UpdateAsync(UpdateBankCodeDto input)
        {
            CheckBeforeUpdate(input);
            var bank = await _repository.GetAsync(input.Id);
            ObjectMapper.Map<UpdateBankCodeDto, BankCode>(input, bank);

            var updatedBankCode = await _repository.UpdateAsync(bank);

            return ObjectMapper.Map<BankCodeDto>(updatedBankCode);
        }
        public async Task DeleteAsync(int id)
        {
            var bank = await _repository.GetAsync(id);
            await _repository.DeleteAsync(bank);
        }

        #region Helper methods
        private void CheckBeforeCreate(CreateBankCodeDto input)
        {
            var validationResultMessage = string.Empty;

            /// <AlternativeFlows>
            /// إضافة نفس الكود لنفس البنك مرتين
            /// </AlternativeFlows>
            /// <message>
            /// الكود موجود مسبقا
            ///</message>
            if (_repository.FirstOrDefault(x => x.Code == input.Code && x.BankId == input.BankId) != null)
            {
                validationResultMessage = $"{L(ValidationResultMessage.CodeAleadyExist)} \n";
            }

            /// <AlternativeFlows>
            /// إضافة نفس الحالة لكودين لنفس البنك
            /// </AlternativeFlows>
            /// <message>
            /// لا يمكنك إضافة نفس الحالة لكودين
            ///</message>
            if (_repository.FirstOrDefault(x => x.CaseStatus == ((CaseStatus)input.CaseStatus) && x.BankId == input.BankId) != null)
            {
                validationResultMessage = $"{L(ValidationResultMessage.YouCanNotAddTheSameStatusForTwoCode)}";
            }

            if (!string.IsNullOrEmpty(validationResultMessage))
                throw new UserFriendlyException(validationResultMessage);
        }
        private void CheckBeforeUpdate(UpdateBankCodeDto input)
        {
            var validationResultMessage = string.Empty;

            /// <AlternativeFlows>
            /// إضافة نفس الكود لنفس البنك مرتين
            /// </AlternativeFlows>
            /// <message>
            /// الكود موجود مسبقا
            ///</message>
            if (_repository.FirstOrDefault(x => x.Code == input.Code && x.Id != input.Id && x.BankId == input.BankId) != null)
            {
                validationResultMessage = $"{L(ValidationResultMessage.CodeAleadyExist)} \n";
            }

            /// <AlternativeFlows>
            /// إضافة نفس الحالة لكودين لنفس البنك
            /// </AlternativeFlows>
            /// <message>
            /// لا يمكنك إضافة نفس الحالة لكودين
            ///</message>
            if (_repository.FirstOrDefault(x => x.CaseStatus == ((CaseStatus)input.CaseStatus) && x.Id != input.Id && x.BankId == input.BankId) != null)
            {
                validationResultMessage = $"{L(ValidationResultMessage.YouCanNotAddTheSameStatusForTwoCode)}";
            }

            if (!string.IsNullOrEmpty(validationResultMessage))
                throw new UserFriendlyException(validationResultMessage);
        }


        #endregion
    }
}
