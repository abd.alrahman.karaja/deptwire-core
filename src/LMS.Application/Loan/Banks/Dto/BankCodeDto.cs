﻿using Abp.Application.Services.Dto;

namespace LMS.Loan.Banks.Dto
{
    public class BankCodeDto : EntityDto
    {
        public string Code { get; set; }
        public int BankId { get; set; }
        public int CaseStatus { get; set; }
    }
}
