﻿namespace LMS.Loan.Banks.Dto
{
    public class ReadBankCodeDto
    {
        public int id { get; set; }
        public string code { get; set; }
        public int cankId { get; set; }
        public int caseStatus { get; set; }
    }
}
