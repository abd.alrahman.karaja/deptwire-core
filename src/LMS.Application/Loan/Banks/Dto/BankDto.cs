﻿using Abp.Application.Services.Dto;

namespace LMS.Loan.Banks.Dto
{
    public class BankDto : EntityDto
    {
        public string Name { get; set; }
        public string ArabicName { get; set; }
        public string ShortName { get; set; }
        public int CountryId { get; set; }
        public string DateFormat { get; set; }
    }
}
