﻿
namespace LMS.Loan.Banks.Dto
{
    public class ReadBankDto
    {
        public int id { get; set; }
        public string name { get; set; }
        public string arabicName { get; set; }
        public string shortName { get; set; }
        public string dateFormat { get; set; }
        public int countryId { get; set; }
    }
}
