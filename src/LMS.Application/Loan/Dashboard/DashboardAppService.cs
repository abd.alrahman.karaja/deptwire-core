﻿using Abp;
using Abp.Runtime.Session;
using LMS.Authorization.Users;
using LMS.Loan.Cases;
using LMS.Loan.Cases.Dto.Calls;
using LMS.Loan.Cases.Dto.Meetings;
using LMS.Loan.Cases.Dto.Offers;
using LMS.Loan.Cases.Dto.Payments;
using LMS.Loan.Cases.Dto.PTP;
using LMS.Loan.Cases.Meetings;
using LMS.Loan.Cases.Offers;
using LMS.Loan.Dashboard.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Loan.Dashboard
{
    public class DashboardAppService : LMSAppServiceBase, IDashboardAppService
    {
        private readonly CaseManager _caseManager;
        private readonly MeetingManager _meetingManager;
        public DashboardAppService(MeetingManager meetingManager, CaseManager caseManager)
        {
            _caseManager = caseManager;
            _meetingManager = meetingManager;
        }
        public async Task<DashboardOutputDto> Get(DashboardInputDto input)
        {
            var dto = new DashboardOutputDto();

            var cases = await _caseManager.GetCasesAsync(input.CurrentUserId, input.DataAccess);
            //It is not All
            if(input.UserId != 0)
            {
                cases = cases.Where(x => x.UserId == input.UserId);
            }
            
            //Total and Close cases
            dto.TotalCase = cases.Count();
            dto.ClosedCase = cases.Count(x => x.WaedStatus == CaseStatus.ClosedAndReturned);

            var date = new DateTime();
            if (!string.IsNullOrEmpty(input.Date))
            {
                date = DateTime.Parse(input.Date);
            }
            else
            {
                date = DateTime.Now;
            }

            var casesIds = cases.Select(x => x.Id).Distinct().ToArray();
            
            var caseOffers = cases.SelectMany(x => x.CaseOffers);
            if (caseOffers.Count() > 0)
            {
                //Offers Reminders
                var offersReminders = caseOffers.Select(x => x.Offer)
                    .Where(x => x.ReminderDate != null && x.ReminderDate.Value.Date == date.Date).ToList();
                dto.OffersReminders = offersReminders.Count;

                //Offers
                var offers = caseOffers.Select(x => x.Offer)
                    .Where(x => x.OfferDate != null && x.OfferDate.Value.Date == date.Date).ToList();
                dto.Offers = offers.Count;
            }

            var caseMeetings = cases.SelectMany(x => x.CaseMeetings);
            if (caseMeetings.Count() > 0)
            {
                //Meetings Reminders
                var meetingsReminders = _meetingManager.GetCaseMeetingsByReminderDate(date.Date, casesIds)
                    .Select(x => x.Meeting).ToList();
                dto.MeetingsReminders = meetingsReminders.Count;

                //Meetings
                var meetings = _meetingManager.GetCaseMeetingsByDate(date)
                    .Select(x => x.Meeting).ToList();
                dto.Meetings = meetings.Count;
            }

            //if (caseMeetings.Count() > 0)
            //{
            //    //Meetings Reminders
            //    var meetingsReminders = caseMeetings.Select(x => x.Meeting)
            //        .Where(x => x.ReminderDate != null && x.ReminderDate.Value.Date == date.Date).ToList();
            //    dto.MeetingsReminders = meetingsReminders.Count;

            //    //Meetings
            //    var meetings = caseMeetings.Select(x => x.Meeting)
            //        .Where(x => x.MeetingDate != null && x.MeetingDate.Value.Date == date.Date).ToList();
            //    dto.Meetings = meetings.Count;
            //}

            var caseCalls = cases.SelectMany(x => x.CaseCalls);
            if (caseCalls.Count() > 0)
            {
                //Calls Reminders
                var callsReminders = caseCalls.Select(x => x.Call)
                    .Where(x => x.ReminderDate != null && x.ReminderDate.Value.Date == date.Date).ToList();
                dto.CallsReminders = callsReminders.Count;

                //Calls
                var calls = caseCalls.Select(x => x.Call)
                    .Where(x => x.CallDate != null && x.CallDate.Value.Date == date.Date).ToList();
                dto.Calls = calls.Count;
            }

            var casePayments = cases.SelectMany(x => x.CasePayments);
            if (casePayments.Count() > 0)
            {
                var PaymentsReminders = casePayments.Select(x => x.Payment)
                    .Where(x => x.ReminderDate != null && x.ReminderDate.Value.Date == date.Date).ToList();
                dto.PaymentsReminders = PaymentsReminders.Count;

                //Money collected
                var paymentsPerMonth = casePayments.Select(x => x.Payment)
                    .Where(x => 
                    x.ReceivedDate != null && 
                    x.ReceivedDate.Value.Year == date.Year &&
                    x.ReceivedDate.Value.Month == date.Month).ToList();
                if (paymentsPerMonth.Any())
                {
                    if(input.UserId != 0)
                    {
                        var paymentsPerMonthForSpecificUser = paymentsPerMonth
                            .Where(x => x.CollectedById != null && x.CollectedById == input.UserId);
                        if (paymentsPerMonthForSpecificUser.Any())
                        {
                            dto.MoneyCollected = paymentsPerMonthForSpecificUser.Sum(x => x.Amount);
                        }
                    }
                    else
                    {
                        dto.MoneyCollected = paymentsPerMonth.Sum(x => x.Amount);
                    }
                }
                
            }

            ////PTP Reminders
            //var casePromiseToPays = cases.SelectMany(x => x.CasePromisesToPays);
            //if (casePromiseToPays.Count() > 0)
            //{
            //    var PromiseToPaysReminders = casePromiseToPays.Select(x => x.PromiseToPay)
            //        .Where(x => x.ReminderDate != null && x.ReminderDate.Value.Date == date.Date).ToList();
            //    dto.PromiseToPaysReminders = PromiseToPaysReminders.Count;
            //}

            return dto;
        }
        public async Task<IList<OfferDashboardDto>> GetOffers(DashboardInputDto input)
        {
            var offersDto = new List<OfferDashboardDto>();

            var cases = await _caseManager.GetCasesAsync(input.CurrentUserId, input.DataAccess);
            //It is not All
            if (input.UserId != 0)
            {
                cases = cases.Where(x => x.UserId == input.UserId);
            }

            var date = DateTime.Parse(input.Date);
            var caseOffers = cases.SelectMany(x => x.CaseOffers);
            var offers = caseOffers.Select(x => x.Offer)
                .Where(x => x.OfferDate != null && x.OfferDate.Value.Date == date.Date).ToList();

            offersDto = (from o in offers
                         select new OfferDashboardDto()
                         {
                             Id = o.Id,
                             ApprovalDate = o.ApprovalDate,
                             EMIs = o.EMIs,
                             FirstPayment = o.FirstPayment,
                             FirstPaymentDate = o.FirstPaymentDate,
                             OfferAmount = o.OfferAmount,
                             OfferDate = o.OfferDate,
                             OfferStatus = (int)o.OfferStatus,
                             CasesIds = caseOffers.Where(x => x.OfferId == o.Id).Select(x => x.CaseId).ToArray()
                         }).ToList();

            return offersDto;
        }
        public async Task<IList<OfferDashboardDto>> GetOffersReminders(DashboardInputDto input)
        {
            var offersDto = new List<OfferDashboardDto>();

            var cases = await _caseManager.GetCasesAsync(input.CurrentUserId, input.DataAccess);
            //It is not All
            if (input.UserId != 0)
            {
                cases = cases.Where(x => x.UserId == input.UserId);
            }

            var date = DateTime.Parse(input.Date);
            var caseOffers = cases.SelectMany(x => x.CaseOffers);
            var offers = caseOffers.Select(x => x.Offer)
                .Where(x => x.ReminderDate != null && x.ReminderDate.Value.Date == date.Date).ToList();

            offersDto = (from o in offers
                         select new OfferDashboardDto()
                         {
                             Id = o.Id,
                             ApprovalDate = o.ApprovalDate,
                             EMIs = o.EMIs,
                             FirstPayment = o.FirstPayment,
                             FirstPaymentDate = o.FirstPaymentDate,
                             OfferAmount = o.OfferAmount,
                             OfferDate = o.OfferDate,
                             OfferStatus = (int)o.OfferStatus,
                             CasesIds = caseOffers.Where(x => x.OfferId == o.Id).Select(x => x.CaseId).ToArray()
                         }).ToList();

            return offersDto;
        }
        public async Task<IList<MeetingDashboardDto>> GetMeetings(DashboardInputDto input)
        {
            var meetingsDto = new List<MeetingDashboardDto>();

            var cases = await _caseManager.GetCasesAsync(input.CurrentUserId, input.DataAccess);
            //It is not All
            if (input.UserId != 0)
            {
                cases = cases.Where(x => x.UserId == input.UserId);
            }

            var date = DateTime.Parse(input.Date);
            var caseMeetings = cases.SelectMany(x => x.CaseMeetings);
            //Meetings
            var meetings = _meetingManager.GetCaseMeetingsByDate(date)
                .Select(x => x.Meeting).ToList();

            meetingsDto = (from o in meetings
                         select new MeetingDashboardDto()
                         {
                             Id = o.Id,
                             Done = o.Done,
                             LegalMeeting = o.LegalMeeting,
                             ManagerTeamName = o.ManagerTeamName != null ? o.ManagerTeamName.FullName : string.Empty,
                             MeetingManagerName = o.MeetingManagerName != null ? o.MeetingManagerName.FullName : string.Empty,
                             MeetingDate = o.MeetingDate,
                             MeetingEnd = o.MeetingEnd,
                             MeetingStart = o.MeetingStart,
                             MeetingWith = o.MeetingWith,
                             CasesIds = caseMeetings.Where(x => x.MeetingId == o.Id).Select(x => x.CaseId).ToArray()
                         }).ToList();

            return meetingsDto;
        }
        public async Task<IList<MeetingDashboardDto>> GetMeetingsReminders(DashboardInputDto input)
        {
            var meetingsDto = new List<MeetingDashboardDto>();

            var cases = await _caseManager.GetCasesAsync(input.CurrentUserId, input.DataAccess);
            var casesIds = cases.Select(x => x.Id).Distinct().ToArray();

            //It is not All
            if (input.UserId != 0)
            {
                cases = cases.Where(x => x.UserId == input.UserId);
            }

            var date = DateTime.Parse(input.Date);
            var caseMeetings = cases.SelectMany(x => x.CaseMeetings);
            //Meetings Reminders
            var meetingsReminders = _meetingManager.GetCaseMeetingsByReminderDate(date.Date, casesIds)
                .Select(x => x.Meeting).ToList();

            meetingsDto = (from o in meetingsReminders
                           select new MeetingDashboardDto()
                           {
                               Id = o.Id,
                               Done = o.Done,
                               LegalMeeting = o.LegalMeeting,
                               ManagerTeamName = o.ManagerTeamName != null ? o.ManagerTeamName.FullName : string.Empty,
                               MeetingManagerName = o.MeetingManagerName != null ? o.MeetingManagerName.FullName : string.Empty,
                               MeetingDate = o.MeetingDate,
                               MeetingEnd = o.MeetingEnd,
                               MeetingStart = o.MeetingStart,
                               MeetingWith = o.MeetingWith,
                               CasesIds = caseMeetings.Where(x => x.MeetingId == o.Id).Select(x => x.CaseId).ToArray()
                           }).ToList();

            return meetingsDto;
        }
        public async Task<IList<CallDashboardDto>> GetCalls(DashboardInputDto input)
        {
            var callsDto = new List<CallDashboardDto>();

            var cases = await _caseManager.GetCasesAsync(input.CurrentUserId, input.DataAccess);
            
            if (input.UserId != 0)
            {
                cases = cases.Where(x => x.UserId == input.UserId);
            }

            var date = DateTime.Parse(input.Date);
            var caseCalls = cases.SelectMany(x => x.CaseCalls);
            var calls = caseCalls.Select(x => x.Call)
                    .Where(x => x.CallDate != null && x.CallDate.Value.Date == date.Date).ToList();

            callsDto = (from o in calls
                        select new CallDashboardDto()
                        {
                            Id = o.Id,
                            CallDate = o.CallDate,
                            CallStart = o.CallStart,
                            CallWith = o.CallWith,
                            CasesIds = caseCalls.Where(x => x.CallId == o.Id).Select(x => x.CaseId).ToArray()
                        }).ToList();

            return callsDto;
        }
        public async Task<IList<CallDashboardDto>> GetCallsReminders(DashboardInputDto input)
        {
            var callsDto = new List<CallDashboardDto>();

            var cases = await _caseManager.GetCasesAsync(input.CurrentUserId, input.DataAccess);

            if (input.UserId != 0)
            {
                cases = cases.Where(x => x.UserId == input.UserId);
            }

            var date = DateTime.Parse(input.Date);
            var caseCalls = cases.SelectMany(x => x.CaseCalls);
            var calls = caseCalls.Select(x => x.Call)
                    .Where(x => x.CallDate != null && x.CallDate.Value.Date == date.Date).ToList();

            callsDto = (from o in calls
                        select new CallDashboardDto()
                        {
                            Id = o.Id,
                            CallDate = o.CallDate,
                            CallStart = o.CallStart,
                            CallWith = o.CallWith,
                            CasesIds = caseCalls.Where(x => x.CallId == o.Id).Select(x => x.CaseId).ToArray()
                        }).ToList();

            return callsDto;
        }
        public async Task<IList<PaymentDashboardDto>> GetMoneyCollectedDetail(DashboardInputDto input)
        {
            var paymentsDto = new List<PaymentDashboardDto>();

            var cases = await _caseManager.GetCasesAsync(input.CurrentUserId, input.DataAccess);

            if (input.UserId != 0)
            {
                cases = cases.Where(x => x.UserId == input.UserId);
            }

            var date = DateTime.Parse(input.Date);

            var casePayments = cases.SelectMany(x => x.CasePayments);
            var paymentsPerMonth = casePayments.Select(x => x.Payment)
                .Where(x =>
                x.ReceivedDate != null &&
                x.ReceivedDate.Value.Year == date.Year &&
                x.ReceivedDate.Value.Month == date.Month).ToList();

            paymentsDto = (from o in paymentsPerMonth
                           select new PaymentDashboardDto()
                           {
                               Id = o.Id,
                               AgentRatio = o.AgentRatio,
                               Amount = o.Amount,
                               AmountByAED = o.AmountByAED,
                               BankRatio = o.BankRatio,
                               ClaimedDate = o.ClaimedDate,
                               CollectedBy = o.CollectedBy != null ? o.CollectedBy.FullName : string.Empty,
                               CommissionAmount = o.CommissionAmount,
                               CommissionRatio = o.CommissionRatio,
                               PlaceOfPayment = o.PlaceOfPayment,
                               ReceiptNo = o.ReceiptNo,
                               ReflectionDate = o.ReflectionDate,
                               TransferredDate = o.TransferredDate,
                               CasesIds = casePayments.Where(x => x.PaymentId == o.Id).Select(x => x.CaseId).ToArray()
                           }).ToList();

            return paymentsDto;
        }
        public async Task<IList<PaymentDashboardDto>> GetPaymentsReminders(DashboardInputDto input)
        {
            var paymentsDto = new List<PaymentDashboardDto>();

            var cases = await _caseManager.GetCasesAsync(input.CurrentUserId, input.DataAccess);

            if (input.UserId != 0)
            {
                cases = cases.Where(x => x.UserId == input.UserId);
            }

            var date = DateTime.Parse(input.Date);

            var casePayments = cases.SelectMany(x => x.CasePayments);
            var PaymentsReminders = casePayments.Select(x => x.Payment)
                    .Where(x => x.ReminderDate != null && x.ReminderDate.Value.Date == date.Date).ToList();

            paymentsDto = (from o in PaymentsReminders
                           select new PaymentDashboardDto()
                           {
                               Id = o.Id,
                               AgentRatio = o.AgentRatio,
                               Amount = o.Amount,
                               AmountByAED = o.AmountByAED,
                               BankRatio = o.BankRatio,
                               ClaimedDate = o.ClaimedDate,
                               CollectedBy = o.CollectedBy != null ? o.CollectedBy.FullName : string.Empty,
                               CommissionAmount = o.CommissionAmount,
                               CommissionRatio = o.CommissionRatio,
                               PlaceOfPayment = o.PlaceOfPayment,
                               ReceiptNo = o.ReceiptNo,
                               ReflectionDate = o.ReflectionDate,
                               TransferredDate = o.TransferredDate,
                               CasesIds = casePayments.Where(x => x.PaymentId == o.Id).Select(x => x.CaseId).ToArray()
                           }).ToList();

            return paymentsDto;
        }

        
    }
}
