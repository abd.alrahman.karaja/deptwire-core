﻿using LMS.Loan.Cases.Dto.Calls;
using LMS.Loan.Cases.Dto.Meetings;
using LMS.Loan.Cases.Dto.Offers;
using LMS.Loan.Cases.Dto.Payments;
using LMS.Loan.Cases.Dto.PTP;
using System.Collections.Generic;

namespace LMS.Loan.Dashboard.Dto
{
    public class DashboardOutputDto
    {
        public int TotalCase { get; set; }
        public int ClosedCase { get; set; }
        public int Offers { get; set; }
        public int Calls { get; set; }
        public int Meetings { get; set; }
        public int OffersReminders { get; set; }
        public int PaymentsReminders { get; set; }
        public int CallsReminders { get; set; }
        public int PromiseToPaysReminders { get; set; }
        public int MeetingsReminders { get; set; }
        public int LawsuitProceduresReminders { get; set; }
        public double MoneyCollected { get; set; }
    }
}
