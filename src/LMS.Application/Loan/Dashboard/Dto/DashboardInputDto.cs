﻿namespace LMS.Loan.Dashboard.Dto
{
    public class DashboardInputDto
    {
        public string Date { get; set; }
        public long UserId { get; set; }
        public long CurrentUserId { get; set; }
        public int DataAccess { get; set; }
    }
}
