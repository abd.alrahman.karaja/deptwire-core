﻿using Abp.Application.Services;
using LMS.Loan.Cases.Dto.Calls;
using LMS.Loan.Cases.Dto.Meetings;
using LMS.Loan.Cases.Dto.Offers;
using LMS.Loan.Cases.Dto.Payments;
using LMS.Loan.Dashboard.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LMS.Loan.Dashboard
{
    public interface IDashboardAppService : IApplicationService
    {
        Task<DashboardOutputDto> Get(DashboardInputDto input);
        Task<IList<OfferDashboardDto>> GetOffers(DashboardInputDto input);
        Task<IList<OfferDashboardDto>> GetOffersReminders(DashboardInputDto input);
        Task<IList<MeetingDashboardDto>> GetMeetings(DashboardInputDto input);
        Task<IList<MeetingDashboardDto>> GetMeetingsReminders(DashboardInputDto input);
        Task<IList<CallDashboardDto>> GetCalls(DashboardInputDto input);
        Task<IList<CallDashboardDto>> GetCallsReminders(DashboardInputDto input);
        Task<IList<PaymentDashboardDto>> GetMoneyCollectedDetail(DashboardInputDto input);
        Task<IList<PaymentDashboardDto>> GetPaymentsReminders(DashboardInputDto input);
    }
}
