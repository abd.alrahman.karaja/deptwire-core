﻿using Abp.Application.Services;
using System.Threading.Tasks;

namespace LMS.Loan.Cases
{
    public interface IImportFromExcelAppService : IApplicationService
    {
        Task SaveFile(int? bankId);
    }
}
