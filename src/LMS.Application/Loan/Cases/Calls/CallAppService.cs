﻿using Abp.UI;
using LMS.Loan.Cases.Dto.Calls;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Loan.Cases.Calls
{
    public class CallAppService : LMSAppServiceBase, ICallAppService
    {
        private readonly CallManager _callManager;
        public CallAppService(CallManager callManager)
        {
            _callManager = callManager;
        }
        public List<ReadCallDto> GetCallsForGrid(int caseId)
        {
            var calls = _callManager.GetCallsForCase(caseId).OrderBy(x => x.CallDate);
            return ObjectMapper.Map<List<ReadCallDto>>(calls);
        }
        public async Task<UpdateCallDto> GetCallForUpdate(int callId)
        {
            var call = await _callManager.GetCallById(callId);
            var caseCalls = await _callManager.GetCaseCallsById(callId);

            var callDto = ObjectMapper.Map<UpdateCallDto>(call);
            callDto.CasesIds = caseCalls.Select(x => x.CaseId).ToArray();

            return callDto;
        }
        public async Task<CallDto> CreateAndAssignAsync(CreateCallDto input)
        {
            var call = ObjectMapper.Map<Call>(input);
            CheckBeforeCreate(call);

            await _callManager.CreateAndAssignAsync(call, input.CasesIds);

            return ObjectMapper.Map<CallDto>(call);
        }
        public async Task<CallDto> UpdateAndAssignAsync(UpdateCallDto input)
        {
            var call = ObjectMapper.Map<Call>(input);
            CheckBeforeUpdate(call);

            await _callManager.UpdateAndAssignAsync(call, input.CasesIds);

            return ObjectMapper.Map<CallDto>(call);
        }
        public async Task Delete(int callId)
        {
            await _callManager.Delete(callId);
        }

        #region Helper Methods
        private void CheckBeforeCreate(Call call)
        {
            var validationResultMessage = string.Empty;

            if (_callManager.CheckIfThereIsAnotherCallAtSameTime(call))
            {
                validationResultMessage = L(ValidationResultMessage.ThereIsAnotherCallAtTheSameTime);
            }

            if (!string.IsNullOrEmpty(validationResultMessage))
                throw new UserFriendlyException(validationResultMessage);
        }
        private void CheckBeforeUpdate(Call call)
        {
            var validationResultMessage = string.Empty;

            if (_callManager.CheckIfThereIsAnotherCallAtSameTime(call))
            {
                validationResultMessage = L(ValidationResultMessage.ThereIsAnotherCallAtTheSameTime);
            }

            if (!string.IsNullOrEmpty(validationResultMessage))
                throw new UserFriendlyException(validationResultMessage);
        }


        #endregion
    }
}
