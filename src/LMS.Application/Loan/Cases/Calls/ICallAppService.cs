﻿using Abp.Application.Services;
using LMS.Loan.Cases.Dto.Calls;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LMS.Loan.Cases.Calls
{
    public interface ICallAppService : IApplicationService
    {
        List<ReadCallDto> GetCallsForGrid(int caseId);
        Task<UpdateCallDto> GetCallForUpdate(int callId);
        Task<CallDto> CreateAndAssignAsync(CreateCallDto call);
        Task<CallDto> UpdateAndAssignAsync(UpdateCallDto call);
        Task Delete(int callId);
    }
}
