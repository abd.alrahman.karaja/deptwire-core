﻿using LMS.Loan.Cases.Dto.PTP;
using LMS.Loan.Cases.Dto.UploadedFile;
using LMS.Loan.Cases.PTP;
using LMS.Loan.Cases.UploadedFiles;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Loan.Cases.UploadedFiles
{
    public class UploadFileAppService : LMSAppServiceBase, IUploadFileAppService
    {
        private readonly UploadFileManager _uploadFileManager;
        public UploadFileAppService(UploadFileManager uploadFileManager)
        {
            _uploadFileManager = uploadFileManager;
        }
        public List<ReadUploadedFileDto> GetUploadedFilesForGrid(int caseId)
        {
            var uploadedFiles = _uploadFileManager.GetUploadedFilesForCase(caseId);
            return ObjectMapper.Map<List<ReadUploadedFileDto>>(uploadedFiles);
        }
        public async Task<UpdateUploadedFileDto> GetUploadedFileForUpdate(int uploadedFileId)
        {
            var uploadedFile = await _uploadFileManager.GetUploadedFileById(uploadedFileId);
            var caseUploadedFiles = await _uploadFileManager.GetCaseUploadedFilesById(uploadedFileId);

            var uploadedFileDto = ObjectMapper.Map<UpdateUploadedFileDto>(uploadedFile);
            uploadedFileDto.CasesIds = caseUploadedFiles.Select(x => x.CaseId).ToArray();

            return uploadedFileDto;
        }
        public async Task<UploadedFileDto> GetUploadedFileById(int uploadedFileId)
        {
            var uploadedFile = await _uploadFileManager.GetUploadedFileById(uploadedFileId);

            var uploadedFileDto = ObjectMapper.Map<UploadedFileDto>(uploadedFile);

            return uploadedFileDto;
        }

        public async Task<UploadedFileDto> CreateAndAssignAsync(CreateUploadedFileDto input)
        {
            var uploadedFile = ObjectMapper.Map<UploadedFile>(input);

            await _uploadFileManager.CreateAndAssignAsync(uploadedFile, input.CasesIds);

            return ObjectMapper.Map<UploadedFileDto>(uploadedFile);
        }
        public async Task<UploadedFileDto> UpdateAndAssignAsync(UpdateUploadedFileDto input)
        {

            var uploadedFile = ObjectMapper.Map<UploadedFile>(input);

            await _uploadFileManager.UpdateAndAssignAsync(uploadedFile, input.CasesIds);

            return ObjectMapper.Map<UploadedFileDto>(uploadedFile);
        }
        public async Task Delete(int uploadedFileId)
        {
            await _uploadFileManager.Delete(uploadedFileId);
        }

        #region Helper Methods

        #endregion
    }
}
