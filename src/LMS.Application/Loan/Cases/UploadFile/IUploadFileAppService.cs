﻿using Abp.Application.Services;
using LMS.Loan.Cases.Dto.PTP;
using LMS.Loan.Cases.Dto.UploadedFile;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LMS.Loan.Cases.UploadedFiles
{
    public interface IUploadFileAppService : IApplicationService
    {
        List<ReadUploadedFileDto> GetUploadedFilesForGrid(int caseId);
        Task<UpdateUploadedFileDto> GetUploadedFileForUpdate(int meetingId);
        Task<UploadedFileDto> GetUploadedFileById(int paymentId);
        Task<UploadedFileDto> CreateAndAssignAsync(CreateUploadedFileDto meeting);
        Task<UploadedFileDto> UpdateAndAssignAsync(UpdateUploadedFileDto meeting);
        Task Delete(int paymentId);

    }
}
