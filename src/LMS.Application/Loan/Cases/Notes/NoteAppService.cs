﻿using Abp.Application.Services;
using LMS.Loan.Cases.Dto.Notes;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Loan.Cases.Notes
{
    public class NoteAppService : LMSAppServiceBase, INoteAppService
    {
        private readonly NoteManager _noteManager;

        public NoteAppService(NoteManager noteManager)
        {
            _noteManager = noteManager;
        }

        public async Task<NoteDto> CreateAndAssignAsync(CreateNoteDto input)
        {
            var note = ObjectMapper.Map<Note>(input);

            await _noteManager.CreateAndAssignAsync(note, input.CasesIds);

            return ObjectMapper.Map<NoteDto>(note);
        }

        public async Task<NoteDto> GetNoteById(int noteId)
        {
            var note = await _noteManager.GetNoteById(noteId);

            var noteDto = ObjectMapper.Map<NoteDto>(note);

            return noteDto;
        }

        public List<NoteDto> GetNotesById(int caseId)
        {
            var notes = _noteManager.GetNotesForCase(caseId).OrderByDescending(x => x.CreationTime);
            return ObjectMapper.Map<List<NoteDto>>(notes);
        }

        public List<CaseNoteDto> GetCaseNotes(int caseId)
        {
            var notes = _noteManager.GetCaseNotes(caseId).OrderByDescending(x => x.CreationTime);
            return ObjectMapper.Map<List<CaseNoteDto>>(notes);
        }

        public List<CaseNoteDto> GetCaseNotesWithoutSpecificAction(CaseNoteInputDto input)
        {
            var notes = _noteManager.GetCaseNotesWithoutSpecificAction(input.CaseId, input.ActionId, input.ActionName, input.Type)
                .OrderByDescending(x => x.CreationTime);
            return ObjectMapper.Map<List<CaseNoteDto>>(notes);
        }
    }
}
