﻿using Abp.Application.Services;
using LMS.Loan.Cases.Dto.Notes;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LMS.Loan.Cases.Notes
{
    public interface INoteAppService : IApplicationService
    {
        List<NoteDto> GetNotesById(int caseId);
        List<CaseNoteDto> GetCaseNotes(int caseId);
        List<CaseNoteDto> GetCaseNotesWithoutSpecificAction(CaseNoteInputDto input);
        Task<NoteDto> GetNoteById(int noteId);
        Task<NoteDto> CreateAndAssignAsync(CreateNoteDto legal);

    }
}
