﻿using Abp.UI;
using LMS.Loan.Cases.Dto.Payments;
using LMS.Loan.Cases.Reminders;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Loan.Cases.Payments
{
    public class PaymentAppService : LMSAppServiceBase, IPaymentAppService
    {
        private readonly PaymentManager _paymentManager;
        public PaymentAppService(PaymentManager paymentManager)
        {
            _paymentManager = paymentManager;
        }
        public List<ReadPaymentDto> GetPaymentsForGrid(int caseId)
        {
            var payments = _paymentManager.GetPaymentsForCase(caseId);
            return ObjectMapper.Map<List<ReadPaymentDto>>(payments);
        }
        public async Task<UpdatePaymentDto> GetPaymentForUpdate(int paymentId)
        {
            var payment = await _paymentManager.GetPaymentById(paymentId);
            var casePayments = await _paymentManager.GetCasePaymentsById(paymentId);

            var paymentDto = ObjectMapper.Map<UpdatePaymentDto>(payment);
            paymentDto.CasesIds = casePayments.Select(x => x.CaseId).ToArray();

            return paymentDto;
        }
        public async Task<PaymentDto> GetPaymentById(int paymentId)
        {
            var payment = await _paymentManager.GetPaymentById(paymentId);

            var paymentDto = ObjectMapper.Map<PaymentDto>(payment);

            return paymentDto;
        }
        
        public async Task<PaymentDto> CreateAndAssignAsync(CreatePaymentDto input)
        {
            var payment = ObjectMapper.Map<Payment>(input);

            await _paymentManager.CreateAndAssignAsync(payment, input.CasesIds);

            return ObjectMapper.Map<PaymentDto>(payment);
        }
        public async Task<PaymentDto> UpdateAndAssignAsync(UpdatePaymentDto input)
        {

            var payment = ObjectMapper.Map<Payment>(input);

            await _paymentManager.UpdateAndAssignAsync(payment, input.CasesIds);

            return ObjectMapper.Map<PaymentDto>(payment);
        }
        public async Task Delete(int paymentId)
        {
            await _paymentManager.Delete(paymentId);
        }

        #region Helper Methods

        #endregion
    }
}
