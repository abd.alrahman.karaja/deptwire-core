﻿using Abp.Application.Services;
using LMS.Loan.Cases.Dto.Payments;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LMS.Loan.Cases.Payments
{
    public interface IPaymentAppService : IApplicationService
    {
        List<ReadPaymentDto> GetPaymentsForGrid(int caseId);
        Task<UpdatePaymentDto> GetPaymentForUpdate(int meetingId);
        Task<PaymentDto> GetPaymentById(int paymentId);
        Task<PaymentDto> CreateAndAssignAsync(CreatePaymentDto meeting);
        Task<PaymentDto> UpdateAndAssignAsync(UpdatePaymentDto meeting);
        Task Delete(int paymentId);
        
    }
}
