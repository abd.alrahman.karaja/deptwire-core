﻿using Abp.UI;
using LMS.Loan.Cases.Dto.Meetings;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Loan.Cases.Meetings
{
    public class MeetingAppService : LMSAppServiceBase, IMeetingAppService
    {
        private readonly MeetingManager _meetingManager;
        public MeetingAppService(MeetingManager meetingManager)
        {
            _meetingManager = meetingManager;
        }
        public List<ReadMeetingDto> GetMeetingsForGrid(int caseId)
        {
            var meetings = _meetingManager.GetMeetingsForCase(caseId).OrderBy(x => x.MeetingDate);
            return ObjectMapper.Map<List<ReadMeetingDto>>(meetings);
        }
        public async Task<UpdateMeetingDto> GetMeetingForUpdate(int meetingId)
        {
            var meeting = await _meetingManager.GetMeetingById(meetingId);
            var caseMeetings = await _meetingManager.GetCaseMeetingsById(meetingId);

            var meetingDto = ObjectMapper.Map<UpdateMeetingDto>(meeting);
            meetingDto.CasesIds = caseMeetings.Select(x => x.CaseId).ToArray();

            return meetingDto;
        }
        public async Task<MeetingDto> CreateAndAssignAsync(CreateMeetingDto input)
        {
            var meeting = ObjectMapper.Map<Meeting>(input);
            CheckBeforeCreate(meeting);

            await _meetingManager.CreateAndAssignAsync(meeting, input.CasesIds);

            return ObjectMapper.Map<MeetingDto>(meeting);
        }
        public async Task<MeetingDto> UpdateAndAssignAsync(UpdateMeetingDto input)
        {
            var meeting = ObjectMapper.Map<Meeting>(input);
            CheckBeforeUpdate(meeting);

            await _meetingManager.UpdateAndAssignAsync(meeting, input.CasesIds);

            return ObjectMapper.Map<MeetingDto>(meeting);
        }
        public async Task Delete(int meetingId)
        {
            await _meetingManager.Delete(meetingId);
        }

        public async Task<MeetingDto> OnCheckDone(MeetingCheckedDoneDto input)
        {
            var meeting = await _meetingManager.GetMeetingById(input.Id);
            meeting.Done = input.Done;
            var updatedMeeting = await _meetingManager.Update(meeting);
            return ObjectMapper.Map<MeetingDto>(updatedMeeting);
        }


        #region Helper Methods
        private void CheckBeforeCreate(Meeting meeting)
        {
            var validationResultMessage = string.Empty;

            if (_meetingManager.CheckIfThereIsAnotherMeetingAtSameTime(meeting))
            {
                validationResultMessage = L(ValidationResultMessage.ThereIsAnotherMeetingAtTheSameTime);
            }

            if (!string.IsNullOrEmpty(validationResultMessage))
                throw new UserFriendlyException(validationResultMessage);
        }
        private void CheckBeforeUpdate(Meeting meeting)
        {
            var validationResultMessage = string.Empty;

            if (_meetingManager.CheckIfThereIsAnotherMeetingAtSameTime(meeting))
            {
                validationResultMessage = L(ValidationResultMessage.ThereIsAnotherMeetingAtTheSameTime);
            }

            if (!string.IsNullOrEmpty(validationResultMessage))
                throw new UserFriendlyException(validationResultMessage);
        }

        

        #endregion
    }
}
