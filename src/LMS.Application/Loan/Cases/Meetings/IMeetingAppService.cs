﻿using Abp.Application.Services;
using LMS.Loan.Cases.Dto.Meetings;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LMS.Loan.Cases.Meetings
{
    public interface IMeetingAppService : IApplicationService
    {
        List<ReadMeetingDto> GetMeetingsForGrid(int caseId);
        Task<UpdateMeetingDto> GetMeetingForUpdate(int meetingId);
        Task<MeetingDto> CreateAndAssignAsync(CreateMeetingDto meeting);
        Task<MeetingDto> UpdateAndAssignAsync(UpdateMeetingDto meeting);
        Task<MeetingDto> OnCheckDone(MeetingCheckedDoneDto input);
        Task Delete(int meetingId);
    }
}
