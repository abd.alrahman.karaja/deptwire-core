﻿using Abp.Application.Services;
using LMS.Loan.Cases.Dto.PTP;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LMS.Loan.Cases.PromiseToPays
{
    public interface IPromiseToPayAppService : IApplicationService
    {
        List<ReadPromiseToPayDto> GetPromiseToPaysForGrid(int caseId);
        Task<UpdatePromiseToPayDto> GetPromiseToPayForUpdate(int meetingId);
        Task<PromiseToPayDto> GetPromiseToPayById(int paymentId);
        Task<PromiseToPayDto> CreateAndAssignAsync(CreatePromiseToPayDto meeting);
        Task<PromiseToPayDto> UpdateAndAssignAsync(UpdatePromiseToPayDto meeting);
        Task Delete(int paymentId);

    }
}
