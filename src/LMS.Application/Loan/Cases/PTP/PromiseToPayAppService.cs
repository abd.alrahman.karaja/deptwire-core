﻿using LMS.Loan.Cases.Dto.PTP;
using LMS.Loan.Cases.PTP;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Loan.Cases.PromiseToPays
{
    public class PromiseToPayAppService : LMSAppServiceBase, IPromiseToPayAppService
    {
        private readonly PromiseToPayManager _promiseToPayManager;
        public PromiseToPayAppService(PromiseToPayManager promiseToPayManager)
        {
            _promiseToPayManager = promiseToPayManager;
        }
        public List<ReadPromiseToPayDto> GetPromiseToPaysForGrid(int caseId)
        {
            var promiseToPays = _promiseToPayManager.GetPromiseToPaysForCase(caseId);
            var promiseToPaysDto = ObjectMapper.Map<List<ReadPromiseToPayDto>>(promiseToPays);
            return promiseToPaysDto;
        }
        public async Task<UpdatePromiseToPayDto> GetPromiseToPayForUpdate(int promiseToPayId)
        {
            var promiseToPay = await _promiseToPayManager.GetPromiseToPayById(promiseToPayId);
            var payments = await _promiseToPayManager.GetPaymentsByPtpId(promiseToPayId);
            var casePromiseToPays = await _promiseToPayManager.GetCasePromiseToPaysById(promiseToPayId);

            promiseToPay.PtpPayments = payments;

            var promiseToPayDto = ObjectMapper.Map<UpdatePromiseToPayDto>(promiseToPay);
            promiseToPayDto.CasesIds = casePromiseToPays.Select(x => x.CaseId).ToArray();

            return promiseToPayDto;
        }
        public async Task<PromiseToPayDto> GetPromiseToPayById(int promiseToPayId)
        {
            var promiseToPay = await _promiseToPayManager.GetPromiseToPayById(promiseToPayId);
            var payments = await _promiseToPayManager.GetPaymentsByPtpId(promiseToPayId);
            promiseToPay.PtpPayments = payments;

            var promiseToPayDto = ObjectMapper.Map<PromiseToPayDto>(promiseToPay);

            return promiseToPayDto;
        }

        public async Task<PromiseToPayDto> CreateAndAssignAsync(CreatePromiseToPayDto input)
        {
            var promiseToPay = ObjectMapper.Map<PromiseToPay>(input);

            await _promiseToPayManager.CreateAndAssignAsync(promiseToPay, input.CasesIds);

            return ObjectMapper.Map<PromiseToPayDto>(promiseToPay);
        }
        public async Task<PromiseToPayDto> UpdateAndAssignAsync(UpdatePromiseToPayDto input)
        {

            var promiseToPay = ObjectMapper.Map<PromiseToPay>(input);

            await _promiseToPayManager.UpdateAndAssignAsync(promiseToPay, input.CasesIds);

            return ObjectMapper.Map<PromiseToPayDto>(promiseToPay);
        }
        public async Task Delete(int promiseToPayId)
        {
            await _promiseToPayManager.Delete(promiseToPayId);
        }

        #region Helper Methods

        #endregion
    }
}
