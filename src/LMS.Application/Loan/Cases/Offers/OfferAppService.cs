﻿using Abp.UI;
using LMS.Loan.Cases.Dto.Offers;
using LMS.Loan.Cases.PromiseToPays;
using LMS.Loan.Cases.Reminders;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Loan.Cases.Offers
{
    public class OfferAppService : LMSAppServiceBase, IOfferAppService
    {
        private readonly OfferManager _offerManager;
        private readonly ReminderManager _reminderManager;
        
        public OfferAppService(
            OfferManager offerManager, 
            ReminderManager reminderManager
            )
        {
            _offerManager = offerManager;
            _reminderManager = reminderManager;
        }
        public List<ReadOfferDto> GetOffersForGrid(int caseId)
        {
            var offers = _offerManager.GetOffersForCase(caseId).OrderBy(x => x.OfferDate);
            return ObjectMapper.Map<List<ReadOfferDto>>(offers);
        }
        public async Task<UpdateOfferDto> GetOfferForUpdate(int offerId)
        {
            var offer = await _offerManager.GetOfferById(offerId);
            var caseOffers = await _offerManager.GetCaseOffersByIdAsync(offerId);
            
            var offerDto = ObjectMapper.Map<UpdateOfferDto>(offer);
            offerDto.CasesIds = caseOffers.Select(x => x.CaseId).ToArray();

            return offerDto;
        }
        public async Task<OfferDto> GetOfferById(int offerId)
        {
            var offer = await _offerManager.GetOfferById(offerId);

            var offerDto = ObjectMapper.Map<OfferDto>(offer);
            
            return offerDto;
        }
        public async Task ChangeStatus(OfferStatusDto offerDto)
        {
            await _offerManager.ChangeStatus(offerDto.OfferStatus, offerDto.Id, offerDto.SuperviorNote);
        }
        public async Task<OfferDto> CreateAndAssignAsync(CreateOfferDto input)
        {
            var offer = ObjectMapper.Map<Offer>(input);

            await _offerManager.CreateAndAssignAsync(offer, input.CasesIds);

            return ObjectMapper.Map<OfferDto>(offer);
        }
        public async Task<OfferDto> UpdateAndAssignAsync(UpdateOfferDto input)
        {
          
            var offer = ObjectMapper.Map<Offer>(input);

            await _offerManager.UpdateAndAssignAsync(offer, input.CasesIds);

            return ObjectMapper.Map<OfferDto>(offer);
        }
        public async Task Delete(int offerId)
        {
            await _offerManager.Delete(offerId);
        }

        #region Helper Methods

        #endregion
    }
}
