﻿using Abp.Application.Services;
using LMS.Loan.Cases.Dto.Offers;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LMS.Loan.Cases.Offers
{
    public interface IOfferAppService : IApplicationService
    {
        List<ReadOfferDto> GetOffersForGrid(int caseId);
        Task<UpdateOfferDto> GetOfferForUpdate(int meetingId);
        Task<OfferDto> GetOfferById(int offerId);
        Task<OfferDto> CreateAndAssignAsync(CreateOfferDto meeting);
        Task<OfferDto> UpdateAndAssignAsync(UpdateOfferDto meeting);
        Task Delete(int offerId);
        Task ChangeStatus(OfferStatusDto offerDto);
    }
}
