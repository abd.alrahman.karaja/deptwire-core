﻿using LMS.Crud.Dto;
using LMS.Loan.Cases.Dto;
using LMS.Loan.Cases.Dto.CaseUploadFiles;
using LMS.Loan.Enums;
using Microsoft.AspNetCore.Mvc;
using Syncfusion.EJ2.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Loan.Cases
{
    public class CaseAppService : LMSAppServiceBase, ICaseAppService
    {
        private readonly CaseManager _caseManager;
        public CaseAppService(CaseManager caseManager)
        {
            _caseManager = caseManager;
        }


        public ViewCaseDto GetCaseDetail(int caseId)
        {
            var currentCase = _caseManager.GetCaseById(caseId);
            var dto = ObjectMapper.Map<ViewCaseDto>(currentCase);
            return dto;
        }

        public UpdateCaseDto GetCaseForEdit(int caseId)
        {
            var currentCase = _caseManager.GetCaseById(caseId);
            return ObjectMapper.Map<UpdateCaseDto>(currentCase);
        }

        public IList<CaseMiniDataDto> GetCasesForCustomer(int customerId)
        {
            var cases = _caseManager.GetCasesForCustomer(customerId);
            var result = (from c in cases
                          select new CaseMiniDataDto()
                          {
                              Id = c.Id,
                              AccountNo = c.AccountNo,
                              Bank = c.Bank?.Name,
                              Agent = c.Agent?.Name,
                              Product = c.Product?.Name,
                              OpenDate = c.OpenDate
                          }).ToList();

            return result;
        }

        public async Task<CaseUploadFileDto> GetFileDetail(int id)
        {
            var caseFileUpload = await _caseManager.GetFileDetailAsync(id);
            return ObjectMapper.Map<CaseUploadFileDto>(caseFileUpload);
        }
        [HttpPost]
        public async Task<ReadGrudDto> GetForGrid([FromBody] DataManagerRequest dm)
        {
            if (dm.Search.Any())
            {
                var userId = dm.Search.FirstOrDefault().Key;

                var data = await _caseManager.GetUserCases(userId);
                IEnumerable<ReadCaseDto> cases = ObjectMapper.Map<List<ReadCaseDto>>(data);

                var operations = new DataOperations();
                if (dm.Where != null && dm.Where.Count > 0)
                {
                    cases = operations.PerformFiltering(cases, dm.Where, "and");
                }

                if (dm.Sorted != null && dm.Sorted.Count > 0)
                {
                    cases = operations.PerformSorting(cases, dm.Sorted);
                }

                var count = cases.Count();

                if (dm.Skip != 0)
                {
                    cases = operations.PerformSkip(cases, dm.Skip);
                }

                if (dm.Take != 0)
                {
                    cases = operations.PerformTake(cases, dm.Take);
                }

                return new ReadGrudDto() { result = cases, count = count };
            }
            return new ReadGrudDto();
        }

        public async Task<CaseDto> AssignCaseToNegotiator(NegotiatorCaseDto input)
        {
            var casee = await _caseManager.AssignCaseToNegotiator(input.CaseId, input.UserId);

            return ObjectMapper.Map<CaseDto>(casee);
        }

        public async Task<UpdateCaseDto> Update(UpdateCaseDto input)
        {
            var oldCase = new Case();
            var casee = _caseManager.GetCaseById(input.Id);
            //Initial old case data
            oldCase.AllocationDate = casee.AllocationDate;
            oldCase.ModeOfAllocation = casee.ModeOfAllocation;
            oldCase.DOX = casee.DOX;
            oldCase.NOC= casee.NOC;
            oldCase.LiabilityMail = casee.LiabilityMail;
            oldCase.BookingDate = casee.BookingDate;
            oldCase.WaedFeedback = casee.WaedFeedback;
            oldCase.ArabicFeedback = casee.ArabicFeedback;
            oldCase.BankFeedback = casee.BankFeedback;

            //Initial new case data
            if (!string.IsNullOrEmpty(input.AllocationDate))
            {
                casee.AllocationDate = DateTime.Parse(input.AllocationDate);
            }
            if (!string.IsNullOrEmpty(input.BookingDate))
            {
                casee.BookingDate = DateTime.Parse(input.BookingDate);
            }
            casee.ModeOfAllocation = (ModeOfAllocation)input.ModeOfAllocation;
            casee.WaedFeedback = input.WaedFeedback;
            casee.ArabicFeedback = input.ArabicFeedback;
            casee.BankFeedback = input.BankFeedback;
            casee.LiabilityMail = (LiabilityMail)input.LiabilityMail;
            casee.DOX = (DOX)input.DOX;
            casee.NOC = input.NOC;
            if (input.ModeOfAllocation == 1 || input.ModeOfAllocation == 2)
            {
                casee.Activity = true;
            }
            var updatedCase = await _caseManager.Update(casee, oldCase);

            return ObjectMapper.Map<UpdateCaseDto>(updatedCase);
        }

        public async Task<bool> ChangeStatusAndAssignAsync(ChangeStatusDto input)
        {
            return await _caseManager.ChangeStatusAndAssignAsync(
                input.Status,
                input.SupervisorNote,
                input.NegotiatorNote,
                input.CasesIds);
        }


    }
}
