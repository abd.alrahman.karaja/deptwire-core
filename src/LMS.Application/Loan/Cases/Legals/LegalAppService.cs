﻿using Abp.Application.Services;
using LMS.Loan.Cases.Dto;
using LMS.Loan.Cases.Dto.Legals;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Loan.Cases.Legals
{
    public class LegalAppService : LMSAppServiceBase, ILegalAppService
    {
        private readonly LegalManager _legalManager;

        public LegalAppService(LegalManager legalManager)
        {
            _legalManager = legalManager;
        }

        public async Task<LegalDto> CreateAndAssignAsync(CreateLegalDto input)
        {
            var legal = ObjectMapper.Map<Legal>(input);

            await _legalManager.CreateAndAssignAsync(legal, input.CasesIds);

            return ObjectMapper.Map<LegalDto>(legal);
        }

        public async Task<LegalDto> GetLegalById(int legalId)
        {
            var legal = await _legalManager.GetLegalById(legalId);

            var legalDto = ObjectMapper.Map<LegalDto>(legal);

            return legalDto;
        }

        public List<LegalDto> GetLegals(int caseId)
        {
            var legals = _legalManager.GetLegalsForCase(caseId).OrderByDescending(x => x.CreationTime);
            return ObjectMapper.Map<List<LegalDto>>(legals);
        }

        public async Task<bool> ChangeLegalStatusAsync(ChangeLegalStatusDto input)
        {
            return await _legalManager.ChangeLegalStatusAsync(
                input.Id, 
                input.LegalStatus,
                input.LegalNote);
        }

        public async Task<ChangeLegalStatusDto> GetLegalStatusById(int id)
        {
            var legal =await  _legalManager.GetLegalById(id);

            return ObjectMapper.Map<ChangeLegalStatusDto>(legal);
        }

        public ChangeLegalStatusDto GetLegalByCaseId(int id)
        {
            throw new System.NotImplementedException();
        }

        
    }
}
