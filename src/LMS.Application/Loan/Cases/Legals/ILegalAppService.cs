﻿using Abp.Application.Services;
using LMS.Loan.Cases.Dto;
using LMS.Loan.Cases.Dto.Legals;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LMS.Loan.Cases.Legals
{
    public interface ILegalAppService : IApplicationService
    {
        List<LegalDto> GetLegals(int caseId);
        Task<LegalDto> GetLegalById(int legalId);
        Task<ChangeLegalStatusDto> GetLegalStatusById(int legalId);
        Task<LegalDto> CreateAndAssignAsync(CreateLegalDto legal);
        ChangeLegalStatusDto GetLegalByCaseId(int id);
        Task<bool> ChangeLegalStatusAsync(ChangeLegalStatusDto input);

    }
}
