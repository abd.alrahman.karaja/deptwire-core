﻿using LMS.Loan.Cases.Dto;
using Syncfusion.EJ2.Base;

namespace LMS.Loan.Cases
{
    public class CaseDataManager: DataManagerRequest
    {
        public long? UserId { get; set; }
    }
}
