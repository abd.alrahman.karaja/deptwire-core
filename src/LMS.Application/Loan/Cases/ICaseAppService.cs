﻿using Abp.Application.Services;
using LMS.Crud.Dto;
using LMS.Loan.Cases.Dto;
using LMS.Loan.Cases.Dto.CaseUploadFiles;
using LMS.Loan.Cases.Dto.Legals;
using Microsoft.AspNetCore.Mvc;
using Syncfusion.EJ2.Base;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LMS.Loan.Cases
{
    public interface ICaseAppService : IApplicationService
    {
        Task<ReadGrudDto> GetForGrid([FromBody] DataManagerRequest dm);
        ViewCaseDto GetCaseDetail(int caseId);
        UpdateCaseDto GetCaseForEdit(int caseId);
        IList<CaseMiniDataDto> GetCasesForCustomer(int customerId);
        Task<CaseUploadFileDto> GetFileDetail(int id);
        
        Task<CaseDto> AssignCaseToNegotiator(NegotiatorCaseDto input);
        Task<UpdateCaseDto> Update(UpdateCaseDto input);
    }
}
