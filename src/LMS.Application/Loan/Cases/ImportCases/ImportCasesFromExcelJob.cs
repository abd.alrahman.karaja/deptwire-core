﻿using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Uow;
using Abp.Localization;
using Abp.Localization.Sources;
using Abp.ObjectMapping;
using Abp.Threading;
using Abp.UI;
using LMS.Loan.Agents;
using LMS.Loan.Banks;
using LMS.Loan.Cases.Dto.CaseUploadFiles;
using LMS.Loan.Cases.Dto.ImportCases;
using LMS.Loan.Customers;
using LMS.Loan.Indexes;
using LMS.Notifications;
using LMS.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Loan.Cases.ImportCases
{
    public class ImportCasesFromExcelJob : BackgroundJob<ImportCasesFromExcelJobArgs>, ITransientDependency
    {
        private readonly ICaseListExcelDataReader _caseListExcelDataReader;
        private readonly IInvalidCaseExporter _invalidCaseExporter;
        private readonly IBinaryObjectManager _binaryObjectManager;
        private readonly IAppNotifier _appNotifier;
        private readonly ILocalizationSource _localizationSource;
        private readonly IObjectMapper _objectMapper;
        private readonly BankManager _bankManager;
        private readonly CaseManager _caseManager;
        private readonly CustomerManager _customerManager;
        private readonly AgentManager _agentManager;
        private readonly CountryManager _countryManager;
        private readonly NationalityManager _nationalityManager;
        private readonly PoliceCaseStatusManager _policeCaseStatusManager;
        private readonly ProductManager _productManager;
        public ImportCasesFromExcelJob(
            ICaseListExcelDataReader caseListExcelDataReader,
            IInvalidCaseExporter invalidCaseExporter,
            IBinaryObjectManager binaryObjectManager,
            IAppNotifier appNotifier,
            ILocalizationManager localizationManager,
            IObjectMapper objectMapper,
            BankManager bankManager,
            CaseManager caseManager,
            CustomerManager customerManager,
            AgentManager agentManager,
            CountryManager countryManager,
            NationalityManager nationalityManager,
            PoliceCaseStatusManager policeCaseStatusManager,
            ProductManager productManager
        )
        {
            _caseListExcelDataReader = caseListExcelDataReader;
            _invalidCaseExporter = invalidCaseExporter;
            _binaryObjectManager = binaryObjectManager;
            _appNotifier = appNotifier;
            _localizationSource = localizationManager.GetSource(LMSConsts.LocalizationSourceName);
            _objectMapper = objectMapper;
            _bankManager = bankManager;
            _caseManager = caseManager;
            _customerManager = customerManager;
            _agentManager = agentManager;
            _countryManager = countryManager;
            _nationalityManager = nationalityManager;
            _policeCaseStatusManager = policeCaseStatusManager;
            _productManager = productManager;
        }

        [UnitOfWork]
        public override void Execute(ImportCasesFromExcelJobArgs args)
        {
            using (CurrentUnitOfWork.SetTenantId(args.TenantId))
            {
                var cases = GetCasesFromExcelOrNull(args);
                if (cases == null || !cases.Any())
                {
                    SendInvalidExcelNotification(args);
                    return;
                }

                CreateCases(args, cases);
            }
        }
        private List<ImportCaseDto> GetCasesFromExcelOrNull(ImportCasesFromExcelJobArgs args)
        {
            try
            {
                var file = AsyncHelper.RunSync(() => _binaryObjectManager.GetOrNullAsync(args.BinaryObjectId));
                
                if (file != null)
                {
                    return _caseListExcelDataReader.GetCasesFromExcel(file.Bytes);
                }
            }
            catch (Exception ex)
            {
                ExceptionNotify(args, "Exception", ex.Message);
            }

            return null;
        }
        private void SendInvalidExcelNotification(ImportCasesFromExcelJobArgs args)
        {
            _appNotifier.FileCantBeConvertedToCaseList(
                args.User,
                _localizationSource.GetString("FileCantBeConvertedToCaseList"));
        }

        private void CreateCases(ImportCasesFromExcelJobArgs args, List<ImportCaseDto> cases)
        {
            var invalidCases = new List<ImportCaseDto>();
            var lackOfCasesData = new List<ImportCaseDto>();

            foreach (var caseDto in cases)
            {
                if (!caseDto.ThereIsLackOfData())
                {
                    try
                    {
                        AsyncHelper.RunSync(() => CreateCaseAsync(caseDto, args));
                    }
                    catch (UserFriendlyException exception)
                    {
                        caseDto.Exception = exception.Message;
                        invalidCases.Add(caseDto);
                    }
                    catch (Exception exception)
                    {
                        caseDto.Exception = exception.ToString();
                        invalidCases.Add(caseDto);
                    }
                }
                else
                {
                    lackOfCasesData.Add(caseDto);
                }
            }

            AsyncHelper.RunSync(() => ProcessImportCasesResultAsync(args, invalidCases, lackOfCasesData));
        }

        private async Task CreateCaseAsync(ImportCaseDto caseDto , ImportCasesFromExcelJobArgs args)
        {
            int bankId;
            if(int.TryParse(caseDto.BankId,out bankId))
            {
                var importedCase = _objectMapper.Map<Case>(caseDto);
                //Agent
                var agent = await _agentManager.GetAgentByIdAsync(int.Parse(caseDto.AgentId));
                importedCase.Agent = agent;
                importedCase.AgentId = agent.Id;
                //Bank
                var bank = await _bankManager.GetBankByIdAsync(int.Parse(caseDto.BankId));
                importedCase.Bank = bank;
                importedCase.BankId = bank.Id;
                //Bank Home
                Country bankHome = await _countryManager.GetCountryByNameAsync(caseDto.BankHome);
                if (bankHome == null)
                {
                    bankHome = await _countryManager.InsertAndGetAsync(caseDto.BankHome);
                }
                importedCase.BankHome = bankHome;
                importedCase.BankHomeId = bankHome.Id;

                //Police Case Status
                PoliceCaseStatus policeCaseStatus = await _policeCaseStatusManager.GetPoliceCaseStatusByNameAsync(caseDto.PoliceCaseStatus);
                if (policeCaseStatus == null)
                {
                    policeCaseStatus = await _policeCaseStatusManager.InsertAndGetAsync(caseDto.PoliceCaseStatus);

                }
                importedCase.PoliceCaseStatus = policeCaseStatus;
                importedCase.PoliceCaseStatusId = policeCaseStatus.Id;

                //Product
                Product product = await _productManager.GetProductByNameAsync(caseDto.Product);
                if (product == null)
                {
                    product = await _productManager.InsertAndGetAsync(caseDto.Product);

                }
                importedCase.Product = product;
                importedCase.ProductId = product.Id;


                var importedCustomer = _objectMapper.Map<Customer>(caseDto);
                //Nationality
                Nationality nationatity = await _nationalityManager.GetNationalityByNameAsync(caseDto.NationalityName);
                if (nationatity == null)
                {
                    nationatity = await _nationalityManager.InsertAndGetAsync(caseDto.NationalityName);
                }
                importedCustomer.Nationality = nationatity;
                importedCustomer.NationalityId = nationatity.Id;

                #region Activity diagram debt1

                if (await CheckCaseIfExistAsync(importedCase))
                {
                    var customerId = await UpdateCustomerAndGetId(importedCustomer);
                    importedCase.CustomerId = customerId;
                    await _caseManager.UpdateCaseForImportAsync(importedCase);
                }
                else
                {
                    var customerId = await InsertOrUpdateCustomerAndGetId(importedCustomer);
                    importedCase.CustomerId = customerId;
                    await _caseManager.InsertCaseForImportAsync(importedCase);
                }

                #endregion
            }
            else
            {
                throw new Exception(L("CantParseBankId"));
            }
            
        }

        private async Task<int> UpdateCustomerAndGetId(Customer importedCustomer)
        {
            var customer = await _customerManager.GetCustomerByNameAsync(importedCustomer.Name, importedCustomer.ArabicName);
            if(customer != null)
                return await _customerManager.UpdateAndGetIdAsync(customer, importedCustomer);

            return 0;
        }
        private async Task<int> InsertOrUpdateCustomerAndGetId(Customer importedCustomer)
        {
            var customer = await _customerManager.GetCustomerByNameAsync(importedCustomer.Name, importedCustomer.ArabicName);
            if (customer != null)
            {
                return await _customerManager.UpdateAndGetIdAsync(customer, importedCustomer);
            }
            else
            {
                return await _customerManager.InsertAndGetIdAsync(importedCustomer);
            }
        }
        private async Task<bool> CheckCaseIfExistAsync(Case importedCase)
        {
            var currentCase = await _caseManager.GetCaseByNoAsync(importedCase.ContractNo, importedCase.AccountNo);
            return currentCase != null;
        }

        private async Task ProcessImportCasesResultAsync(ImportCasesFromExcelJobArgs args, List<ImportCaseDto> invalidCases, List<ImportCaseDto> lackOfCasesData)
        {
            if (invalidCases.Any())
            {
                var file = _invalidCaseExporter.ExportToFile(invalidCases);
                await _appNotifier.SomeCasesCouldntBeImported(args.User, file.FileToken, file.FileType, file.FileName);
            }
            else if(lackOfCasesData.Any())
            {
                var file = _invalidCaseExporter.ExportToFile(lackOfCasesData);
            }
            else
            {
                await _appNotifier.SendMessageAsync(
                    args.User,
                    "SuccessfullyImported",
                    _localizationSource.GetString("AllCasesSuccessfullyImportedFromExcel"),
                    Abp.Notifications.NotificationSeverity.Success);
            }
        }

        private async void ExceptionNotify(ImportCasesFromExcelJobArgs args, string notificationName, string body)
        {
            await _appNotifier.SendMessageAsync(
                    args.User,
                    notificationName,
                    _localizationSource.GetString(body),
                    Abp.Notifications.NotificationSeverity.Error);
        }

    }
}
