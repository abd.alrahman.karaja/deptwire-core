﻿using LMS.Loan.Cases.Dto.ImportCases;
using LMS.Loan.Shared.Dto;
using System.Collections.Generic;

namespace LMS.Loan.Cases.ImportCases
{
    public interface IInvalidCaseExporter
    {
        FileDto ExportToFile(List<ImportCaseDto> userListDtos);
    }
}
