﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Abp.Localization;
using Abp.Localization.Sources;
using LMS.Application.Extentions;
using LMS.DataExporting.Excel.EpPlus;
using LMS.Loan.Banks;
using LMS.Loan.Cases.Dto.ImportCases;
using LMS.Loan.Cases.ImportCases;
using LMS.Loan.Enums;
using LMS.Reflection.Extentions;
using OfficeOpenXml;

namespace LMS.Authorization.Users.Importing
{
    public class CaseListExcelDataReader : EpPlusExcelImporterBase<ImportCaseDto>, ICaseListExcelDataReader
    {
        private readonly ILocalizationSource _localizationSource;
        private readonly BankManager _bankManager;

        public CaseListExcelDataReader(ILocalizationManager localizationManager, BankManager bankManager)
        {
            _localizationSource = localizationManager.GetSource(LMSConsts.LocalizationSourceName);
            _bankManager = bankManager;
        }

        public List<ImportCaseDto> GetCasesFromExcel(byte[] fileBytes)
        {
            return ProcessExcelFile(fileBytes, ProcessExcelRow);
        }

        private ImportCaseDto ProcessExcelRow(ExcelWorksheet worksheet, int row)
        {
            if (IsRowEmpty(worksheet, row))
            {
                return null;
            }

            var exceptionMessage = new StringBuilder();
            var caseDto = new ImportCaseDto();

            try
            {
                var bankId = GetRequiredValueFromRowOrNull(worksheet, row, 4, nameof(caseDto.BankId), exceptionMessage).Trim();
                var dateFormate = GetBankDateFormate(int.Parse(bankId));
                if (string.IsNullOrEmpty(dateFormate))
                    return null;

                caseDto.AgentName = GetRequiredValueFromRowOrNull(worksheet, row, 1, nameof(caseDto.AgentName), exceptionMessage).Trim();
                caseDto.AgentId = GetRequiredValueFromRowOrNull(worksheet, row, 2, nameof(caseDto.AgentId), exceptionMessage).Trim();
                caseDto.BankName = GetRequiredValueFromRowOrNull(worksheet, row, 3, nameof(caseDto.BankName), exceptionMessage).Trim();
                caseDto.BankId = bankId;
                caseDto.BankHome = GetRequiredValueFromRowOrNull(worksheet, row, 5, nameof(caseDto.BankHome), exceptionMessage).Trim();
                caseDto.CIF = worksheet.Cells[row, 6].Value?.ToString().Trim().DeleteDash();
                caseDto.CustomerName = worksheet.Cells[row, 7].Value?.ToString().Trim().DeleteDash();
                caseDto.AccountNo = worksheet.Cells[row, 8].Value?.ToString().Trim().DeleteDash();
                caseDto.ContractNo = worksheet.Cells[row, 9].Value?.ToString().Trim().DeleteDash();
                caseDto.FullAccount = worksheet.Cells[row, 10].Value?.ToString().Trim().DeleteDash();
                caseDto.Product = worksheet.Cells[row, 11].Value?.ToString().Trim().DeleteDash();
                caseDto.TypeOfAssist = worksheet.Cells[row, 12].Value?.ToString().Trim().DeleteDash();
                caseDto.Manufacturer = worksheet.Cells[row, 13].Value?.ToString().Trim().DeleteDash();
                caseDto.OriginalAmount = GetEnumValue(worksheet, row, 14, OriginalAmount.None);
                caseDto.TotalAmount = GetDoubleValue(worksheet, row, 15);
                caseDto.Outstanding = GetDoubleValue(worksheet, row, 16);
                caseDto.PrincipalAmount = GetDoubleValue(worksheet, row, 17);
                caseDto.LastCreditLimit = GetDoubleValue(worksheet, row, 18);
                caseDto.MainLedgerBalance = GetDoubleValue(worksheet, row, 19);
                caseDto.InstallmentAmount = GetDoubleValue(worksheet, row, 20);
                caseDto.BucketStage = worksheet.Cells[row, 21].Value?.ToString().Trim().DeleteDash();
                caseDto.Overdues = GetDoubleValue(worksheet, row, 22);
                caseDto.MinPayment = GetDoubleValue(worksheet, row, 23);
                caseDto.WriteOffDate = GetDateTimeValue(worksheet, row, 24, dateFormate);
                caseDto.WriteOffAge = worksheet.Cells[row, 25].Value?.ToString().Trim().DeleteDash();
                caseDto.DPD = GetIntValue(worksheet, row, 26);
                caseDto.OpenDate = GetDateTimeValue(worksheet, row, 27, dateFormate);
                caseDto.CloseDate = GetDateTimeValue(worksheet, row, 28, dateFormate);
                caseDto.LastPaymentAmount = GetDoubleValue(worksheet, row, 29);
                caseDto.LastPaymentDate = GetDateTimeValue(worksheet, row, 30, dateFormate);
                caseDto.LastPurchaseAmount = GetDoubleValue(worksheet, row, 31);
                caseDto.LastPurchaseDate= GetDateTimeValue(worksheet, row, 32, dateFormate);
                caseDto.DOB = GetDateTimeValue(worksheet, row, 33, dateFormate);
                caseDto.Gender = worksheet.Cells[row, 34].Value?.ToString().Trim().DeleteDash();
                caseDto.Age = GetIntValue(worksheet, row, 35); 
                caseDto.NationalityName = worksheet.Cells[row, 36].Value?.ToString().Trim().DeleteDash();
                caseDto.ResidenceNo = worksheet.Cells[row, 37].Value?.ToString().Trim().DeleteDash();
                caseDto.VisaNo = worksheet.Cells[row, 38].Value?.ToString().Trim().DeleteDash();
                caseDto.PassportNo = worksheet.Cells[row, 39].Value?.ToString().Trim().DeleteDash();
                caseDto.PassportExpiry = GetDateTimeValue(worksheet, row, 40, dateFormate); 
                caseDto.MotherName = worksheet.Cells[row, 41].Value?.ToString().Trim().DeleteDash();
                caseDto.CompanyName = worksheet.Cells[row, 42].Value?.ToString().Trim().DeleteDash();
                caseDto.CustomerMobileByBank = worksheet.Cells[row, 43].Value?.ToString().Trim().DeleteDash();
                caseDto.CustomerEmailByBank = worksheet.Cells[row, 44].Value?.ToString().Trim().DeleteDash();
                caseDto.ReferenceNameByBank = worksheet.Cells[row, 45].Value?.ToString().Trim().DeleteDash();
                caseDto.ReferenceMobileByBank = worksheet.Cells[row, 46].Value?.ToString().Trim().DeleteDash();
                caseDto.PoliceCaseAmount = worksheet.Cells[row, 47].Value?.ToString().Trim().DeleteDash();
                caseDto.PoliceStationName = worksheet.Cells[row, 48].Value?.ToString().Trim().DeleteDash();
                caseDto.PoliceCaseStatus = worksheet.Cells[row, 49].Value?.ToString().Trim().DeleteDash();
                caseDto.PoliceCaseNo = worksheet.Cells[row, 50].Value?.ToString().Trim().DeleteDash();
                caseDto.PoliceCaseDate = worksheet.Cells[row, 51].Value?.ToString().Trim().DeleteDash();
                caseDto.FinalJugement = worksheet.Cells[row, 52].Value?.ToString().Trim().DeleteDash();

            }
            catch (System.Exception exception)
            {
                caseDto.Exception = exception.Message;
            }

            return caseDto;
        }

        private string GetBankDateFormate(int bankId)
        {
            var bank = _bankManager.GetBankById(bankId);
            if (bank != null)
            {
                return bank.DateFormat;
            }
            return string.Empty;
        }

        private T GetEnumValue<T>(ExcelWorksheet worksheet, int row, int column, T defaultValue)
        {
            var cellValue = worksheet.Cells[row, column].Value.ToString();

            try
            {
                return ConvertToEnum(cellValue.Trim(), defaultValue);
            }
            catch
            {
                return defaultValue;
            }
        }

        private DateTime? GetDateTimeValue(ExcelWorksheet worksheet, int row, int column, string format)
        {
            var obj = worksheet.Cells[row, column].Value;
            var cellValue = worksheet.Cells[row, column].Value.ToString().Trim();
            try
            {
                DateTime? date = null;

                if (!string.IsNullOrEmpty(cellValue) && !cellValue.Equals("-"))
                {
                    date = cellValue.Trim().GetDateByFormat(format);
                    if (date != null)
                    {
                        return cellValue.Trim().GetDateByFormat(format);
                    }
                    else
                    {
                        date = DateTime.FromOADate(double.Parse(cellValue));
                    }
                    
                }
                return date;
            }
            catch
            {
                return null;
            }
        }

        private double GetDoubleValue(ExcelWorksheet worksheet, int row, int column)
        {
            var cellValue = worksheet.Cells[row, column].Value.ToString();

            try
            {
                return double.Parse(cellValue.Trim());
            }
            catch
            {
                return 0.0;
            }
        }

        private int GetIntValue(ExcelWorksheet worksheet, int row, int column)
        {
            var cellValue = (string)worksheet.Cells[row, column].Value;

            try
            {
                return int.Parse(cellValue.Trim());
            }
            catch
            {
                return 0;
            }
        }

        private string GetRequiredValueFromRowOrNull(ExcelWorksheet worksheet, int row, int column, string columnName, StringBuilder exceptionMessage)
        {
            var cellValue = worksheet.Cells[row, column].Value;

            if (cellValue != null && !string.IsNullOrWhiteSpace(cellValue.ToString()))
            {
                return cellValue.ToString();
            }

            exceptionMessage.Append(GetLocalizedExceptionMessagePart(columnName));
            return null;
        }

        private string[] GetAssignedRoleNamesFromRow(ExcelWorksheet worksheet, int row, int column)
        {
            var cellValue = worksheet.Cells[row, column].Value;

            if (cellValue == null || string.IsNullOrWhiteSpace(cellValue.ToString()))
            {
                return new string[0];
            }

            return cellValue.ToString().Split(',').Where(s => !string.IsNullOrWhiteSpace(s)).Select(s => s.Trim()).ToArray();
        }

        private string GetLocalizedExceptionMessagePart(string parameter)
        {
            return _localizationSource.GetString("{0}IsInvalid", _localizationSource.GetString(parameter)) + "; ";
        }

        private bool IsRowEmpty(ExcelWorksheet worksheet, int row)
        {
            return worksheet.Cells[row, 1].Value == null || string.IsNullOrWhiteSpace(worksheet.Cells[row, 1].Value.ToString());
        }

        private T ConvertToEnum<T>(string value, T defaultValue)
        {
            try
            {
                return (T)Enum.Parse(typeof(T), value, true);
            }
            catch
            {
                return defaultValue;
            }
        }

        
    }
}
