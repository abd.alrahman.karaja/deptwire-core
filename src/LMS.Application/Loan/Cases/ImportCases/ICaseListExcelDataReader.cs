﻿using Abp.Dependency;
using LMS.Loan.Cases.Dto.ImportCases;
using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Loan.Cases.ImportCases
{
    public interface ICaseListExcelDataReader : ITransientDependency
    {
        List<ImportCaseDto> GetCasesFromExcel(byte[] fileBytes);
    }
}
