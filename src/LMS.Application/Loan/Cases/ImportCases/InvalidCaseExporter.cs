﻿using LMS.DataExporting.Excel.EpPlus;
using LMS.Loan.Cases.Dto.ImportCases;
using LMS.Loan.Shared.Dto;
using LMS.Storage;
using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Loan.Cases.ImportCases
{
    public class InvalidCaseExporter : EpPlusExcelExporterBase, IInvalidCaseExporter
    {
        public InvalidCaseExporter(ITempFileCacheManager tempFileCacheManager)
            : base(tempFileCacheManager)
        {
        }

        public FileDto ExportToFile(List<ImportCaseDto> caseListDtos)
        {
            return CreateExcelPackage(
                "InvalidCaseImportList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("InvalidUserImports"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        "Agent",
                        "Agent ID",
                        "Bank",
                        "Bank ID",
                        "Bank Home",
                        "CIF",
                        "Customer",
                        "Account NO.",
                        "Contract NO.",
                        "Full Account",
                        "Product",
                        "Type of Assist",
                        "Manufacturer",
                        "Original Amount",
                        "Total Amount",
                        "O/S",
                        "P/A",
                        "Last Credit Limit",
                        "Main Ledger Balance",
                        "EMI",
                        "BKT. Stage",
                        "O/D",
                        "Min. Payment",
                        "W-Off Date",
                        "W-Off Age",
                        "DPD",
                        "Open",
                        "Close",
                        "Last Payment Amount",
                        "Last Payment Date",
                        "Last Purchase Amount",
                        "Last Purchase Date",
                        "DOB",
                        "Gender",
                        "Age",
                        "Nationality",
                        "Residence No.",
                        "Visa No.",
                        "Passport No.",
                        "Passport Expiry",
                        "Mother Name",
                        "Company Name",
                        "CM Mob. by Bank",
                        "CM Email by Bank",
                        "Reference by Bank",
                        "Reference Mob. by Bank",
                        "Police Case Amount",
                        "Police Station",
                        "Police Case Status",
                        "Police Case No.",
                        "Police Case Date",
                        "Final Jugement"
                        );

                    AddObjects(
                        sheet, 2, caseListDtos,
                        _ => _.AgentName,
                        _ => _.AgentId,
                        _ => _.BankName,
                        _ => _.BankId,
                        _ => _.BankHome,
                        _ => _.CIF,
                        _ => _.CustomerName,
                        _ => _.AccountNo,
                        _ => _.ContractNo,
                        _ => _.FullAccount,
                        _ => _.Product,
                        _ => _.TypeOfAssist,
                        _ => _.Manufacturer,
                        _ => _.OriginalAmount,
                        _ => _.TotalAmount,
                        _ => _.Outstanding,
                        _ => _.PrincipalAmount,
                        _ => _.LastCreditLimit,
                        _ => _.MainLedgerBalance,
                        _ => _.InstallmentAmount,
                        _ => _.BucketStage,
                        _ => _.Overdues,
                        _ => _.MinPayment,
                        _ => _.WriteOffDate,
                        _ => _.WriteOffAge,
                        _ => _.DPD,
                        _ => _.OpenDate,
                        _ => _.CloseDate,
                        _ => _.LastPaymentAmount,
                        _ => _.LastPaymentDate,
                        _ => _.LastPurchaseAmount,
                        _ => _.LastPurchaseDate,
                        _ => _.DOB,
                        _ => _.Gender,
                        _ => _.Age,
                        _ => _.NationalityName,
                        _ => _.ResidenceNo,
                        _ => _.VisaNo,
                        _ => _.PassportNo,
                        _ => _.PassportExpiry,
                        _ => _.MotherName,
                        _ => _.CompanyName,
                        _ => _.CustomerMobileByBank,
                        _ => _.CustomerEmailByBank,
                        _ => _.ReferenceNameByBank,
                        _ => _.ReferenceMobileByBank,
                        _ => _.PoliceCaseAmount,
                        _ => _.PoliceStationName,
                        _ => _.PoliceCaseStatus,
                        _ => _.PoliceCaseNo,
                        _ => _.PoliceCaseDate,
                        _ => _.FinalJugement
                        );

                    for (var i = 1; i <= 9; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}
