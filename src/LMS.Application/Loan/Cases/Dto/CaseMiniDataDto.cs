﻿using Abp.Application.Services.Dto;
using System;

namespace LMS.Loan.Cases.Dto
{
    public class CaseMiniDataDto: EntityDto
    {
        public string AccountNo { get; set; }
        public string Bank { get; set; }
        public string Agent { get; set; }
        public string Product { get; set; }
        public DateTime? OpenDate { get; set; }
        public bool Checked { get; set; }
    }
}
