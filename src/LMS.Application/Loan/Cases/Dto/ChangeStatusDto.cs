﻿using Abp.Application.Services.Dto;

namespace LMS.Loan.Cases.Dto
{
    public class ChangeStatusDto: EntityDto
    {
        public int Status { get; set; }
        public string SupervisorNote { get; set; }
        public string NegotiatorNote { get; set; }
        public int[] CasesIds { get; set; }
    }
}
