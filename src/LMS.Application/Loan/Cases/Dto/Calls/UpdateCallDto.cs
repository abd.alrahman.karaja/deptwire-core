﻿using Abp.Application.Services.Dto;
using System;

namespace LMS.Loan.Cases.Dto.Calls
{
    public class UpdateCallDto : EntityDto
    {
        public string CallWith { get; set; }
        public string CallDate { get; set; }
        public string CallStart { get; set; }
        public string NegotiatorNote { get; set; }
        public string ReminderDate { get; set; }
        public string ReminderNote { get; set; }
        public int[] CasesIds { get; set; }
    }
}
