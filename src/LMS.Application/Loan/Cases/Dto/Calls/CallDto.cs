﻿using Abp.Application.Services.Dto;
using System;

namespace LMS.Loan.Cases.Dto.Calls
{
    public class CallDto : EntityDto
    {
        public string CallWith { get; set; }
        public DateTime? CallDate { get; set; }
        public DateTime? CallStart { get; set; }
        public string NegotiatorNote { get; set; }
        public DateTime? ReminderDate { get; set; }
        public string ReminderNote { get; set; }
    }
}
