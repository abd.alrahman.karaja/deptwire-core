﻿using Abp;
using System;

namespace LMS.Loan.Cases.Dto.ImportCases
{
    public class ImportCasesFromExcelJobArgs
    {
        public int? TenantId { get; set; }
        public Guid BinaryObjectId { get; set; }
        public int? BankId { get; set; }
        public UserIdentifier User { get; set; }
    }
}
