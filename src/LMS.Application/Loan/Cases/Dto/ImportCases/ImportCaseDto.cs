﻿using LMS.Loan.Enums;
using System;

namespace LMS.Loan.Cases.Dto.ImportCases
{
    public class ImportCaseDto
    {
        public string AgentName { get; set; }
        public string AgentId { get; set; }
        public string BankName { get; set; }
        public string BankId { get; set; }
        public string BankHome { get; set; }
        public string CIF { get; set; }
        public string CustomerName { get; set; }
        public string AccountNo { get; set; }
        public string ContractNo { get; set; }
        public string FullAccount { get; set; }
        public string Product { get; set; }
        public string TypeOfAssist { get; set; }
        public string Manufacturer { get; set; }
        public OriginalAmount OriginalAmount { get; set; }
        public double TotalAmount { get; set; }
        public double Outstanding { get; set; }
        public double PrincipalAmount { get; set; }
        public double LastCreditLimit { get; set; }
        public double MainLedgerBalance { get; set; }
        public double InstallmentAmount { get; set; }
        public string BucketStage { get; set; }
        public double Overdues { get; set; }
        public double MinPayment { get; set; }
        public DateTime? WriteOffDate { get; set; }
        public string WriteOffAge { get; set; }
        public int DPD { get; set; }
        public DateTime? OpenDate { get; set; }
        public DateTime? CloseDate { get; set; }
        public double? LastPaymentAmount { get; set; }
        public DateTime? LastPaymentDate { get; set; }
        public double? LastPurchaseAmount { get; set; }
        public DateTime? LastPurchaseDate { get; set; }
        public DateTime? DOB { get; set; }

        #region Customer Name
        public string Name { get; set; }
        public string ArabicName { get; set; }
        public string Gender { get; set; }
        public int Age { get; set; }
        public string NationalityName { get; set; }
        public string ResidenceNo { get; set; }
        public string VisaNo { get; set; }
        public string PassportNo { get; set; }
        public DateTime? PassportExpiry { get; set; }
        public string MotherName { get; set; }
        public string CompanyName { get; set; }
        public string CustomerMobileByBank { get; set; }
        public string CustomerEmailByBank { get; set; }
        public string ReferenceNameByBank { get; set; }
        public string ReferenceMobileByBank { get; set; }
        #endregion
        #region Police Info
        public string PoliceCaseAmount { get; set; }
        public string PoliceStationName { get; set; }
        public string PoliceCaseStatus { get; set; }
        public string PoliceCaseNo { get; set; }
        public string PoliceCaseDate { get; set; }
        public string FinalJugement { get; set; }
        #endregion
        #region Waed Info
        public string UserName { get; set; }//
        public string BookingDate { get; set; }
        public string AllocationDate { get; set; }
        public string ModeOfAllocation { get; set; }
        public string Activity { get; set; }
        public string WaedStatus { get; set; }
        public string WaedFeedback { get; set; }
        public string BankStatus { get; set; }
        public string BankFeedback { get; set; }
        public string ArabicFeedback { get; set; }
        public string BankStatusCode { get; set; }
        public string KeepBackToBank { get; set; }
        public string NegotiatorNote { get; set; }
        public string SupervisorNote { get; set; }
        #region Legal Info
        public string LegalStatus { get; set; }
        public string LegalNote { get; set; }
        #endregion
        public string LibilityEmail { get; set; }
        public string DOX { get; set; }
        public string NOC { get; set; }
        public string InsideOrOutsideBankCountry { get; set; }
        public string InsideOrOutsideSyria { get; set; }
        public string CurrentLocation { get; set; }
        public string CustomerCareer { get; set; }
        public string CurrentJob { get; set; }
        public string HomeAddress { get; set; }
        public string CmMobileNO { get; set; }
        public string CmOtherMobileNO { get; set; }
        public string HomePhoneNumber { get; set; }
        #endregion

        /// <summary>
        /// Can be set when reading data from excel or when importing user
        /// </summary>
        public string Exception { get; set; }

        public bool CanBeImported()
        {
            return string.IsNullOrEmpty(Exception);
        }

        public bool ThereIsLackOfData()
        {
            if (
                string.IsNullOrEmpty(CustomerName)
                )
            {
                return true;
            }

            if (
                CustomerName.Equals("-")
                )
            {
                return true;
            }

            return false;
        }
    }
}
