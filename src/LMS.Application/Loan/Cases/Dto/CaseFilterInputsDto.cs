﻿using System;

namespace LMS.Loan.Cases.Dto
{
    public class CaseFilterInputsDto
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int BankId { get; set; }
        public string AccountNo { get; set; }
        public string ContractNo { get; set; }
        public DateTime? BookingDate { get; set; }
        public string BucketStage { get; set; }
        public int ProductId { get; set; }
        public int LegalId { get; set; }
        public int Status { get; set; }
    }
}
