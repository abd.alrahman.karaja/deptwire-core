﻿using LMS.Loan.Agents.Dto;
using LMS.Loan.Banks.Dto;
using LMS.Loan.Cases.Dto.Payments;
using LMS.Loan.Cases.Dto.PTP;
using LMS.Loan.Customers.Dto;
using LMS.Loan.Indexes.Shared.Dto;
using LMS.Users.Dto;
using System;
using System.Collections.Generic;

namespace LMS.Loan.Cases
{
    public class ReadCaseDto
    {
        public int id { get; set; }
        public string accountNo { get; set; }
        public string contractNo { get; set; }
        public string fullAccount { get; set; }
        public string typeOfAssist { get; set; }
        public string manufacturer { get; set; }
        public string originalAmount { get; set; }
        public double totalAmount { get; set; }
        public double outstanding { get; set; }
        public double principalAmount { get; set; }
        public double lastCreditLimit { get; set; }
        public double mainLedgerBalance { get; set; }
        public double installmentAmount { get; set; }
        public string bucketStage { get; set; }
        public double overdues { get; set; }
        public double minPayment { get; set; }
        public DateTime? writeOffDate { get; set; }
        public string writeOffAge { get; set; }
        public int dpd { get; set; }
        public DateTime? openDate { get; set; }
        public DateTime? closeDate { get; set; }
        public double? lastPaymentAmount { get; set; }
        public DateTime? lastPaymentDate { get; set; }
        public double? lastPurchaseAmount { get; set; }
        public DateTime? lastPurchaseDate { get; set; }
        public string cif { get; set; }
        public string companyName { get; set; }

        #region Police Info

        public string policeCaseAmount { get; set; }
        public string policeStation { get; set; }
        public string policeCaseStatus { get; set; }
        public string policeCaseNo { get; set; }
        public DateTime? policeCaseDate { get; set; }
        public string finalJugement { get; set; }
        #endregion

        #region Waed Info

        public DateTime? bookingDate { get; set; }
        public DateTime? allocationDate { get; set; }
        public string modeOfAllocation { get; set; }
        public bool activity { get; set; }
        public string waedFeedback { get; set; }
        public string bankFeedback { get; set; }
        public string arabicFeedback { get; set; }
        public bool keepBackToBank { get; set; }
        public string negotiatorNote { get; set; }
        public string supervisorNote { get; set; }
        public string legalNote { get; set; }
        public string liabilityMail { get; set; }
        public string dox { get; set; }
        public string noc { get; set; }
        public int WaedStatus { get; set; }
        public int BankStatus { get; set; }

        #endregion

        public long? userId { get; set; }
        public ReadAgentDto agent { get; set; }
        public ReadBankDto bank { get; set; }
        public ReadIndexDto bankHome { get; set; }
        public ReadCustomerDto customer { get; set; }
        public ReadIndexDto product { get; set; }
        public ReadUserDto user { get; set; }
        public ReadBankCodeDto bankCode { get; set; }
    }
}
