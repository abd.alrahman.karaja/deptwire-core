﻿namespace LMS.Loan.Cases.Dto
{
    public class NegotiatorCaseDto
    {
        public int CaseId { get; set; }
        public int UserId { get; set; }
    }
}
