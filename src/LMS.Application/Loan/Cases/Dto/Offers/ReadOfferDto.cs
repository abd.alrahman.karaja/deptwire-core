﻿using Abp.Application.Services.Dto;
using System;

namespace LMS.Loan.Cases.Dto.Offers
{
    public class ReadOfferDto : EntityDto
    {
        public double OfferAmount { get; set; }
        public DateTime? OfferDate { get; set; }
        public int EMIs { get; set; }
        public double FirstPayment { get; set; }
        public DateTime? FirstPaymentDate { get; set; }
        public int OfferStatus { get; set; }
        public DateTime? ApprovalDate { get; set; }
        public string NegotiatorNote { get; set; }
        public string SuperviorNote { get; set; }
    }
}
