﻿using LMS.Loan.Cases.Dto.Reminders;
using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Loan.Cases.Dto.Offers
{
    public class CreateOfferDto
    {
        public double OfferAmount { get; set; }
        public string OfferDate { get; set; }
        public int EMIs { get; set; }
        public double FirstPayment { get; set; }
        public string FirstPaymentDate { get; set; }
        public int OfferStatus { get; set; }
        public string NegotiatorNote { get; set; }
        public string ReminderDate { get; set; }
        public string ReminderNote { get; set; }
        public int[] CasesIds { get; set; }
    }
}
