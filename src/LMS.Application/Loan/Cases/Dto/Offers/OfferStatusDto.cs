﻿using Abp.Application.Services.Dto;

namespace LMS.Loan.Cases.Dto.Offers
{
    public class OfferStatusDto : EntityDto
    {
        public int OfferStatus { get; set; }
        public string SuperviorNote { get; set; }
    }
}
