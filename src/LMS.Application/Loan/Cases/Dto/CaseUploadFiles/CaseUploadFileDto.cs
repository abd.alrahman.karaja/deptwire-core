﻿using Abp.Application.Services.Dto;

namespace LMS.Loan.Cases.Dto.CaseUploadFiles
{
    public class CaseUploadFileDto : EntityDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Path { get; set; }
        public int? FileCategoryId { get; set; }
        public int? CaseId { get; set; }
    }
}
