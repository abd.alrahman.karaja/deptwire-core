﻿using Abp.Application.Services.Dto;

namespace LMS.Loan.Cases.Dto.PTP
{
    public class UpdatePromiseToPayPaymentDto : EntityDto
    {
        /// <summary>
        /// رقم الدفعة
        /// </summary>
        public int PaymentNo { get; set; }
        /// <summary>
        /// رقم الدفعة بالتسوية و في حال كانت دفعة جزئية بدون تسوية لا يوضع رقم
        /// </summary>
        public int ModeOfPayment { get; set; }

        /// <summary>
        /// مبلغ الدفعة
        /// </summary>
        public double PtpAmount { get; set; }

        /// <summary>
        /// تاريخ الدفعة
        /// </summary>
        public string PaymentDate { get; set; }

        /// <summary>
        /// الدفعة المحصلة
        /// </summary>
        public double CollectedPayment { get; set; }

        /// <summary>
        /// تأكيد الدفعة من قبل البنك
        /// </summary>
        public bool BankConfirmation { get; set; }

        /// <summary>
        /// ملخص التسوية
        /// </summary>
        public string Note { get; set; }

        public string NegotiatorNote { get; set; }

        public string SuperviorNote { get; set; }

        public int? PromiseToPayId { get; set; }
    }
}
