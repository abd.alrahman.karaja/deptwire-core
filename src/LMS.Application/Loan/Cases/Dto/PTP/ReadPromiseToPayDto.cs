﻿using System;
using System.Collections.Generic;

namespace LMS.Loan.Cases.Dto.PTP
{
    public class ReadPromiseToPayDto
    {
        public int id { get; set; }
        /// <summary>
        /// مبلغ التسوية
        /// </summary>
        public double settlementAmount { get; set; }

        /// <summary>
        /// تاريخ الدفعة
        /// </summary>
        public DateTime? settlementDate { get; set; }

        /// <summary>
        /// عدد اقساط التسوية
        /// </summary>
        public int emis { get; set; }

        public IList<ReadPromiseToPayPaymentDto> ptpPayments { get; set; }
    }
}
