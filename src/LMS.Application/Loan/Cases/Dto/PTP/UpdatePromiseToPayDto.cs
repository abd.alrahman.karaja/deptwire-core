﻿using Abp.Application.Services.Dto;
using System.Collections.Generic;

namespace LMS.Loan.Cases.Dto.PTP
{
    public class UpdatePromiseToPayDto : EntityDto
    {
        /// <summary>
        /// مبلغ التسوية
        /// </summary>
        public double SettlementAmount { get; set; }

        /// <summary>
        /// تاريخ الدفعة
        /// </summary>
        public string SettlementDate { get; set; }

        /// <summary>
        /// عدد اقساط التسوية
        /// </summary>
        public int EMIS { get; set; }

        public string ReminderDate { get; set; }
        public string ReminderNote { get; set; }
        public int[] CasesIds { get; set; }
        public IList<UpdatePromiseToPayPaymentDto> PtpPayments { get; set; }

    }
}
