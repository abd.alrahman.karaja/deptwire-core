﻿using LMS.Loan.Cases.Dto.Offers;
using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Loan.Cases.Dto.PTP
{
    public class ReadPromiseToPayPaymentDto
    {
        public int id { get; set; }
        /// <summary>
        /// رقم الدفعة
        /// </summary>
        public int paymentNo { get; set; }
        /// <summary>
        /// رقم الدفعة بالتسوية و في حال كانت دفعة جزئية بدون تسوية لا يوضع رقم
        /// </summary>
        public int modeOfPayment { get; set; }

        /// <summary>
        /// مبلغ الدفعة
        /// </summary>
        public double ptpAmount { get; set; }

        /// <summary>
        /// تاريخ الدفعة
        /// </summary>
        public DateTime? paymentDate { get; set; }

        /// <summary>
        /// الدفعة المحصلة
        /// </summary>
        public double collectedPayment { get; set; }

        /// <summary>
        /// تأكيد الدفعة من قبل البنك
        /// </summary>
        public bool bankConfirmation { get; set; }

        public string negotiatorNote { get; set; }
        public string superviorNote { get; set; }

        public int? offerId { get; set; }

        public ReadOfferDto offer { get; set; }

    }
}
