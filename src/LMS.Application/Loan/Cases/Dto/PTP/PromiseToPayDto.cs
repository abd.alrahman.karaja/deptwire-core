﻿using Abp.Application.Services.Dto;
using LMS.Loan.Cases.Dto.Offers;
using LMS.Loan.Enums;
using System;
using System.Collections.Generic;

namespace LMS.Loan.Cases.Dto.PTP
{
    public class PromiseToPayDto : EntityDto
    {
        /// <summary>
        /// مبلغ التسوية
        /// </summary>
        public double SettlementAmount { get; set; }

        /// <summary>
        /// تاريخ الدفعة
        /// </summary>
        public DateTime? SettlementDate { get; set; }

        /// <summary>
        /// عدد اقساط التسوية
        /// </summary>
        public int EMIS { get; set; }

        public string NegotiatorNote { get; set; }
        public string SuperviorNote { get; set; }

        #region Offer
        public int? OfferId { get; set; }
        public OfferDto Offer { get; set; }
        #endregion

        public IList<PromiseToPayPaymentDto> PtpPayments { get; set; }



    }
}
