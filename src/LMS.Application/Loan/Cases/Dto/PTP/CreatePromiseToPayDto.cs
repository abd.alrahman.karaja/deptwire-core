﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Loan.Cases.Dto.PTP
{
    public class CreatePromiseToPayDto
    {
        /// <summary>
        /// مبلغ التسوية
        /// </summary>
        public double SettlementAmount { get; set; }

        /// <summary>
        /// تاريخ الدفعة
        /// </summary>
        public string SettlementDate { get; set; }

        /// <summary>
        /// عدد اقساط التسوية
        /// </summary>
        public int EMIS { get; set; }

        public string ReminderDate { get; set; }
        public string ReminderNote { get; set; }
        public int[] CasesIds { get; set; }
        public IList<CreatePromiseToPayPaymentDto> PtpPayments { get; set; }

    }
}
