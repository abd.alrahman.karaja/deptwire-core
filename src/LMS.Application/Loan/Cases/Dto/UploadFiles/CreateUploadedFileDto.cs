﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Loan.Cases.Dto.UploadedFile
{
    public class CreateUploadedFileDto
    {
        public string Name { get; set; }

        /// <summary>
        /// تاريخ 
        /// </summary>
        public DateTime? UploadDate { get; set; }

        /// <summary>
        /// نوع الملف
        /// </summary>
        public int Type { get; set; }


        /// <summary>
        /// ملاحظات 
        /// </summary>
        public string Note { get; set; }

        /// <summary>
        /// المسار 
        /// </summary>
        public string Path { get; set; }
        public int[] CasesIds { get; set; }
    }
}
