﻿using Abp.Application.Services.Dto;
using LMS.Loan.Enums;
using System;

namespace LMS.Loan.Cases.Dto.UploadedFile
{
    public class UploadedFileDto : EntityDto
    {
        public string Name { get; set; }

        /// <summary>
        /// تاريخ 
        /// </summary>
        public DateTime? UploadDate { get; set; }

        /// <summary>
        /// نوع الملف
        /// </summary>
        public int Type { get; set; }


        /// <summary>
        /// ملاحظات 
        /// </summary>
        public string Note { get; set; }

        /// <summary>
        /// المسار 
        /// </summary>
        public string Path { get; set; }

        public int[] CasesIds { get; set; }
    }
}
