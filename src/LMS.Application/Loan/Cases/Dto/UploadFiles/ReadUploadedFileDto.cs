﻿using System;

namespace LMS.Loan.Cases.Dto.UploadedFile
{
    public class ReadUploadedFileDto
    {
        public int id { get; set; }
        public string name { get; set; }

        /// <summary>
        /// تاريخ 
        /// </summary>
        public DateTime? uploadDate { get; set; }

        /// <summary>
        /// نوع الملف
        /// </summary>
        public int type { get; set; }


        /// <summary>
        /// ملاحظات 
        /// </summary>
        public string note { get; set; }

        /// <summary>
        /// المسار 
        /// </summary>
        public string path { get; set; }

    }
}
