﻿namespace LMS.Loan.Cases.Dto.Legals
{
    public class CreateLegalDto
    {
        public int LegalStatus { get; set; }
        public string SupervisorNote { get; set; }
        public int[] CasesIds { get; set; }
    }
}
