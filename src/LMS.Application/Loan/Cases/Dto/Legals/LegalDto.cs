﻿using Abp.Application.Services.Dto;
using LMS.Users.Dto;
using System;

namespace LMS.Loan.Cases.Dto.Legals
{
    public class LegalDto : EntityDto
    {
        public int LegalStatus { get; set; }
        public string LegalNote { get; set; }
        public string SupervisorNote { get; set; }
        public DateTime? CreationTime { get; set; }
        public UserMiniDataDto CreatorUser { get; set; }
    }
}
