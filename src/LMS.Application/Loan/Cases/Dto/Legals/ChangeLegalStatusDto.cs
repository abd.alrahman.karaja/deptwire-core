﻿using Abp.Application.Services.Dto;

namespace LMS.Loan.Cases.Dto.Legals
{
    public class ChangeLegalStatusDto : EntityDto
    {
        public int LegalStatus { get; set; }
        public string LegalNote { get; set; }
        //public int[] CasesIds { get; set; }
    }
}
