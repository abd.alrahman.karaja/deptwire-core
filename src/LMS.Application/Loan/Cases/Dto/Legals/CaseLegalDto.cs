﻿using Abp.Application.Services.Dto;

namespace LMS.Loan.Cases.Dto.Legals
{
    public class CaseLegalDto : EntityDto
    {
        public int LegalId { get; set; }
        public LegalDto Legal { get; set; }
    }
}
