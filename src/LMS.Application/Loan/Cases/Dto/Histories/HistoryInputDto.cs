﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Loan.Cases.Dto.Histories
{
    public class HistoryInputDto
    {
        public long UserId { get; set; }
        public int NumberOfMonth{ get; set; }
    }
}
