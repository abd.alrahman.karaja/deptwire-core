﻿using Abp.Application.Services.Dto;
using LMS.Users.Dto;
using System;

namespace LMS.Loan.Cases.Dto.Histories
{
    public class CaseHistoryDto : EntityDto
    {
        public int ActionType { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int ActionId { get; set; }
        public int CaseId { get; set; }
        public DateTime CreationTime { get; set; }
        public UserMiniDataDto CreatorUser { get; set; }
    }
}
