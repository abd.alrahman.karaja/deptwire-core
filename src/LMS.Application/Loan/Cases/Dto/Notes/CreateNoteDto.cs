﻿namespace LMS.Loan.Cases.Dto.Notes
{
    public class CreateNoteDto
    {
        public string NoteText { get; set; }
        public int[] CasesIds { get; set; }
    }
}
