﻿namespace LMS.Loan.Cases.Dto.Notes
{
    public class CaseNoteInputDto
    {
        public int CaseId { get; set; }
        public int ActionName { get; set; }
        public int? ActionId { get; set; }
        public int Type { get; set; }

    }
}
