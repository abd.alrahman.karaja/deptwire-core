﻿using Abp.Application.Services.Dto;

namespace LMS.Loan.Cases.Dto.Notes
{
    public class CaseNoteDto : EntityDto
    {
        public int ActionName { get; set; }
        public int? ActionId { get; set; }
        public int? Type { get; set; }
        public int NoteId { get; set; }
        public NoteDto Note { get; set; }
    }
}
