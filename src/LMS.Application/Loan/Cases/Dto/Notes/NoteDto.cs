﻿using Abp.Application.Services.Dto;
using LMS.Users.Dto;
using System;

namespace LMS.Loan.Cases.Dto.Notes
{
    public class NoteDto : EntityDto
    {
        public string NoteText { get; set; }
        public DateTime? CreationTime { get; set; }
        public UserMiniDataDto CreatorUser { get; set; }
    }
}
