﻿using Abp.Application.Services.Dto;
using LMS.Users.Dto;
using System;

namespace LMS.Loan.Cases.Dto.Meetings
{
    public class MeetingDto : EntityDto
    {
        public UserMiniDataDto ManagerTeamName { get; set; }
        public UserMiniDataDto MeetingManagerName { get; set; }
        public string MeetingWith { get; set; }
        public DateTime? MeetingDate { get; set; }
        public DateTime? MeetingStart { get; set; }
        public DateTime? MeetingEnd { get; set; }
        public bool LegalMeeting { get; set; }
        public bool Done { get; set; }
    }
}
