﻿using Abp.Application.Services.Dto;
using System;

namespace LMS.Loan.Cases.Dto.Meetings
{
    public class UpdateMeetingDto : EntityDto
    {
        public long? ManagerTeamNameId { get; set; }
        public long? MeetingManagerNameId { get; set; }
        public string MeetingWith { get; set; }
        public string MeetingDate { get; set; }
        public string MeetingStart { get; set; }
        public string MeetingEnd { get; set; }
        public bool LegalMeeting { get; set; }
        public bool Done { get; set; }
        public int[] CasesIds { get; set; }
    }
}
