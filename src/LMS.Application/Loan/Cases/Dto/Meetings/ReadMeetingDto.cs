﻿using Abp.Application.Services.Dto;
using LMS.Loan.Cases.Dto.Reminders;
using LMS.Users.Dto;
using System;
using System.Collections.Generic;

namespace LMS.Loan.Cases.Dto.Meetings
{
    public class ReadMeetingDto : EntityDto
    {
        public UserMiniDataDto ManagerTeamName { get; set; }
        public UserMiniDataDto MeetingManagerName { get; set; }
        public string MeetingWith { get; set; }
        public DateTime? MeetingDate { get; set; }
        public DateTime? MeetingStart { get; set; }
        public DateTime? MeetingEnd { get; set; }
        public bool LegalMeeting { get; set; }
        public bool Done { get; set; }
        public int CaseId { get; set; }
        public ReminderDto Reminder { get; set; }
    }
}
