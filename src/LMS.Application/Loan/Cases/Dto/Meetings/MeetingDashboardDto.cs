﻿using Abp.Application.Services.Dto;
using LMS.Users.Dto;
using System;

namespace LMS.Loan.Cases.Dto.Meetings
{
    public class MeetingDashboardDto : EntityDto
    {
        public string ManagerTeamName { get; set; }
        public string MeetingManagerName { get; set; }
        public string MeetingWith { get; set; }
        public DateTime? MeetingDate { get; set; }
        public DateTime? MeetingStart { get; set; }
        public DateTime? MeetingEnd { get; set; }
        public bool LegalMeeting { get; set; }
        public bool Done { get; set; }
        public int[] CasesIds { get; set; }
    }
}
