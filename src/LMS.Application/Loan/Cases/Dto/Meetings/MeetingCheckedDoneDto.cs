﻿using Abp.Application.Services.Dto;

namespace LMS.Loan.Cases.Dto.Meetings
{
    public class MeetingCheckedDoneDto : EntityDto
    {
        public bool Done { get; set; }
    }
}
