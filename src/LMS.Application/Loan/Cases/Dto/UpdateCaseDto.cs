﻿using Abp.Application.Services.Dto;

namespace LMS.Loan.Cases.Dto
{
    public class UpdateCaseDto : EntityDto
    {
        /// <summary>
        /// الفيدباك الخاص بالوعد
        /// </summary>
        public string WaedFeedback { get; set; }

        /// <summary>
        /// الفيدباك الخاص بالبنك
        /// </summary>
        public string BankFeedback { get; set; }

        /// <summary>
        /// فيدباك باللغة العربية خاص ببنك الراجحي
        /// </summary>
        public string ArabicFeedback { get; set; }

        /// <summary>
        /// ملاحظات اضافية للمفاوض للقضيه نفسها
        /// </summary>
        public string NegotiatorNote { get; set; }

        /// <summary>
        /// ملاحظة اضافية لقائد الفريق او المنسقة او الادارة لنفس القضية
        /// </summary>
        public string SupervisorNote { get; set; }

        
        /// <summary>
        /// ايميل مطالبة بالمديونية
        /// </summary>
        public int LiabilityMail { get; set; }

        /// <summary>
        /// ورقيات
        /// </summary>
        public int DOX { get; set; }

        /// <summary>
        /// براءة الذمة 
        /// </summary>
        public string NOC { get; set; }

        public string BookingDate { get; set; }
        public string AllocationDate { get; set; }
        public int ModeOfAllocation { get; set; }
     

        
        //public BankCodeDto BankCode { get; set; }
        
        
    }
}
