﻿using Abp.Application.Services.Dto;
using LMS.Loan.Currencies.Dto;
using LMS.Loan.Indexes.Dto;
using LMS.Users.Dto;
using System;

namespace LMS.Loan.Cases.Dto.Payments
{
    public class PaymentDto : EntityDto
    {
        /// <summary>
        /// رقم الايصال
        /// </summary>
        public string ReceiptNo { get; set; }
        /// <summary>
        /// مكان الدفعة
        /// </summary>
        public string PlaceOfPayment { get; set; }
        /// <summary>
        /// المبلغ
        /// </summary>
        public double Amount { get; set; }
        /// <summary>
        /// المبلغ بالدرهم الاماراتي
        /// </summary>
        public double AmountByAED { get; set; }
        /// <summary>
        /// حالة تحويل الحوالة
        /// </summary>
        public IndexDto TransferredStatus { get; set; }
        /// <summary>
        /// تاريخ تحويل الحوالة
        /// </summary>
        public DateTime? TransferredDate { get; set; }
        /// <summary>
        /// تاريخ استلام الدفعة من قبل البنك 
        /// </summary>
        public DateTime? ReflectionDate { get; set; }
        /// <summary>
        /// حالة اقفال الدفعة 
        /// </summary>
        public IndexDto ClaimedStatus { get; set; }
        /// <summary>
        /// تاريخ اقفال الدفعة 
        /// </summary>
        public DateTime? ClaimedDate { get; set; }
        /// <summary>
        /// النسبة المقدمة من البنك
        /// </summary>
        public decimal BankRatio { get; set; }
        /// <summary>
        /// نسبة الوكيل
        /// </summary>
        public decimal AgentRatio { get; set; }
        /// <summary>
        /// نسبة العمولة 
        /// </summary>
        public decimal CommissionRatio { get; set; }
        /// <summary>
        /// مبلغ العمولة
        /// </summary>
        public double CommissionAmount { get; set; }
        /// <summary>
        /// مبلغ العمولة بالدينار
        /// </summary>
        public double CommissionAmountAED { get; set; }
        /// <summary>
        /// مبلغ العمولة بالدولار
        /// </summary>
        public double CommissionAmountUSD { get; set; }
        /// <summary>
        /// حالة استلام عمولة الدفعة
        /// </summary>
        public IndexDto ReceivedStatus { get; set; }
        /// <summary>
        /// تاريخ استلام عمولة الدفعة
        /// </summary>
        public DateTime? ReceivedDate { get; set; }

        public CurrencyDto Currency { get; set; }

        public UserDto CollectedBy { get; set; }
        public int CaseId { get; set; }
        public DateTime? ReminderDate { get; set; }
        public string ReminderNote { get; set; }

    }
}
