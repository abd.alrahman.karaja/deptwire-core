﻿using LMS.Loan.Currencies.Shared.Dto;
using LMS.Loan.Indexes.Shared.Dto;
using LMS.Users.Dto;
using System;

namespace LMS.Loan.Cases.Dto.Payments
{
    public class ReadPaymentDto
    {
        public int id { get; set; }
        /// <summary>
        /// رقم الايصال
        /// </summary>
        public string receiptNo { get; set; }
        /// <summary>
        /// مكان الدفعة
        /// </summary>
        public string placeOfPayment { get; set; }
        /// <summary>
        /// المبلغ
        /// </summary>
        public double amount { get; set; }
        /// <summary>
        /// المبلغ بالدرهم الاماراتي
        /// </summary>
        public double amountByAED { get; set; }
        /// <summary>
        /// حالة تحويل الحوالة
        /// </summary>
        public ReadIndexDto transferredStatus { get; set; }
        /// <summary>
        /// تاريخ تحويل الحوالة
        /// </summary>
        public DateTime? transferredDate { get; set; }
        /// <summary>
        /// تاريخ استلام الدفعة من قبل البنك 
        /// </summary>
        public DateTime? reflectionDate { get; set; }
        /// <summary>
        /// حالة اقفال الدفعة 
        /// </summary>
        public ReadIndexDto claimedStatus { get; set; }
        /// <summary>
        /// تاريخ اقفال الدفعة 
        /// </summary>
        public DateTime? claimedDate { get; set; }
        /// <summary>
        /// النسبة المقدمة من البنك
        /// </summary>
        public decimal bankRatio { get; set; }
        /// <summary>
        /// نسبة الوكيل
        /// </summary>
        public decimal agentRatio { get; set; }
        /// <summary>
        /// نسبة العمولة 
        /// </summary>
        public decimal commissionRatio { get; set; }
        /// <summary>
        /// مبلغ العمولة
        /// </summary>
        public double commissionAmount { get; set; }
        /// <summary>
        /// مبلغ العمولة بالدينار
        /// </summary>
        public double commissionAmountAED { get; set; }
        /// <summary>
        /// مبلغ العمولة بالدولار
        /// </summary>
        public double commissionAmountUSD { get; set; }
        /// <summary>
        /// حالة استلام عمولة الدفعة
        /// </summary>
        public ReadIndexDto receivedStatus { get; set; }
        /// <summary>
        /// تاريخ استلام عمولة الدفعة
        /// </summary>
        public DateTime? receivedDate { get; set; }

        public ReadCurrencyDto currency { get; set; }

        public ReadUserDto collectedBy { get; set; }
        public int caseId { get; set; }
    }
}
