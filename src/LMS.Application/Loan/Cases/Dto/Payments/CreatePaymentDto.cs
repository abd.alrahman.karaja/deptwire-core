﻿using System;

namespace LMS.Loan.Cases.Dto.Payments
{
    public class CreatePaymentDto
    {
        /// <summary>
        /// رقم الايصال
        /// </summary>
        public string ReceiptNo { get; set; }
        /// <summary>
        /// مكان الدفعة
        /// </summary>
        public string PlaceOfPayment { get; set; }
        /// <summary>
        /// المبلغ
        /// </summary>
        public double Amount { get; set; }
        /// <summary>
        /// المبلغ بالدرهم الاماراتي
        /// </summary>
        public double AmountByAED { get; set; }
        /// <summary>
        /// حالة تحويل الحوالة
        /// </summary>
        public int TransferredStatusId { get; set; }
        /// <summary>
        /// تاريخ تحويل الحوالة
        /// </summary>
        public string TransferredDate { get; set; }
        /// <summary>
        /// تاريخ استلام الدفعة من قبل البنك 
        /// </summary>
        public string ReflectionDate { get; set; }
        /// <summary>
        /// حالة اقفال الدفعة 
        /// </summary>
        public int ClaimedStatusId { get; set; }
        /// <summary>
        /// تاريخ اقفال الدفعة 
        /// </summary>
        public string ClaimedDate { get; set; }
        /// <summary>
        /// النسبة المقدمة من البنك
        /// </summary>
        public decimal BankRatio { get; set; }
        /// <summary>
        /// نسبة الوكيل
        /// </summary>
        public decimal AgentRatio { get; set; }
        /// <summary>
        /// نسبة العمولة 
        /// </summary>
        public decimal CommissionRatio { get; set; }
        /// <summary>
        /// مبلغ العمولة
        /// </summary>
        public double CommissionAmount { get; set; }
        /// <summary>
        /// مبلغ العمولة بالدينار
        /// </summary>
        public double CommissionAmountAED { get; set; }
        /// <summary>
        /// مبلغ العمولة بالدولار
        /// </summary>
        public double CommissionAmountUSD { get; set; }
        /// <summary>
        /// حالة استلام عمولة الدفعة
        /// </summary>
        public int ReceivedStatusId { get; set; }
        /// <summary>
        /// تاريخ استلام عمولة الدفعة
        /// </summary>
        public string ReceivedDate { get; set; }

        public int CurrencyId { get; set; }

        public long CollectedById { get; set; }
        public string ReminderDate { get; set; }
        public string ReminderNote { get; set; }
        public int[] CasesIds { get; set; }
    }
}
