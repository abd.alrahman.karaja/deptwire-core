﻿using Abp.Application.Services.Dto;
using System;

namespace LMS.Loan.Cases.Dto.Reminders
{
    public class ReminderDto : EntityDto
    {
        public DateTime? ReminderDate { get; set; }
        public string Note { get; set; }
    }
}
