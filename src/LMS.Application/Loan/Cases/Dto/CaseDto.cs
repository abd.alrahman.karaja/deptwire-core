﻿using Abp.Application.Services.Dto;
using LMS.Loan.Agents.Dto;
using LMS.Loan.Banks.Dto;
using LMS.Loan.Cases.Dto.Legals;
using LMS.Loan.Cases.Dto.Notes;
using LMS.Loan.Cases.Dto.Payments;
using LMS.Loan.Cases.Dto.PTP;
using LMS.Loan.Customers.Dto;
using LMS.Loan.Indexes.Dto;
using LMS.Users.Dto;
using System;
using System.Collections.Generic;

namespace LMS.Loan.Cases.Dto
{
    public class CaseDto : EntityDto
    {
        public CaseDto()
        {
            PromisesToPay = new List<PromiseToPayDto>();
            Payments = new List<PaymentDto>();
            CaseLegals = new List<CaseLegalDto>();
            CaseNotes = new List<CaseNoteDto>();
        }

        /// <summary>
        /// رقم حساب المنتج
        /// </summary>
        public string AccountNo { get; set; }

        /// <summary>
        /// رقم العقد المنتج الموقع لبنك الراجحي
        /// </summary>
        public string ContractNo { get; set; }

        /// <summary>
        /// رقم الحساب الكامل
        /// </summary>
        public string FullAccount { get; set; }

        /// <summary>
        /// نوع المساعده في حال كان منتج ABF  لبنك راك 
        /// </summary>
        public string TypeOfAssist { get; set; }

        /// <summary>
        /// اسم الشركة المصنعة في حال كان المنتج سيارة
        /// </summary>
        public string Manufacturer { get; set; }

        /// <summary>
        /// أصل مبلغ المديونية
        /// </summary>
        public string OriginalAmount { get; set; }

        /// <summary>
        /// أصل المبلغ مضاف اليه ربح البنك ( المبلغ المطلوب من قبل البنك ) ينتهي عند نهاية التعامل ( الراجحي )
        /// </summary>
        public double TotalAmount { get; set; }

        /// <summary>
        /// المبلغ المطالب به لتاريخه مع ربح البنك وغرامات التأخير بالاضافة للفوائد العقدية
        /// </summary>
        public double Outstanding { get; set; }

        /// <summary>
        /// المبلغ المطالب به لتاريخه مع ربح البنك
        /// </summary>
        public double PrincipalAmount { get; set; }

        /// <summary>
        /// اخر حد ائتماني للبطاقة ( ENBD )
        /// </summary>
        public double LastCreditLimit { get; set; }

        /// <summary>
        /// قيمة الرصيد الموجود في الحساب لصالح العميل ان وجد و تخص بنك الراجحي
        /// </summary>
        public double MainLedgerBalance { get; set; }

        /// <summary>
        /// قيمة القسط الشهري
        /// </summary>
        public double InstallmentAmount { get; set; }

        /// <summary>
        /// مرحلة الاقساط المتأخره
        /// </summary>
        public string BucketStage { get; set; }

        /// <summary>
        /// مجموع المتأخرات المستحقة لتاريخه ( تضاف في حال راك بكت او الخليج الكويتي )
        /// </summary>
        public double Overdues { get; set; }

        /// <summary>
        /// اقل دفعة مطلوبة في حالة البكت لبنك RAK
        /// </summary>
        public double MinPayment { get; set; }

        /// <summary>
        /// تاريخ قدم الدين اي تاريخ الوصول الى مرحلة Recovery
        /// </summary>
        public DateTime? WriteOffDate { get; set; }

        /// <summary>
        /// فئة سنين تاريخ قدم الدين مثال: 1-5 سنين
        /// </summary>
        public string WriteOffAge { get; set; }

        /// <summary>
        /// عدد ايام التأخير
        /// </summary>
        public int DPD { get; set; }

        /// <summary>
        /// تاريخ بداية التعامل
        /// </summary>
        public DateTime? OpenDate { get; set; }

        /// <summary>
        /// تاريخ نهاية التعامل
        /// </summary>
        public DateTime? CloseDate { get; set; }

        /// <summary>
        /// قيمة اخر دفعة
        /// </summary>
        public double? LastPaymentAmount { get; set; }

        /// <summary>
        /// تاريخ اخر دفعة
        /// </summary>
        public DateTime? LastPaymentDate { get; set; }

        /// <summary>
        /// اخر قيمة مشتريات بالبطاقة
        /// </summary>
        public double? LastPurchaseAmount { get; set; }

        /// <summary>
        /// تاريخ اخر عملية شراء بالبطاقة
        /// </summary>
        public DateTime? LastPurchaseDate { get; set; }

        /// <summary>
        /// الرقم التعريفي للعميل لدى البنك
        /// </summary>
        public string CIF { get; set; }

        /// <summary>
        /// جهة عمل الزبون اثناء الحصول على المديونية
        /// </summary>
        public string CompanyName { get; set; }

        #region Police Info

        /// <summary>
        /// قيمة شيك البلاغ
        /// </summary>
        public string PoliceCaseAmount { get; set; }

        /// <summary>
        /// /// <summary>
        /// اسم مركز الشرطة
        /// </summary>
        /// </summary>
        public string PoliceStation { get; set; }

        /// <summary>
        /// حالة قضية الشرطة في البنك
        /// </summary>
        public IndexDto PoliceCaseStatus { get; set; }

        /// <summary>
        /// تاريخ / رقم القضية
        /// </summary>
        public string PoliceCaseNo { get; set; }

        /// <summary>
        /// تاريخ القضية المفصل
        /// </summary>
        public DateTime? PoliceCaseDate { get; set; }

        /// <summary>
        /// الحكم النهائي
        /// </summary>
        public string FinalJugement { get; set; }
        #endregion

        #region Waed Info

        /// <summary>
        /// تاريخ تخصيص القضية لأول مرة من قبل البنك
        /// </summary>
        public DateTime? BookingDate { get; set; }

        /// <summary>
        /// اخر تاريخ خصصت فيه هذه القضية 
        /// </summary>
        public DateTime? AllocationDate { get; set; }

        /// <summary>
        /// وضع او حالة التخصيص
        /// </summary>
        public string ModeOfAllocation { get; set; }

        /// <summary>
        /// هل القضية نشطة او فعالة هذا الشهر او غير نشطة بناء على تخصيص البنك
        /// </summary>
        public bool Activity { get; set; }

        /// <summary>
        /// الفيدباك الخاص بالوعد
        /// </summary>
        public string WaedFeedback { get; set; }

        /// <summary>
        /// الفيدباك الخاص بالبنك
        /// </summary>
        public string BankFeedback { get; set; }

        /// <summary>
        /// فيدباك باللغة العربية خاص ببنك الراجحي
        /// </summary>
        public string ArabicFeedback { get; set; }

        /// <summary>
        /// الاحتفاظ بالقضية او ارجاعها للبنك
        /// </summary>
        public bool KeepBackToBank { get; set; }

        /// <summary>
        /// ملاحظات اضافية للمفاوض للقضيه نفسها
        /// </summary>
        public string NegotiatorNote { get; set; }

        /// <summary>
        /// ملاحظة اضافية لقائد الفريق او المنسقة او الادارة لنفس القضية
        /// </summary>
        public string SupervisorNote { get; set; }

        /// <summary>
        /// شرح الحالة القانونية
        /// </summary>
        public string LegalNote { get; set; }

        /// <summary>
        /// ايميل مطالبة بالمديونية
        /// </summary>
        public string LiabilityMail { get; set; }

        /// <summary>
        /// ورقيات
        /// </summary>
        public string DOX { get; set; }

        /// <summary>
        /// براءة الذمة 
        /// </summary>
        public string NOC { get; set; }

        public int WaedStatus { get; set; }
        public int BankStatus { get; set; }
        #endregion

        public long? UserId { get; set; }
        public AgentDto Agent { get; set; }
        public BankDto Bank { get; set; }
        public IndexDto BankHome { get; set; }
        public CustomerDto Customer { get; set; }
        public IndexDto Product { get; set; }
        public UserDto User { get; set; }
        public BankCodeDto BankCode { get; set; }
        public IndexDto LegalStatus { get; set; }
        public IList<PromiseToPayDto> PromisesToPay { get; set; }
        public IList<PaymentDto> Payments { get; set; }
        public IList<CaseLegalDto> CaseLegals { get; set; }
        public IList<CaseNoteDto> CaseNotes { get; set; }
    }
}
