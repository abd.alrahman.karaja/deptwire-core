﻿using Abp.Application.Services;
using LMS.Loan.Cases.Dto.Histories;
using System.Collections.Generic;

namespace LMS.Loan.Cases.Histories
{
    public interface IHistoryAppService : IApplicationService
    {
        IList<CaseHistoryDto> GetForCase(int caseId);
    }
}
