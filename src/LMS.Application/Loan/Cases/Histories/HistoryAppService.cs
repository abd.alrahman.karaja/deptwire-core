﻿using LMS.Loan.Cases.Dto.Histories;
using LMS.Loan.Cases.History;
using System.Collections.Generic;

namespace LMS.Loan.Cases.Histories
{
    public class HistoryAppService : LMSAppServiceBase, IHistoryAppService
    {
        private readonly HistoryManager _historyManager;

        public HistoryAppService(HistoryManager historyManager)
        {
            _historyManager = historyManager;
        }

        public IList<CaseHistoryDto> GetForCase(int caseId)
        {
            var histories = _historyManager.GetHistoryForCase(caseId);
            return ObjectMapper.Map<List<CaseHistoryDto>>(histories);
        }

        public IList<CaseHistoryDto> GetForUser(HistoryInputDto input)
        {
            var histories = _historyManager.GetHistoryForUser(input.UserId, input.NumberOfMonth);
            return ObjectMapper.Map<List<CaseHistoryDto>>(histories);
        }
    }
}
