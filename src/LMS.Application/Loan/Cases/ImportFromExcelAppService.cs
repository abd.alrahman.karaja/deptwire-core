﻿using Abp.BackgroundJobs;
using LMS.Storage;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Loan.Cases
{
    public class ImportFromExcelAppService : LMSAppServiceBase, IImportFromExcelAppService
    {
        protected readonly IBinaryObjectManager BinaryObjectManager;
        protected readonly IBackgroundJobManager BackgroundJobManager;
        public ImportFromExcelAppService(
            IBinaryObjectManager binaryObjectManager,
            IBackgroundJobManager backgroundJobManager)
        {
            BinaryObjectManager = binaryObjectManager;
            BackgroundJobManager = backgroundJobManager;
        }

        [HttpPost]
        public async Task SaveFile(int? bankId)
        {
            throw new NotImplementedException();
        }
    }
}
