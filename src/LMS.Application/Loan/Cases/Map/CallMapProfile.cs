﻿using AutoMapper;
using LMS.Loan.Cases.Calls;
using LMS.Loan.Cases.Dto.Calls;

namespace LMS.Loan.Cases.Map
{
    public class CallMapProfile : Profile
    {
        public CallMapProfile()
        {
            CreateMap<Call, CallDto>();
            CreateMap<Call, ReadCallDto>();
            CreateMap<CreateCallDto, Call>();
            CreateMap<UpdateCallDto, Call>();
            CreateMap<Call, UpdateCallDto>();
        }
    }
}
