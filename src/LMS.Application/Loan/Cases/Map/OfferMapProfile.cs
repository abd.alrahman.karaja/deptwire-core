﻿using AutoMapper;
using LMS.Loan.Cases.Dto.Offers;
using LMS.Loan.Cases.Offers;

namespace LMS.Loan.Cases.Map
{
    public class OfferMapProfile : Profile
    {
        public OfferMapProfile()
        {
            CreateMap<Offer, OfferDto>();
            CreateMap<Offer, ReadOfferDto>();
            CreateMap<CreateOfferDto, Offer>();
            CreateMap<UpdateOfferDto, Offer>();
            CreateMap<Offer, UpdateOfferDto>();
            CreateMap<Offer, OfferDashboardDto>()
                .ForMember(x => x.CasesIds, member => member.Ignore());
        }
    }
}
