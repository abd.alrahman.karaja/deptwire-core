﻿using AutoMapper;
using LMS.Loan.Cases.CaseUploadFiles;

namespace LMS.Loan.Cases.Map
{
    public class CaseUploadFileMapProfile : Profile
    {
        public CaseUploadFileMapProfile()
        {
            CreateMap<CaseUploadFile, CaseUploadFile>();
        }
    }
}
