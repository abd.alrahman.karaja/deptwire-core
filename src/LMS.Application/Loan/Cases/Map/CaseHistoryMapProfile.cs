﻿using AutoMapper;
using LMS.Loan.Cases.Dto.Histories;
using LMS.Loan.Cases.History;

namespace LMS.Loan.Cases.Map
{
    public class CaseHistoryMapProfile : Profile
    {
        public CaseHistoryMapProfile()
        {
            CreateMap<CaseHistory, CaseHistoryDto>();
        }
    }
}
