﻿using AutoMapper;
using LMS.Loan.Cases.Dto.Notes;
using LMS.Loan.Cases.Notes;

namespace LMS.Loan.Cases.Map
{
    public class NoteMapProfile : Profile
    {
        public NoteMapProfile()
        {
            CreateMap<Note, NoteDto>();
            CreateMap<CaseNote, CaseNoteDto>();
            CreateMap<CreateNoteDto, Note>();
        }
    }
}
