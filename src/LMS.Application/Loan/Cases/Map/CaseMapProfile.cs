﻿using AutoMapper;
using LMS.Loan.Cases.Dto;
using LMS.Loan.Cases.Dto.ImportCases;
using LMS.Loan.Enums;
using System;

namespace LMS.Loan.Cases.Map
{
    public class CaseMapProfile : Profile
    {
        public CaseMapProfile()
        {
            CreateMap<Case, CaseDto>();
            CreateMap<Case, ViewCaseDto>();
            CreateMap<UpdateCaseDto, Case>();
            CreateMap<Case, UpdateCaseDto>();
            CreateMap<ImportCaseDto, Case>()
                .ForMember(x => x.Agent, x => x.Ignore())
                .ForMember(x => x.Bank, x => x.Ignore())
                .ForMember(x => x.BankHome, x => x.Ignore())
                .ForMember(x => x.Product, x => x.Ignore())
                .ForMember(x => x.PoliceCaseStatus, x => x.Ignore())
                .ForMember(x => x.Customer, x => x.Ignore());

            
            CreateMap<Case, ReadCaseDto>();
        }

        private T MapToEnum<T>(string value,T defaultValue)
        {
            try
            {
                return (T)Enum.Parse(typeof(T), value, true);
            }
            catch
            {
                return defaultValue;
            }
        }
    }
}
