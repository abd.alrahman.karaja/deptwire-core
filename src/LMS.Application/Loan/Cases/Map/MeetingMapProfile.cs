﻿using AutoMapper;
using LMS.Loan.Cases.Dto.Meetings;
using LMS.Loan.Cases.Meetings;

namespace LMS.Loan.Cases.Map
{
    public class MeetingMapProfile : Profile
    {
        public MeetingMapProfile()
        {
            CreateMap<Meeting, MeetingDto>();
            CreateMap<Meeting, ReadMeetingDto>();
            CreateMap<CreateMeetingDto, Meeting>();
            CreateMap<UpdateMeetingDto, Meeting>();
            CreateMap<Meeting, UpdateMeetingDto>();
        }
    }
}
