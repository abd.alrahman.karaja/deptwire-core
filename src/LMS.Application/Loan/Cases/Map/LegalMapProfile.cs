﻿using AutoMapper;
using LMS.Loan.Cases.Dto.Legals;
using LMS.Loan.Cases.Legals;

namespace LMS.Loan.Cases.Map
{
    public class LegalMapProfile : Profile
    {
        public LegalMapProfile()
        {
            CreateMap<Legal, LegalDto>();
            CreateMap<CaseLegal, CaseLegalDto>();
            CreateMap<CreateLegalDto, Legal>();
            CreateMap<ChangeLegalStatusDto, Legal>();
            CreateMap<Legal, ChangeLegalStatusDto>();
        }
    }
}
