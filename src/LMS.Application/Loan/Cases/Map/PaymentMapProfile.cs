﻿using AutoMapper;
using LMS.Loan.Cases.Dto.Payments;
using LMS.Loan.Cases.Payments;

namespace LMS.Loan.Cases.Map
{
    public class PaymentMapProfile : Profile
    {
        public PaymentMapProfile()
        {
            CreateMap<Payment, PaymentDto>();
            CreateMap<Payment, ReadPaymentDto>();
            CreateMap<CreatePaymentDto, Payment>();
            CreateMap<UpdatePaymentDto, Payment>();
            CreateMap<Payment, UpdatePaymentDto>();
        }
    }
}
