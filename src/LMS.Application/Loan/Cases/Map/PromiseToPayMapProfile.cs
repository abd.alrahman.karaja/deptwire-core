﻿using AutoMapper;
using LMS.Loan.Cases.Dto.PTP;
using LMS.Loan.Cases.PTP;

namespace LMS.Loan.Cases.Map
{
    public class PromiseToPayMapProfile : Profile
    {
        public PromiseToPayMapProfile()
        {
            CreateMap<PromiseToPay, PromiseToPayDto>();
            CreateMap<PromiseToPay, ReadPromiseToPayDto>();
            CreateMap<CreatePromiseToPayDto, PromiseToPay>();
            CreateMap<UpdatePromiseToPayDto, PromiseToPay>();
            CreateMap<PromiseToPay, UpdatePromiseToPayDto>();

            CreateMap<PromiseToPayPayment, PromiseToPayPaymentDto>();
            CreateMap<PromiseToPayPayment, ReadPromiseToPayPaymentDto>();
            CreateMap<CreatePromiseToPayPaymentDto, PromiseToPayPayment>();
            CreateMap<UpdatePromiseToPayPaymentDto, PromiseToPayPayment>();
            CreateMap<PromiseToPayPayment, UpdatePromiseToPayPaymentDto>();
        }
    }
}
