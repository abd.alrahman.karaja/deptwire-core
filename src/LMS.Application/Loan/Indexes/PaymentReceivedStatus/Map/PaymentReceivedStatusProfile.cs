﻿using AutoMapper;
using LMS.Loan.Indexes.Dto;
using LMS.Loan.Indexes.Shared.Dto;

namespace LMS.Loan.Indexes.PaymentsReceivedStatus.Map
{
    public class PaymentReceivedStatusProfile : Profile
    {
        public PaymentReceivedStatusProfile()
        {
            CreateMap<PaymentReceivedStatus, IndexDto>();
            CreateMap<PaymentReceivedStatus, ReadIndexDto>();
            CreateMap<PaymentReceivedStatus, UpdateIndexDto>();
            CreateMap<PaymentReceivedStatus, IndexForDropdownDto>();
            CreateMap<IndexDto, PaymentReceivedStatus>();
            CreateMap<CreateIndexDto, PaymentReceivedStatus>();
            CreateMap<UpdateIndexDto, PaymentReceivedStatus>();
        }
    }
}
