﻿using AutoMapper;
using LMS.Loan.Indexes.Dto;
using LMS.Loan.Indexes.Shared.Dto;

namespace LMS.Loan.Indexes.TransferredsStatus.Map
{
    public class TransferredStatusProfile : Profile
    {
        public TransferredStatusProfile()
        {
            CreateMap<TransferredStatus, IndexDto>();
            CreateMap<TransferredStatus, ReadIndexDto>();
            CreateMap<TransferredStatus, UpdateIndexDto>();
            CreateMap<TransferredStatus, IndexForDropdownDto>();
            CreateMap<IndexDto, TransferredStatus>();
            CreateMap<CreateIndexDto, TransferredStatus>();
            CreateMap<UpdateIndexDto, TransferredStatus>();
        }
    }
}
