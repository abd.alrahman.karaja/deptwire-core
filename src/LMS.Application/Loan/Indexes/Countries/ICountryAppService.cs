﻿
using LMS.Loan.Indexes.Dto;

namespace LMS.Loan.Indexes.Countries
{
    public interface ICountryAppService : IIndexAppService
    {
        void Test(IndexDto indexDto);
    }
}
