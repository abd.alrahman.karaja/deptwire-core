﻿using AutoMapper;
using LMS.Loan.Indexes.Dto;
using LMS.Loan.Indexes.Shared.Dto;

namespace LMS.Loan.Indexes.PoliceCaseStatuses.Map
{
    public class PoliceCaseStatuseMapProfile : Profile
    {
        public PoliceCaseStatuseMapProfile()
        {
            CreateMap<PoliceCaseStatus, IndexDto>();
            CreateMap<PoliceCaseStatus, ReadIndexDto>();
            CreateMap<PoliceCaseStatus, UpdateIndexDto>();
            CreateMap<PoliceCaseStatus, IndexForDropdownDto>();
            CreateMap<IndexDto, PoliceCaseStatus>();
            CreateMap<CreateIndexDto, PoliceCaseStatus>();
            CreateMap<UpdateIndexDto, PoliceCaseStatus>();
        }
    }
}
