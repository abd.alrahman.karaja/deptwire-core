﻿using AutoMapper;
using LMS.Loan.Indexes.Dto;
using LMS.Loan.Indexes.Shared.Dto;

namespace LMS.Loan.Indexes.Nationalities.Map
{
    public class NationalityProfile : Profile
    {
        public NationalityProfile()
        {
            CreateMap<Nationality, IndexDto>();
            CreateMap<Nationality, ReadIndexDto>();
            CreateMap<Nationality, UpdateIndexDto>();
            CreateMap<Nationality, IndexForDropdownDto>();
            CreateMap<IndexDto, Nationality>();
            CreateMap<CreateIndexDto, Nationality>();
            CreateMap<UpdateIndexDto, Nationality>();
        }
    }
}
