﻿using Abp.Domain.Repositories;
using Abp.UI;
using LMS.Crud.Dto;
using LMS.Loan.Indexes.Dto;
using LMS.Loan.Indexes.FileCategories;
using LMS.Loan.Indexes.Shared.Dto;
using Microsoft.AspNetCore.Mvc;
using Syncfusion.EJ2.Base;
using Syncfusion.EJ2.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Loan.Indexes.FileCategories
{
    public class FileCategoryAppService : LMSAppServiceBase, IFileCategoryAppService
    { 
        private readonly IRepository<FileCategory> _repository;
        public FileCategoryAppService(IRepository<FileCategory> repository)
        {
            _repository = repository;
        }
        public async Task<IList<IndexDto>> GetAllAsync()
        {
            var fileCategories = await _repository.GetAllListAsync();
            return ObjectMapper.Map<List<IndexDto>>(fileCategories);
        }
        public async Task<IList<IndexForDropdownDto>> GetForDropdown()
        {
            IEnumerable<FileCategory> allStatus = await _repository.GetAllListAsync();
            allStatus = allStatus.OrderBy(x => x.Order);
            return ObjectMapper.Map<List<IndexForDropdownDto>>(allStatus);
        }
        [HttpPost]
        public async Task<ReadGrudDto> GetForGrid([FromBody] DataManagerRequest dm)
        {
            var data = await _repository.GetAllListAsync();
            IEnumerable<ReadIndexDto> fileCategories = ObjectMapper.Map<List<ReadIndexDto>>(data);

            var operations = new DataOperations();
            if (dm.Where != null && dm.Where.Count > 0)
            {
                fileCategories = operations.PerformFiltering(fileCategories, dm.Where, "and");
            }

            if (dm.Sorted != null && dm.Sorted.Count > 0)
            {
                fileCategories = operations.PerformSorting(fileCategories, dm.Sorted);
            }

            IEnumerable groupDs = new List<ReadIndexDto>();
            if (dm.Group != null)
            {
                groupDs = operations.PerformSelect(fileCategories, dm.Group);
            }

            var count = fileCategories.Count();

            if (dm.Skip != 0)
            {
                fileCategories = operations.PerformSkip(fileCategories, dm.Skip);
            }

            if (dm.Take != 0)
            {
                fileCategories = operations.PerformTake(fileCategories, dm.Take);
            }

            return new ReadGrudDto() { result = fileCategories, count = count, groupDs = groupDs };
        }
        public async Task<IndexDto> GetById(int id)
        {
            var fileCategory = await _repository.GetAsync(id);
            return ObjectMapper.Map<IndexDto>(fileCategory);
        }
        public async Task<UpdateIndexDto> GetForEdit(int id)
        {
            var fileCategory = await _repository.GetAsync(id);
            return ObjectMapper.Map<UpdateIndexDto>(fileCategory);
        }
        public async Task<IndexDto> CreateAsync(CreateIndexDto input)
        {
            CheckBeforeCreate(input);

            var fileCategory = ObjectMapper.Map<FileCategory>(input);

            await _repository.InsertAndGetIdAsync(fileCategory);

            return ObjectMapper.Map<IndexDto>(fileCategory);
        }
        public async Task<IndexDto> UpdateAsync(UpdateIndexDto input)
        {
            CheckBeforeUpdate(input);

            var fileCategory = await _repository.GetAsync(input.Id);

            ObjectMapper.Map<UpdateIndexDto, FileCategory>(input, fileCategory);

            var updatedFileCategory = await _repository.UpdateAsync(fileCategory);

            return ObjectMapper.Map<IndexDto>(updatedFileCategory);
        }
        public async Task DeleteAsync(int id)
        {
            var fileCategory = await _repository.GetAsync(id);
            await _repository.DeleteAsync(fileCategory);
        }

        #region Helper methods
        private void CheckBeforeCreate(CreateIndexDto input)
        {
            var validationResultMessage = string.Empty;

            if (_repository.FirstOrDefault(x => x.Name == input.Name) != null)
            {
                validationResultMessage = L(ValidationResultMessage.NameAleadyExist);
            }
            
            if (!string.IsNullOrEmpty(validationResultMessage))
                throw new UserFriendlyException(validationResultMessage);
        }
        private void CheckBeforeUpdate(UpdateIndexDto input)
        {
            var validationResultMessage = string.Empty;

            if (_repository.FirstOrDefault(x => x.Name == input.Name && x.Id != input.Id) != null)
            {
                validationResultMessage = L(ValidationResultMessage.NameAleadyExist);
            }

            if (!string.IsNullOrEmpty(validationResultMessage))
                throw new UserFriendlyException(validationResultMessage);
        }

        public void Test(IndexDto indexDto)
        {
            throw new UserFriendlyException(L(ValidationResultMessage.EmailAddressAleadyExist));
        }
        #endregion
    }
}
