﻿
using LMS.Loan.Indexes.Dto;

namespace LMS.Loan.Indexes.FileCategories
{
    public interface IFileCategoryAppService : IIndexAppService
    {
        void Test(IndexDto indexDto);
    }
}
