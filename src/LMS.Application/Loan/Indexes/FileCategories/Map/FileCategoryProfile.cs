﻿using AutoMapper;
using LMS.Loan.Indexes.Dto;
using LMS.Loan.Indexes.Shared.Dto;

namespace LMS.Loan.Indexes.FileCategories.Map
{
    public class FileCategoryProfile : Profile
    {
        public FileCategoryProfile()
        {
            CreateMap<FileCategory, IndexDto>();
            CreateMap<FileCategory, ReadIndexDto>();
            CreateMap<FileCategory, UpdateIndexDto>();
            CreateMap<FileCategory, IndexForDropdownDto>();
            CreateMap<IndexDto, FileCategory>();
            CreateMap<CreateIndexDto, FileCategory>();
            CreateMap<UpdateIndexDto, FileCategory>();
        }
    }
}
