﻿using AutoMapper;
using LMS.Loan.Indexes.Dto;
using LMS.Loan.Indexes.Shared.Dto;

namespace LMS.Loan.Indexes.LegalsStatus.Map
{
    public class PaymentClaimedStatusProfile : Profile
    {
        public PaymentClaimedStatusProfile()
        {
            CreateMap<PaymentClaimedStatus, IndexDto>();
            CreateMap<PaymentClaimedStatus, ReadIndexDto>();
            CreateMap<PaymentClaimedStatus, UpdateIndexDto>();
            CreateMap<PaymentClaimedStatus, IndexForDropdownDto>();
            CreateMap<IndexDto, PaymentClaimedStatus>();
            CreateMap<CreateIndexDto, PaymentClaimedStatus>();
            CreateMap<UpdateIndexDto, PaymentClaimedStatus>();
        }
    }
}
