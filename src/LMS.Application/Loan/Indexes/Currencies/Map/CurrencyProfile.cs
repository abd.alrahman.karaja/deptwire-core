﻿using AutoMapper;
using LMS.Loan.Currencies.Dto;
using LMS.Loan.Currencies.Shared.Dto;
using LMS.Loan.Indexes;

namespace LMS.Loan.Currencies.Currencies.Map
{
    public class CurrencyProfile : Profile
    {
        public CurrencyProfile()
        {
            CreateMap<Currency, CurrencyDto>();
            CreateMap<Currency, ReadCurrencyDto>();
            CreateMap<Currency, UpdateCurrencyDto>();
            CreateMap<Currency, CurrencyForDropdownDto>();
            CreateMap<CurrencyDto, Currency>();
            CreateMap<CreateCurrencyDto, Currency>();
            CreateMap<UpdateCurrencyDto, Currency>();
        }
    }
}
