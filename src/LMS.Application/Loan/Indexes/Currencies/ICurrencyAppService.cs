﻿using Abp.Application.Services;
using LMS.Crud.Dto;
using LMS.Loan.Currencies.Dto;
using LMS.Loan.Currencies.Shared.Dto;
using Microsoft.AspNetCore.Mvc;
using Syncfusion.EJ2.Base;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LMS.Loan.Currencies
{
    public interface ICurrencyAppService : IApplicationService
    {
        Task<IList<CurrencyDto>> GetAllAsync();
        Task<IList<CurrencyForDropdownDto>> GetForDropdown();
        Task<ReadGrudDto> GetForGrid([FromBody] DataManagerRequest dm);
        Task<CurrencyDto> GetById(int id);
        Task<UpdateCurrencyDto> GetForEdit(int id);
        Task<CurrencyDto> CreateAsync(CreateCurrencyDto input);
        Task<CurrencyDto> UpdateAsync(UpdateCurrencyDto input);
        Task DeleteAsync(int id);

    }
}
