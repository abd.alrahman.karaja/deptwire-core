﻿using Abp;
using Abp.Domain.Repositories;
using Abp.UI;
using LMS.Crud.Dto;
using LMS.Loan.Currencies.Dto;
using LMS.Loan.Currencies.Shared.Dto;
using LMS.Loan.Indexes;
using Microsoft.AspNetCore.Mvc;
using Syncfusion.EJ2.Base;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Loan.Currencies.Currencies
{
    public class CurrencyAppService : AbpServiceBase, ICurrencyAppService
    {
        private readonly IRepository<Currency> _repository;
        public CurrencyAppService(IRepository<Currency> repository)
        {
            _repository = repository;
        }
        public async Task<IList<CurrencyDto>> GetAllAsync()
        {
            var currencies = await _repository.GetAllListAsync();
            return ObjectMapper.Map<List<CurrencyDto>>(currencies);
        }
        public async Task<IList<CurrencyForDropdownDto>> GetForDropdown()
        {
            IEnumerable<Currency> allStatus = await _repository.GetAllListAsync();
            allStatus = allStatus.OrderBy(x => x.Order);
            return ObjectMapper.Map<List<CurrencyForDropdownDto>>(allStatus);
        }
        [HttpPost]
        public async Task<ReadGrudDto> GetForGrid([FromBody] DataManagerRequest dm)
        {
            var data = await _repository.GetAllListAsync();
            IEnumerable<ReadCurrencyDto> currencies = ObjectMapper.Map<List<ReadCurrencyDto>>(data);

            var operations = new DataOperations();
            if (dm.Where != null && dm.Where.Count > 0)
            {
                currencies = operations.PerformFiltering(currencies, dm.Where, "and");
            }

            if (dm.Sorted != null && dm.Sorted.Count > 0)
            {
                currencies = operations.PerformSorting(currencies, dm.Sorted);
            }

            var count = currencies.Count();

            if (dm.Skip != 0)
            {
                currencies = operations.PerformSkip(currencies, dm.Skip);
            }

            if (dm.Take != 0)
            {
                currencies = operations.PerformTake(currencies, dm.Take);
            }

            return new ReadGrudDto() { result = currencies, count = count };
        }
        public async Task<UpdateCurrencyDto> GetForEdit(int id)
        {
            var currency = await _repository.GetAsync(id);
            var currencyDto = ObjectMapper.Map<UpdateCurrencyDto>(currency);
            return currencyDto;
        }
        public async Task<CurrencyDto> GetById(int id)
        {
            var currency = await _repository.GetAsync(id);
            return ObjectMapper.Map<CurrencyDto>(currency);
        }
        public async Task<CurrencyDto> CreateAsync(CreateCurrencyDto input)
        {
            CheckBeforeCreate(input);
            var currency = ObjectMapper.Map<Currency>(input);

            await _repository.InsertAndGetIdAsync(currency);

            return ObjectMapper.Map<CurrencyDto>(currency);
        }
        public async Task<CurrencyDto> UpdateAsync(UpdateCurrencyDto input)
        {
            CheckBeforeUpdate(input);
            var currency = await _repository.GetAsync(input.Id);
            ObjectMapper.Map<UpdateCurrencyDto, Currency>(input, currency);

            var updatedCurrency = await _repository.UpdateAsync(currency);

            return ObjectMapper.Map<CurrencyDto>(updatedCurrency);
        }
        public async Task DeleteAsync(int id)
        {
            var currency = await _repository.GetAsync(id);
            await _repository.DeleteAsync(currency);
        }

        #region Helper methods
        private void CheckBeforeCreate(CreateCurrencyDto input)
        {
            var validationResultMessage = string.Empty;

            if (_repository.FirstOrDefault(x => x.Name == input.Name) != null)
            {
                validationResultMessage = $"{L(ValidationResultMessage.NameAleadyExist)} \n";
            }

            if (!string.IsNullOrEmpty(validationResultMessage))
                throw new UserFriendlyException(validationResultMessage);
        }
        private void CheckBeforeUpdate(UpdateCurrencyDto input)
        {
            var validationResultMessage = string.Empty;

            if (_repository.FirstOrDefault(x => x.Name == input.Name && x.Id != input.Id) != null)
            {
                validationResultMessage = $"{L(ValidationResultMessage.NameAleadyExist)} \n";
            }

            if (!string.IsNullOrEmpty(validationResultMessage))
                throw new UserFriendlyException(validationResultMessage);
        }
        #endregion
    }
}
