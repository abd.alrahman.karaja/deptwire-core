﻿using LMS.Loan.Indexes.Shared.Dto;

namespace LMS.Loan.Currencies.Shared.Dto
{
    public class ReadCurrencyDto : ReadIndexDto
    {
        public string code { get; set; }
    }
}
