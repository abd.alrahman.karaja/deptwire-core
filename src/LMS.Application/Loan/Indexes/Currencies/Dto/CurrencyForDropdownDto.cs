﻿using LMS.Loan.Indexes.Shared.Dto;

namespace LMS.Loan.Currencies.Shared.Dto
{
    public class CurrencyForDropdownDto : IndexForDropdownDto
    {
        public string Code { get; set; }
    }
}
