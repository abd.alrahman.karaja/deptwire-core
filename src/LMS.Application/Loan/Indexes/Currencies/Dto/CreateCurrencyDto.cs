﻿using LMS.Loan.Indexes.Dto;

namespace LMS.Loan.Currencies.Dto
{
    public class CreateCurrencyDto: CreateIndexDto
    {
        public string Code { get; set; }
    }

}
