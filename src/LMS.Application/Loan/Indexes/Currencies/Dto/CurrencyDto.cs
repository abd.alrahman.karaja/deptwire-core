﻿using LMS.Loan.Indexes.Dto;

namespace LMS.Loan.Currencies.Dto
{
    public class CurrencyDto : IndexDto
    {
        public string Code { get; set; }
        
    }
}
