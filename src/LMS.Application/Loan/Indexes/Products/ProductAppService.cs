﻿using Abp;
using Abp.Domain.Repositories;
using Abp.UI;
using LMS.Crud.Dto;
using LMS.Loan.Indexes.Dto;
using LMS.Loan.Indexes.Nationalities;
using LMS.Loan.Indexes.Shared.Dto;
using Microsoft.AspNetCore.Mvc;
using Syncfusion.EJ2.Base;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Loan.Indexes.Products
{
    public class ProductAppService : AbpServiceBase, IProductAppService
    {
        private readonly IRepository<Product> _repository;
        public ProductAppService(IRepository<Product> repository)
        {
            _repository = repository;
        }
        public async Task<IList<IndexDto>> GetAllAsync()
        {
            var products = await _repository.GetAllListAsync();
            return ObjectMapper.Map<List<IndexDto>>(products);
        }
        public async Task<IList<IndexForDropdownDto>> GetForDropdown()
        {
            IEnumerable<Product> allStatus = await _repository.GetAllListAsync();
            allStatus = allStatus.OrderBy(x => x.Order);
            return ObjectMapper.Map<List<IndexForDropdownDto>>(allStatus);
        }
        [HttpPost]
        public async Task<ReadGrudDto> GetForGrid([FromBody] DataManagerRequest dm)
        {
            var data = await _repository.GetAllListAsync();
            IEnumerable<ReadIndexDto> products = ObjectMapper.Map<List<ReadIndexDto>>(data);

            var operations = new DataOperations();
            if (dm.Where != null && dm.Where.Count > 0)
            {
                products = operations.PerformFiltering(products, dm.Where, "and");
            }

            if (dm.Sorted != null && dm.Sorted.Count > 0)
            {
                products = operations.PerformSorting(products, dm.Sorted);
            }

            var count = products.Count();

            if (dm.Skip != 0)
            {
                products = operations.PerformSkip(products, dm.Skip);
            }

            if (dm.Take != 0)
            {
                products = operations.PerformTake(products, dm.Take);
            }

            return new ReadGrudDto() { result = products, count = count };
        }
        public async Task<UpdateIndexDto> GetForEdit(int id)
        {
            var product = await _repository.GetAsync(id);
            var productDto = ObjectMapper.Map<UpdateIndexDto>(product);
            return productDto;
        }
        public async Task<IndexDto> GetById(int id)
        {
            var product = await _repository.GetAsync(id);
            return ObjectMapper.Map<IndexDto>(product);
        }
        public async Task<IndexDto> CreateAsync(CreateIndexDto input)
        {
            CheckBeforeCreate(input);

            var product = ObjectMapper.Map<Product>(input);

            await _repository.InsertAndGetIdAsync(product);

            return ObjectMapper.Map<IndexDto>(product);
        }
        public async Task<IndexDto> UpdateAsync(UpdateIndexDto input)
        {
            CheckBeforeUpdate(input);
            var product = await _repository.GetAsync(input.Id);
            ObjectMapper.Map<UpdateIndexDto, Product>(input, product);

            var updatedProduct = await _repository.UpdateAsync(product);

            return ObjectMapper.Map<IndexDto>(updatedProduct);
        }
        public async Task DeleteAsync(int id)
        {
            var product = await _repository.GetAsync(id);
            await _repository.DeleteAsync(product);
        }

        #region Helper methods
        private void CheckBeforeCreate(CreateIndexDto input)
        {
            var validationResultMessage = string.Empty;

            if (_repository.FirstOrDefault(x => x.Name == input.Name) != null)
            {
                validationResultMessage = $"{L(ValidationResultMessage.NameAleadyExist)} \n";
            }

            if (!string.IsNullOrEmpty(validationResultMessage))
                throw new UserFriendlyException(validationResultMessage);
        }
        private void CheckBeforeUpdate(UpdateIndexDto input)
        {
            var validationResultMessage = string.Empty;

            if (_repository.FirstOrDefault(x => x.Name == input.Name && x.Id != input.Id) != null)
            {
                validationResultMessage = $"{L(ValidationResultMessage.NameAleadyExist)} \n";
            }

            if (!string.IsNullOrEmpty(validationResultMessage))
                throw new UserFriendlyException(validationResultMessage);
        }
        #endregion
    }
}
