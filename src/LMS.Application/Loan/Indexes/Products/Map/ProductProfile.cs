﻿using AutoMapper;
using LMS.Loan.Indexes.Dto;
using LMS.Loan.Indexes.Shared.Dto;

namespace LMS.Loan.Indexes.Products.Map
{
    public class ProductProfile: Profile
    {
        public ProductProfile()
        {
            CreateMap<Product, IndexDto>();
            CreateMap<Product, ReadIndexDto>();
            CreateMap<Product, UpdateIndexDto>();
            CreateMap<Product, IndexForDropdownDto>();
            CreateMap<IndexDto, Product>();
            CreateMap<CreateIndexDto, Product>();
            CreateMap<UpdateIndexDto, Product>();
        }
    }
}
