﻿using Abp.Application.Services.Dto;

namespace LMS.Loan.Indexes.Shared.Dto
{
    public class IndexForDropdownDto : EntityDto
    {
        public string Name { get; set; }
    }
}
