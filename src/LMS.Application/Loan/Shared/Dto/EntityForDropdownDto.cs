﻿
namespace LMS.Loan.Shared.Dto
{
    public class EntityForDropdownDto
    {
        public int id { get; set; }
        public string name { get; set; }
    }
}
