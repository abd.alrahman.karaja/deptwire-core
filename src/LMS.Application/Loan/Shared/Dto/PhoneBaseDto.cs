﻿using Abp.Application.Services.Dto;

namespace LMS.Loan.Shared.Dto
{
    public class PhoneBaseDto : EntityDto
    {
        public string Number { get; set; }
    }
}
