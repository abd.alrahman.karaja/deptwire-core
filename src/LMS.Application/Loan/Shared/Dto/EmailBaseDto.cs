﻿using Abp.Application.Services.Dto;

namespace LMS.Loan.Shared.Dto
{
    public class EmailBaseDto:EntityDto
    {
        public string EmailAddress { get; set; }
    }
}
