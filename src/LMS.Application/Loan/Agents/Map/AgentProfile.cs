﻿using AutoMapper;
using LMS.Loan.Agents.Dto;

namespace LMS.Loan.Agents.Map
{
    public class AgentProfile : Profile
    {
        public AgentProfile()
        {
            CreateMap<Agent, AgentDto>();
            CreateMap<Agent, ReadAgentDto>();
            CreateMap<Agent, UpdateAgentDto>();
            CreateMap<AgentDto, Agent>();
            CreateMap<CreateAgentDto, Agent>();
            CreateMap<UpdateAgentDto, Agent>();
        }
    }
}
