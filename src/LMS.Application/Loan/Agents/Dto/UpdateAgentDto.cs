﻿using Abp.Application.Services.Dto;

namespace LMS.Loan.Agents.Dto
{
    public class UpdateAgentDto : EntityDto
    {
        public string Name { get; set; }
        public string ShortName { get; set; }
        public int CountryId { get; set; }
    }
}
