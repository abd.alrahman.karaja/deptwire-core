﻿using Abp.Application.Services.Dto;
using LMS.Loan.Enums;
using LMS.Loan.Indexes.Dto;
using System;

namespace LMS.Loan.Agents.Dto
{
    public class AgentDto:EntityDto
    {
        public string Name { get; set; }
        public string ShortName { get; set; }
        public int CountryId { get; set; }
        public IndexDto Country { get; set; }
    }
}
