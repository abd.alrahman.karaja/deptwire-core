﻿namespace LMS.Loan.Agents.Dto
{
    public class ReadAgentDto
    {
        public int id { get; set; }
        public string name { get; set; }
        public string shortName { get; set; }
        public int countryId { get; set; }
        public string countryName { get; set; }
    }
}
