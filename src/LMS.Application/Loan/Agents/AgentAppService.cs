﻿using Abp;
using Abp.Domain.Repositories;
using Abp.UI;
using LMS.Crud.Dto;
using LMS.Loan.Agents;
using LMS.Loan.Agents.Dto;
using Microsoft.AspNetCore.Mvc;
using Syncfusion.EJ2.Base;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Loan.Agents
{
    public class AgentAppService : AbpServiceBase, IAgentAppService
    {
        private readonly IRepository<Agent> _repository;

        public AgentAppService(IRepository<Agent> repository)
        {
            _repository = repository;
        }

        public async Task<IList<AgentDto>> GetAllAsync()
        {
            var agents = await _repository.GetAllListAsync();
            return ObjectMapper.Map<List<AgentDto>>(agents);
        }

        [HttpPost]
        public async Task<ReadGrudDto> GetForGrid([FromBody] DataManagerRequest dm)
        {
            var data = await _repository.GetAllListAsync();
            IEnumerable<AgentDto> agents = ObjectMapper.Map<List<AgentDto>>(data);

            var operations = new DataOperations();
            if (dm.Where != null && dm.Where.Count > 0)
            {
                agents = operations.PerformFiltering(agents, dm.Where, "and");
            }

            if (dm.Sorted != null && dm.Sorted.Count > 0)
            {
                agents = operations.PerformSorting(agents, dm.Sorted);
            }

            var count = agents.Count();

            if (dm.Skip != 0)
            {
                agents = operations.PerformSkip(agents, dm.Skip);
            }

            if (dm.Take != 0)
            {
                agents = operations.PerformTake(agents, dm.Take);
            }

            return new ReadGrudDto() { result = agents, count = count };
        }

        public async Task<UpdateAgentDto> GetForEdit(int id)
        {
            var agent = await _repository.GetAsync(id);
            return ObjectMapper.Map<UpdateAgentDto>(agent);
        }

        public async Task<AgentDto> GetById(int id)
        {
            var agent = await _repository.GetAsync(id);
            return ObjectMapper.Map<AgentDto>(agent);
        }

        public async Task<AgentDto> CreateAsync(CreateAgentDto input)
        {
            CheckBeforeCreate(input);

            var agent = ObjectMapper.Map<Agent>(input);

            await _repository.InsertAndGetIdAsync(agent);

            return ObjectMapper.Map<AgentDto>(agent);
        }

        public async Task<AgentDto> UpdateAsync(UpdateAgentDto input)
        {
            CheckBeforeUpdate(input);
            var agent = await _repository.GetAsync(input.Id);
            ObjectMapper.Map<UpdateAgentDto, Agent>(input, agent);

            var updatedAgent = await _repository.UpdateAsync(agent);

            return ObjectMapper.Map<AgentDto>(updatedAgent);
        }

        public async Task DeleteAsync(int id)
        {
            var agent = await _repository.GetAsync(id);
            await _repository.DeleteAsync(agent);
        }

        #region Helper methods
        private void CheckBeforeCreate(CreateAgentDto input)
        {
            var validationResultMessage = string.Empty;

            if (_repository.FirstOrDefault(x => x.Name == input.Name) != null)
            {
                validationResultMessage = $"{L(ValidationResultMessage.NameAleadyExist)} \n";
            }

            if (_repository.FirstOrDefault(x => x.ShortName == input.ShortName) != null)
            {
                validationResultMessage = $"{L(ValidationResultMessage.ShortNameAleadyExist)} \n";
            }

            if (!string.IsNullOrEmpty(validationResultMessage))
                throw new UserFriendlyException(validationResultMessage);
        }

        private void CheckBeforeUpdate(UpdateAgentDto input)
        {
            var validationResultMessage = string.Empty;

            if (_repository.FirstOrDefault(x => x.Name == input.Name && x.Id != input.Id) != null)
            {
                validationResultMessage = $"{L(ValidationResultMessage.NameAleadyExist)} \n";
            }

            if (_repository.FirstOrDefault(x => x.ShortName == input.ShortName && x.Id != input.Id) != null)
            {
                validationResultMessage = $"{L(ValidationResultMessage.ShortNameAleadyExist)} \n";
            }

            if (!string.IsNullOrEmpty(validationResultMessage))
                throw new UserFriendlyException(validationResultMessage);
        }

        #endregion
    }
}
