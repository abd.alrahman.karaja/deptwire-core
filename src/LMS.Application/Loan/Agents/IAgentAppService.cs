﻿using Abp.Application.Services;
using LMS.Crud.Dto;
using LMS.Loan.Agents.Dto;
using Microsoft.AspNetCore.Mvc;
using Syncfusion.EJ2.Base;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LMS.Loan.Agents
{
    public interface IAgentAppService : IApplicationService
    {
        Task<IList<AgentDto>> GetAllAsync();
        Task<ReadGrudDto> GetForGrid([FromBody] DataManagerRequest dm);
        Task<AgentDto> GetById(int id);
        Task<UpdateAgentDto> GetForEdit(int id);
        Task<AgentDto> CreateAsync(CreateAgentDto input);
        Task<AgentDto> UpdateAsync(UpdateAgentDto input);
        Task DeleteAsync(int id);
    }
}
