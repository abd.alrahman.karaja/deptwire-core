﻿namespace LMS
{
    public class AppConsts
    {
        /// <summary>
        /// Default pass phrase for SimpleStringCipher decrypt/encrypt operations
        /// </summary>
        public const string DefaultPassPhrase = "gsKxGZ012HLL3MI5";
        public const double MaxPageSize = 100000;
        public const int DefaultPageSize = 10;
    }

    public class ValidationResultMessage
    {
        public const string EmployeeNameAleadyExist = "EmployeeNameAleadyExist";
        public const string EmailAddressAleadyExist = "EmailAddressAleadyExist";
        public const string NameAleadyExist = "NameAleadyExist";
        public const string ShortNameAleadyExist = "ShortNameAleadyExist";
        public const string CodeAleadyExist = "CodeAleadyExist";
        public const string YouCanNotAddTheSameStatusForTwoCode = "YouCanNotAddTheSameStatusForTwoCode";
        public const string ThereIsAnotherMeetingAtTheSameTime = "ThereIsAnotherMeetingAtTheSameTime";
        public const string ThereIsAnotherCallAtTheSameTime = "ThereIsAnotherCallAtTheSameTime";
    }
}
