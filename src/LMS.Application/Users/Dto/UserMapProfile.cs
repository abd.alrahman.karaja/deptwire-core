﻿using AutoMapper;
using LMS.Authorization.Users;

namespace LMS.Users.Dto
{
    public class UserMapProfile : Profile
    {
        public UserMapProfile()
        {
            CreateMap<UserDto, User>();
            CreateMap<UserDto, User>()
                .ForMember(x => x.Roles, opt => opt.Ignore())
                .ForMember(x => x.CreationTime, opt => opt.Ignore());

            CreateMap<CreateUserDto, User>();
            CreateMap<CreateUserDto, User>().ForMember(x => x.Roles, opt => opt.Ignore());
            CreateMap<User, ReadUserDto>()
                .ForMember(x => x.teamLeaderName, opt => opt.MapFrom(tl => tl.TeamLeader.FullName));
            CreateMap<User, UserForDropdownDto>();
            CreateMap<User, TeamLeaderDto>()
                .ForMember(x => x.teamLeaderName, opt => opt.MapFrom(tl => tl.TeamLeader.FullName));
            CreateMap<User, ReadUserDto>();
            CreateMap<User, UserMiniDataDto>();
        }
    }
}
