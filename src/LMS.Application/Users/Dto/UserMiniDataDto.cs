﻿using Abp.Application.Services.Dto;

namespace LMS.Users.Dto
{
    public class UserMiniDataDto : EntityDto<long>
    {
        public string UserName { get; set; }
        public string FullName { get; set; }
    }
}
