﻿namespace LMS.Users.Dto
{
    public class TeamLeaderDto
    {
        public long teamLeaderId { get; set; }
        public string teamLeaderName { get; set; }
    }
}
