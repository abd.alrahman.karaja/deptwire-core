﻿using LMS.Authorization.Users;
using Newtonsoft.Json;

namespace LMS.Users.Dto
{
    public class ReadUserDto
    {
        public long id { get; set; }
        public string userName { get; set; }
        public string name { get; set; }
        public string surname { get; set; }
        public string emailAddress { get; set; }
        public bool isActive { get; set; }
        public string fullName { get; set; }
        public string userType { get; set; }
        public long? teamLeaderId { get; set; }
        public string teamLeaderName { get; set; }
    }
}
