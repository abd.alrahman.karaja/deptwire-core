﻿using LMS.Loan.Shared.Dto;

namespace LMS.Users.Dto
{
    public class UserForDropdownDto 
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
    }
}
