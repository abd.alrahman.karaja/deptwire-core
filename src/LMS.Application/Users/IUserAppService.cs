using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using LMS.Crud.Dto;
using LMS.Roles.Dto;
using LMS.Users.Dto;
using Microsoft.AspNetCore.Mvc;
using Syncfusion.EJ2.Base;

namespace LMS.Users
{
    public interface IUserAppService : IAsyncCrudAppService<UserDto, long, PagedUserResultRequestDto, CreateUserDto, UserDto>
    {
        Task<ListResultDto<RoleDto>> GetRoles();
        Task<ReadGrudDto> GetForGrid([FromBody] DataManagerRequest dm);
        Task ChangeLanguage(ChangeUserLanguageDto input);
        Task<bool> ChangePassword(ChangePasswordDto input);
        Task<IList<UserForDropdownDto>> GetUsersForDropdown();
        Task<IList<TeamLeaderDto>> GetTeamLeaders();
        Task<IList<UserMiniDataDto>> GetUsersByDataAccessType(long userId);
    }
}
