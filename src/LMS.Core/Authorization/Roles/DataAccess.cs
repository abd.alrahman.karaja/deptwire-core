﻿namespace LMS.Authorization.Roles
{
    public enum DataAccess
    {
        Me,
        MeAndSubordinates,
        All
    }
}
