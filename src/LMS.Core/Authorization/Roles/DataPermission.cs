﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using LMS.Authorization.Users;
using System.ComponentModel.DataAnnotations.Schema;

namespace LMS.Authorization.Roles
{
    public class DataPermission : FullAuditedEntity, IMayHaveTenant
    {
        public int? TenantId { get; set; }
        public int RoleId { get; set; }
        [ForeignKey("RoleId")]
        public Role Role { get; set; }
        public long UserId { get; set; }
        [ForeignKey("UserId")]
        public User User { get; set; }
    }
}
