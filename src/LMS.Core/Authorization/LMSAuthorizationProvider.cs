﻿using Abp.Authorization;
using Abp.Localization;
using Abp.MultiTenancy;
using System.Collections.Generic;

namespace LMS.Authorization
{
    public class LMSAuthorizationProvider : AuthorizationProvider
    {
        public override void SetPermissions(IPermissionDefinitionContext context)
        {
            //Users
            context.CreatePermission(PermissionNames.Pages_Users,L("Users"));
            context.CreatePermission(PermissionNames.Pages_Users_Create);
            context.CreatePermission(PermissionNames.Pages_Users_Edit, L("EditUser"));
            context.CreatePermission(PermissionNames.Pages_Users_Delete, L("DeleteUser"));
            context.CreatePermission(PermissionNames.Pages_Users_ResetPassword, L("ResetPassword"));
            //Roles
            context.CreatePermission(PermissionNames.Pages_Roles, L("Roles"));
            context.CreatePermission(PermissionNames.Pages_Roles_Create, L("CreateNewRole"));
            context.CreatePermission(PermissionNames.Pages_Roles_Edit, L("EditRole"));
            context.CreatePermission(PermissionNames.Pages_Roles_Delete, L("DeleteRole"));
            //Tenants
            context.CreatePermission(PermissionNames.Pages_Tenants, L("Tenants"), multiTenancySides: MultiTenancySides.Host);
            //Nationalities
            context.CreatePermission(PermissionNames.Pages_Nationalities, L("Nationalities"));
            context.CreatePermission(PermissionNames.Pages_Nationalities_Create, L("CreateNewNationality"));
            context.CreatePermission(PermissionNames.Pages_Nationalities_Edit, L("EditNationality"));
            context.CreatePermission(PermissionNames.Pages_Nationalities_Delete, L("DeleteNationality"));
            //Countries
            context.CreatePermission(PermissionNames.Pages_Countries, L("Countries"));
            context.CreatePermission(PermissionNames.Pages_Countries_Create, L("CreateNewCountry"));
            context.CreatePermission(PermissionNames.Pages_Countries_Edit, L("EditCountry"));
            context.CreatePermission(PermissionNames.Pages_Countries_Delete, L("DeleteCountry"));
            //Case Status
            //context.CreatePermission(PermissionNames.Pages_CaseStatus, L("CaseStatus"));
            //context.CreatePermission(PermissionNames.Pages_CaseStatus_Create, L("CreateNewCaseStatus"));
            //context.CreatePermission(PermissionNames.Pages_CaseStatus_Edit, L("EditCaseStatus"));
            //context.CreatePermission(PermissionNames.Pages_CaseStatus_Delete, L("DeleteCaseStatus"));
            //Employees
            //context.CreatePermission(PermissionNames.Pages_Employees, L("Employees"));
            //context.CreatePermission(PermissionNames.Pages_Employees_Create, L("CreateNewEmployee"));
            //context.CreatePermission(PermissionNames.Pages_Employees_Edit, L("EditEmployee"));
            //context.CreatePermission(PermissionNames.Pages_Employees_Delete, L("DeleteEmployee"));
            //Banks
            context.CreatePermission(PermissionNames.Pages_Banks, L("Banks"));
            context.CreatePermission(PermissionNames.Pages_Banks_Create, L("CreateNewBank"));
            context.CreatePermission(PermissionNames.Pages_Banks_Edit, L("EditBank"));
            context.CreatePermission(PermissionNames.Pages_Banks_Delete, L("DeleteBank"));
            //BankCodes
            context.CreatePermission(PermissionNames.Pages_BankCodes, L("BankCodes"));
            context.CreatePermission(PermissionNames.Pages_BankCodes_Create, L("CreateNewBankCode"));
            context.CreatePermission(PermissionNames.Pages_BankCodes_Edit, L("EditBankCode"));
            context.CreatePermission(PermissionNames.Pages_BankCodes_Delete, L("DeleteBankCode"));
            //Agents
            context.CreatePermission(PermissionNames.Pages_Agents, L("Agents"));
            context.CreatePermission(PermissionNames.Pages_Agents_Create, L("CreateNewAgent"));
            context.CreatePermission(PermissionNames.Pages_Agents_Edit, L("EditAgent"));
            context.CreatePermission(PermissionNames.Pages_Agents_Delete, L("DeleteAgent"));

            //Products
            context.CreatePermission(PermissionNames.Pages_Products, L("Products"));
            context.CreatePermission(PermissionNames.Pages_Products_Create, L("CreateNewProduct"));
            context.CreatePermission(PermissionNames.Pages_Products_Edit, L("EditProduct"));
            context.CreatePermission(PermissionNames.Pages_Products_Delete, L("DeleteProduct"));

            //Currencies
            context.CreatePermission(PermissionNames.Pages_Currencies, L("Currencies"));
            context.CreatePermission(PermissionNames.Pages_Currencies_Create, L("CreateNewCurrency"));
            context.CreatePermission(PermissionNames.Pages_Currencies_Edit, L("EditCurrency"));
            context.CreatePermission(PermissionNames.Pages_Currencies_Delete, L("DeleteCurrency"));

            //PaymentClaimedStatus
            context.CreatePermission(PermissionNames.Pages_PaymentClaimedStatus, L("PaymentClaimedStatus"));
            context.CreatePermission(PermissionNames.Pages_PaymentClaimedStatus_Create, L("CreateNewPaymentClaimedStatus"));
            context.CreatePermission(PermissionNames.Pages_PaymentClaimedStatus_Edit, L("EditPaymentClaimedStatus"));
            context.CreatePermission(PermissionNames.Pages_PaymentClaimedStatus_Delete, L("DeletePaymentClaimedStatus"));

            //TransferredStatus
            context.CreatePermission(PermissionNames.Pages_TransferredStatus, L("TransferredStatus"));
            context.CreatePermission(PermissionNames.Pages_TransferredStatus_Create, L("CreateNewTransferredStatus"));
            context.CreatePermission(PermissionNames.Pages_TransferredStatus_Edit, L("EditTransferredStatus"));
            context.CreatePermission(PermissionNames.Pages_TransferredStatus_Delete, L("DeleteTransferredStatus"));

            //PaymentReceivedStatus
            context.CreatePermission(PermissionNames.Pages_PaymentReceivedStatus, L("PaymentReceivedStatus"));
            context.CreatePermission(PermissionNames.Pages_PaymentReceivedStatus_Create, L("CreateNewPaymentReceivedStatus"));
            context.CreatePermission(PermissionNames.Pages_PaymentReceivedStatus_Edit, L("EditPaymentReceivedStatus"));
            context.CreatePermission(PermissionNames.Pages_PaymentReceivedStatus_Delete, L("DeletePaymentReceivedStatus"));


            //Import from excel
            context.CreatePermission(PermissionNames.Pages_ImportFromExcel, L("ImportFromExcel"));

            //Cases
            context.CreatePermission(PermissionNames.Pages_Cases, L("Cases"));
            context.CreatePermission(PermissionNames.Pages_Cases_ChangeStatus, L("ChangeCaseStatus"));
            context.CreatePermission(PermissionNames.Pages_Cases_AssignCase, L("AssignCasesToUser"));
            context.CreatePermission(PermissionNames.Pages_Cases_Edit, L("EditCase"));
            context.CreatePermission(PermissionNames.Pages_Cases_EditCustomer, L("EditCustomer"));
            context.CreatePermission(PermissionNames.Pages_Cases_UploadFile, L("UploadFile"));
            context.CreatePermission(PermissionNames.Pages_Cases_SupervisorNote, L("SupervisorNote"));
            context.CreatePermission(PermissionNames.Pages_Cases_NegotiatorNote, L("NegotiatorNote"));
            context.CreatePermission(PermissionNames.Pages_Cases_LegalNote, L("LegalNote"));

            context.CreatePermission(PermissionNames.Pages_Cases_ChangeStatus_UnderTracing, L("ChangeStatusToUnderTracing"));
            context.CreatePermission(PermissionNames.Pages_Cases_ChangeStatus_Retracing, L("ChangeStatusToRetracing"));
            context.CreatePermission(PermissionNames.Pages_Cases_ChangeStatus_ThirdPartyContact, L("ChangeStatusToThirdPartyContact"));
            context.CreatePermission(PermissionNames.Pages_Cases_ChangeStatus_Contacted, L("ChangeStatusToContacted"));
            context.CreatePermission(PermissionNames.Pages_Cases_ChangeStatus_UnderNegotiation, L("ChangeStatusToUnderNegotiation"));
            context.CreatePermission(PermissionNames.Pages_Cases_ChangeStatus_Offer, L("ChangeStatusToOffer"));
            context.CreatePermission(PermissionNames.Pages_Cases_ChangeStatus_Approval, L("ChangeStatusToApproval"));
            context.CreatePermission(PermissionNames.Pages_Cases_ChangeStatus_Declined, L("ChangeStatusToDeclined"));
            context.CreatePermission(PermissionNames.Pages_Cases_ChangeStatus_PTP, L("ChangeStatusToPTP"));
            context.CreatePermission(PermissionNames.Pages_Cases_ChangeStatus_Paid, L("ChangeStatusToPaid"));
            context.CreatePermission(PermissionNames.Pages_Cases_ChangeStatus_Settled, L("ChangeStatusToSettled"));
            context.CreatePermission(PermissionNames.Pages_Cases_ChangeStatus_AlreadySettled, L("ChangeStatusToAlreadySettled"));
            context.CreatePermission(PermissionNames.Pages_Cases_ChangeStatus_BrokenPromise, L("ChangeStatusToBrokenPromise"));
            context.CreatePermission(PermissionNames.Pages_Cases_ChangeStatus_RefusedToPay, L("ChangeStatusToRefusedToPay"));
            context.CreatePermission(PermissionNames.Pages_Cases_ChangeStatus_Dead, L("ChangeStatusToDead"));
            context.CreatePermission(PermissionNames.Pages_Cases_ChangeStatus_ClosedAndReturned, L("ChangeStatusToClosedAndReturned"));


            //Meetings
            context.CreatePermission(PermissionNames.Pages_Meetings, L("Meetings"));
            context.CreatePermission(PermissionNames.Pages_Meetings_Create, L("CreateNewMeeting"));
            context.CreatePermission(PermissionNames.Pages_Meetings_Edit, L("EditMeeting"));
            context.CreatePermission(PermissionNames.Pages_Meetings_Delete, L("DeleteMeeting"));

            //Legals
            context.CreatePermission(PermissionNames.Pages_Legals, L("Legals"));
            context.CreatePermission(PermissionNames.Pages_Legals_Create, L("CreateNewLawsuit"));
            context.CreatePermission(PermissionNames.Pages_Legals_ChangeLegalStatus, L("ChangeLawsuitStatus"));

            //Offers
            context.CreatePermission(PermissionNames.Pages_Offers, L("Offers"));
            context.CreatePermission(PermissionNames.Pages_Offers_Create, L("CreateNewOffer"));
            context.CreatePermission(PermissionNames.Pages_Offers_Edit, L("EditOffer"));
            context.CreatePermission(PermissionNames.Pages_Offers_Delete, L("DeleteOffer"));
            context.CreatePermission(PermissionNames.Pages_Offers_ChangeStatus, L("ChangeOfferStatus"));

            //Payments
            context.CreatePermission(PermissionNames.Pages_Payments, L("Payments"));
            context.CreatePermission(PermissionNames.Pages_Payments_Create, L("CreateNewPayment"));
            context.CreatePermission(PermissionNames.Pages_Payments_Edit, L("EditPayment"));
            context.CreatePermission(PermissionNames.Pages_Payments_Delete, L("DeletePayment"));

            //Calls
            context.CreatePermission(PermissionNames.Pages_Calls, L("Calls"));
            context.CreatePermission(PermissionNames.Pages_Calls_Create, L("CreateNewCall"));
            context.CreatePermission(PermissionNames.Pages_Calls_Edit, L("EditCall"));
            context.CreatePermission(PermissionNames.Pages_Calls_Delete, L("DeleteCall"));

            //PTPs
            context.CreatePermission(PermissionNames.Pages_PTPs, L("PTPs"));
            context.CreatePermission(PermissionNames.Pages_PTPs_Create, L("CreateNewPTP"));
            context.CreatePermission(PermissionNames.Pages_PTPs_Edit, L("EditPTP"));
            context.CreatePermission(PermissionNames.Pages_PTPs_Delete, L("DeletePTP"));

            //Customers
            context.CreatePermission(PermissionNames.Pages_Customers_Edit, L("EditCustomer"));

            //Dashboard
            context.CreatePermission(PermissionNames.Pages_Dashboard_Admin, L("AdminDashboard"));
            context.CreatePermission(PermissionNames.Pages_Dashboard_Negotiator, L("NegotiatorDashboard"));
            context.CreatePermission(PermissionNames.Pages_Dashboard_NegotiatorTeamLeader, L("NegotiatorTeamLeaderDashboard"));
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, LMSConsts.LocalizationSourceName);
        }
    }
}
