﻿
namespace LMS.Authorization.Users
{
    public enum UserType
    {
        None,
        Coordinator,
        Mis,
        NegotiatorTeamLeader,
        Negotiator
    }
}
