﻿namespace LMS.Authorization
{
    public static class PermissionNames
    {
        public const string Pages_Tenants = "Pages.Tenants";
        public const string Pages_Administration_Users = "Pages.Administration.Users";
        public const string Pages_Administration_Users_ChangePermissions = "Pages.Administration.Users.ChangePermissions";
        public const string Pages_Administration_Roles_Edit = "Pages.Administration.Roles.Edit";
        //Users
        public const string Pages_Users = "Pages.Users";
        public const string Pages_Users_Create = "Pages.Users.Create";
        public const string Pages_Users_Edit = "Pages.Users.Edit";
        public const string Pages_Users_Delete = "Pages.Users.Delete";
        public const string Pages_Users_ResetPassword = "Pages.Users.ResetPassword";
        //Roles
        public const string Pages_Roles = "Pages.Roles";
        public const string Pages_Roles_Create = "Pages.Roles.Create";
        public const string Pages_Roles_Edit = "Pages.Roles.Edit";
        public const string Pages_Roles_Delete = "Pages.Roles.Delete";
        //Nationalities
        public const string Pages_Nationalities = "Pages.Nationalities";
        public const string Pages_Nationalities_Create = "Pages.Nationalities.Create";
        public const string Pages_Nationalities_Edit = "Pages.Nationalities.Edit";
        public const string Pages_Nationalities_Delete = "Pages.Nationalities.Delete";
        //Countries
        public const string Pages_Countries = "Pages.Countries";
        public const string Pages_Countries_Create = "Pages.Countries.Create";
        public const string Pages_Countries_Edit = "Pages.Countries.Edit";
        public const string Pages_Countries_Delete = "Pages.Countries.Delete";
        //Case Status
        public const string Pages_CaseStatus = "Pages.CaseStatus";
        public const string Pages_CaseStatus_Create = "Pages.CaseStatus.Create";
        public const string Pages_CaseStatus_Edit = "Pages.CaseStatus.Edit";
        public const string Pages_CaseStatus_Delete = "Pages.CaseStatus.Delete";
        //Employees
        public const string Pages_Employees = "Pages.Employees";
        public const string Pages_Employees_Create = "Pages.Employees.Create";
        public const string Pages_Employees_Edit = "Pages.Employees.Edit";
        public const string Pages_Employees_Delete = "Pages.Employees.Delete";
        //Banks
        public const string Pages_Banks = "Pages.Banks";
        public const string Pages_Banks_Create = "Pages.Banks.Create";
        public const string Pages_Banks_Edit = "Pages.Banks.Edit";
        public const string Pages_Banks_Delete = "Pages.Banks.Delete";
        //BankCodes
        public const string Pages_BankCodes = "Pages.BankCodes";
        public const string Pages_BankCodes_Create = "Pages.BankCodes.Create";
        public const string Pages_BankCodes_Edit = "Pages.BankCodes.Edit";
        public const string Pages_BankCodes_Delete = "Pages.BankCodes.Delete";
        //Agents
        public const string Pages_Agents = "Pages.Agents";
        public const string Pages_Agents_Create = "Pages.Agents.Create";
        public const string Pages_Agents_Edit = "Pages.Agents.Edit";
        public const string Pages_Agents_Delete = "Pages.Agents.Delete";

        //Products
        public const string Pages_Products = "Pages.Products";
        public const string Pages_Products_Create = "Pages.Products.Create";
        public const string Pages_Products_Edit = "Pages.Products.Edit";
        public const string Pages_Products_Delete = "Pages.Products.Delete";

        //Currencies
        public const string Pages_Currencies = "Pages.Currencies";
        public const string Pages_Currencies_Create = "Pages.Currencies.Create";
        public const string Pages_Currencies_Edit = "Pages.Currencies.Edit";
        public const string Pages_Currencies_Delete = "Pages.Currencies.Delete";

        //TransferredStatus
        public const string Pages_TransferredStatus = "Pages.TransferredStatus";
        public const string Pages_TransferredStatus_Create = "Pages.TransferredStatus.Create";
        public const string Pages_TransferredStatus_Edit = "Pages.TransferredStatus.Edit";
        public const string Pages_TransferredStatus_Delete = "Pages.TransferredStatus.Delete";

        //PaymentReceivedStatus
        public const string Pages_PaymentReceivedStatus = "Pages.PaymentReceivedStatus";
        public const string Pages_PaymentReceivedStatus_Create = "Pages.PaymentReceivedStatus.Create";
        public const string Pages_PaymentReceivedStatus_Edit = "Pages.PaymentReceivedStatus.Edit";
        public const string Pages_PaymentReceivedStatus_Delete = "Pages.PaymentReceivedStatus.Delete";

        //PaymentClaimedStatus
        public const string Pages_PaymentClaimedStatus = "Pages.PaymentClaimedStatus";
        public const string Pages_PaymentClaimedStatus_Create = "Pages.PaymentClaimedStatus.Create";
        public const string Pages_PaymentClaimedStatus_Edit = "Pages.PaymentClaimedStatus.Edit";
        public const string Pages_PaymentClaimedStatus_Delete = "Pages.PaymentClaimedStatus.Delete";

        //Import from excel
        public const string Pages_ImportFromExcel = "Pages.ImportFromExcel";

        //Case
        public const string Pages_Cases = "Pages.Cases";
        public const string Pages_Cases_ChangeStatus = "Pages.Cases.ChangeStatus";
        public const string Pages_Cases_AssignCase = "Pages.Cases.AssignCase";
        public const string Pages_Cases_EditCustomer = "Pages.Cases.EditCustomer";
        public const string Pages_Cases_Edit = "Pages.Cases.Edit";
        public const string Pages_Cases_UploadFile = "Pages.Cases.UploadFile";
        public const string Pages_Cases_NegotiatorNote = "Pages.Cases.NegotiatorNote";
        public const string Pages_Cases_SupervisorNote = "Pages.Cases.SupervisorNote";
        public const string Pages_Cases_LegalNote = "Pages.Cases.LegalNote";

        public const string Pages_Cases_ChangeStatus_UnderTracing = "Pages.Cases.ChangeStatus.UnderTracing";
        public const string Pages_Cases_ChangeStatus_Retracing = "Pages.Cases.ChangeStatus.Retracing";
        public const string Pages_Cases_ChangeStatus_ThirdPartyContact = "Pages.Cases.ChangeStatus.ThirdPartyContact";
        public const string Pages_Cases_ChangeStatus_Contacted = "Pages.Cases.ChangeStatus.Contacted";
        public const string Pages_Cases_ChangeStatus_UnderNegotiation = "Pages.Cases.ChangeStatus.UnderNegotiation";
        public const string Pages_Cases_ChangeStatus_Offer = "Pages.Cases.ChangeStatus.Offer";
        public const string Pages_Cases_ChangeStatus_Approval = "Pages.Cases.ChangeStatus.Approval";
        public const string Pages_Cases_ChangeStatus_Declined = "Pages.Cases.ChangeStatus.Declined";
        public const string Pages_Cases_ChangeStatus_Paid = "Pages.Cases.ChangeStatus.Paid";
        public const string Pages_Cases_ChangeStatus_PTP = "Pages.Cases.ChangeStatus.PTP";
        public const string Pages_Cases_ChangeStatus_Settled = "Pages.Cases.ChangeStatus.Settled";
        public const string Pages_Cases_ChangeStatus_AlreadySettled = "Pages.Cases.ChangeStatus.AlreadySettled";
        public const string Pages_Cases_ChangeStatus_BrokenPromise = "Pages.Cases.ChangeStatus.BrokenPromise";
        public const string Pages_Cases_ChangeStatus_RefusedToPay = "Pages.Cases.ChangeStatus.RefusedToPay";
        public const string Pages_Cases_ChangeStatus_Dead = "Pages.Cases.ChangeStatus.Dead";
        public const string Pages_Cases_ChangeStatus_ClosedAndReturned = "Pages.Cases.ChangeStatus.ClosedAndReturned";

        //Offers
        public const string Pages_Offers = "Pages.Offers";
        public const string Pages_Offers_Create = "Pages.Offers.Create";
        public const string Pages_Offers_Edit = "Pages.Offers.Edit";
        public const string Pages_Offers_Delete = "Pages.Offers.Delete";
        public const string Pages_Offers_ChangeStatus = "Pages.Offers.ChangeStatus";

        //Meetings
        public const string Pages_Meetings = "Pages.Meetings";
        public const string Pages_Meetings_Create = "Pages.Meetings.Create";
        public const string Pages_Meetings_Edit = "Pages.Meetings.Edit";
        public const string Pages_Meetings_Delete = "Pages.Meetings.Delete";

        //Calls
        public const string Pages_Calls = "Pages.Calls";
        public const string Pages_Calls_Create = "Pages.Calls.Create";
        public const string Pages_Calls_Edit = "Pages.Calls.Edit";
        public const string Pages_Calls_Delete = "Pages.Calls.Delete";

        //PTPs
        public const string Pages_PTPs = "Pages.PTPs";
        public const string Pages_PTPs_Create = "Pages.PTPs.Create";
        public const string Pages_PTPs_Edit = "Pages.PTPs.Edit";
        public const string Pages_PTPs_Delete = "Pages.PTPs.Delete";

        //Payments
        public const string Pages_Payments = "Pages.Payments";
        public const string Pages_Payments_Create = "Pages.Payments.Create";
        public const string Pages_Payments_Edit = "Pages.Payments.Edit";
        public const string Pages_Payments_Delete = "Pages.Payments.Delete";

        //Customers
        public const string Pages_Customers = "Pages.Customers";
        public const string Pages_Customers_Edit = "Pages.Customers.Edit";

        //Legals
        public const string Pages_Legals = "Pages.Legals";
        public const string Pages_Legals_Create = "Pages.Legals.Create";
        public const string Pages_Legals_ChangeLegalStatus = "Pages.Legals.ChangeLegalStatus";

        //Dashboard
        public const string Pages_Dashboard_Admin = "Pages.Dashboard.Admin";
        public const string Pages_Dashboard_NegotiatorTeamLeader = "Pages.Dashboard.NegotiatorTeamLeader";
        public const string Pages_Dashboard_Negotiator = "Pages.Dashboard.Negotiator";

    }
}
