﻿using Abp.Domain.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace LMS
{
    public abstract class LmsDomainServiceBase : DomainService
    {
        protected LmsDomainServiceBase()
        {
            LocalizationSourceName = LMSConsts.LocalizationSourceName;
        }
    }
}
