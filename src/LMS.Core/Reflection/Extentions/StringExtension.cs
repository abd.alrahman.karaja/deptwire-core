﻿namespace LMS.Reflection.Extentions
{
    public static class StringExtension
    {
        public static string DeleteDash(this string value)
        {
            return value.Equals("-") ? string.Empty : value;
        }
    }
}
