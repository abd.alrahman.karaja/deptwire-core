﻿namespace LMS.Loan.Enums
{
    public enum ActionType
    {
        None,
        ImportFromExcel,
        ChangeStatus,
        Offer,
        PTP,
        Payment,
        Meeting,
        Call,
        Legal,
        AssignCase,
        EditCaseInfo
    }
}
