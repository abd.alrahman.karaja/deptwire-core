﻿namespace LMS.Loan.Enums
{
    public enum NOC
    {
        None,
        Need,
        Requested,
        Received,
        Delivered
    }
}
