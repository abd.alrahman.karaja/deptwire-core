﻿namespace LMS.Loan.Enums
{
    public enum DOX
    {
        None,
        Need,
        Requested,
        Received,
        NotAvailable
    }
}
