﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Loan.Enums
{
    public enum  ModeOfAllocation
    {
        None,

        /// <summary>
        /// ضية جديدة
        /// </summary>
        NewCase,

        /// <summary>
        /// إعادة تخصيص
        /// </summary>
        Reallocate,

        /// <summary>
        /// مسحوبة
        /// </summary>
        Withdrawn
    }
}
