﻿namespace LMS.Loan.Enums
{
    public enum OriginalAmount
    {
        None,
        CreditLimit,
        LoanAmount
    }
}
