﻿namespace LMS.Loan.Enums
{
    public enum ModeOfPayment
    {
        PartialPayment,
        FinalPayment,
        MonthlyPayment,
        FullPayment
    }
}
