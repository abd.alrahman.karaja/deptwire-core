﻿namespace LMS.Loan.Enums
{
    public enum SocialMediaSite
    {
        Facebook,
        Twitter,
        YouTube,
        Instagram,
        WhatsApp,
        LinkedIn,
        TikTok,
        Pinterest,
        Snapchat
    }
}
