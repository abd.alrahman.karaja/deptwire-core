﻿using Abp;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Services;
using Abp.Domain.Uow;
using Abp.Notifications;
using Abp.Runtime.Session;
using LMS.Authorization.Roles;
using LMS.Authorization.Users;
using LMS.Loan.Cases.CaseUploadFiles;
using LMS.Loan.Cases.History;
using LMS.Loan.Cases.Notes;
using LMS.Loan.Enums;
using LMS.Loan.Indexes;
using LMS.Notifications;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Loan.Cases
{
    public class CaseManager : IDomainService, ITransientDependency
    {
        private readonly IRepository<Case> _caseRepository;
        private readonly IRepository<CaseUploadFile> _caseUploadFileRepository;
        private readonly IRepository<Role> _roleRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly ProductManager _productManager;
        private readonly CountryManager _countryManager;
        private readonly UserManager _userManager;
        private readonly NoteManager _noteManager;
        private readonly HistoryManager _historyManager;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly INotificationPublisher _notificationPublisher;
        public CaseManager(
            IUnitOfWorkManager unitOfWorkManager,
            IRepository<Case> caseRepository,
            HistoryManager historyManager,
            IRepository<CaseUploadFile> caseUploadFileRepository,
            IRepository<Role> roleRepository,
            IRepository<User, long> userRepository,
            ProductManager productManager,
            CountryManager countryManager,
            UserManager userManager,
            NoteManager noteManager,
            INotificationPublisher notificationPublisher
            )
        {
            _unitOfWorkManager = unitOfWorkManager;
            _caseRepository = caseRepository;
            _historyManager = historyManager;
            _caseUploadFileRepository = caseUploadFileRepository;
            _roleRepository = roleRepository;
            _userRepository = userRepository;
            _productManager = productManager;
            _countryManager = countryManager;
            _userManager = userManager;
            _noteManager = noteManager;
            _notificationPublisher = notificationPublisher;
            AbpSession = NullAbpSession.Instance;
        }

        public async Task<IQueryable<Case>> GetUserCases(string userId)
        {
            var user = await _userManager.FindByIdAsync(userId);
            var dataAccess = await _userManager.GetDataAccessType(user);

            var cases = GetAllCases();

            if (dataAccess == DataAccess.Me)
            {
                return cases.Where(x => x.UserId != null && x.UserId.Value == user.Id);
            }
            else if (dataAccess == DataAccess.MeAndSubordinates)
            {
                var dependencesUsers = _userManager.GetCurrentAndSubordinates(user);
                var dependencesUsersIds = dependencesUsers.Select(d => d.Id).ToArray();
                return cases.Where(x => x.UserId != null &&
                (x.UserId.Value == user.Id || dependencesUsersIds.Contains(x.UserId.Value)));
            }
            var ss = cases.ToList();
            return cases;
        }

        public IAbpSession AbpSession { get; set; }

        public Case GetCaseById(int caseId)
        {
            return GetAllCases().FirstOrDefault(x => x.Id == caseId);
        }

        public async Task<Case> GetCaseByIdAsync(int caseId)
        {
            return await _caseRepository.FirstOrDefaultAsync(x => x.Id == caseId);
        }

        /// <summary>
        /// Get cases for this customer
        /// </summary>
        /// <param name="customerId">Customer id</param>
        /// <returns>Customer cases</returns>
        public IList<Case> GetCasesForCustomer(int customerId)
        {
            return _caseRepository.GetAllIncluding(
                a => a.Agent,
                b => b.Bank,
                p => p.Product
                ).Where(x => x.CustomerId == customerId).ToList();
        }


        public async Task<IQueryable<Case>> GetCasesAsync(long userId, int dataAccessType)
        {
            var user = await _userManager.GetUserByIdAsync(userId);
            var dataAccess = (DataAccess)dataAccessType;
            var cases = GetAllCasesWithDetails();

            if (dataAccess == DataAccess.Me)
            {
                return cases.Where(x => x.UserId != null && x.UserId.Value == user.Id);
            }
            else if (dataAccess == DataAccess.MeAndSubordinates)
            {
                var dependencesUsers = _userManager.GetCurrentAndSubordinates(user);
                var dependencesUsersIds = dependencesUsers.Select(d => d.Id).ToArray();
                return cases.Where(x => x.UserId != null &&
                (x.UserId.Value == user.Id || dependencesUsersIds.Contains(x.UserId.Value)));
            }

            return cases;
        }

        public async Task<Case> AssignCaseToNegotiator(int caseId, int userId)
        {
            var casee = await _caseRepository.FirstOrDefaultAsync(caseId);
            casee.UserId = userId;
            if (casee.WaedStatus == CaseStatus.None)
                casee.WaedStatus = CaseStatus.Blank;

            //NotNot ارسال اشعار للمفاوض انه تم اسناد القضية له
            var user = await _userManager.FindByIdAsync(userId.ToString());
            if(user != null)
            {
                var titile = $"Assign a new case";
                var body = $"A new case has been assigned to you - Case ID {caseId}";
                UserIdentifier[] userIdentifiers = new UserIdentifier[] { user.ToUserIdentifier() };

                await _notificationPublisher.PublishAsync(
                        titile,
                        new MessageNotificationData(body),
                        severity: NotificationSeverity.Info,
                        userIds: userIdentifiers
                        );

                //History
                await _historyManager.CreateAsync(
                        caseId,
                        ActionType.AssignCase,
                        titile,
                        $"The case {caseId} has been assigned to user {user.FullName}");
            }
            return await _caseRepository.UpdateAsync(casee);
        }

        public async Task<CaseUploadFile> GetFileDetailAsync(int id)
        {
            return await _caseUploadFileRepository.FirstOrDefaultAsync(id);
        }

        public async Task<Case> Update(Case newCase, Case oldCase)
        {
            var updatedFields = ": <br />";
            if(newCase.LiabilityMail != oldCase.LiabilityMail)
            {
                updatedFields += $"Liability Mail : {newCase.LiabilityMail} => {oldCase.LiabilityMail} <br />";
            }
            if (newCase.DOX != oldCase.DOX)
            {
                updatedFields += $"DOX : {oldCase.DOX}  =>  {newCase.DOX} <br />";
            }
            if (newCase.NOC != oldCase.NOC)
            {
                updatedFields += $"NOC : {oldCase.NOC}  =>  {newCase.NOC} <br />";
            }
            if (newCase.BookingDate != oldCase.BookingDate)
            {
                var oldDate = oldCase.BookingDate != null ? oldCase.BookingDate.Value.ToString("g") : "";
                var newDate = newCase.BookingDate != null ? newCase.BookingDate.Value.ToString("g") : "";
                updatedFields += $"Booking Date : {oldDate}  =>  {newDate} <br />";
            }
            if (newCase.AllocationDate != oldCase.AllocationDate)
            {
                var oldDate = oldCase.AllocationDate != null ? oldCase.AllocationDate.Value.ToString("g") : "";
                var newDate = newCase.AllocationDate != null ? newCase.AllocationDate.Value.ToString("g") : "";
                updatedFields += $"Allocation Date : {oldDate} => {newDate} <br />";
            }
            if (newCase.ModeOfAllocation != oldCase.ModeOfAllocation)
            {
                updatedFields += $"Mode Of Allocation : {oldCase.ModeOfAllocation.ToString()} => {newCase.ModeOfAllocation.ToString()} <br />";
            }
            if (newCase.WaedFeedback != oldCase.WaedFeedback)
            {
                updatedFields += $"Waed Feedback : {oldCase.WaedFeedback} => {newCase.WaedFeedback} <br />";
            }
            if (newCase.BankFeedback != oldCase.BankFeedback)
            {
                updatedFields += $"Bank Feedback : {oldCase.BankFeedback} => {newCase.BankFeedback} <br />";
            }
            if (newCase.ArabicFeedback != oldCase.ArabicFeedback)
            {
                updatedFields += $"Arabic Feedback : {oldCase.ArabicFeedback} => {newCase.ArabicFeedback} <br />";
            }
            var updatedCase = await _caseRepository.UpdateAsync(newCase);
            //History
            await _historyManager.CreateAsync(
                    newCase.Id,
                    ActionType.EditCaseInfo,
                    "Edit Case Info",
                    $"Edit case info  {updatedFields}");

            return updatedCase;
        }

        
        public async Task<Case> InsertCaseForImportAsync(Case importedCase)
        {
            importedCase.Activity = true;
            importedCase.Product = await GetOrInsertProduct(importedCase.Product.Name);
            importedCase.BankHome = await GetOrInsertCountry(importedCase.Product.Name);

            return await _caseRepository.InsertAsync(importedCase);
        }

        public async Task<Case> UpdateCaseForImportAsync(Case importedCase)
        {
            var currentCase = await GetCaseByNoAsync(importedCase.ContractNo, importedCase.AccountNo);

            currentCase.Agent = importedCase.Agent;
            currentCase.AgentId = importedCase.AgentId;
            currentCase.BankId = importedCase.BankId;
            currentCase.Bank = importedCase.Bank;
            currentCase.BankHome = importedCase.BankHome;
            currentCase.CIF = importedCase.CIF;
            currentCase.AccountNo = importedCase.AccountNo;
            currentCase.ContractNo = importedCase.ContractNo;
            currentCase.FullAccount = importedCase.FullAccount;
            currentCase.Product = importedCase.Product;
            currentCase.ProductId = importedCase.ProductId;
            currentCase.TypeOfAssist = importedCase.TypeOfAssist;
            currentCase.Manufacturer = importedCase.Manufacturer;
            currentCase.OriginalAmount = importedCase.OriginalAmount;
            currentCase.TotalAmount = importedCase.TotalAmount;
            currentCase.Outstanding = importedCase.Outstanding;
            currentCase.PrincipalAmount = importedCase.PrincipalAmount;
            currentCase.LastCreditLimit = importedCase.LastCreditLimit;
            currentCase.MainLedgerBalance = importedCase.MainLedgerBalance;
            currentCase.InstallmentAmount = importedCase.InstallmentAmount;
            currentCase.BucketStage = importedCase.BucketStage;
            currentCase.Overdues = importedCase.Overdues;
            currentCase.MinPayment = importedCase.MinPayment;
            currentCase.WriteOffDate = importedCase.WriteOffDate;
            currentCase.WriteOffAge = importedCase.WriteOffAge;
            currentCase.DPD = importedCase.DPD;
            currentCase.OpenDate = importedCase.OpenDate;
            currentCase.CloseDate = importedCase.CloseDate;
            currentCase.LastPaymentAmount = importedCase.LastPaymentAmount;
            currentCase.LastPaymentDate = importedCase.LastPaymentDate;
            currentCase.LastPurchaseAmount = importedCase.LastPurchaseAmount;
            currentCase.LastPurchaseDate = importedCase.LastPurchaseDate;
            currentCase.CompanyName = importedCase.CompanyName;
            currentCase.PoliceCaseAmount = importedCase.PoliceCaseAmount;
            currentCase.PoliceStation = importedCase.PoliceStation;
            currentCase.PoliceCaseStatus = importedCase.PoliceCaseStatus;
            currentCase.PoliceCaseNo = importedCase.PoliceCaseNo;
            currentCase.PoliceCaseDate = importedCase.PoliceCaseDate;
            currentCase.FinalJugement = importedCase.FinalJugement;
            if (importedCase.ModeOfAllocation == ModeOfAllocation.Reallocate || importedCase.ModeOfAllocation == ModeOfAllocation.NewCase)
            {
                currentCase.Activity = true;
            }

            return await _caseRepository.UpdateAsync(currentCase);
        }

        private async Task<Product> GetOrInsertProduct(string name)
        {
            Product product = await _productManager.GetProductByNameAsync(name);
            if (product != null)
            {
                product = await _productManager.InsertAndGetAsync(name);
            }

            return product;
        }

        private async Task<Country> GetOrInsertCountry(string name)
        {
            Country country = await _countryManager.GetCountryByNameAsync(name);
            if (country != null)
            {
                country = await _countryManager.InsertAndGetAsync(name);
            }

            return country;
        }

        public async Task<Case> GetCaseByNoAsync(string contractNo, string accountNo)
        {
            Case returnedCase = null;
            if (!string.IsNullOrEmpty(contractNo) && _caseRepository.FirstOrDefault(x => x.ContractNo.Equals(contractNo)) != null)
            {
                returnedCase = await _caseRepository.FirstOrDefaultAsync(x => x.ContractNo.Equals(contractNo));
            }
            else if (string.IsNullOrEmpty(contractNo) && _caseRepository.FirstOrDefault(x => x.AccountNo.Equals(accountNo)) != null)
            {
                returnedCase = await _caseRepository.FirstOrDefaultAsync(x => x.AccountNo.Equals(accountNo));
            }

            return returnedCase;
        }

        public async Task<Case> ChangeStatusWithNotify(int caseId, CaseStatus status)
        {
            var cas = await _caseRepository.FirstOrDefaultAsync(caseId);
            cas.WaedStatus = status;
            return await _caseRepository.UpdateAsync(cas);
        }

        public async Task<Case> ChangeStatus(int caseId, CaseStatus status)
        {
            var cas = await _caseRepository.FirstOrDefaultAsync(caseId);
            cas.WaedStatus = status;
            return await _caseRepository.UpdateAsync(cas);
        }

        public async Task<bool> ChangeStatusAndAssignAsync(int status, string supervisorNote, string negotiatorNote, int[] casesIds)
        {
            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    var caseStatus = (CaseStatus)status;
                    foreach (var caseId in casesIds)
                    {
                        await ChangeStatus(caseId, (CaseStatus)status);

                        //Note
                        var noteType = 1;
                        var note = new Note();
                        if(!string.IsNullOrEmpty(negotiatorNote))
                        {
                            note.NoteText = negotiatorNote;
                            noteType = 1;
                        }
                        else
                        {
                            note.NoteText = supervisorNote;
                            noteType = 2;
                        }
                        await _noteManager.CreateAsync(note, caseId, caseId, ActionName.ChangeStatus, noteType);
                    }

                    //Send Notification
                    var casesIdsStr = string.Join(",", casesIds);
                    var titile = $"Change status to {caseStatus}";
                    var body = $"Cases Status has been changed to ({caseStatus}) - Cases IDs({casesIdsStr})";
                    UserIdentifier[] userIdentifiers = await GetUserIdentifiersForNotify(casesIds[0]);

                    await _notificationPublisher.PublishAsync(
                            titile,
                            new MessageNotificationData(body),
                            severity: NotificationSeverity.Info,
                            userIds: userIdentifiers
                            );

                    //History
                    foreach (var caseId in casesIds)
                    {
                        await _historyManager.CreateAsync(
                            caseId,
                            ActionType.ChangeStatus,
                            titile,
                            body);
                    }

                    unitOfWork.Complete();
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        
        

        public async Task<UserIdentifier[]> GetUserIdentifiersForNotify(int caseId)
        {
            var caseUser = await GetCaseUser(caseId);
            var teamLeaderUser = await _userManager.GetTeamLeader(caseUser);
            var cordinatorsUsers = await GetCoordinatorUsers();

            var identifiers = new List<UserIdentifier>();
            identifiers.Add(caseUser.ToUserIdentifier());
            if (teamLeaderUser != null)
            {
                identifiers.Add(teamLeaderUser.ToUserIdentifier());
            }

            if(cordinatorsUsers != null)
            {
                foreach (var user in cordinatorsUsers)
                {
                    identifiers.Add(user.ToUserIdentifier());
                }
            }

            return identifiers.ToArray();
        }

        private async Task<User> GetCaseUser(int caseId)
        {
            var cas = await _caseRepository.FirstOrDefaultAsync(caseId);
            var userId = cas.UserId.Value.ToString();
            return await _userManager.FindByIdAsync(userId);
        }

        private async Task<IList<User>> GetCoordinatorUsers()
        {
            var coordinatorUsers = new List<User>();
            var role = await _roleRepository.FirstOrDefaultAsync(x => x.Name == "Coordinator");
            var users = _userManager.GetAllUsers();
            foreach (var us in users)
            {
                var isInRole = await _userManager.IsInRoleAsync(us, role.Name);
                if (isInRole)
                    coordinatorUsers.Add(us);
            }

            return coordinatorUsers;
        }

        public IQueryable<Case> GetAllCases()
        {
            return _caseRepository.GetAllIncluding(
                    a => a.Agent,
                    b => b.Bank,
                    bh => bh.BankHome,
                    c => c.Customer,
                    bc => bc.BankCode,
                    ps => ps.PoliceCaseStatus,
                    u => u.User,
                    n => n.Customer.Nationality,
                    ph => ph.Customer.Phones,
                    oph => oph.Customer.OfficePhone,
                    m => m.Customer.Mobiles,
                    rf => rf.Customer.ReferencePhones,
                    sm => sm.Customer.SocialMediaAccounts,
                    em => em.Customer.Emails
                    ).Include(le => le.CaseLegals).ThenInclude(l => l.Legal)
                    .Include(cn => cn.CaseNotes).ThenInclude(n => n.Note).ThenInclude(u => u.CreatorUser);
        }
        public IQueryable<Case> GetAllCasesWithDetails()
        {
            return _caseRepository.GetAllIncluding(
                    a => a.Agent,
                    b => b.Bank,
                    bh => bh.BankHome,
                    c => c.Customer,
                    bc => bc.BankCode,
                    ps => ps.PoliceCaseStatus,
                    u => u.User,
                    n => n.Customer.Nationality,
                    ph => ph.Customer.Phones,
                    oph => oph.Customer.OfficePhone,
                    m => m.Customer.Mobiles,
                    rf => rf.Customer.ReferencePhones,
                    sm => sm.Customer.SocialMediaAccounts,
                    em => em.Customer.Emails
                    )
                .Include(cn => cn.CaseNotes).ThenInclude(n => n.Note).ThenInclude(u => u.CreatorUser)
                .Include(co => co.CaseOffers).ThenInclude(of => of.Offer)
                .Include(le => le.CaseLegals).ThenInclude(l => l.Legal)
                .Include(cp => cp.CasePayments).ThenInclude(p => p.Payment)
                .Include(cp => cp.CaseCalls).ThenInclude(p => p.Call)
                .Include(cp => cp.CaseMeetings).ThenInclude(p => p.Meeting)
                .Include(cp => cp.CasePromisesToPays).ThenInclude(p => p.PromiseToPay);
        }

    }
}
