﻿namespace LMS.Loan.Cases.Notes
{
    public enum ActionName
    {
        Case,
        Offer,
        PTP,
        Payment,
        Legal,
        ChangeStatus,
        Call
    }
}
