﻿using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations.Schema;

namespace LMS.Loan.Cases.Notes
{
    public class CaseNote : FullAuditedEntity
    {
        public ActionName ActionName { get; set; }
        public int? ActionId { get; set; }
        public int? Type { get; set; }
        #region Case
        public int CaseId { get; set; }
        [ForeignKey("CaseId")]
        public virtual Case Case { get; set; }
        #endregion

        #region Note
        public int NoteId { get; set; }
        [ForeignKey("NoteId")]
        public virtual Note Note { get; set; }
        #endregion

    }
}
