﻿using Abp.Domain.Repositories;
using Abp.Domain.Services;
using Abp.Domain.Uow;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Loan.Cases.Notes
{
    public class NoteManager : IDomainService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        //private readonly CaseManager _caseManager;
        private readonly IRepository<Note> _noteRepository;
        private readonly IRepository<CaseNote> _caseNoteRepository;


        public NoteManager(
            IUnitOfWorkManager unitOfWorkManager,
            //CaseManager caseManager,
            IRepository<Note> noteRepository,
            IRepository<CaseNote> caseNoteRepository
            )
        {
            _unitOfWorkManager = unitOfWorkManager;
            _noteRepository = noteRepository;
            _caseNoteRepository = caseNoteRepository;
            //_caseManager = caseManager;
        }

        public List<Note> GetNotesForCase(int caseId)
        {
            var caseNote = _caseNoteRepository
                .GetAllIncluding(
                    m => m.Note,
                    cu => cu.Note.CreatorUser
                ).Where(x => x.CaseId == caseId);

            if (!caseNote.Any())
                return new List<Note>();

            var note = (from o in caseNote
                        select o.Note);
            return note.OrderByDescending(x => x.Id).ToList();
        }

        public IList<CaseNote> GetCaseNotes(int caseId)
        {
            var notes = new List<Note>();
            var caseNotes = _caseNoteRepository.GetAllIncluding(n => n.Note)
                .Include(n => n.Note).ThenInclude(u => u.CreatorUser);

            return caseNotes.Where(x => x.CaseId == caseId).ToList();
        }

        public IList<CaseNote> GetCaseNotesWithoutSpecificAction(
            int caseId, 
            int? actionId, 
            int actionName,
            int type)
        {
            var notes = new List<Note>();
            var caseNotes = _caseNoteRepository.GetAllIncluding(n => n.Note)
                .Include(n => n.Note).ThenInclude(u => u.CreatorUser);

            return caseNotes.Where(x =>
            x.CaseId == caseId &&
            x.Type != type &&
            x.ActionId != actionId &&
            x.ActionName != (ActionName)actionName).ToList();
        }

        public List<Note> GetNotesByActionInfo(int caseId, int? actionId, int actionName)
        {
            var notes = new List<Note>();
            var caseNotes = _caseNoteRepository.GetAllIncluding(x => x.Note)
                .Where(x => x.CaseId == caseId && x.ActionId == actionId && x.ActionName == (ActionName)actionName).ToList();
            notes = caseNotes.Select(x => x.Note).ToList();
            return notes;
        }

        public async Task<Note> GetNoteById(int noteId)
        {
            return await _noteRepository.FirstOrDefaultAsync(noteId);
        }
        public async Task<IList<CaseNote>> GetCaseNotesByIdAsync(int noteId)
        {
            return await _caseNoteRepository.GetAllListAsync(x => x.Note.Id == noteId);
        }

        public IQueryable<CaseNote> GetCaseNotesByCasesIdsAsync(int[] casesIds)
        {
            return _caseNoteRepository.GetAllIncluding(x => x.Note)
                .Where(x => casesIds.Contains(x.CaseId));
        }

        public async Task CreateAndAssignAsync(Note note, int[] casesIds)
        {
            using (var unitOfWork = _unitOfWorkManager.Begin())
            {
                var noteId = _noteRepository.InsertAndGetId(note);

                foreach (var caseId in casesIds)
                {
                    await _caseNoteRepository.InsertAsync(new CaseNote() { CaseId = caseId, NoteId = noteId });
                }

                unitOfWork.Complete();
            }
        }

        public async Task<CaseNote> CreateAsync(
            Note note, 
            int caseId, 
            int? actionId = null,
            ActionName actionName = ActionName.Case,
            int? type = null)
        {
            var noteId = _noteRepository.InsertAndGetId(note);
            return await _caseNoteRepository.InsertAsync(
                new CaseNote()
                {
                    CaseId = caseId,
                    NoteId = noteId,
                    ActionId = actionId,
                    ActionName = actionName,
                    Type = type // 1 = Negotiator note , 2 = Supervisor note
                });
        }

        public async Task<Note> UpdateTextNoteAsync(
            string noteText,
            int caseId,
            int? actionId = null,
            ActionName actionName = ActionName.Case,
            int? type = null)
        {
            var caseNote = await _caseNoteRepository.FirstOrDefaultAsync((System.Linq.Expressions.Expression<System.Func<CaseNote, bool>>)(x =>
                x.CaseId == caseId &&
                x.Type == type &&
                x.ActionId == actionId &&
                x.ActionName == actionName));
            if(caseNote != null)
            {
                var note = await _noteRepository.FirstOrDefaultAsync(x => x.Id == caseNote.NoteId);
                if(noteText != note.NoteText)
                {
                    note.NoteText = noteText;
                    return await _noteRepository.UpdateAsync(note);
                }
            }

            return null;
        }
    }
}
