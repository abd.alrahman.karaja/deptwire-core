﻿using Abp.Domain.Entities.Auditing;
using LMS.Authorization.Users;
using System.ComponentModel.DataAnnotations.Schema;

namespace LMS.Loan.Cases.Notes
{
    public class Note : FullAuditedEntity
    {
        public string NoteText { get; set; }

        [ForeignKey("CreatorUserId")]
        public virtual User CreatorUser { get; set; }
    }
}
