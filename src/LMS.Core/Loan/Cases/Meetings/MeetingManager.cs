﻿using Abp;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Services;
using Abp.Domain.Uow;
using Abp.Notifications;
using LMS.Authorization.Users;
using LMS.Loan.Cases.History;
using LMS.Loan.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Loan.Cases.Meetings
{
    public class MeetingManager : IDomainService, ITransientDependency
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<Meeting> _meetingRepository;
        private readonly IRepository<CaseMeeting> _caseMeetingRepository;
        private readonly INotificationPublisher _notificationPublisher;
        private readonly HistoryManager _historyManager;
        private readonly UserManager _userManager;
        public MeetingManager(
            IUnitOfWorkManager unitOfWorkManager,
            IRepository<Meeting> meetingRepository,
            IRepository<CaseMeeting> caseMeetingRepository,
            INotificationPublisher notificationPublisher,
            HistoryManager historyManager,
            UserManager userManager
            )
        {
            _unitOfWorkManager = unitOfWorkManager;
            _meetingRepository = meetingRepository;
            _caseMeetingRepository = caseMeetingRepository;
            _notificationPublisher = notificationPublisher;
            _historyManager = historyManager;
            _userManager = userManager;
        }

        public bool CheckIfThereIsAnotherMeetingAtSameTime(Meeting input)
        {
            var meeting = _meetingRepository.FirstOrDefault(x =>
                x.MeetingDate != null &&
                x.MeetingStart != null &&
                x.MeetingDate.Value.Date == input.MeetingDate.Value.Date &&
                x.MeetingStart.Value.Hour == input.MeetingStart.Value.Hour &&
                x.Id != input.Id
                );
            return meeting != null;
        }

        public List<Meeting> GetMeetingsForCase(int caseId)
        {
            var caseMeeting = _caseMeetingRepository
                .GetAllIncluding(
                m => m.Meeting,
                mt => mt.Meeting.ManagerTeamName,
                mn => mn.Meeting.MeetingManagerName
                ).Where(x => x.CaseId == caseId);

            if (!caseMeeting.Any())
                return new List<Meeting>();

            var meeting = (from o in caseMeeting
                    select o.Meeting);
            return meeting.ToList();
        }

        public List<CaseMeeting> GetCaseMeetingsByDate(DateTime date)
        {
            var caseMeetings = _caseMeetingRepository
                .GetAllIncluding(
                m => m.Meeting,
                mt => mt.Meeting.ManagerTeamName,
                mn => mn.Meeting.MeetingManagerName
                ).Where(x => 
                x.Meeting.MeetingDate != null && 
                x.Meeting.MeetingDate.Value.Date == date.Date);

            return caseMeetings.ToList();
        }

        public List<CaseMeeting> GetCaseMeetingsByReminderDate(DateTime reminderDate, int[] casesIds)
        {
            var caseMeetings = _caseMeetingRepository
                .GetAllIncluding(
                m => m.Meeting,
                mt => mt.Meeting.ManagerTeamName,
                mn => mn.Meeting.MeetingManagerName
                ).Where(x =>
                x.Meeting.ReminderDate != null &&
                x.Meeting.ReminderDate.Value.Date == reminderDate.Date &&
                casesIds.Contains(x.CaseId));

            return caseMeetings.ToList();
        }

        public async Task<Meeting> GetMeetingById(int meetingId)
        {
            return await _meetingRepository.FirstOrDefaultAsync(meetingId);
        }
        public async Task<IList<CaseMeeting>> GetCaseMeetingsById(int meetingId)
        {
            return await _caseMeetingRepository.GetAllListAsync(x => x.Meeting.Id == meetingId);
        }

        public async Task CreateAndAssignAsync(Meeting meeting, int[] casesIds)
        {
            //Meeting with Mr. Ahmed regarding the issues
            using (var unitOfWork = _unitOfWorkManager.Begin())
            {
                var meetingId = _meetingRepository.InsertAndGetId(meeting);

                foreach (var caseId in casesIds)
                {
                    await _caseMeetingRepository.InsertAsync(new CaseMeeting() { CaseId = caseId, MeetingId = meetingId });
                }

                //Send Notification
                var casesIdsStr = string.Join(",", casesIds);
                var managerUser = await _userManager.FindByIdAsync(meeting.MeetingManagerNameId.ToString());
                var teamLeaderUser = await _userManager.FindByIdAsync(meeting.ManagerTeamNameId.ToString());

                var titile = $"Meeting with Mr. {meeting.MeetingWith} regarding the cases {casesIdsStr}";
                var body = $"Meeting with Mr. {meeting.MeetingWith} regarding the cases {casesIdsStr}  ";
                body += $"<br /> Date : {meeting.MeetingDate} <br /> Time : from {meeting.MeetingStart.Value.ToShortTimeString()} to {meeting.MeetingEnd.Value.ToShortTimeString()}";
                body += $"<br /> Manager Team Name : {teamLeaderUser.FullName} <br /> MeetingManagerName : {managerUser.FullName}";
                
                UserIdentifier[] userIdentifiers = new UserIdentifier[] 
                { 
                    managerUser.ToUserIdentifier(), 
                    teamLeaderUser.ToUserIdentifier()
                };

                await _notificationPublisher.PublishAsync(
                        titile,
                        new MessageNotificationData(body),
                        severity: NotificationSeverity.Info,
                        userIds: userIdentifiers
                        );

                //History
                foreach (var caseId in casesIds)
                {
                    await _historyManager.CreateAsync(
                        caseId,
                        ActionType.Meeting,
                        $"Add meeting with Mr. {meeting.MeetingWith} regarding the cases {casesIdsStr}",
                        body);
                }

                unitOfWork.Complete();
            }
        }

        public async Task UpdateAndAssignAsync(Meeting meeting, int[] casesIds)
        {
            using (var unitOfWork = _unitOfWorkManager.Begin())
            {
                var updatedMeeting =  _meetingRepository.Update(meeting);
                var caseMeetings =  _caseMeetingRepository.GetAllList(x => x.MeetingId == meeting.Id);

                await AssignNewCasses(caseMeetings, casesIds);
                await UnAssignRemovedCasses(caseMeetings, casesIds);

                unitOfWork.Complete();
            }
        }

        public async Task<Meeting> Update(Meeting meeting)
        {
            return await _meetingRepository.UpdateAsync(meeting);
        }

        public async Task Delete(int meetingId)
        {
            using (var unitOfWork = _unitOfWorkManager.Begin())
            {
                var caseMeetings = _caseMeetingRepository.GetAllList(x => x.MeetingId == meetingId);

                foreach (var caseMeeting in caseMeetings)
                {
                    await _caseMeetingRepository.DeleteAsync(caseMeeting);
                }

                await _meetingRepository.DeleteAsync(meetingId);

                unitOfWork.Complete();
            }
        }

        #region Helper Methods
        private async Task UnAssignRemovedCasses(List<CaseMeeting> caseMeetings, int[] casesIds)
        {
            foreach (var caseMeeting in caseMeetings)
            {
                if (!casesIds.Contains(caseMeeting.CaseId))
                {
                    await _caseMeetingRepository.DeleteAsync(caseMeeting);

                }
            }
        }

        private async Task AssignNewCasses(List<CaseMeeting> caseMeetings, int[] casesIds)
        {
            var meetingId = caseMeetings.FirstOrDefault().MeetingId;

            foreach (var caseId in casesIds)
            {
                if (!caseMeetings.Any(x => x.CaseId == caseId))
                {
                    await _caseMeetingRepository.InsertAsync(new CaseMeeting() { CaseId = caseId, MeetingId = meetingId });

                }
            }
        }
        #endregion

    }
}
