﻿using LMS.Authorization.Users;
using LMS.Loan.Shared;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace LMS.Loan.Cases.Meetings
{
    public class Meeting : WaedAction
    {
        public string MeetingWith { get; set; }
        public DateTime? MeetingDate { get; set; }
        public DateTime? MeetingStart { get; set; }
        public DateTime? MeetingEnd { get; set; }
        public bool LegalMeeting { get; set; }
        public bool Done { get; set; }

        #region Manager Team Name
        public long? ManagerTeamNameId { get; set; }
        [ForeignKey("ManagerTeamNameId")]
        public virtual User ManagerTeamName { get; set; }
        #endregion

        #region Meeting Manager Name
        public long? MeetingManagerNameId { get; set; }
        [ForeignKey("MeetingManagerNameId")]
        public virtual User MeetingManagerName { get; set; }
        #endregion

    }
}
