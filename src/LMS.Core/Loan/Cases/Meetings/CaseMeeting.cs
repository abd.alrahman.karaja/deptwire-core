﻿using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations.Schema;

namespace LMS.Loan.Cases.Meetings
{
    public class CaseMeeting : FullAuditedEntity
    {
        #region Case
        public int CaseId { get; set; }
        [ForeignKey("CaseId")]
        public virtual Case Case { get; set; }
        #endregion

        #region Meeting
        public int MeetingId { get; set; }
        [ForeignKey("MeetingId")]
        public virtual Meeting Meeting { get; set; }
        #endregion
    }
}
