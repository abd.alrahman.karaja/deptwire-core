﻿using Abp.Domain.Entities.Auditing;
using LMS.Loan.Cases.Meetings;
using LMS.Loan.Cases.Offers;
using System;

namespace LMS.Loan.Reminders
{
    public class Reminder : FullAuditedEntity
    {
        public DateTime? ReminderDate { get; set; }
        public string Note { get; set; }

        #region Meeting
        public int? MeetingId { get; set; }
        public virtual Meeting Meeting { get; set; }
        #endregion

        #region Offer
        public int? OfferId { get; set; }
        public virtual Offer Offer { get; set; }
        #endregion
    }
}
