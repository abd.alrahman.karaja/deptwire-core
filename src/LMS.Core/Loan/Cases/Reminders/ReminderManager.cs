﻿using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Services;
using LMS.Loan.Reminders;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Loan.Cases.Reminders
{
    public class ReminderManager : IDomainService, ITransientDependency
    {
        private readonly IRepository<Reminder> _repository;
        public ReminderManager(IRepository<Reminder> repository)
        {
            this._repository = repository;
        }

        public async Task<Reminder> GetReminderById(int id)
        {
            return await _repository.FirstOrDefaultAsync(id);
        }

        public async Task<Reminder> CreateAsync(Reminder reminder)
        {
            return await _repository.InsertAsync(reminder);
        }

        public async Task<Reminder> UpdateAsync(Reminder reminder)
        {
            return await _repository.UpdateAsync(reminder);
        }

        #region Offer
        public async Task<Reminder> GetLastReminderForOffer(int offerId)
        {
            var reminders = await _repository.GetAllListAsync(x => x.OfferId == offerId);
            return reminders.OrderByDescending(x => x.ReminderDate).FirstOrDefault();
        }
        public async Task<IList<Reminder>> GetAllRemindersByOfferId(int offerId)
        {
            return await _repository.GetAllListAsync(x => x.OfferId == offerId);
        }
        #endregion
    }
}
