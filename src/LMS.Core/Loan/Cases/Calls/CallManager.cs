﻿using Abp;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Services;
using Abp.Domain.Uow;
using Abp.Notifications;
using LMS.Authorization.Users;
using LMS.Loan.Cases.History;
using LMS.Loan.Cases.Notes;
using LMS.Loan.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Loan.Cases.Calls
{
    public class CallManager : IDomainService, ITransientDependency
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<Call> _callRepository;
        private readonly IRepository<CaseCall> _caseCallRepository;
        private readonly INotificationPublisher _notificationPublisher;
        private readonly HistoryManager _historyManager;
        private readonly UserManager _userManager;
        private readonly CaseManager _caseManager;
        private readonly NoteManager _noteManager;

        public CallManager(
            IUnitOfWorkManager unitOfWorkManager, 
            IRepository<Call> callRepository, 
            IRepository<CaseCall> caseCallRepository, 
            INotificationPublisher notificationPublisher, 
            HistoryManager historyManager, 
            UserManager userManager,
            CaseManager caseManager,
            NoteManager noteManager)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _callRepository = callRepository;
            _caseCallRepository = caseCallRepository;
            _notificationPublisher = notificationPublisher;
            _historyManager = historyManager;
            _userManager = userManager;
            _caseManager = caseManager;
            _noteManager = noteManager;
        }

        public bool CheckIfThereIsAnotherCallAtSameTime(Call input)
        {
            var call = _callRepository.FirstOrDefault(x =>
                x.CallStart != null &&
                x.CallDate != null &&
                x.CallDate.Value.Date == input.CallDate.Value.Date &&
                x.CallStart.Value.Hour == input.CallStart.Value.Hour &&
                x.Id != input.Id
                );
            return call != null;
        }

        public List<Call> GetCallsForCase(int caseId)
        {
            var caseCall = _caseCallRepository
                .GetAllIncluding(
                m => m.Call
                ).Where(x => x.CaseId == caseId);

            if (!caseCall.Any())
                return new List<Call>();

            var call = (from o in caseCall
                           select o.Call);
            return call.ToList();
        }

        public async Task<Call> GetCallById(int callId)
        {
            return await _callRepository.FirstOrDefaultAsync(callId);
        }
        public async Task<IList<CaseCall>> GetCaseCallsById(int callId)
        {
            return await _caseCallRepository.GetAllListAsync(x => x.Call.Id == callId);
        }

        public async Task CreateAndAssignAsync(Call call, int[] casesIds)
        {
            using (var unitOfWork = _unitOfWorkManager.Begin())
            {
                var callId = _callRepository.InsertAndGetId(call);

                foreach (var caseId in casesIds)
                {
                    await _caseCallRepository.InsertAsync(new CaseCall() { CaseId = caseId, CallId = callId });
                    var note = new Note()
                    {
                        NoteText = call.NegotiatorNote
                    };
                    await _noteManager.CreateAsync(note, caseId, caseId, ActionName.Call, 1);
                }

                //Send Notification
                var casesIdsStr = string.Join(",", casesIds);

                var titile = $"Add call with Mr. {call.CallWith} regarding the cases {casesIdsStr}";
                var body = $"Add call with Mr. {call.CallWith} regarding the cases {casesIdsStr}  ";
                body += $"<br /> Date : {call.CallDate} <br /> Time : from {call.CallStart.Value.ToShortTimeString()}";

                UserIdentifier[] userIdentifiers = await GetUserIdentifiersForNotify(casesIds[0]);

                await _notificationPublisher.PublishAsync(
                        titile,
                        new MessageNotificationData(body),
                        severity: NotificationSeverity.Info,
                        userIds: userIdentifiers
                        );

                //History
                foreach (var caseId in casesIds)
                {
                    await _historyManager.CreateAsync(
                        caseId,
                        ActionType.Call,
                        titile,
                        body);
                }
                unitOfWork.Complete();
            }
        }

        private async Task<UserIdentifier[]> GetUserIdentifiersForNotify(int caseId)
        {
            var caseUser = await GetCaseUser(caseId);
            var teamLeaderUser = await _userManager.GetTeamLeader(caseUser);
            var cordinatorsUsers = await _userManager.GetUsersInRoleAsync("Coordinator");

            var identifiers = new List<UserIdentifier>();
            identifiers.Add(caseUser.ToUserIdentifier());
            if (teamLeaderUser != null)
            {
                identifiers.Add(teamLeaderUser.ToUserIdentifier());
            }

            if (cordinatorsUsers != null)
            {
                foreach (var user in cordinatorsUsers)
                {
                    identifiers.Add(user.ToUserIdentifier());
                }
            }

            return identifiers.ToArray();
        }

        private async Task<User> GetCaseUser(int caseId)
        {
            var cas = await _caseManager.GetCaseByIdAsync(caseId);
            var userId = cas.UserId.Value.ToString();
            return await _userManager.FindByIdAsync(userId);
        }
        public async Task UpdateAndAssignAsync(Call call, int[] casesIds)
        {
            using (var unitOfWork = _unitOfWorkManager.Begin())
            {
                var updatedCall = _callRepository.Update(call);
                var caseCalls = _caseCallRepository.GetAllList(x => x.CallId == call.Id);

                await AssignNewCasses(caseCalls, casesIds);
                await UnAssignRemovedCasses(caseCalls, casesIds);

                unitOfWork.Complete();
            }
        }

        public async Task Delete(int callId)
        {
            using (var unitOfWork = _unitOfWorkManager.Begin())
            {
                var caseCalls = _caseCallRepository.GetAllList(x => x.CallId == callId);

                foreach (var caseCall in caseCalls)
                {
                    await _caseCallRepository.DeleteAsync(caseCall);
                }

                await _callRepository.DeleteAsync(callId);

                unitOfWork.Complete();
            }
        }

        #region Helper Methods
        private async Task UnAssignRemovedCasses(List<CaseCall> caseCalls, int[] casesIds)
        {
            foreach (var caseCall in caseCalls)
            {
                if (!casesIds.Contains(caseCall.CaseId))
                {
                    await _caseCallRepository.DeleteAsync(caseCall);

                }
            }
        }

        private async Task AssignNewCasses(List<CaseCall> caseCalls, int[] casesIds)
        {
            var callId = caseCalls.FirstOrDefault().CallId;

            foreach (var caseId in casesIds)
            {
                if (!caseCalls.Any(x => x.CaseId == caseId))
                {
                    await _caseCallRepository.InsertAsync(new CaseCall() { CaseId = caseId, CallId = callId });

                }
            }
        }
        #endregion

    }
}
