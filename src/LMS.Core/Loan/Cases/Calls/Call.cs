﻿using LMS.Loan.Shared;
using System;

namespace LMS.Loan.Cases.Calls
{
    public class Call : WaedAction
    {
        public string CallWith { get; set; }
        public DateTime? CallDate { get; set; }
        public DateTime? CallStart { get; set; }
        public string NegotiatorNote { get; set; }
    }
}
