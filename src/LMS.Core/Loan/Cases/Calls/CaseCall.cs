﻿using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations.Schema;

namespace LMS.Loan.Cases.Calls
{
    public class CaseCall : FullAuditedEntity
    {
        #region Case
        public int CaseId { get; set; }
        [ForeignKey("CaseId")]
        public virtual Case Case { get; set; }
        #endregion

        #region Meeting
        public int CallId { get; set; }
        [ForeignKey("CallId")]
        public virtual Call Call { get; set; }
        #endregion
    }
}
