﻿using LMS.Loan.Shared;
using System;

namespace LMS.Loan.Cases.Offers
{
    public class Offer : WaedAction
    {
        public double OfferAmount { get; set; }
        public DateTime? OfferDate { get; set; }
        public int EMIs { get; set; }
        public double FirstPayment { get; set; }
        public DateTime? FirstPaymentDate { get; set; }
        public OfferStatus OfferStatus { get; set; }
        public DateTime? ApprovalDate { get; set; }
        public string NegotiatorNote { get; set; }
        public string SuperviorNote { get; set; }
    }
}
