﻿using Abp;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Services;
using Abp.Domain.Uow;
using Abp.Notifications;
using LMS.Loan.Cases.History;
using LMS.Loan.Cases.Notes;
using LMS.Loan.Cases.PromiseToPays;
using LMS.Loan.Cases.Reminders;
using LMS.Loan.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Loan.Cases.Offers
{
    public class OfferManager : IDomainService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<Offer> _offerRepository;
        private readonly IRepository<CaseOffer> _caseOfferRepository;
        private readonly ReminderManager _reminderManager;
        private readonly PromiseToPayManager _promiseToPayManager;
        private readonly CaseManager _caseManager;
        private readonly NoteManager _noteManager;
        private readonly HistoryManager _historyManager;
        private readonly INotificationPublisher _notificationPublisher;

        public OfferManager(
            IUnitOfWorkManager unitOfWorkManager,
            IRepository<Offer> offerRepository,
            IRepository<CaseOffer> caseOfferRepository,
            ReminderManager reminderManager,
            PromiseToPayManager promiseToPayManager,
            CaseManager caseManager,
            NoteManager noteManager,
            HistoryManager historyManager,
            INotificationPublisher notificationPublisher)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _offerRepository = offerRepository;
            _caseOfferRepository = caseOfferRepository;
            _reminderManager = reminderManager;
            _promiseToPayManager = promiseToPayManager;
            _caseManager = caseManager;
            _noteManager = noteManager;
            _historyManager = historyManager;
            _notificationPublisher = notificationPublisher;
        }

        public List<Offer> GetOffersForCase(int caseId)
        {
            var caseOffer = _caseOfferRepository
                .GetAllIncluding(
                m => m.Offer
                ).Where(x => x.CaseId == caseId);

            if (!caseOffer.Any())
                return new List<Offer>();

            var offer = (from o in caseOffer
                         select o.Offer);
            return offer.OrderByDescending(x => x.Id).ToList();
        }

        public async Task<Offer> GetOfferById(int offerId)
        {
            return await _offerRepository.FirstOrDefaultAsync(offerId);
        }
        public async Task<IList<CaseOffer>> GetCaseOffersByIdAsync(int offerId)
        {
            return await _caseOfferRepository.GetAllListAsync(x => x.Offer.Id == offerId);
        }

        public IQueryable<CaseOffer> GetCaseOffersByCasesIdsAsync(int[] casesIds)
        {
            return _caseOfferRepository.GetAllIncluding(x => x.Offer)
                .Where(x => casesIds.Contains(x.CaseId));
        }

        public async Task CreateAndAssignAsync(Offer offer, int[] casesIds)
        {
            using (var unitOfWork = _unitOfWorkManager.Begin())
            {
                var offerId = _offerRepository.InsertAndGetId(offer);

                foreach (var caseId in casesIds)
                {
                    await _caseOfferRepository.InsertAsync(new CaseOffer() { CaseId = caseId, OfferId = offerId });
                    await _caseManager.ChangeStatus(caseId, CaseStatus.Offer);
                    var note = new Note();
                    var type = 0;
                    if (!string.IsNullOrEmpty(offer.NegotiatorNote))
                    {
                        //Negotiator Note
                        note = new Note()
                        {
                            NoteText = offer.NegotiatorNote
                        };
                        type = 1;

                    }
                    if (!string.IsNullOrEmpty(offer.SuperviorNote))
                    {
                        //Supervior Note
                        note = new Note()
                        {
                            NoteText = offer.SuperviorNote
                        };
                        type = 2;
                    }
                    await _noteManager.CreateAsync(
                            note,
                            caseId,
                            offerId,
                            ActionName.Offer,
                            type);

                }

                //Send Notification
                var casesIdsStr = string.Join(",", casesIds);

                var titile = $"New offer for the cases {casesIdsStr}";
                var body = $"A new offer has been created for the cases {casesIdsStr} ";
                body += offer.OfferAmount > 0 ? $"<br /> OfferAmount : {offer.OfferAmount}" : "";
                body += $"<br /> Date : {offer.OfferDate.Value.ToString("g")}" ;
                body += $"<br /> EMIs : {offer.EMIs}";

                UserIdentifier[] userIdentifiers = await _caseManager.GetUserIdentifiersForNotify(casesIds[0]);

                await _notificationPublisher.PublishAsync(
                        titile,
                        new MessageNotificationData(body),
                        severity: NotificationSeverity.Info,
                        userIds: userIdentifiers
                        );

                //History
                foreach (var caseId in casesIds)
                {
                    await _historyManager.CreateAsync(
                        caseId,
                        ActionType.Offer,
                        titile,
                        body);
                }
                unitOfWork.Complete();
            }
        }

        public async Task UpdateAndAssignAsync(Offer offer, int[] casesIds)
        {
            using (var unitOfWork = _unitOfWorkManager.Begin())
            {
                var updatedOffer = _offerRepository.Update(offer);

                var caseOffers = _caseOfferRepository.GetAllList(x => x.OfferId == offer.Id);

                await AssignNewCasses(caseOffers, casesIds);
                await UnAssignRemovedCasses(caseOffers, casesIds);
                foreach (var caseId in casesIds)
                {
                    if (!string.IsNullOrEmpty(offer.NegotiatorNote))
                    {
                        await _noteManager.UpdateTextNoteAsync(
                            offer.NegotiatorNote,
                            caseId,
                            offer.Id,
                            ActionName.Offer,
                            1);
                    }
                    if (!string.IsNullOrEmpty(offer.SuperviorNote))
                    {
                        await _noteManager.UpdateTextNoteAsync(
                            offer.SuperviorNote,
                            caseId,
                            offer.Id,
                            ActionName.Offer,
                            2);
                    }
                }

                unitOfWork.Complete();
            }
        }

        public async Task Delete(int offerId)
        {
            using (var unitOfWork = _unitOfWorkManager.Begin())
            {
                var caseOffers = _caseOfferRepository.GetAllList(x => x.OfferId == offerId);

                foreach (var caseOffer in caseOffers)
                {
                    await _caseOfferRepository.DeleteAsync(caseOffer);
                }

                await _offerRepository.DeleteAsync(offerId);

                unitOfWork.Complete();
            }
        }

        public async Task ChangeStatus(int status, int offerId, string supervisorNote)
        {
            using (var unitOfWork = _unitOfWorkManager.Begin())
            {
                var offer = await _offerRepository.FirstOrDefaultAsync(offerId);
                offer.OfferStatus = (OfferStatus)status;
                offer.SuperviorNote = supervisorNote;

                if (offer.OfferStatus == OfferStatus.Approved)
                {
                    offer.ApprovalDate = DateTime.Now;
                    var casesIds = _caseOfferRepository.GetAllList(x => x.OfferId == offer.Id)
                        .Select(x => x.CaseId).ToArray();
                    await _promiseToPayManager.Generate(offer, casesIds);
                    foreach (var caseId in casesIds)
                    {
                        await _caseManager.ChangeStatus(caseId, CaseStatus.PTP);
                        await _noteManager.UpdateTextNoteAsync(
                            offer.SuperviorNote,
                            caseId,
                            offer.Id,
                            ActionName.Offer,
                            2);
                    }

                    //Send Notification
                    var casesIdsStr = string.Join(",", casesIds);
                    var titile = $"Change status to {OfferStatus.Approved.ToString()}";
                    var body = $"Cases Status has been changed to ({OfferStatus.Approved.ToString()}) - Cases IDs({casesIdsStr})";
                    UserIdentifier[] userIdentifiers = await _caseManager.GetUserIdentifiersForNotify(casesIds[0]);

                    await _notificationPublisher.PublishAsync(
                            titile,
                            new MessageNotificationData(body),
                            severity: NotificationSeverity.Info,
                            userIds: userIdentifiers
                            );

                    //History
                    foreach (var caseId in casesIds)
                    {
                        await _historyManager.CreateAsync(
                            caseId,
                            ActionType.ChangeStatus,
                            titile,
                            body);
                    }
                }

                await _offerRepository.UpdateAsync(offer);

                unitOfWork.Complete();
            }
        }

        

        #region Helper Methods
        private async Task UnAssignRemovedCasses(List<CaseOffer> caseOffers, int[] casesIds)
        {
            foreach (var caseOffer in caseOffers)
            {
                if (!casesIds.Contains(caseOffer.CaseId))
                {
                    await _caseOfferRepository.DeleteAsync(caseOffer);

                }
            }
        }

        private async Task AssignNewCasses(List<CaseOffer> caseOffers, int[] casesIds)
        {
            var offerId = caseOffers.FirstOrDefault().OfferId;

            foreach (var caseId in casesIds)
            {
                if (!caseOffers.Any(x => x.CaseId == caseId))
                {
                    await _caseOfferRepository.InsertAsync(new CaseOffer() { CaseId = caseId, OfferId = offerId });

                }
            }
        }
        #endregion

    }
}
