﻿
namespace LMS.Loan.Cases.Offers
{
    public enum OfferStatus
    {
        Pending,
        Approved,
        Declined,
        Paid,
        Cancelled
    }
}
