﻿using Abp.Domain.Entities.Auditing;
using LMS.Authorization.Users;
using LMS.Loan.Indexes;
using LMS.Loan.Shared;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace LMS.Loan.Cases.Payments
{
    public class Payment: WaedAction
    {
        /// <summary>
        /// رقم الايصال
        /// </summary>
        public string ReceiptNo { get; set; }
        /// <summary>
        /// مكان الدفعة
        /// </summary>
        public string PlaceOfPayment { get; set; }
        /// <summary>
        /// المبلغ
        /// </summary>
        public double Amount { get; set; }
        /// <summary>
        /// المبلغ بالدرهم الاماراتي
        /// </summary>
        public double AmountByAED { get; set; }
        /// <summary>
        /// حالة تحويل الحوالة
        /// </summary>
        public TransferredStatus TransferredStatus { get; set; }
        /// <summary>
        /// تاريخ تحويل الحوالة
        /// </summary>
        public DateTime? TransferredDate { get; set; }
        /// <summary>
        /// تاريخ استلام الدفعة من قبل البنك 
        /// </summary>
        public DateTime? ReflectionDate { get; set; }
        /// <summary>
        /// حالة اقفال الدفعة 
        /// </summary>
        public PaymentClaimedStatus ClaimedStatus { get; set; }
        /// <summary>
        /// تاريخ اقفال الدفعة 
        /// </summary>
        public DateTime? ClaimedDate { get; set; }
        /// <summary>
        /// النسبة المقدمة من البنك
        /// </summary>
        public decimal BankRatio { get; set; }
        /// <summary>
        /// نسبة الوكيل
        /// </summary>
        public decimal AgentRatio { get; set; }
        /// <summary>
        /// نسبة العمولة 
        /// </summary>
        public decimal CommissionRatio { get; set; }
        /// <summary>
        /// مبلغ العمولة
        /// </summary>
        public double CommissionAmount { get; set; }
        /// <summary>
        /// مبلغ العمولة بالدينار
        /// </summary>
        public double CommissionAmountAED { get; set; }
        /// <summary>
        /// مبلغ العمولة بالدولار
        /// </summary>
        public double CommissionAmountUSD { get; set; }
        /// <summary>
        /// حالة استلام عمولة الدفعة
        /// </summary>
        public PaymentReceivedStatus ReceivedStatus { get; set; }
        /// <summary>
        /// تاريخ استلام عمولة الدفعة
        /// </summary>
        public DateTime? ReceivedDate { get; set; }

        #region العملة
        public int? CurrencyId { get; set; }
        [ForeignKey("CurrencyId")]
        public virtual Currency Currency { get; set; }
        #endregion

        #region اسم المفاوض الذي حصل الدفعة
        public long? CollectedById { get; set; }
        [ForeignKey("CollectedById")]
        public virtual User CollectedBy { get; set; }
        #endregion

    }
}
