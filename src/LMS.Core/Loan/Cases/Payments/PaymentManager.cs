﻿using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Services;
using Abp.Domain.Uow;
using LMS.Loan.Cases.Reminders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Loan.Cases.Payments
{
    public class PaymentManager : IDomainService, ITransientDependency
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<Payment> _paymentRepository;
        private readonly IRepository<CasePayment> _casePaymentRepository;
        private readonly CaseManager _caseManager;
        public PaymentManager(
            IUnitOfWorkManager unitOfWorkManager,
            IRepository<Payment> paymentRepository,
            IRepository<CasePayment> casePaymentRepository,
            CaseManager caseManager
            )
        {
            _unitOfWorkManager = unitOfWorkManager;
            _paymentRepository = paymentRepository;
            _casePaymentRepository = casePaymentRepository;
            _caseManager = caseManager;
        }

        public List<Payment> GetPaymentsForCase(int caseId)
        {
            var casePayment = _casePaymentRepository
                .GetAllIncluding(
                m => m.Payment
                ).Where(x => x.CaseId == caseId);

            if (!casePayment.Any())
                return new List<Payment>();

            var payment = (from o in casePayment
                         select o.Payment);
            return payment.OrderByDescending(x => x.Id).ToList();
        }

        public async Task<Payment> GetPaymentById(int paymentId)
        {
            return await _paymentRepository.FirstOrDefaultAsync(paymentId);
        }
        public async Task<IList<CasePayment>> GetCasePaymentsById(int paymentId)
        {
            return await _casePaymentRepository.GetAllListAsync(x => x.Payment.Id == paymentId);
        }

        public async Task CreateAndAssignAsync(Payment payment, int[] casesIds)
        {
            using (var unitOfWork = _unitOfWorkManager.Begin())
            {
                var paymentId = _paymentRepository.InsertAndGetId(payment);

                foreach (var caseId in casesIds)
                {
                    await _caseManager.ChangeStatus(caseId, CaseStatus.Paid);
                    await _casePaymentRepository.InsertAsync(new CasePayment() { CaseId = caseId, PaymentId = paymentId });
                }

                unitOfWork.Complete();
            }
        }

        public async Task UpdateAndAssignAsync(Payment payment, int[] casesIds)
        {
            using (var unitOfWork = _unitOfWorkManager.Begin())
            {
                var updatedPayment = _paymentRepository.Update(payment);

                var casePayments = _casePaymentRepository.GetAllList(x => x.PaymentId == payment.Id);

                await AssignNewCasses(casePayments, casesIds);
                await UnAssignRemovedCasses(casePayments, casesIds);

                unitOfWork.Complete();
            }
        }

        public async Task Delete(int paymentId)
        {
            using (var unitOfWork = _unitOfWorkManager.Begin())
            {
                var casePayments = _casePaymentRepository.GetAllList(x => x.PaymentId == paymentId);

                foreach (var casePayment in casePayments)
                {
                    await _casePaymentRepository.DeleteAsync(casePayment);
                }

                await _paymentRepository.DeleteAsync(paymentId);

                unitOfWork.Complete();
            }
        }


        #region Helper Methods
        private async Task UnAssignRemovedCasses(List<CasePayment> casePayments, int[] casesIds)
        {
            foreach (var casePayment in casePayments)
            {
                if (!casesIds.Contains(casePayment.CaseId))
                {
                    await _casePaymentRepository.DeleteAsync(casePayment);

                }
            }
        }

        private async Task AssignNewCasses(List<CasePayment> casePayments, int[] casesIds)
        {
            var paymentId = casePayments.FirstOrDefault().PaymentId;

            foreach (var caseId in casesIds)
            {
                if (!casePayments.Any(x => x.CaseId == caseId))
                {
                    await _casePaymentRepository.InsertAsync(new CasePayment() { CaseId = caseId, PaymentId = paymentId });

                }
            }
        }
        #endregion

    }
}
