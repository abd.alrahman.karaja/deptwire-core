﻿using Abp.Domain.Entities.Auditing;
using LMS.Loan.Cases.Payments;
using System.ComponentModel.DataAnnotations.Schema;

namespace LMS.Loan.Cases
{
    public class CasePayment : FullAuditedEntity
    {
        #region Case
        public int CaseId { get; set; }
        [ForeignKey("CaseId")]
        public virtual Case Case { get; set; }
        #endregion

        #region Payment
        public int PaymentId { get; set; }
        [ForeignKey("PaymentId")]
        public virtual Payment Payment { get; set; }
        #endregion
    }
}
