﻿using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations.Schema;

namespace LMS.Loan.Cases.Legals
{
    public class CaseLegal: FullAuditedEntity
    {
        #region Case
        public int CaseId { get; set; }
        [ForeignKey("CaseId")]
        public virtual Case Case { get; set; }
        #endregion

        #region Legal
        public int LegalId { get; set; }
        [ForeignKey("LegalId")]
        public virtual Legal Legal { get; set; }
        #endregion
    }
}
