﻿using Abp.Domain.Entities.Auditing;
using LMS.Authorization.Users;
using System.ComponentModel.DataAnnotations.Schema;

namespace LMS.Loan.Cases.Legals
{
    public class Legal : FullAuditedEntity
    {
        public LegalStatus LegalStatus { get; set; }
        public string LegalNote { get; set; }
        public string SupervisorNote { get; set; }

        [ForeignKey("CreatorUserId")]
        public virtual User CreatorUser { get; set; }
    }
}
