﻿namespace LMS.Loan.Cases.Legals
{
    public enum LegalStatus
    {
        NotStart,
        WarningLetter,
        LawsuitLetter,
        NotaryWarningLetter,
        RequestOSC,
        ReceivedOSC,
        OSCDeliveredToCustomer,
        OSCReturnedToBank
    }
}
