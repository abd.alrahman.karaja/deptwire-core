﻿using Abp.Domain.Repositories;
using Abp.Domain.Services;
using Abp.Domain.Uow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Loan.Cases.Legals
{
    public class LegalManager : IDomainService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly CaseManager _caseManager;
        private readonly IRepository<Legal> _legalRepository;
        private readonly IRepository<CaseLegal> _caseLegalRepository;


        public LegalManager(
            IUnitOfWorkManager unitOfWorkManager,
            CaseManager caseManager,
            IRepository<Legal> legalRepository,
            IRepository<CaseLegal> caseLegalRepository
            )
        {
            _unitOfWorkManager = unitOfWorkManager;
            _legalRepository = legalRepository;
            _caseLegalRepository = caseLegalRepository;
            _caseManager = caseManager;
        }

        public List<Legal> GetLegalsForCase(int caseId)
        {
            var caseLegal = _caseLegalRepository
                .GetAllIncluding(
                    m => m.Legal,
                    cu => cu.Legal.CreatorUser
                ).Where(x => x.CaseId == caseId);

            if (!caseLegal.Any())
                return new List<Legal>();

            var legal = (from o in caseLegal
                        select o.Legal);
            return legal.OrderByDescending(x => x.Id).ToList();
        }

        public async Task<Legal> GetLegalById(int legalId)
        {
            return await _legalRepository.FirstOrDefaultAsync(legalId);
        }
        public async Task<IList<CaseLegal>> GetCaseLegalsByIdAsync(int legalId)
        {
            return await _caseLegalRepository.GetAllListAsync(x => x.Legal.Id == legalId);
        }

        public IQueryable<CaseLegal> GetCaseLegalsByCasesIdsAsync(int[] casesIds)
        {
            return _caseLegalRepository.GetAllIncluding(x => x.Legal)
                .Where(x => casesIds.Contains(x.CaseId));
        }

        public async Task CreateAndAssignAsync(Legal legal, int[] casesIds)
        {
            using (var unitOfWork = _unitOfWorkManager.Begin())
            {
                var legalId = _legalRepository.InsertAndGetId(legal);

                foreach (var caseId in casesIds)
                {
                    await _caseLegalRepository.InsertAsync(new CaseLegal() { CaseId = caseId, LegalId = legalId });
                }

                unitOfWork.Complete();
            }
        }

        public async Task<CaseLegal> CreateAsync(Legal legal, int caseId)
        {
            var legalId = _legalRepository.InsertAndGetId(legal);
            return await _caseLegalRepository.InsertAsync(new CaseLegal() { CaseId = caseId, LegalId = legalId });
        }

        public async Task<bool> ChangeLegalStatusAsync(
            int id, 
            int legalStatus,
            string legalNote)
        {
            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    var legal = await _legalRepository.FirstOrDefaultAsync(id);
                    legal.LegalNote = legalNote;
                    legal.LegalStatus = (LegalStatus)legalStatus;

                    //NotNot اشعار إلى المفاوض والمنسق لاعلامه انه تم تغيير الحالة 
                    unitOfWork.Complete();
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }
    }
}

