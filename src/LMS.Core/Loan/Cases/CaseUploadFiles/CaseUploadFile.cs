﻿using LMS.Loan.Shared;

namespace LMS.Loan.Cases.CaseUploadFiles
{
    public class CaseUploadFile : UploadFileBase
    {
        public int? CaseId { get; set; }
        public Case Case { get; set; }
    }
}
