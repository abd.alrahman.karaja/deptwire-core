﻿using Abp.Domain.Entities.Auditing;
using LMS.Authorization.Users;
using LMS.Loan.Agents;
using LMS.Loan.Banks;
using LMS.Loan.Cases.Calls;
using LMS.Loan.Cases.Legals;
using LMS.Loan.Cases.Meetings;
using LMS.Loan.Cases.Notes;
using LMS.Loan.Cases.Offers;
using LMS.Loan.Customers;
using LMS.Loan.Enums;
using LMS.Loan.Indexes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace LMS.Loan.Cases
{
    public class Case : FullAuditedEntity
    {
        public Case()
        {
            CasePromisesToPays = new List<CasePromiseToPay>();
            CasePayments = new List<CasePayment>();
            CaseOffers = new List<CaseOffer>();
            CaseCalls = new List<CaseCall>();
            CaseMeetings = new List<CaseMeeting>();
            CaseLegals = new List<CaseLegal>();
            CaseNotes = new List<CaseNote>();
        }

        /// <summary>
        /// رقم حساب المنتج
        /// </summary>
        public string AccountNo { get; set; }

        /// <summary>
        /// رقم العقد المنتج الموقع لبنك الراجحي
        /// </summary>
        public string ContractNo { get; set; }

        /// <summary>
        /// رقم الحساب الكامل
        /// </summary>
        public string FullAccount { get; set; }

        /// <summary>
        /// نوع المساعده في حال كان منتج ABF  لبنك راك 
        /// </summary>
        public string TypeOfAssist { get; set; }

        /// <summary>
        /// اسم الشركة المصنعة في حال كان المنتج سيارة
        /// </summary>
        public string Manufacturer { get; set; }

        /// <summary>
        /// أصل مبلغ المديونية
        /// </summary>
        public OriginalAmount OriginalAmount { get; set; }

        /// <summary>
        /// أصل المبلغ مضاف اليه ربح البنك ( المبلغ المطلوب من قبل البنك ) ينتهي عند نهاية التعامل ( الراجحي )
        /// </summary>
        public double TotalAmount { get; set; }

        /// <summary>
        /// المبلغ المطالب به لتاريخه مع ربح البنك وغرامات التأخير بالاضافة للفوائد العقدية
        /// </summary>
        public double Outstanding { get; set; }

        /// <summary>
        /// المبلغ المطالب به لتاريخه مع ربح البنك
        /// </summary>
        public double PrincipalAmount { get; set; }

        /// <summary>
        /// اخر حد ائتماني للبطاقة ( ENBD )
        /// </summary>
        public double LastCreditLimit { get; set; }

        /// <summary>
        /// قيمة الرصيد الموجود في الحساب لصالح العميل ان وجد و تخص بنك الراجحي
        /// </summary>
        public double MainLedgerBalance { get; set; }

        /// <summary>
        /// قيمة القسط الشهري
        /// </summary>
        public double InstallmentAmount { get; set; }

        /// <summary>
        /// مرحلة الاقساط المتأخره
        /// </summary>
        public string BucketStage { get; set; }

        /// <summary>
        /// مجموع المتأخرات المستحقة لتاريخه ( تضاف في حال راك بكت او الخليج الكويتي )
        /// </summary>
        public double Overdues { get; set; }

        /// <summary>
        /// اقل دفعة مطلوبة في حالة البكت لبنك RAK
        /// </summary>
        public double MinPayment { get; set; }

        /// <summary>
        /// تاريخ قدم الدين اي تاريخ الوصول الى مرحلة Recovery
        /// </summary>
        public DateTime? WriteOffDate { get; set; }

        /// <summary>
        /// فئة سنين تاريخ قدم الدين مثال: 1-5 سنين
        /// </summary>
        public string WriteOffAge { get; set; }

        /// <summary>
        /// عدد ايام التأخير
        /// </summary>
        public int DPD { get; set; }

        /// <summary>
        /// تاريخ بداية التعامل
        /// </summary>
        public DateTime? OpenDate { get; set; }

        /// <summary>
        /// تاريخ نهاية التعامل
        /// </summary>
        public DateTime? CloseDate { get; set; }

        /// <summary>
        /// قيمة اخر دفعة
        /// </summary>
        public double? LastPaymentAmount { get; set; }

        /// <summary>
        /// تاريخ اخر دفعة
        /// </summary>
        public DateTime? LastPaymentDate { get; set; }

        /// <summary>
        /// اخر قيمة مشتريات بالبطاقة
        /// </summary>
        public double? LastPurchaseAmount { get; set; }

        /// <summary>
        /// تاريخ اخر عملية شراء بالبطاقة
        /// </summary>
        public DateTime? LastPurchaseDate { get; set; }

        /// <summary>
        /// الرقم التعريفي للعميل لدى البنك
        /// </summary>
        public string CIF { get; set; }

        /// <summary>
        /// جهة عمل الزبون اثناء الحصول على المديونية
        /// </summary>
        public string CompanyName { get; set; }

        #region Police Info

        /// <summary>
        /// قيمة شيك البلاغ
        /// </summary>
        public string PoliceCaseAmount { get; set; }

        /// <summary>
        /// /// <summary>
        /// اسم مركز الشرطة
        /// </summary>
        /// </summary>
        public string PoliceStation { get; set; }

        /// <summary>
        /// حالة قضية الشرطة في البنك
        /// </summary>
        public int? PoliceCaseStatusId { get; set; }
        public virtual PoliceCaseStatus PoliceCaseStatus { get; set; }

        /// <summary>
        /// تاريخ / رقم القضية
        /// </summary>
        public string PoliceCaseNo { get; set; }

        /// <summary>
        /// تاريخ القضية المفصل
        /// </summary>
        public DateTime? PoliceCaseDate { get; set; }

        /// <summary>
        /// الحكم النهائي
        /// </summary>
        public string FinalJugement { get; set; }
        #endregion

        #region Waed Info

        /// <summary>
        /// تاريخ تخصيص القضية لأول مرة من قبل البنك
        /// </summary>
        public DateTime? BookingDate { get; set; }

        /// <summary>
        /// اخر تاريخ خصصت فيه هذه القضية 
        /// </summary>
        public DateTime? AllocationDate { get; set; }

        /// <summary>
        /// وضع او حالة التخصيص
        /// </summary>
        public ModeOfAllocation ModeOfAllocation { get; set; }

        /// <summary>
        /// هل القضية نشطة او فعالة هذا الشهر او غير نشطة بناء على تخصيص البنك
        /// </summary>
        public bool Activity { get; set; }

        /// <summary>
        /// الفيدباك الخاص بالوعد
        /// </summary>
        public string WaedFeedback { get; set; }

        /// <summary>
        /// الفيدباك الخاص بالبنك
        /// </summary>
        public string BankFeedback { get; set; }

        /// <summary>
        /// فيدباك باللغة العربية خاص ببنك الراجحي
        /// </summary>
        public string ArabicFeedback { get; set; }

        /// <summary>
        /// الاحتفاظ بالقضية او ارجاعها للبنك
        /// </summary>
        public bool KeepBackToBank { get; set; }

        /// <summary>
        /// ملاحظات اضافية للمفاوض للقضيه نفسها
        /// </summary>
        public string NegotiatorNote { get; set; }

        /// <summary>
        /// ملاحظة اضافية لقائد الفريق او المنسقة او الادارة لنفس القضية
        /// </summary>
        public string SupervisorNote { get; set; }

        /// <summary>
        /// ايميل مطالبة بالمديونية
        /// </summary>
        public LiabilityMail LiabilityMail { get; set; }

        /// <summary>
        /// ورقيات
        /// </summary>
        public DOX DOX { get; set; }

        /// <summary>
        /// براءة الذمة 
        /// </summary>
        public string NOC { get; set; }

        /// <summary>
        /// حالة الدورة
        /// </summary>

        public virtual CaseStatus WaedStatus { get; set; }

        /// <summary>
        /// حالة الدورة من طرف البنك
        /// </summary>
        public virtual CaseStatus BankStatus { get; set; }
        #endregion

        #region Reference

        #region Agent
        public int AgentId { get; set; }
        [ForeignKey("AgentId")]
        public virtual Agent Agent { get; set; }
        #endregion

        #region Bank
        public int? BankId { get; set; }
        [ForeignKey("BankId")]
        public virtual Bank Bank { get; set; }
        #endregion

        #region Bank Home
        public int? BankHomeId { get; set; }
        [ForeignKey("BankHomeId")]
        public virtual Country BankHome { get; set; }
        #endregion

        #region Customer
        public int CustomerId { get; set; }
        [ForeignKey("CustomerId")]
        public virtual Customer Customer { get; set; }
        #endregion

        #region Product
        public int ProductId { get; set; }
        [ForeignKey("ProductId")]
        public virtual Product Product { get; set; }
        #endregion

        #region Bank Code
        public int? BankCodeId { get; set; }
        [ForeignKey("BankCodeId")]
        public virtual BankCode BankCode { get; set; }
        #endregion
        
        #region User
        public long? UserId { get; set; }

        [ForeignKey("UserId")]
        public virtual User User { get; set; }
        #endregion

        #endregion

        #region Lists
        public virtual IList<CasePromiseToPay> CasePromisesToPays { get; set; }
        public virtual IList<CasePayment> CasePayments { get; set; }
        public virtual IList<CaseOffer> CaseOffers { get; set; }
        public virtual IList<CaseCall> CaseCalls { get; set; }
        public virtual IList<CaseMeeting> CaseMeetings { get; set; }
        public virtual IList<CaseLegal> CaseLegals { get; set; }
        public virtual IList<CaseNote> CaseNotes { get; set; }
        #endregion
    }
}
