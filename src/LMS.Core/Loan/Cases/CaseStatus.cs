﻿namespace LMS.Loan.Cases
{
    public enum CaseStatus
    {
        None,
        Blank,
        UnderTracing,
        Retracing,
        ThirdPartyContact,
        Contacted,
        UnderNegotiation,
        Offer,
        Approval,
        Declined,
        PTP,
        Paid,
        Settled,
        AlreadySettled,
        BrokenPromise,
        RefusedToPay,
        Dead,
        ClosedAndReturned
    }
}
