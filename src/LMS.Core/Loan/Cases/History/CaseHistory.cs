﻿using Abp.Domain.Entities.Auditing;
using LMS.Authorization.Users;
using LMS.Loan.Enums;
using System.ComponentModel.DataAnnotations.Schema;

namespace LMS.Loan.Cases.History
{
    public class CaseHistory: FullAuditedEntity
    {
        public ActionType ActionType { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int ActionId { get; set; }

        #region Case
        public int CaseId { get; set; }
        [ForeignKey("CaseId")]
        public virtual Case Case { get; set; }
        #endregion

        #region CreatorUser
        [ForeignKey("CreatorUserId")]
        public virtual User CreatorUser { get; set; }
        #endregion
    }
}
