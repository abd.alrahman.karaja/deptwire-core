﻿using Abp.Domain.Repositories;
using Abp.Domain.Services;
using LMS.Loan.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Loan.Cases.History
{
    public class HistoryManager : IDomainService
    {
        private readonly IRepository<CaseHistory> _historyRepository;

        public HistoryManager(IRepository<CaseHistory> historyRepository)
        {
            _historyRepository = historyRepository;
        }

        public async Task CreateAsync(
            int caseId,
            ActionType actionType,
            string name,
            string description,
            int? actionId = null)
        {
            var history = new CaseHistory()
            {
                CaseId = caseId,
                ActionType = actionType,
                Name = name,
                Description = description,
                ActionId = actionId != null ? actionId.Value : caseId,
            };

            await _historyRepository.InsertAsync(history);
        }

        public List<CaseHistory> GetHistoryForCase(int caseId)
        {
            var histories = _historyRepository
                .GetAllIncluding(
                    cu => cu.CreatorUser
                ).Where(x => x.CaseId == caseId);

            if (histories == null || !histories.Any())
                return new List<CaseHistory>();

            return histories.OrderByDescending(x=>x.Id).ToList();
        }

        public List<CaseHistory> GetHistoryForUser(long userId,int numberOfMonth)
        {
            var date = DateTime.Now.Date.AddMonths(-numberOfMonth);
            var histories = _historyRepository
                .GetAllIncluding(
                    cu => cu.CreatorUser
                ).Where(x =>
                x.CreatorUserId == userId &&
                x.CreationTime > date);

            if (histories == null || !histories.Any())
                return new List<CaseHistory>();

            return histories.OrderByDescending(x => x.Id).ToList();
        }
    }
}
