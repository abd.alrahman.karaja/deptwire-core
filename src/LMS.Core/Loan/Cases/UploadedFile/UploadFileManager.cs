﻿using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Services;
using Abp.Domain.Uow;
using LMS.Loan.Cases.PTP;
using LMS.Loan.Cases.Reminders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Loan.Cases.UploadedFiles
{
    public class UploadFileManager : IDomainService, ITransientDependency
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<UploadedFile> _uploadedFileRepository;
        private readonly IRepository<CaseUploadedFile> _caseUploadedFileRepository;
        private readonly ReminderManager _reminderManager;
        public UploadFileManager(
            IUnitOfWorkManager unitOfWorkManager,
            IRepository<UploadedFile>  uploadedFileRepository,
            IRepository<CaseUploadedFile> caseUploadedFileRepository,
            ReminderManager reminderManager
            )
        {
            _unitOfWorkManager = unitOfWorkManager;
            _uploadedFileRepository = uploadedFileRepository;
            _caseUploadedFileRepository = caseUploadedFileRepository;
            _reminderManager = reminderManager;
        }

        public List<UploadedFile> GetUploadedFilesForCase(int caseId)
        {
            var caseUploadedFile = _caseUploadedFileRepository
                .GetAllIncluding(
                m => m.UploadedFile
                ).Where(x => x.CaseId == caseId);

            if (!caseUploadedFile.Any())
                return new List<UploadedFile>();

            var uploadedFiles = (from o in caseUploadedFile
                           select o.UploadedFile);
            return uploadedFiles.OrderByDescending(x => x.Id).ToList();
        }

        public async Task<UploadedFile> GetUploadedFileById(int uploadedFileId)
        {
            return await _uploadedFileRepository.FirstOrDefaultAsync(uploadedFileId);
        }
        public async Task<IList<CaseUploadedFile>> GetCaseUploadedFilesById(int uploadedFileId)
        {
            return await _caseUploadedFileRepository.GetAllListAsync(x => x.UploadedFile.Id == uploadedFileId);
        }

        public async Task CreateAndAssignAsync(UploadedFile payment, int[] casesIds)
        {
            using (var unitOfWork = _unitOfWorkManager.Begin())
            {
                var uploadedFileId = _uploadedFileRepository.InsertAndGetId(payment);

                foreach (var caseId in casesIds)
                {
                    await _caseUploadedFileRepository.InsertAsync(new CaseUploadedFile() { CaseId = caseId, UploadedFileId = uploadedFileId });
                }

                unitOfWork.Complete();
            }
        }

        public async Task UpdateAndAssignAsync(UploadedFile payment, int[] casesIds)
        {
            using (var unitOfWork = _unitOfWorkManager.Begin())
            {
                var updatedUploadedFile = _uploadedFileRepository.Update(payment);

                var caseUploadedFiles = _caseUploadedFileRepository.GetAllList(x => x.UploadedFileId == payment.Id);

                await AssignNewCasses(caseUploadedFiles, casesIds);
                await UnAssignRemovedCasses(caseUploadedFiles, casesIds);

                unitOfWork.Complete();
            }
        }

        public async Task Delete(int uploadedFileId)
        {
            using (var unitOfWork = _unitOfWorkManager.Begin())
            {
                var caseUploadedFiles = _caseUploadedFileRepository.GetAllList(x => x.UploadedFileId == uploadedFileId);

                foreach (var caseUploadedFile in caseUploadedFiles)
                {
                    await _caseUploadedFileRepository.DeleteAsync(caseUploadedFile);
                }

                await _uploadedFileRepository.DeleteAsync(uploadedFileId);

                unitOfWork.Complete();
            }
        }


        #region Helper Methods
        private async Task UnAssignRemovedCasses(List<CaseUploadedFile> caseUploadedFiles, int[] casesIds)
        {
            foreach (var caseUploadedFile in caseUploadedFiles)
            {
                if (!casesIds.Contains(caseUploadedFile.CaseId))
                {
                    await _caseUploadedFileRepository.DeleteAsync(caseUploadedFile);

                }
            }
        }

        private async Task AssignNewCasses(List<CaseUploadedFile> caseUploadedFiles, int[] casesIds)
        {
            var uploadedFileId = caseUploadedFiles.FirstOrDefault().UploadedFileId;

            foreach (var caseId in casesIds)
            {
                if (!caseUploadedFiles.Any(x => x.CaseId == caseId))
                {
                    await _caseUploadedFileRepository.InsertAsync(new CaseUploadedFile() { CaseId = caseId, UploadedFileId = uploadedFileId });

                }
            }
        }
        #endregion

    }
}
