﻿using Abp.Domain.Entities.Auditing;
using LMS.Loan.Enums;
using LMS.Loan.Shared;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace LMS.Loan.Cases.UploadedFiles
{
    public class UploadedFile : FullAuditedEntity
    {
        /// <summary>
        /// الاسم
        /// </summary>
        public string Name{ get; set; }

        /// <summary>
        /// تاريخ 
        /// </summary>
        public DateTime? UploadDate{ get; set; }

        /// <summary>
        /// نوع الملف
        /// </summary>
        public FileType Type { get; set; }


        /// <summary>
        /// ملاحظات 
        /// </summary>
        public string Note { get; set; }

        /// <summary>
        /// المسار 
        /// </summary>
        public string Path { get; set; }


    }
}
