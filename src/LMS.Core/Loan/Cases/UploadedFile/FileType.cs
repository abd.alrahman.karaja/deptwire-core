﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Loan.Cases.UploadedFiles
{
    public enum FileType
    {
        Legal,
        CustomerInfo,
        Other
    }
}
