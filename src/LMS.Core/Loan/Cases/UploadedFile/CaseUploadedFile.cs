﻿using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations.Schema;

namespace LMS.Loan.Cases.UploadedFiles
{
    public class CaseUploadedFile : FullAuditedEntity
    {
        #region Case
        public int CaseId { get; set; }
        [ForeignKey("CaseId")]
        public virtual Case Case { get; set; }
        #endregion

        #region PromiseToPay
        public int UploadedFileId { get; set; }
        [ForeignKey("UploadedFileId")]
        public virtual UploadedFile UploadedFile { get; set; }
        #endregion
    }
}
