﻿using Abp.Domain.Entities.Auditing;
using LMS.Loan.Cases.Offers;
using LMS.Loan.Enums;
using LMS.Loan.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace LMS.Loan.Cases.PTP
{
    public class PromiseToPay : WaedAction
    {
        public PromiseToPay()
        {
            PtpPayments = new List<PromiseToPayPayment>();
        }
        /// <summary>
        /// مبلغ التسوية
        /// </summary>
        public double SettlementAmount{ get; set; }

        /// <summary>
        /// تاريخ الدفعة
        /// </summary>
        public DateTime? SettlementDate{ get; set; }

        /// <summary>
        /// عدد اقساط التسوية
        /// </summary>
        public int EMIS { get; set; }
        
        public string NegotiatorNote { get; set; }
        public string SuperviorNote { get; set; }

        #region Offer
        public int? OfferId { get; set; }
        [ForeignKey("OfferId")]
        public virtual Offer Offer { get; set; }
        #endregion

        public virtual IList<PromiseToPayPayment> PtpPayments { get; set; }
    }
}
