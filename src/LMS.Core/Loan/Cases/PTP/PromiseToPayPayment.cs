﻿using Abp.Domain.Entities.Auditing;
using LMS.Loan.Enums;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace LMS.Loan.Cases.PTP
{
    public class PromiseToPayPayment : FullAuditedEntity
    {
        /// <summary>
        /// رقم الدفعة
        /// </summary>
        public int PaymentNo { get; set; }
        /// <summary>
        /// رقم الدفعة بالتسوية و في حال كانت دفعة جزئية بدون تسوية لا يوضع رقم
        /// </summary>
        public ModeOfPayment ModeOfPayment { get; set; }

        /// <summary>
        /// مبلغ الدفعة
        /// </summary>
        public double PtpAmount { get; set; }

        /// <summary>
        /// تاريخ الدفعة
        /// </summary>
        public DateTime? PaymentDate { get; set; }

        /// <summary>
        /// الدفعة المحصلة
        /// </summary>
        public double CollectedPayment { get; set; }

        /// <summary>
        /// تأكيد الدفعة من قبل البنك
        /// </summary>
        public bool BankConfirmation { get; set; }

        /// <summary>
        /// ملخص التسوية
        /// </summary>
        public string Note { get; set; }

        public string NegotiatorNote { get; set; }
        public string SuperviorNote { get; set; }

        #region Promise To Pay
        public int? PromiseToPayId { get; set; }
        [ForeignKey("PromiseToPayId")]
        public virtual PromiseToPay PromiseToPay { get; set; }
        #endregion
    }
}
