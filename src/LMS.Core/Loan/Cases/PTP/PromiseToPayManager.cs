﻿using Abp;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Services;
using Abp.Domain.Uow;
using Abp.Notifications;
using LMS.Loan.Cases.History;
using LMS.Loan.Cases.Notes;
using LMS.Loan.Cases.Offers;
using LMS.Loan.Cases.PTP;
using LMS.Loan.Cases.Reminders;
using LMS.Loan.Enums;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Loan.Cases.PromiseToPays
{
    public class PromiseToPayManager : IDomainService, ITransientDependency
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<PromiseToPay> _ptpRepository;
        private readonly IRepository<PromiseToPayPayment> _paymentRepository;
        private readonly IRepository<CasePromiseToPay> _casePromiseToPayRepository;
        private readonly ReminderManager _reminderManager;
        private readonly CaseManager _caseManager;
        private readonly NoteManager _noteManager;
        private readonly HistoryManager _historyManager;
        private readonly INotificationPublisher _notificationPublisher;

        public PromiseToPayManager(IUnitOfWorkManager unitOfWorkManager, IRepository<PromiseToPay> ptpRepository, IRepository<PromiseToPayPayment> paymentRepository, IRepository<CasePromiseToPay> casePromiseToPayRepository, ReminderManager reminderManager, CaseManager caseManager, NoteManager noteManager, HistoryManager historyManager, INotificationPublisher notificationPublisher)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _ptpRepository = ptpRepository;
            _paymentRepository = paymentRepository;
            _casePromiseToPayRepository = casePromiseToPayRepository;
            _reminderManager = reminderManager;
            _caseManager = caseManager;
            _noteManager = noteManager;
            _historyManager = historyManager;
            _notificationPublisher = notificationPublisher;
        }

        public List<PromiseToPay> GetPromiseToPaysForCase(int caseId)
        {
            var casePromiseToPay = _casePromiseToPayRepository
                .GetAllIncluding(x => x.PromiseToPay)
                .Include(x => x.PromiseToPay).ThenInclude(x => x.PtpPayments)
                .Where(x => x.CaseId == caseId);

            if (!casePromiseToPay.Any())
                return new List<PromiseToPay>();

            var ptps = (from o in casePromiseToPay
                           select o.PromiseToPay);
            return ptps.OrderByDescending(x => x.Id).ToList();
        }

        public async Task<PromiseToPay> GetPromiseToPayById(int paymentId)
        {
            return await _ptpRepository.FirstOrDefaultAsync(paymentId);
        }
        public async Task<IList<CasePromiseToPay>> GetCasePromiseToPaysById(int ptpId)
        {
            return await _casePromiseToPayRepository.GetAllListAsync(x => x.PromiseToPay.Id == ptpId);
        }

        public async Task<IList<PromiseToPayPayment>> GetPaymentsByPtpId(int ptpId)
        {
            return await _paymentRepository.GetAllListAsync(x => x.PromiseToPayId == ptpId);
        }

        public async Task CreateAndAssignAsync(PromiseToPay payment, int[] casesIds)
        {
            using (var unitOfWork = _unitOfWorkManager.Begin())
            {
                var paymentId = _ptpRepository.InsertAndGetId(payment);

                
                foreach (var caseId in casesIds)
                {
                    await _casePromiseToPayRepository.InsertAsync(new CasePromiseToPay() { CaseId = caseId, PromiseToPayId = paymentId });
                }

                unitOfWork.Complete();
            }
        }

        public async Task Generate(Offer offer , int[] casesIds)
        {
            var ptp = new PromiseToPay();
            ptp.OfferId = offer.Id;
            ptp.EMIS = offer.EMIs;
            if (offer.EMIs == 0)
            {
                ptp.SettlementAmount = offer.OfferAmount;
                ptp.SettlementDate = offer.ApprovalDate;

                var ptpPayment = new PromiseToPayPayment()
                {
                    PtpAmount = offer.FirstPayment,
                    PaymentNo = 1,
                    PaymentDate = offer.FirstPaymentDate,
                    ModeOfPayment = ModeOfPayment.PartialPayment
                };
                ptp.PtpPayments.Add(ptpPayment);
            }
            else
            {
                ptp.SettlementAmount = offer.OfferAmount;
                ptp.SettlementDate = offer.ApprovalDate;

                ptp.PtpPayments.Add(new PromiseToPayPayment()
                {
                    PtpAmount = offer.FirstPayment,
                    PaymentNo = 1,
                    PaymentDate = offer.FirstPaymentDate,
                    ModeOfPayment = offer.EMIs == 1 ? ModeOfPayment.FullPayment : ModeOfPayment.MonthlyPayment
                });

                if(offer.EMIs > 1)
                {
                    var remainingAmount = Math.Round(offer.OfferAmount - offer.FirstPayment);
                    var count = offer.EMIs - 1;
                    var payment = remainingAmount / (count);
                    for(int i = 1; i <= count; i++)
                    {
                        var date = offer.FirstPaymentDate.Value.AddMonths(i);
                        ptp.PtpPayments.Add(new PromiseToPayPayment()
                        {
                            PtpAmount = payment,
                            PaymentNo = i + 1,
                            PaymentDate = date,
                            ModeOfPayment = i == count ? ModeOfPayment.FinalPayment : ModeOfPayment.MonthlyPayment
                        });
                    }
                }

                var ptpId = await _ptpRepository.InsertAndGetIdAsync(ptp);
                foreach (var caseId in casesIds)
                {
                    await _casePromiseToPayRepository.InsertAsync(new CasePromiseToPay() { CaseId = caseId, PromiseToPayId = ptpId });
                }

                await SendNotificationGeneratePtp(casesIds);
            }
        }

        private async Task SendNotificationGeneratePtp(int[] casesIds)
        {
            UserIdentifier[] userIdentifiers = await _caseManager.GetUserIdentifiersForNotify(casesIds[0]);

            //Send Notification
            var casesIdsStr = string.Join(",", casesIds);
            var titile = $"Generate a promise to pay and change status to (PTP) - Cases IDs ({casesIdsStr}) ";
            var body = $"A promise to pay was generated after the offer was approved by the bank - Cases IDs({casesIdsStr})";

            await _notificationPublisher.PublishAsync(
                    titile,
                    new MessageNotificationData(body),
                    severity: NotificationSeverity.Info,
                    userIds: userIdentifiers
                    );

            //History
            foreach (var caseId in casesIds)
            {
                await _historyManager.CreateAsync(
                    caseId,
                    ActionType.ChangeStatus,
                    titile,
                    body);
            }
        }

        public async Task UpdateAndAssignAsync(PromiseToPay ptp, int[] casesIds)
        {
            using (var unitOfWork = _unitOfWorkManager.Begin())
            {
                var updatedPromiseToPay = _ptpRepository.Update(ptp);

                var casePromiseToPays = _casePromiseToPayRepository.GetAllList(x => x.PromiseToPayId == ptp.Id);

                await RemovePayments(updatedPromiseToPay);
                await AddNewPayments(updatedPromiseToPay);

                await AssignNewCasses(casePromiseToPays, casesIds);
                await UnAssignRemovedCasses(casePromiseToPays, casesIds);

                unitOfWork.Complete();
            }
        }

        private async Task RemovePayments(PromiseToPay ptp)
        {
            var newPayments = ptp.PtpPayments;
            var oldPaymanets = await _paymentRepository.GetAllListAsync(x => x.PromiseToPayId == ptp.Id);

            foreach (var oldPayment in oldPaymanets)
            {
                var isExist = newPayments.Any(x => x.Id == oldPayment.Id);
                if (!isExist)
                {
                    await _paymentRepository.DeleteAsync(oldPayment);
                }
            }
        }

        private async Task AddNewPayments(PromiseToPay ptp)
        {
            var newPayments = ptp.PtpPayments;
            var oldPaymanets = await _paymentRepository.GetAllListAsync(x => x.PromiseToPayId == ptp.Id);
            foreach (var newPayment in newPayments)
            {
                var isExist = oldPaymanets.Any(x => x.Id == newPayment.Id);
                if (!isExist)
                {
                    await _paymentRepository.InsertAsync(newPayment);
                }
            }
        }

        public async Task Delete(int paymentId)
        {
            using (var unitOfWork = _unitOfWorkManager.Begin())
            {
                var casePromiseToPays = _casePromiseToPayRepository.GetAllList(x => x.PromiseToPayId == paymentId);

                foreach (var casePromiseToPay in casePromiseToPays)
                {
                    await _casePromiseToPayRepository.DeleteAsync(casePromiseToPay);
                }

                await _ptpRepository.DeleteAsync(paymentId);

                unitOfWork.Complete();
            }
        }


        #region Helper Methods
        private async Task UnAssignRemovedCasses(List<CasePromiseToPay> casePromiseToPays, int[] casesIds)
        {
            foreach (var casePromiseToPay in casePromiseToPays)
            {
                if (!casesIds.Contains(casePromiseToPay.CaseId))
                {
                    await _casePromiseToPayRepository.DeleteAsync(casePromiseToPay);

                }
            }
        }

        private async Task AssignNewCasses(List<CasePromiseToPay> casePromiseToPays, int[] casesIds)
        {
            var paymentId = casePromiseToPays.FirstOrDefault().PromiseToPayId;

            foreach (var caseId in casesIds)
            {
                if (!casePromiseToPays.Any(x => x.CaseId == caseId))
                {
                    await _casePromiseToPayRepository.InsertAsync(new CasePromiseToPay() { CaseId = caseId, PromiseToPayId = paymentId });

                }
            }
        }
        #endregion

    }
}
