﻿using Abp.Domain.Entities.Auditing;
using LMS.Loan.Cases.PTP;
using System.ComponentModel.DataAnnotations.Schema;

namespace LMS.Loan.Cases
{
    public class CasePromiseToPay : FullAuditedEntity
    {
        #region Case
        public int CaseId { get; set; }
        [ForeignKey("CaseId")]
        public virtual Case Case { get; set; }
        #endregion

        #region PromiseToPay
        public int PromiseToPayId { get; set; }
        [ForeignKey("PromiseToPayId")]
        public virtual PromiseToPay PromiseToPay { get; set; }
        #endregion
    }
}
