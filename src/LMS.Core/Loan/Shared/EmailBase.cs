﻿using Abp.Domain.Entities;

namespace LMS.Loan.Shared
{
    public class EmailBase : Entity
    {
        public string EmailAddress { get; set; }
    }
}
