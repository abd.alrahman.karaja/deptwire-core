﻿using Abp.Domain.Entities.Auditing;
using LMS.Loan.Indexes;

namespace LMS.Loan.Shared
{
    public class UploadFileBase : FullAuditedEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Path { get; set; }

        public int? FileCategoryId { get; set; }
        public FileCategory FileCategory { get; set; }
    }
}
