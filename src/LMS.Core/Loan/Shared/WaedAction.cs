﻿using Abp.Domain.Entities.Auditing;
using System;

namespace LMS.Loan.Shared
{
    public class WaedAction : FullAuditedEntity
    {
        public DateTime? ReminderDate { get; set; }
        public string ReminderNote { get; set; }
    }
}
