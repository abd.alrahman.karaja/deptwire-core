﻿using Abp.Domain.Entities.Auditing;

namespace LMS.Loan.Shared
{
    public class PhoneBase : FullAuditedEntity
    {
        public string Number { get; set; }
    }
}
