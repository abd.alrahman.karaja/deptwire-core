﻿using Abp.Domain.Entities.Auditing;
using LMS.Loan.Indexes;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LMS.Loan.Agents
{
    public class Agent : FullAuditedEntity
    {

        [MaxLength(LMSConsts.MinStringLength)]
        public string Name { get; set; }

        [MaxLength(LMSConsts.MinStringLength)]
        public string ArabicName { get; set; }

        [MaxLength(LMSConsts.MinStringLength)]
        public string ShortName { get; set; }
        public int CountryId { get; set; }
        [ForeignKey("CountryId")]
        public virtual Country Country { get; set; }


    }
}
