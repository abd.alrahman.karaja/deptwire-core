﻿using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Services;
using System.Threading.Tasks;

namespace LMS.Loan.Agents
{
    public class AgentManager : IDomainService, ITransientDependency
    {
        private readonly IRepository<Agent> _agentRepository;
        public AgentManager(IRepository<Agent> agentRepository)
        {
            _agentRepository = agentRepository;
        }

        public Agent GetAgentById(int id)
        {
            return  _agentRepository.FirstOrDefault(id);
        }

        public async Task<Agent> GetAgentByIdAsync(int id)
        {
            return await _agentRepository.FirstOrDefaultAsync(id);
        }
    }
}
