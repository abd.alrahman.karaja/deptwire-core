﻿namespace LMS.Loan.Indexes
{
    public class Currency : IndexTypeEntity
    {
        public string Code { get; set; }
    }
}
