﻿using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Services;
using System.Threading.Tasks;

namespace LMS.Loan.Indexes
{
    public class NationalityManager : IDomainService, ITransientDependency
    {
        private readonly IRepository<Nationality> _repository;
        public NationalityManager(IRepository<Nationality> repository)
        {
            _repository = repository;
        }

        public async Task<Nationality> GetNationalityByNameAsync(string name)
        {
            var Nationality = await _repository.FirstOrDefaultAsync(x => x.Name.Equals(name));
            return Nationality;
        }

        public Nationality GetNationalityByName(string name)
        {
            var Nationality = _repository.FirstOrDefault(x => x.Name.Equals(name));
            return Nationality;
        }

        public async Task<Nationality> InsertAndGetAsync(string name)
        {
            var Nationality = new Nationality()
            {
                Name = name,
                Order = 200
            };
            var id = _repository.InsertAndGetId(Nationality);
            return await _repository.FirstOrDefaultAsync(id);
        }
    }
}
