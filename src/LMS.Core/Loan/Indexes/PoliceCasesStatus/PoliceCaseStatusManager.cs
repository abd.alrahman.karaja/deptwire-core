﻿using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Loan.Indexes
{
    public class PoliceCaseStatusManager : IDomainService, ITransientDependency
    {
        private readonly IRepository<PoliceCaseStatus> _repository;
        public PoliceCaseStatusManager(IRepository<PoliceCaseStatus> repository)
        {
            _repository = repository;
        }

        public async Task<PoliceCaseStatus> GetPoliceCaseStatusByNameAsync(string name)
        {
            var policeCaseStatus = await _repository.FirstOrDefaultAsync(x => x.Name.Equals(name));
            return policeCaseStatus;
        }

        public PoliceCaseStatus GetPoliceCaseStatusByName(string name)
        {
            var policeCaseStatus = _repository.FirstOrDefault(x => x.Name.Equals(name));
            return policeCaseStatus;
        }

        public async Task<PoliceCaseStatus> InsertAndGetAsync(string name)
        {
            var policeCaseStatus = new PoliceCaseStatus()
            {
                Name = name,
                Order = 200
            };
            var id = _repository.InsertAndGetId(policeCaseStatus);
            return await _repository.FirstOrDefaultAsync(id);
        }

        public PoliceCaseStatus InsertAndGet(string name)
        {
            var policeCaseStatus = new PoliceCaseStatus()
            {
                Name = name,
                Order = 200
            };
            var id = _repository.InsertAndGetId(policeCaseStatus);
            return _repository.FirstOrDefault(id);
        }
    }
}
