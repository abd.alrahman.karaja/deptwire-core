﻿using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Loan.Indexes
{
    public class CountryManager : IDomainService, ITransientDependency
    {
        private readonly IRepository<Country> _repository;
        public CountryManager(IRepository<Country> repository)
        {
            _repository = repository;
        }

        public async Task<Country> GetCountryByNameAsync(string name)
        {
            var country = await _repository.FirstOrDefaultAsync(x => x.Name.Equals(name));
            return country;
        }

        public Country GetCountryByName(string name)
        {
            return _repository.FirstOrDefault(x => x.Name.Equals(name));
        }

        public async Task<Country> GetByIdAsync(int id)
        {
            var country = await _repository.FirstOrDefaultAsync(id);
            return country;
        }

        public async Task<Country> InsertAndGetAsync(string name)
        {
            var country = new Country()
            {
                Name = name,
                Order = 200
            };
            var id = _repository.InsertAndGetId(country);
            return await _repository.FirstOrDefaultAsync(id);
        }

        public Country InsertAndGet(string name)
        {
            var country = new Country()
            {
                Name = name,
                Order = 200
            };
            var id = _repository.InsertAndGetId(country);
            return _repository.FirstOrDefault(id);
        }
    }
}
