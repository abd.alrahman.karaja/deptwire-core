﻿using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Loan.Indexes
{
    public class ProductManager : IDomainService, ITransientDependency
    {
        private readonly IRepository<Product> _repository;
        public ProductManager(IRepository<Product> repository)
        {
            _repository = repository;
        }

        public async Task<Product> GetProductByNameAsync(string name)
        {
            var product = await _repository.FirstOrDefaultAsync(x => x.Name.Equals(name));
            return product;
        }

        public Product GetProductByName(string name)
        {
            var product = _repository.FirstOrDefault(x => x.Name.Equals(name));
            return product;
        }

        public async Task<Product> InsertAndGetAsync(string name)
        {
            var product = new Product()
            {
                Name = name,
                Order = 200
            };
            var id =  _repository.InsertAndGetId(product);
            return await _repository.FirstOrDefaultAsync(id);
        }

        public Product InsertAndGet(string name)
        {
            var product = new Product()
            {
                Name = name,
                Order = 200
            };
            var id = _repository.InsertAndGetId(product);
            return _repository.FirstOrDefault(id);
        }
    }
}
