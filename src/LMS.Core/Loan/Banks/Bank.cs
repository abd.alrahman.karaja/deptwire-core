﻿using Abp.Domain.Entities.Auditing;
using LMS.Loan.Indexes;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LMS.Loan.Banks
{
    public class Bank : FullAuditedEntity
    {
        public Bank()
        {
            Codes = new List<BankCode>(); 
        }

        [MaxLength(LMSConsts.MinStringLength)]
        public string Name { get; set; }

        [MaxLength(LMSConsts.MinStringLength)]
        public string ArabicName { get; set; }

        [MaxLength(LMSConsts.MinStringLength)]
        public string ShortName { get; set; }

        [MaxLength(LMSConsts.MinStringLength)]
        public string DateFormat { get; set; }
        public int CountryId { get; set; }
        [ForeignKey("CountryId")]
        public virtual Country Country { get; set; }

        public virtual IList<BankCode> Codes { get; set; }

    }
}
