﻿using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Services;
using System.Threading.Tasks;

namespace LMS.Loan.Banks
{
    public class BankManager : IDomainService, ITransientDependency
    {
        private readonly IRepository<Bank> _bankRepository;
        public BankManager(IRepository<Bank> bankRepository)
        {
            _bankRepository = bankRepository;
        }

        public async Task<Bank> GetBankByIdAsync(int id)
        {
            return await _bankRepository.FirstOrDefaultAsync(id);
        }

        public Bank GetBankById(int id)
        {
            return _bankRepository.FirstOrDefault(id);
        }
    }
}
