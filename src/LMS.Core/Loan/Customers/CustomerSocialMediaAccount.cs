﻿using Abp.Domain.Entities.Auditing;
using LMS.Loan.Enums;
using System.ComponentModel.DataAnnotations.Schema;

namespace LMS.Loan.Customers
{
    public class CustomerSocialMediaAccount : FullAuditedEntity
    {
        public SocialMediaSite SocialMediaSite { get; set; }
        public string Name { get; set; }

        #region Customer
        public int CustomerId { get; set; }
        [ForeignKey("CustomerId")]
        public virtual Customer Customer { get; set; }
        #endregion
    }
}
