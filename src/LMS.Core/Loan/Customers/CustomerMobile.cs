﻿using LMS.Loan.Shared;
using System.ComponentModel.DataAnnotations.Schema;

namespace LMS.Loan.Customers
{
    public class CustomerMobile : PhoneBase
    {
        #region Customer
        public int CustomerId { get; set; }
        [ForeignKey("CustomerId")]
        public virtual Customer Customer { get; set; }
        #endregion
    }
}
