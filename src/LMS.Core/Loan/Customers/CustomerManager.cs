﻿using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Services;
using Abp.Domain.Uow;
using LMS.Loan.Indexes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LMS.Loan.Customers
{
    public class CustomerManager : IDomainService,ITransientDependency
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<Customer> _repository;
        private readonly IRepository<CustomerPhone> _phoneRepository;
        private readonly IRepository<CustomerOfficePhone> _officePhoneRepository;
        private readonly IRepository<CustomerReferencePhone> _referencePhoneRepository;
        private readonly IRepository<CustomerMobile> _mobileRepository;
        private readonly IRepository<CustomerEmail> _emailRepository;
        private readonly IRepository<CustomerSocialMediaAccount> _socialMediaAccountRepository;
        private readonly NationalityManager _nationalityManager;
        public CustomerManager(
            IUnitOfWorkManager unitOfWorkManager,
            IRepository<Customer> repository, 
            NationalityManager nationalityManager,
            IRepository<CustomerPhone> phoneRepository,
            IRepository<CustomerOfficePhone> officePhoneRepository,
            IRepository<CustomerReferencePhone> referencePhoneRepository,
            IRepository<CustomerMobile> mobileRepository,
            IRepository<CustomerEmail> emailRepository,
            IRepository<CustomerSocialMediaAccount> socialMediaAccountRepository
            )
        {
            _unitOfWorkManager = unitOfWorkManager;
            _repository = repository;
            _nationalityManager = nationalityManager;
            _phoneRepository = phoneRepository;
            _officePhoneRepository = officePhoneRepository;
            _referencePhoneRepository = referencePhoneRepository;
            _mobileRepository = mobileRepository;
            _emailRepository = emailRepository;
            _socialMediaAccountRepository = socialMediaAccountRepository;
        }

        public async Task<Customer> GetCustomerByNameAsync(string name , string arabicName)
        {
            Customer customer;
            if (!string.IsNullOrEmpty(name))
            {
                customer = await _repository.FirstOrDefaultAsync(x => x.Name.Equals(name));
            }
            else
            {
                customer = await _repository.FirstOrDefaultAsync(x => x.ArabicName.Equals(arabicName));
            }
            
            return customer;
        }

        public Customer GetCustomerById(int customerId)
        {
            var customer = _repository.GetAllIncluding(
                    ph => ph.Phones,
                    offPh => offPh.OfficePhone,
                    m => m.Mobiles,
                    refPh => refPh.ReferencePhones,
                    sm => sm.SocialMediaAccounts,
                    e => e.Emails
                ).FirstOrDefault(x => x.Id == customerId);

            return customer;
        }
        public async Task UpdateAsync(
            Customer customer, 
            IList<CustomerPhone> phones,
            IList<CustomerOfficePhone> officePhones,
            IList<CustomerReferencePhone> referencePhones,
            IList<CustomerMobile> mobiles,
            IList<CustomerEmail> emails,
            IList<CustomerSocialMediaAccount> accounts
            )
        {
            //var customer = GetCustomerById(input.Id);
            using (var unitOfWork = _unitOfWorkManager.Begin())
            {
                var updatedCustomer = await _repository.UpdateAsync(customer);

                await RemoveCustomerPhones(updatedCustomer, phones);
                await AddNewCustomerPhones(updatedCustomer, phones);

                await RemoveCustomerOfficePhones(updatedCustomer, officePhones);
                await AddNewCustomerOfficePhones(updatedCustomer, officePhones);

                await RemoveCustomerReferencePhones(updatedCustomer, referencePhones);
                await AddNewCustomerReferencePhones(updatedCustomer, referencePhones);

                await RemoveCustomerMobiles(updatedCustomer, mobiles);
                await AddNewCustomerMobiles(updatedCustomer, mobiles);

                await RemoveCustomerEmails(updatedCustomer, emails);
                await AddNewCustomerEmails(updatedCustomer, emails);

                await RemoveCustomerAccounts(updatedCustomer, accounts);
                await AddNewCustomerAccounts(updatedCustomer, accounts);


                unitOfWork.Complete();
            }
        }
        public async Task<int> InsertAndGetIdAsync(Customer customer)
        {
            return await _repository.InsertAndGetIdAsync(customer);
        }
        public async Task<int> UpdateAndGetIdAsync(Customer currentCustomer , Customer importedCustomer)
        {
            currentCustomer.Name = importedCustomer.Name;
            currentCustomer.ArabicName = importedCustomer.ArabicName;
            currentCustomer.DOB = importedCustomer.DOB;
            currentCustomer.Gender = importedCustomer.Gender;
            currentCustomer.Age = importedCustomer.Age;
            currentCustomer.CustomerEmailByBank = importedCustomer.CustomerEmailByBank;
            currentCustomer.CustomerMobileByBank = importedCustomer.CustomerMobileByBank;
            currentCustomer.MotherName = importedCustomer.MotherName;
            currentCustomer.ResidenceNo = importedCustomer.ResidenceNo;
            currentCustomer.VisaNo = importedCustomer.VisaNo;
            currentCustomer.PassportNo = importedCustomer.PassportNo;
            currentCustomer.PassportExpiry = importedCustomer.PassportExpiry;
            currentCustomer.ReferenceNameByBank = importedCustomer.ReferenceMobileByBank;
            currentCustomer.ReferenceMobileByBank = importedCustomer.ReferenceMobileByBank;

            Nationality nationality = await GetOrInsertNationality(importedCustomer.Nationality.Name);
            currentCustomer.Nationality = nationality;
            currentCustomer.NationalityId = nationality.Id;

            return await _repository.InsertOrUpdateAndGetIdAsync(currentCustomer);
        }
        private async Task<Nationality> GetOrInsertNationality(string name)
        {
            Nationality nationality = await _nationalityManager.GetNationalityByNameAsync(name);
            if (nationality != null)
            {
                nationality = await _nationalityManager.InsertAndGetAsync(name);
            }

            return nationality;
        }

        #region Helper Methods
        private async Task RemoveCustomerPhones(Customer customer, IList<CustomerPhone> newPhones)
        {
            var oldPhones = await _phoneRepository.GetAllListAsync(x => x.CustomerId == customer.Id);

            foreach (var oldPhone in oldPhones)
            {
                var isExist = newPhones.Any(x => x.Number == oldPhone.Number.Trim());
                if (!isExist)
                {
                    await _phoneRepository.DeleteAsync(oldPhone);
                }
            }
        }

        private async Task AddNewCustomerPhones(Customer customer, IList<CustomerPhone> newPhones)
        {
            var oldPhones = await _phoneRepository.GetAllListAsync(x=>x.CustomerId == customer.Id);
            foreach (var newPhone in newPhones)
            {
                var isExist = oldPhones.Any(x => x.Number == newPhone.Number.Trim());
                if (!isExist)
                {
                    await _phoneRepository.InsertAsync(newPhone);
                }
            }
        }

        private async Task RemoveCustomerOfficePhones(Customer customer, IList<CustomerOfficePhone> newPhones)
        {
            var oldPhones = await _officePhoneRepository.GetAllListAsync(x => x.CustomerId == customer.Id);

            foreach (var oldPhone in oldPhones)
            {
                var isExist = newPhones.Any(x => x.Number == oldPhone.Number.Trim());
                if (!isExist)
                {
                    await _officePhoneRepository.DeleteAsync(oldPhone);
                }
            }
        }

        private async Task AddNewCustomerOfficePhones(Customer customer, IList<CustomerOfficePhone> newPhones)
        {
            var oldPhones = await _officePhoneRepository.GetAllListAsync(x => x.CustomerId == customer.Id);
            foreach (var newPhone in newPhones)
            {
                var isExist = oldPhones.Any(x => x.Number == newPhone.Number.Trim());
                if (!isExist)
                {
                    await _officePhoneRepository.InsertAsync(newPhone);
                }
            }
        }

        private async Task RemoveCustomerReferencePhones(Customer customer, IList<CustomerReferencePhone> newPhones)
        {
            var oldPhones = await _referencePhoneRepository.GetAllListAsync(x => x.CustomerId == customer.Id);

            foreach (var oldPhone in oldPhones)
            {
                var isExist = newPhones.Any(x => x.Number == oldPhone.Number.Trim());
                if (!isExist)
                {
                    await _referencePhoneRepository.DeleteAsync(oldPhone);
                }
            }
        }

        private async Task AddNewCustomerReferencePhones(Customer customer, IList<CustomerReferencePhone> newPhones)
        {
            var oldPhones = await _referencePhoneRepository.GetAllListAsync(x => x.CustomerId == customer.Id);
            foreach (var newPhone in newPhones)
            {
                var isExist = oldPhones.Any(x => x.Number == newPhone.Number.Trim());
                if (!isExist)
                {
                    await _referencePhoneRepository.InsertAsync(newPhone);
                }
            }
        }

        private async Task RemoveCustomerMobiles(Customer customer, IList<CustomerMobile> newPhones)
        {
            var oldPhones = await _mobileRepository.GetAllListAsync(x => x.CustomerId == customer.Id);

            foreach (var oldPhone in oldPhones)
            {
                var isExist = newPhones.Any(x => x.Number == oldPhone.Number.Trim());
                if (!isExist)
                {
                    await _mobileRepository.DeleteAsync(oldPhone);
                }
            }
        }

        private async Task AddNewCustomerMobiles(Customer customer, IList<CustomerMobile> newPhones)
        {
            var oldPhones = await _mobileRepository.GetAllListAsync(x => x.CustomerId == customer.Id);
            foreach (var newPhone in newPhones)
            {
                var isExist = oldPhones.Any(x => x.Number == newPhone.Number.Trim());
                if (!isExist)
                {
                    await _mobileRepository.InsertAsync(newPhone);
                }
            }
        }

        private async Task RemoveCustomerEmails(Customer customer, IList<CustomerEmail> newMails)
        {
            var oldMails = await _emailRepository.GetAllListAsync(x => x.CustomerId == customer.Id);

            foreach (var oldMail in oldMails)
            {
                var isExist = newMails.Any(x => x.EmailAddress == oldMail.EmailAddress.Trim());
                if (!isExist)
                {
                    await _emailRepository.DeleteAsync(oldMail);
                }
            }
        }

        private async Task AddNewCustomerEmails(Customer customer, IList<CustomerEmail> newMails)
        {
            var oldMails = await _emailRepository.GetAllListAsync(x => x.CustomerId == customer.Id);
            foreach (var newMail in newMails)
            {
                var isExist = oldMails.Any(x => x.EmailAddress == newMail.EmailAddress.Trim());
                if (!isExist)
                {
                    await _emailRepository.InsertAsync(newMail);
                }
            }
        }

        private async Task RemoveCustomerAccounts(Customer customer, IList<CustomerSocialMediaAccount> newAccounts)
        {
            var oldAccounts = await _socialMediaAccountRepository.GetAllListAsync(x => x.CustomerId == customer.Id);

            foreach (var oldAccount in oldAccounts)
            {
                var isExist = newAccounts.Any(x => x.Name == oldAccount.Name.Trim());
                if (!isExist)
                {
                    await _socialMediaAccountRepository.DeleteAsync(oldAccount);
                }
            }
        }

        private async Task AddNewCustomerAccounts(Customer customer, IList<CustomerSocialMediaAccount> newAccounts)
        {
            var oldAccounts = await _socialMediaAccountRepository.GetAllListAsync(x => x.CustomerId == customer.Id);
            foreach (var newAccount in newAccounts)
            {
                var isExist = oldAccounts.Any(x => x.Name == newAccount.Name.Trim());
                if (!isExist)
                {
                    await _socialMediaAccountRepository.InsertAsync(newAccount);
                }
            }
        }

        #endregion
    }
}
