﻿using Abp.Domain.Entities.Auditing;
using LMS.Loan.Indexes;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LMS.Loan.Customers
{
    public class TempCustomer : FullAuditedEntity
    {

        #region Personal Info

        /// <summary>
        /// الاسم
        /// </summary>
        [MaxLength(LMSConsts.MinStringLength)]
        public string Name { get; set; }

        /// <summary>
        /// اسم العميل بالعربي
        /// </summary>
        [MaxLength(LMSConsts.MinStringLength)]
        public string ArabicName { get; set; }

        /// <summary>
        /// اسم الام
        /// </summary>
        [MaxLength(LMSConsts.MinStringLength)]
        public string MotherName { get; set; }

        /// <summary>
        /// الجنس
        /// </summary>
        public string Gender { get; set; }

        /// <summary>
        /// العمر
        /// </summary>
        public int Age { get; set; }

        /// <summary>
        /// اسم شركة الزبون
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// المواليد
        /// </summary>
        public DateTime? DOB { get; set; }
        #endregion

        #region Contact Info

        /// <summary>
        /// رقم موبايل الزبون كما هو مزود من قبل البنك 
        /// </summary>
        public string CustomerMobileByBank { get; set; }

        /// <summary>
        /// ايميل الزبون كما هو مزود من قبل البنك 
        /// </summary>
        public string CustomerEmailByBank { get; set; }

        /// <summary>
        /// اسم الشخص المرجعي او الصديق كما هو مزود من قبل البنك
        /// </summary>
        public string ReferenceNameByBank { get; set; }

        /// <summary>
        /// رقم موبايل الصديق المزود من قبل البنك
        /// </summary>
        public string ReferenceMobileByBank { get; set; }

        #endregion

        #region Passport Info

        /// <summary>
        /// رقم جواز السفر
        /// </summary>
        public string PassportNo { get; set; }

        /// <summary>
        /// صلاحية جواز السفر
        /// </summary>
        public DateTime? PassportExpiry { get; set; }
        #endregion

        #region Other Inof

        /// <summary>
        /// رقم الإقامة
        /// </summary>
        public string ResidenceNo { get; set; }

        /// <summary>
        /// رقم التأشيرة
        /// </summary>
        public string VisaNo { get; set; }

        #endregion

        /// <summary>
        /// تواجد العميل خارج او داخل موطن البنك
        /// </summary>
        public string InsideOrOutsideBankCountry { get; set; }

        /// <summary>
        /// خارج او داخل سوريا
        /// </summary>
        public string InsideOrOutsideSyria { get; set; }

        /// <summary>
        /// مكان تواجد الزبون حاليا
        /// </summary>
        public string CurrentLocation { get; set; }

        /// <summary>
        /// مهنة العميل
        /// </summary>
        public string CustomerCareer { get; set; }

        /// <summary>
        /// جهة عمل الزبون الحالي
        /// </summary>
        public string CurrentJob { get; set; }

        /// <summary>
        /// العنوان في سوريا
        /// </summary>
        public string HomeAddress { get; set; }
        public string CmMobileNO { get; set; }
        public string CmOtherMobileNO { get; set; }
        public string HomePhoneNumber { get; set; }
        public string CmOtherPhone { get; set; }
        public string CmOfficePhone { get; set; }
        public string CmReferncePhone { get; set; }
        public string CmOtherReferncePhone { get; set; }
        public string CmEmail { get; set; }
        public string CmOtherEmail { get; set; }
        public string CmSites { get; set; }

        public int NationalityId { get; set; }
        [ForeignKey("NationalityId")]
        public virtual Nationality Nationality { get; set; }

        public int CustomerId { get; set; }
        [ForeignKey("CustomerId")]
        public virtual Customer Customer { get; set; }

    }
}
