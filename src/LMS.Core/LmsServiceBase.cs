﻿using Abp;
using System;
using System.Collections.Generic;
using System.Text;

namespace LMS
{
    /// <summary>
    /// This class can be used as a base class for services in this application.
    /// It has some useful objects property-injected and has some basic methods most of services may need to.
    /// It's suitable for non domain nor application service classes.
    /// For domain services inherit <see cref="LmsDomainServiceBase"/>.
    /// For application services inherit RoroAppServiceBase.
    /// </summary>
    public class LmsServiceBase : AbpServiceBase
    {
        protected LmsServiceBase()
        {
            LocalizationSourceName = LMSConsts.LocalizationSourceName;
        }
    }
}
