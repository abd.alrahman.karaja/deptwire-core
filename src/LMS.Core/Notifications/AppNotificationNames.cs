namespace LMS.Notifications
{
    /// <summary>
    /// Constants for notification names used in this application.
    /// </summary>
    public static class AppNotificationNames
    {
        public const string WelcomeToTheApplication = "App.WelcomeToTheApplication";
        public const string NewUserRegistered = "App.NewUserRegistered";
        public const string NewTenantRegistered = "App.NewTenantRegistered";
        public const string GdprDataPrepared = "App.GdprDataPrepared";
        public const string TenantsMovedToEdition = "App.TenantsMovedToEdition"; 
        public const string DownloadInvalidImportUsers = "App.DownloadInvalidImportUsers"; 
        public const string DownloadInvalidImportCases = "App.DownloadInvalidImportCases";
        public const string NewFFWRegistered = "App.NewFFWRegistered"; 
        public const string ExtraCharge = "App.ExtraCharge";
        public const string FFWRejected = "App.FFWRejected"; 
        public const string FFWApproved = "App.FFWApproved"; 
        public const string UploadedDocument = "App.UploadedDocument"; //
        public const string FFWPendingApproval = "App.FFWPendingApproval";
        public const string DriverInRoute = "App.DriverInRoute";
        public const string RequestedDocument = "App.RequestedDocument";

        public const string UnderTracing = "UnderTracing";
        public const string Retracing = "Retracing";
        public const string ThirdPartyContact = "ThirdPartyContact";
        public const string Contacted = "Contacted";
        public const string UnderNegotiation = "UnderNegotiation";
        public const string Offer = "Offer";
        public const string Approval = "Approval";
        public const string Declined = "Declined";
        public const string PTP = "PTP";
        public const string Paid = "Paid";
        public const string Settled = "Settled";
        public const string AlreadySettled = "AlreadySettled";
        public const string BrokenPromise = "BrokenPromise";
        public const string RefusedToPay = "RefusedToPay";
        public const string Dead = "Dead";
        public const string ClosedAndReturned = "ClosedAndReturned";

        public static string ChangeStatusTo(string status)
        {
            return $"ChangeStatusTo{status}";
        }
    }
}