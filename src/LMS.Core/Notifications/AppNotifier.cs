﻿using Abp;
using Abp.Localization;
using Abp.Notifications;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Notifications
{
    public class AppNotifier: LmsDomainServiceBase, IAppNotifier
    {
        private readonly INotificationPublisher _notificationPublisher;

        public AppNotifier(INotificationPublisher notificationPublisher)
        {
            _notificationPublisher = notificationPublisher;
        }
        public async Task SendMessageAsync(UserIdentifier user, string notificationName, string message, NotificationSeverity severity = NotificationSeverity.Info)
        {
            await _notificationPublisher.PublishAsync(
                notificationName,
                new MessageNotificationData(message),
                severity: severity,
                userIds: new[] { user }
                );
        }
        public async Task FileCantBeConvertedToCaseList(UserIdentifier user, string message)
        {
            await _notificationPublisher.PublishAsync(
                "FileImportFailed",
                new MessageNotificationData(message),
                severity: NotificationSeverity.Warn,
                userIds: new[] { user }
                );
        }
        public async Task SomeCasesCouldntBeImported(UserIdentifier argsUser, string fileToken, string fileType, string fileName)
        {
            var notificationData = new LocalizableMessageNotificationData(
                new LocalizableString(
                    "SomeCasesCouldntBeImported",
                    LMSConsts.LocalizationSourceName
                )
            );

            notificationData["fileToken"] = fileToken;
            notificationData["fileType"] = fileType;
            notificationData["fileName"] = fileName;

            await _notificationPublisher.PublishAsync(AppNotificationNames.DownloadInvalidImportCases, notificationData, userIds: new[] { argsUser });
        }

    }
}
