﻿using Abp;
using Abp.Notifications;
using System.Threading.Tasks;

namespace LMS.Notifications
{
    public interface IAppNotifier
    {
        Task SendMessageAsync(UserIdentifier user, string notificationName, string message, NotificationSeverity severity = NotificationSeverity.Info);
        Task FileCantBeConvertedToCaseList(UserIdentifier user, string message);
        Task SomeCasesCouldntBeImported(UserIdentifier argsUser, string fileToken, string fileType, string fileName);
    }
}
