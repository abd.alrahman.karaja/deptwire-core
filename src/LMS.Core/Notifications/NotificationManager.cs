﻿using Abp.Domain.Services;
using Abp.Notifications;
using LMS.Authorization.Roles;
using LMS.Authorization.Users;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Notifications
{
    public class NotificationManager: IDomainService
    {
        public readonly INotificationPublisher _notificationPublisher;
        public readonly RoleManager _roleManager;
        public readonly UserManager _userManager;

        public NotificationManager(
            INotificationPublisher notificationPublisher,
            RoleManager roleManager,
            UserManager userManager)
        {
            _notificationPublisher = notificationPublisher;
            _roleManager = roleManager;
            _userManager = userManager;
        }

        public async Task SendToNegotiatorAsync(string body,User user)
        {

        }
    }
}
