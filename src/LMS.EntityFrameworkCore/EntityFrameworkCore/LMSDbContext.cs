﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using LMS.Authorization.Roles;
using LMS.Authorization.Users;
using LMS.MultiTenancy;
using LMS.Loan.Indexes;
using LMS.Loan.Customers;
using LMS.Loan.Employees;
using LMS.Loan.Agents;
using LMS.Loan.Banks;
using LMS.Loan.Cases;
using LMS.Loan.Workflows;
using LMS.Loan.Cases.Payments;
using LMS.Loan.Cases.PTP;
using LMS.Loan.Cases.History;
using LMS.Storage;
using LMS.Loan.Cases.CaseUploadFiles;
using LMS.Loan.Reminders;
using LMS.Loan.Cases.Meetings;
using LMS.Loan.Cases.Offers;
using LMS.Loan.Cases.Calls;
using LMS.Loan.Cases.Notes;
using LMS.Loan.Cases.UploadedFiles;
using LMS.Loan.Cases.Legals;

namespace LMS.EntityFrameworkCore
{
    public class LMSDbContext : AbpZeroDbContext<Tenant, Role, User, LMSDbContext>
    {
        public DbSet<Country> Countries { get; set; }
        public DbSet<DataPermission> DataPermissions { get; set; }
        public DbSet<Nationality> Nationalities { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Agent> Agents { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Currency> Currencies { get; set; }
        public DbSet<Bank> Banks { get; set; }
        public DbSet<BankCode> BankCodes { get; set; }
        public DbSet<Case> Cases { get; set; }
        public DbSet<Payment> Payments { get; set; }
        public DbSet<PromiseToPay> PromisesToPay { get; set; }
        public DbSet<CaseHistory> CaseHistories { get; set; }
        public DbSet<WorkflowSetting> WorkflowSettings { get; set; }
        public DbSet<WorkflowItem> WorkflowItems { get; set; }
        public DbSet<WorkflowApproval> WorkflowApprovals { get; set; }
        public DbSet<WorkflowStep> WorkflowSteps { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<CaseUploadedFile> CaseUploadedFiles { get; set; }
        public DbSet<UploadedFile> UploadedFiles { get; set; }
        public DbSet<PaymentClaimedStatus> PaymentClaimedStatuses { get; set; }
        public DbSet<PaymentReceivedStatus> PaymentReceivedStatuses { get; set; }
        public DbSet<TransferredStatus> TransferredStatuses { get; set; }
        public DbSet<BinaryObject> BinaryObject { get; set; }
        public DbSet<CaseUploadFile> CaseUploadFiles { get; set; }
        public DbSet<TempCustomer> TempCustomers { get; set; }
        public DbSet<Legal> Legals { get; set; }
        public DbSet<CaseLegal> CaseLegals { get; set; }
        public DbSet<FileCategory> FileCategories { get; set; }
        public DbSet<Reminder> Reminders { get; set; }
        public DbSet<CasePromiseToPay> CasePromisesToPay { get; set; }
        public DbSet<PromiseToPayPayment> PromisesToPayPayments { get; set; }
        public DbSet<CasePayment> CasePayments { get; set; }
        public DbSet<CustomerReferencePhone> CustomerReferencePhones { get; set; }
        public DbSet<CustomerPhone> CustomerPhones { get; set; }
        public DbSet<CustomerOfficePhone> CustomerOfficePhones { get; set; }
        public DbSet<CustomerEmail> CustomerEmails { get; set; }
        public DbSet<CustomerMobile> CustomerMobiles { get; set; }
        public DbSet<PoliceCaseStatus> PoliceCaseStatuses { get; set; }
        public DbSet<CustomerSocialMediaAccount> CustomerSocialMediaAccounts { get; set; }
        public DbSet<Offer> Offers { get; set; }
        public DbSet<Meeting> Meetings { get; set; }
        public DbSet<CaseOffer> CaseOffers { get; set; }
        public DbSet<CaseMeeting> CaseMeetings { get; set; }
        public DbSet<Call> Calls { get; set; }
        public DbSet<CaseCall> CaseCall { get; set; }
        public DbSet<Note> Notes { get; set; }
        public DbSet<CaseNote> CaseNotes { get; set; }

        public LMSDbContext(DbContextOptions<LMSDbContext> options)
            : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<Employee>()
             .HasOne(x => x.User);
            builder.Entity<Customer>()
             .HasOne(x => x.Nationality);
            builder.Entity<Agent>()
             .HasOne(x => x.Country);
            builder.Entity<Bank>()
             .HasOne(x => x.Country);
            builder.Entity<BankCode>()
             .HasOne(x => x.Bank)
             .WithMany(x=> x.Codes);
            builder.Entity<WorkflowApproval>()
            .HasOne(x => x.WorkflowSetting)
            .WithMany(x => x.WorkflowApprovals);
            builder.Entity<WorkflowApproval>()
            .HasOne(x => x.User);
            builder.Entity<WorkflowStep>()
            .HasOne(x => x.WorkflowItem)
            .WithMany(x => x.WorkflowSteps);
            builder.Entity<WorkflowStep>()
            .HasOne(x => x.Employee);
            builder.Entity<WorkflowItem>()
             .HasOne(x => x.WorkflowSetting);

        }
    }
}
