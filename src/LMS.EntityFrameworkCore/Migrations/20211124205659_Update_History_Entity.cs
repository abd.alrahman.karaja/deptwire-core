﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LMS.Migrations
{
    public partial class Update_History_Entity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CaseHistories_AbpUsers_UserId",
                table: "CaseHistories");

            migrationBuilder.DropIndex(
                name: "IX_CaseHistories_UserId",
                table: "CaseHistories");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "CaseHistories");

            migrationBuilder.CreateIndex(
                name: "IX_CaseHistories_CreatorUserId",
                table: "CaseHistories",
                column: "CreatorUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_CaseHistories_AbpUsers_CreatorUserId",
                table: "CaseHistories",
                column: "CreatorUserId",
                principalTable: "AbpUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CaseHistories_AbpUsers_CreatorUserId",
                table: "CaseHistories");

            migrationBuilder.DropIndex(
                name: "IX_CaseHistories_CreatorUserId",
                table: "CaseHistories");

            migrationBuilder.AddColumn<long>(
                name: "UserId",
                table: "CaseHistories",
                type: "bigint",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_CaseHistories_UserId",
                table: "CaseHistories",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_CaseHistories_AbpUsers_UserId",
                table: "CaseHistories",
                column: "UserId",
                principalTable: "AbpUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
