﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LMS.Migrations
{
    public partial class Update_Bank_And_Agent_Entity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ForeignName",
                table: "Banks");

            migrationBuilder.DropColumn(
                name: "ForeignName",
                table: "Agents");

            migrationBuilder.AddColumn<string>(
                name: "ArabicName",
                table: "Banks",
                maxLength: 150,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DateFormat",
                table: "Banks",
                maxLength: 150,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ShortName",
                table: "Banks",
                maxLength: 150,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ArabicName",
                table: "Agents",
                maxLength: 150,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ShortName",
                table: "Agents",
                maxLength: 150,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ArabicName",
                table: "Banks");

            migrationBuilder.DropColumn(
                name: "DateFormat",
                table: "Banks");

            migrationBuilder.DropColumn(
                name: "ShortName",
                table: "Banks");

            migrationBuilder.DropColumn(
                name: "ArabicName",
                table: "Agents");

            migrationBuilder.DropColumn(
                name: "ShortName",
                table: "Agents");

            migrationBuilder.AddColumn<string>(
                name: "ForeignName",
                table: "Banks",
                type: "nvarchar(150)",
                maxLength: 150,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ForeignName",
                table: "Agents",
                type: "nvarchar(150)",
                maxLength: 150,
                nullable: true);
        }
    }
}
