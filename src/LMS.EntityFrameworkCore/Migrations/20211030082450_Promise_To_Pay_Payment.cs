﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LMS.Migrations
{
    public partial class Promise_To_Pay_Payment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BankConfirmation",
                table: "PromisesToPay");

            migrationBuilder.DropColumn(
                name: "CollectedPayment",
                table: "PromisesToPay");

            migrationBuilder.DropColumn(
                name: "ModeOfPayment",
                table: "PromisesToPay");

            migrationBuilder.DropColumn(
                name: "Note",
                table: "PromisesToPay");

            migrationBuilder.DropColumn(
                name: "PaymentDate",
                table: "PromisesToPay");

            migrationBuilder.DropColumn(
                name: "PtpAmount",
                table: "PromisesToPay");

            migrationBuilder.AddColumn<string>(
                name: "NegotiatorNote",
                table: "PromisesToPay",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "OfferId",
                table: "PromisesToPay",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SuperviorNote",
                table: "PromisesToPay",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "PromiseToPayPayment",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    PaymentNo = table.Column<int>(nullable: false),
                    ModeOfPayment = table.Column<int>(nullable: false),
                    PtpAmount = table.Column<double>(nullable: false),
                    PaymentDate = table.Column<DateTime>(nullable: true),
                    CollectedPayment = table.Column<double>(nullable: false),
                    BankConfirmation = table.Column<bool>(nullable: false),
                    Note = table.Column<string>(nullable: true),
                    NegotiatorNote = table.Column<string>(nullable: true),
                    SuperviorNote = table.Column<string>(nullable: true),
                    PromiseToPayId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PromiseToPayPayment", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PromiseToPayPayment_PromisesToPay_PromiseToPayId",
                        column: x => x.PromiseToPayId,
                        principalTable: "PromisesToPay",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PromisesToPay_OfferId",
                table: "PromisesToPay",
                column: "OfferId");

            migrationBuilder.CreateIndex(
                name: "IX_PromiseToPayPayment_PromiseToPayId",
                table: "PromiseToPayPayment",
                column: "PromiseToPayId");

            migrationBuilder.AddForeignKey(
                name: "FK_PromisesToPay_Offers_OfferId",
                table: "PromisesToPay",
                column: "OfferId",
                principalTable: "Offers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PromisesToPay_Offers_OfferId",
                table: "PromisesToPay");

            migrationBuilder.DropTable(
                name: "PromiseToPayPayment");

            migrationBuilder.DropIndex(
                name: "IX_PromisesToPay_OfferId",
                table: "PromisesToPay");

            migrationBuilder.DropColumn(
                name: "NegotiatorNote",
                table: "PromisesToPay");

            migrationBuilder.DropColumn(
                name: "OfferId",
                table: "PromisesToPay");

            migrationBuilder.DropColumn(
                name: "SuperviorNote",
                table: "PromisesToPay");

            migrationBuilder.AddColumn<bool>(
                name: "BankConfirmation",
                table: "PromisesToPay",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<double>(
                name: "CollectedPayment",
                table: "PromisesToPay",
                type: "float",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<int>(
                name: "ModeOfPayment",
                table: "PromisesToPay",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Note",
                table: "PromisesToPay",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "PaymentDate",
                table: "PromisesToPay",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "PtpAmount",
                table: "PromisesToPay",
                type: "float",
                nullable: false,
                defaultValue: 0.0);
        }
    }
}
