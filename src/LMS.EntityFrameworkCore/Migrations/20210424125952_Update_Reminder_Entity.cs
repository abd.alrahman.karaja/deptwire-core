﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LMS.Migrations
{
    public partial class Update_Reminder_Entity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Meetings_AbpUsers_UserId",
                table: "Meetings");

            migrationBuilder.DropForeignKey(
                name: "FK_Offers_AbpUsers_UserId",
                table: "Offers");

            migrationBuilder.DropForeignKey(
                name: "FK_Payments_AbpUsers_CollectedById1",
                table: "Payments");

            migrationBuilder.DropForeignKey(
                name: "FK_Payments_AbpUsers_UserId",
                table: "Payments");

            migrationBuilder.DropForeignKey(
                name: "FK_PromisesToPay_AbpUsers_UserId",
                table: "PromisesToPay");

            migrationBuilder.DropIndex(
                name: "IX_PromisesToPay_UserId",
                table: "PromisesToPay");

            migrationBuilder.DropIndex(
                name: "IX_Payments_CollectedById1",
                table: "Payments");

            migrationBuilder.DropIndex(
                name: "IX_Payments_UserId",
                table: "Payments");

            migrationBuilder.DropIndex(
                name: "IX_Offers_ReminderId",
                table: "Offers");

            migrationBuilder.DropIndex(
                name: "IX_Offers_UserId",
                table: "Offers");

            migrationBuilder.DropIndex(
                name: "IX_Meetings_ReminderId",
                table: "Meetings");

            migrationBuilder.DropIndex(
                name: "IX_Meetings_UserId",
                table: "Meetings");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "PromisesToPay");

            migrationBuilder.DropColumn(
                name: "CollectedById1",
                table: "Payments");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Payments");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Offers");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Meetings");

            migrationBuilder.AddColumn<int>(
                name: "MeetingId",
                table: "Reminders",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "OfferId",
                table: "Reminders",
                nullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "CollectedById",
                table: "Payments",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Payments_CollectedById",
                table: "Payments",
                column: "CollectedById");

            migrationBuilder.CreateIndex(
                name: "IX_Offers_ReminderId",
                table: "Offers",
                column: "ReminderId",
                unique: true,
                filter: "[ReminderId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Meetings_ReminderId",
                table: "Meetings",
                column: "ReminderId",
                unique: true,
                filter: "[ReminderId] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_Payments_AbpUsers_CollectedById",
                table: "Payments",
                column: "CollectedById",
                principalTable: "AbpUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Payments_AbpUsers_CollectedById",
                table: "Payments");

            migrationBuilder.DropIndex(
                name: "IX_Payments_CollectedById",
                table: "Payments");

            migrationBuilder.DropIndex(
                name: "IX_Offers_ReminderId",
                table: "Offers");

            migrationBuilder.DropIndex(
                name: "IX_Meetings_ReminderId",
                table: "Meetings");

            migrationBuilder.DropColumn(
                name: "MeetingId",
                table: "Reminders");

            migrationBuilder.DropColumn(
                name: "OfferId",
                table: "Reminders");

            migrationBuilder.AddColumn<long>(
                name: "UserId",
                table: "PromisesToPay",
                type: "bigint",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "CollectedById",
                table: "Payments",
                type: "int",
                nullable: true,
                oldClrType: typeof(long),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CollectedById1",
                table: "Payments",
                type: "bigint",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "UserId",
                table: "Payments",
                type: "bigint",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "UserId",
                table: "Offers",
                type: "bigint",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "UserId",
                table: "Meetings",
                type: "bigint",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_PromisesToPay_UserId",
                table: "PromisesToPay",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Payments_CollectedById1",
                table: "Payments",
                column: "CollectedById1");

            migrationBuilder.CreateIndex(
                name: "IX_Payments_UserId",
                table: "Payments",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Offers_ReminderId",
                table: "Offers",
                column: "ReminderId");

            migrationBuilder.CreateIndex(
                name: "IX_Offers_UserId",
                table: "Offers",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Meetings_ReminderId",
                table: "Meetings",
                column: "ReminderId");

            migrationBuilder.CreateIndex(
                name: "IX_Meetings_UserId",
                table: "Meetings",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Meetings_AbpUsers_UserId",
                table: "Meetings",
                column: "UserId",
                principalTable: "AbpUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Offers_AbpUsers_UserId",
                table: "Offers",
                column: "UserId",
                principalTable: "AbpUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Payments_AbpUsers_CollectedById1",
                table: "Payments",
                column: "CollectedById1",
                principalTable: "AbpUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Payments_AbpUsers_UserId",
                table: "Payments",
                column: "UserId",
                principalTable: "AbpUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PromisesToPay_AbpUsers_UserId",
                table: "PromisesToPay",
                column: "UserId",
                principalTable: "AbpUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
