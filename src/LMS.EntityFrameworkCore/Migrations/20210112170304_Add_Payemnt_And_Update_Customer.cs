﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LMS.Migrations
{
    public partial class Add_Payemnt_And_Update_Customer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CompanyName",
                table: "Customers");

            migrationBuilder.AddColumn<int>(
                name: "BankId",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CompanyName",
                table: "Cases",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "PaymentClaimedStatuses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(maxLength: 100, nullable: true),
                    Order = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaymentClaimedStatuses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PaymentReceivedStatuses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(maxLength: 100, nullable: true),
                    Order = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaymentReceivedStatuses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PromisesToPay",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    SettlementAmount = table.Column<double>(nullable: false),
                    SettlementDate = table.Column<DateTime>(nullable: true),
                    EMIS = table.Column<int>(nullable: false),
                    ModeOfPayment = table.Column<int>(nullable: false),
                    PtpAmount = table.Column<double>(nullable: false),
                    PaymentDate = table.Column<DateTime>(nullable: true),
                    CollectedPayment = table.Column<double>(nullable: false),
                    BankConfirmation = table.Column<bool>(nullable: false),
                    Note = table.Column<string>(nullable: true),
                    CaseId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PromisesToPay", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PromisesToPay_Cases_CaseId",
                        column: x => x.CaseId,
                        principalTable: "Cases",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TransferredStatuses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(maxLength: 100, nullable: true),
                    Order = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransferredStatuses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Payments",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    ReceiptNo = table.Column<string>(nullable: true),
                    PlaceOfPayment = table.Column<string>(nullable: true),
                    Amount = table.Column<double>(nullable: false),
                    AmountByAED = table.Column<double>(nullable: false),
                    TransferredStatusId = table.Column<int>(nullable: true),
                    TransferredDate = table.Column<DateTime>(nullable: true),
                    ReflectionDate = table.Column<DateTime>(nullable: true),
                    ClaimedStatusId = table.Column<int>(nullable: true),
                    ClaimedDate = table.Column<DateTime>(nullable: true),
                    BankRatio = table.Column<decimal>(nullable: false),
                    AgentRatio = table.Column<decimal>(nullable: false),
                    CommissionRatio = table.Column<decimal>(nullable: false),
                    CommissionAmount = table.Column<double>(nullable: false),
                    CommissionAmountAED = table.Column<double>(nullable: false),
                    CommissionAmountUSD = table.Column<double>(nullable: false),
                    ReceivedStatusId = table.Column<int>(nullable: true),
                    ReceivedDate = table.Column<DateTime>(nullable: true),
                    CurrencyId = table.Column<int>(nullable: true),
                    CollectedById = table.Column<int>(nullable: true),
                    CollectedById1 = table.Column<long>(nullable: true),
                    CaseId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Payments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Payments_Cases_CaseId",
                        column: x => x.CaseId,
                        principalTable: "Cases",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Payments_PaymentClaimedStatuses_ClaimedStatusId",
                        column: x => x.ClaimedStatusId,
                        principalTable: "PaymentClaimedStatuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Payments_AbpUsers_CollectedById1",
                        column: x => x.CollectedById1,
                        principalTable: "AbpUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Payments_Currencies_CurrencyId",
                        column: x => x.CurrencyId,
                        principalTable: "Currencies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Payments_PaymentReceivedStatuses_ReceivedStatusId",
                        column: x => x.ReceivedStatusId,
                        principalTable: "PaymentReceivedStatuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Payments_TransferredStatuses_TransferredStatusId",
                        column: x => x.TransferredStatusId,
                        principalTable: "TransferredStatuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Cases_BankId",
                table: "Cases",
                column: "BankId");

            migrationBuilder.CreateIndex(
                name: "IX_Payments_CaseId",
                table: "Payments",
                column: "CaseId");

            migrationBuilder.CreateIndex(
                name: "IX_Payments_ClaimedStatusId",
                table: "Payments",
                column: "ClaimedStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_Payments_CollectedById1",
                table: "Payments",
                column: "CollectedById1");

            migrationBuilder.CreateIndex(
                name: "IX_Payments_CurrencyId",
                table: "Payments",
                column: "CurrencyId");

            migrationBuilder.CreateIndex(
                name: "IX_Payments_ReceivedStatusId",
                table: "Payments",
                column: "ReceivedStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_Payments_TransferredStatusId",
                table: "Payments",
                column: "TransferredStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_PromisesToPay_CaseId",
                table: "PromisesToPay",
                column: "CaseId");

            migrationBuilder.AddForeignKey(
                name: "FK_Cases_Banks_BankId",
                table: "Cases",
                column: "BankId",
                principalTable: "Banks",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cases_Banks_BankId",
                table: "Cases");

            migrationBuilder.DropTable(
                name: "Payments");

            migrationBuilder.DropTable(
                name: "PromisesToPay");

            migrationBuilder.DropTable(
                name: "PaymentClaimedStatuses");

            migrationBuilder.DropTable(
                name: "PaymentReceivedStatuses");

            migrationBuilder.DropTable(
                name: "TransferredStatuses");

            migrationBuilder.DropIndex(
                name: "IX_Cases_BankId",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "BankId",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "CompanyName",
                table: "Cases");

            migrationBuilder.AddColumn<string>(
                name: "CompanyName",
                table: "Customers",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
