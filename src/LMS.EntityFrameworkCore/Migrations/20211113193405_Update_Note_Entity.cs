﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LMS.Migrations
{
    public partial class Update_Note_Entity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "EntityId",
                table: "CaseNotes",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntityName",
                table: "CaseNotes",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Type",
                table: "CaseNotes",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "NegotiatorCaseNotes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    EntityName = table.Column<string>(nullable: true),
                    EntityId = table.Column<int>(nullable: true),
                    CaseId = table.Column<int>(nullable: false),
                    NoteId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NegotiatorCaseNotes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_NegotiatorCaseNotes_Cases_CaseId",
                        column: x => x.CaseId,
                        principalTable: "Cases",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_NegotiatorCaseNotes_Notes_NoteId",
                        column: x => x.NoteId,
                        principalTable: "Notes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SupervisorCaseNotes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    EntityName = table.Column<string>(nullable: true),
                    EntityId = table.Column<int>(nullable: true),
                    CaseId = table.Column<int>(nullable: false),
                    NoteId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SupervisorCaseNotes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SupervisorCaseNotes_Cases_CaseId",
                        column: x => x.CaseId,
                        principalTable: "Cases",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SupervisorCaseNotes_Notes_NoteId",
                        column: x => x.NoteId,
                        principalTable: "Notes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_NegotiatorCaseNotes_CaseId",
                table: "NegotiatorCaseNotes",
                column: "CaseId");

            migrationBuilder.CreateIndex(
                name: "IX_NegotiatorCaseNotes_NoteId",
                table: "NegotiatorCaseNotes",
                column: "NoteId");

            migrationBuilder.CreateIndex(
                name: "IX_SupervisorCaseNotes_CaseId",
                table: "SupervisorCaseNotes",
                column: "CaseId");

            migrationBuilder.CreateIndex(
                name: "IX_SupervisorCaseNotes_NoteId",
                table: "SupervisorCaseNotes",
                column: "NoteId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "NegotiatorCaseNotes");

            migrationBuilder.DropTable(
                name: "SupervisorCaseNotes");

            migrationBuilder.DropColumn(
                name: "EntityId",
                table: "CaseNotes");

            migrationBuilder.DropColumn(
                name: "EntityName",
                table: "CaseNotes");

            migrationBuilder.DropColumn(
                name: "Type",
                table: "CaseNotes");
        }
    }
}
