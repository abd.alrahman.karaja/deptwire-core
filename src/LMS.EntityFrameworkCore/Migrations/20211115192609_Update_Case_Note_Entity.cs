﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LMS.Migrations
{
    public partial class Update_Case_Note_Entity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EntityId",
                table: "CaseNotes");

            migrationBuilder.DropColumn(
                name: "EntityName",
                table: "CaseNotes");

            migrationBuilder.AddColumn<int>(
                name: "ActionId",
                table: "CaseNotes",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ActionName",
                table: "CaseNotes",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ActionId",
                table: "CaseNotes");

            migrationBuilder.DropColumn(
                name: "ActionName",
                table: "CaseNotes");

            migrationBuilder.AddColumn<int>(
                name: "EntityId",
                table: "CaseNotes",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntityName",
                table: "CaseNotes",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
