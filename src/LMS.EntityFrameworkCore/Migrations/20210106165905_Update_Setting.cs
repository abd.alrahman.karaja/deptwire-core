﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LMS.Migrations
{
    public partial class Update_Setting : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Customers_Countries_CountryId",
                table: "Customers");

            migrationBuilder.DropIndex(
                name: "IX_Customers_CountryId",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "Address1",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "Address2",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "BirthDate",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "City",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "CountryId",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "Email",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "ForeignName",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "HomePhone",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "MobilePhone",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "Note",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "Number",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "OfficePhone",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "OtherPhone",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "PassportNumber",
                table: "Customers");

            migrationBuilder.AddColumn<int>(
                name: "Age",
                table: "Customers",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "ArabicName",
                table: "Customers",
                maxLength: 150,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CmEmail",
                table: "Customers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CmMobileNO",
                table: "Customers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CmOfficePhone",
                table: "Customers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CmOtherEmail",
                table: "Customers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CmOtherMobileNO",
                table: "Customers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CmOtherPhone",
                table: "Customers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CmOtherReferncePhone",
                table: "Customers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CmReferncePhone",
                table: "Customers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CmSites",
                table: "Customers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CompanyName",
                table: "Customers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CurrentJob",
                table: "Customers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CurrentLocation",
                table: "Customers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CustomerCareer",
                table: "Customers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CustomerEmailByBank",
                table: "Customers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CustomerMobileByBank",
                table: "Customers",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DOB",
                table: "Customers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Gender",
                table: "Customers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "HomeAddress",
                table: "Customers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "HomePhoneNumber",
                table: "Customers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "InsideOrOutsideBankCountry",
                table: "Customers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "InsideOrOutsideSyria",
                table: "Customers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MotherName",
                table: "Customers",
                maxLength: 150,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "PassportExpiry",
                table: "Customers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PassportNo",
                table: "Customers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ReferenceMobileByBank",
                table: "Customers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ReferenceNameByBank",
                table: "Customers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ResidenceNo",
                table: "Customers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "VisaNo",
                table: "Customers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AccountNo",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Activity",
                table: "Cases",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "AgentId",
                table: "Cases",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "AllocationDate",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ArabicFeedback",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BankCodeId",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BankFeedback",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BankHomeId",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BankId",
                table: "Cases",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "BankStatusId",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "BookingDate",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BucketStage",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CIF",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CloseDate",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ContractNo",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CustomerId",
                table: "Cases",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "DOX",
                table: "Cases",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "DPD",
                table: "Cases",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "FinalJugement",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FullAccount",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "InstallmentAmount",
                table: "Cases",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<bool>(
                name: "KeepBackToBank",
                table: "Cases",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<double>(
                name: "LastCreditLimit",
                table: "Cases",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "LastPaymentAmount",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastPaymentDate",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "LastPurchaseAmount",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastPurchaseDate",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LegalNote",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "LegalStatusId",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "LiabilityMail",
                table: "Cases",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<double>(
                name: "MainLedgerBalance",
                table: "Cases",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<string>(
                name: "Manufacturer",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "MinPayment",
                table: "Cases",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<int>(
                name: "ModeOfAllocation",
                table: "Cases",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "NOC",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NegotiatorNote",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "OpenDate",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "OriginalAmount",
                table: "Cases",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<double>(
                name: "Outstanding",
                table: "Cases",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "Overdues",
                table: "Cases",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<string>(
                name: "PoliceCaseAmount",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PoliceCaseDate",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PoliceCaseNo",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PoliceCaseStatus",
                table: "Cases",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "PoliceStation",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "PrincipalAmount",
                table: "Cases",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<int>(
                name: "ProductId",
                table: "Cases",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "SupervisorNote",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "TotalAmount",
                table: "Cases",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<string>(
                name: "TypeOfAssist",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "UserId",
                table: "Cases",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<string>(
                name: "WaedFeedback",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "WaedStatusId",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "WriteOffAge",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "WriteOffDate",
                table: "Cases",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "LegalStatus",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(maxLength: 100, nullable: true),
                    Order = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LegalStatus", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Product",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(maxLength: 100, nullable: true),
                    Order = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Product", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Cases_AgentId",
                table: "Cases",
                column: "AgentId");

            migrationBuilder.CreateIndex(
                name: "IX_Cases_BankCodeId",
                table: "Cases",
                column: "BankCodeId");

            migrationBuilder.CreateIndex(
                name: "IX_Cases_BankHomeId",
                table: "Cases",
                column: "BankHomeId");

            migrationBuilder.CreateIndex(
                name: "IX_Cases_BankId",
                table: "Cases",
                column: "BankId");

            migrationBuilder.CreateIndex(
                name: "IX_Cases_BankStatusId",
                table: "Cases",
                column: "BankStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_Cases_CustomerId",
                table: "Cases",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Cases_LegalStatusId",
                table: "Cases",
                column: "LegalStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_Cases_ProductId",
                table: "Cases",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_Cases_UserId",
                table: "Cases",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Cases_WaedStatusId",
                table: "Cases",
                column: "WaedStatusId");

            migrationBuilder.AddForeignKey(
                name: "FK_Cases_Agents_AgentId",
                table: "Cases",
                column: "AgentId",
                principalTable: "Agents",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Cases_BankCodes_BankCodeId",
                table: "Cases",
                column: "BankCodeId",
                principalTable: "BankCodes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Cases_Countries_BankHomeId",
                table: "Cases",
                column: "BankHomeId",
                principalTable: "Countries",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Cases_Banks_BankId",
                table: "Cases",
                column: "BankId",
                principalTable: "Banks",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Cases_CaseStatuses_BankStatusId",
                table: "Cases",
                column: "BankStatusId",
                principalTable: "CaseStatuses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Cases_Customers_CustomerId",
                table: "Cases",
                column: "CustomerId",
                principalTable: "Customers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Cases_LegalStatus_LegalStatusId",
                table: "Cases",
                column: "LegalStatusId",
                principalTable: "LegalStatus",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Cases_Product_ProductId",
                table: "Cases",
                column: "ProductId",
                principalTable: "Product",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Cases_AbpUsers_UserId",
                table: "Cases",
                column: "UserId",
                principalTable: "AbpUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Cases_CaseStatuses_WaedStatusId",
                table: "Cases",
                column: "WaedStatusId",
                principalTable: "CaseStatuses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cases_Agents_AgentId",
                table: "Cases");

            migrationBuilder.DropForeignKey(
                name: "FK_Cases_BankCodes_BankCodeId",
                table: "Cases");

            migrationBuilder.DropForeignKey(
                name: "FK_Cases_Countries_BankHomeId",
                table: "Cases");

            migrationBuilder.DropForeignKey(
                name: "FK_Cases_Banks_BankId",
                table: "Cases");

            migrationBuilder.DropForeignKey(
                name: "FK_Cases_CaseStatuses_BankStatusId",
                table: "Cases");

            migrationBuilder.DropForeignKey(
                name: "FK_Cases_Customers_CustomerId",
                table: "Cases");

            migrationBuilder.DropForeignKey(
                name: "FK_Cases_LegalStatus_LegalStatusId",
                table: "Cases");

            migrationBuilder.DropForeignKey(
                name: "FK_Cases_Product_ProductId",
                table: "Cases");

            migrationBuilder.DropForeignKey(
                name: "FK_Cases_AbpUsers_UserId",
                table: "Cases");

            migrationBuilder.DropForeignKey(
                name: "FK_Cases_CaseStatuses_WaedStatusId",
                table: "Cases");

            migrationBuilder.DropTable(
                name: "LegalStatus");

            migrationBuilder.DropTable(
                name: "Product");

            migrationBuilder.DropIndex(
                name: "IX_Cases_AgentId",
                table: "Cases");

            migrationBuilder.DropIndex(
                name: "IX_Cases_BankCodeId",
                table: "Cases");

            migrationBuilder.DropIndex(
                name: "IX_Cases_BankHomeId",
                table: "Cases");

            migrationBuilder.DropIndex(
                name: "IX_Cases_BankId",
                table: "Cases");

            migrationBuilder.DropIndex(
                name: "IX_Cases_BankStatusId",
                table: "Cases");

            migrationBuilder.DropIndex(
                name: "IX_Cases_CustomerId",
                table: "Cases");

            migrationBuilder.DropIndex(
                name: "IX_Cases_LegalStatusId",
                table: "Cases");

            migrationBuilder.DropIndex(
                name: "IX_Cases_ProductId",
                table: "Cases");

            migrationBuilder.DropIndex(
                name: "IX_Cases_UserId",
                table: "Cases");

            migrationBuilder.DropIndex(
                name: "IX_Cases_WaedStatusId",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "Age",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "ArabicName",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "CmEmail",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "CmMobileNO",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "CmOfficePhone",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "CmOtherEmail",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "CmOtherMobileNO",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "CmOtherPhone",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "CmOtherReferncePhone",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "CmReferncePhone",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "CmSites",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "CompanyName",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "CurrentJob",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "CurrentLocation",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "CustomerCareer",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "CustomerEmailByBank",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "CustomerMobileByBank",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "DOB",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "Gender",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "HomeAddress",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "HomePhoneNumber",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "InsideOrOutsideBankCountry",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "InsideOrOutsideSyria",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "MotherName",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "PassportExpiry",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "PassportNo",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "ReferenceMobileByBank",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "ReferenceNameByBank",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "ResidenceNo",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "VisaNo",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "AccountNo",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "Activity",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "AgentId",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "AllocationDate",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "ArabicFeedback",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "BankCodeId",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "BankFeedback",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "BankHomeId",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "BankId",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "BankStatusId",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "BookingDate",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "BucketStage",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "CIF",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "CloseDate",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "ContractNo",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "CustomerId",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "DOX",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "DPD",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "FinalJugement",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "FullAccount",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "InstallmentAmount",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "KeepBackToBank",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "LastCreditLimit",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "LastPaymentAmount",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "LastPaymentDate",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "LastPurchaseAmount",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "LastPurchaseDate",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "LegalNote",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "LegalStatusId",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "LiabilityMail",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "MainLedgerBalance",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "Manufacturer",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "MinPayment",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "ModeOfAllocation",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "NOC",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "NegotiatorNote",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "OpenDate",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "OriginalAmount",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "Outstanding",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "Overdues",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "PoliceCaseAmount",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "PoliceCaseDate",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "PoliceCaseNo",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "PoliceCaseStatus",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "PoliceStation",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "PrincipalAmount",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "ProductId",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "SupervisorNote",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "TotalAmount",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "TypeOfAssist",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "WaedFeedback",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "WaedStatusId",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "WriteOffAge",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "WriteOffDate",
                table: "Cases");

            migrationBuilder.AddColumn<string>(
                name: "Address1",
                table: "Customers",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Address2",
                table: "Customers",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "BirthDate",
                table: "Customers",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "City",
                table: "Customers",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CountryId",
                table: "Customers",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "Customers",
                type: "nvarchar(150)",
                maxLength: 150,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ForeignName",
                table: "Customers",
                type: "nvarchar(150)",
                maxLength: 150,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "HomePhone",
                table: "Customers",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MobilePhone",
                table: "Customers",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Note",
                table: "Customers",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Number",
                table: "Customers",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OfficePhone",
                table: "Customers",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OtherPhone",
                table: "Customers",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PassportNumber",
                table: "Customers",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Customers_CountryId",
                table: "Customers",
                column: "CountryId");

            migrationBuilder.AddForeignKey(
                name: "FK_Customers_Countries_CountryId",
                table: "Customers",
                column: "CountryId",
                principalTable: "Countries",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
