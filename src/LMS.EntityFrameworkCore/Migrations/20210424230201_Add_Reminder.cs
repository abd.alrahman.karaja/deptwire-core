﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LMS.Migrations
{
    public partial class Add_Reminder : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Meetings_Reminders_ReminderId",
                table: "Meetings");

            migrationBuilder.DropForeignKey(
                name: "FK_Offers_Reminders_ReminderId",
                table: "Offers");

            migrationBuilder.DropForeignKey(
                name: "FK_Payments_Reminders_ReminderId",
                table: "Payments");

            migrationBuilder.DropForeignKey(
                name: "FK_PromisesToPay_Reminders_ReminderId",
                table: "PromisesToPay");

            migrationBuilder.DropIndex(
                name: "IX_PromisesToPay_ReminderId",
                table: "PromisesToPay");

            migrationBuilder.DropIndex(
                name: "IX_Payments_ReminderId",
                table: "Payments");

            migrationBuilder.DropIndex(
                name: "IX_Offers_ReminderId",
                table: "Offers");

            migrationBuilder.DropIndex(
                name: "IX_Meetings_ReminderId",
                table: "Meetings");

            migrationBuilder.DropColumn(
                name: "ReminderId",
                table: "PromisesToPay");

            migrationBuilder.DropColumn(
                name: "ReminderId",
                table: "Payments");

            migrationBuilder.DropColumn(
                name: "ReminderId",
                table: "Offers");

            migrationBuilder.DropColumn(
                name: "ReminderId",
                table: "Meetings");

            migrationBuilder.AddColumn<DateTime>(
                name: "ReminderDate",
                table: "PromisesToPay",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ReminderNote",
                table: "PromisesToPay",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ReminderDate",
                table: "Payments",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ReminderNote",
                table: "Payments",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ReminderDate",
                table: "Offers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ReminderNote",
                table: "Offers",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ReminderDate",
                table: "Meetings",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ReminderNote",
                table: "Meetings",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Reminders_MeetingId",
                table: "Reminders",
                column: "MeetingId");

            migrationBuilder.CreateIndex(
                name: "IX_Reminders_OfferId",
                table: "Reminders",
                column: "OfferId");

            migrationBuilder.AddForeignKey(
                name: "FK_Reminders_Meetings_MeetingId",
                table: "Reminders",
                column: "MeetingId",
                principalTable: "Meetings",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Reminders_Offers_OfferId",
                table: "Reminders",
                column: "OfferId",
                principalTable: "Offers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Reminders_Meetings_MeetingId",
                table: "Reminders");

            migrationBuilder.DropForeignKey(
                name: "FK_Reminders_Offers_OfferId",
                table: "Reminders");

            migrationBuilder.DropIndex(
                name: "IX_Reminders_MeetingId",
                table: "Reminders");

            migrationBuilder.DropIndex(
                name: "IX_Reminders_OfferId",
                table: "Reminders");

            migrationBuilder.DropColumn(
                name: "ReminderDate",
                table: "PromisesToPay");

            migrationBuilder.DropColumn(
                name: "ReminderNote",
                table: "PromisesToPay");

            migrationBuilder.DropColumn(
                name: "ReminderDate",
                table: "Payments");

            migrationBuilder.DropColumn(
                name: "ReminderNote",
                table: "Payments");

            migrationBuilder.DropColumn(
                name: "ReminderDate",
                table: "Offers");

            migrationBuilder.DropColumn(
                name: "ReminderNote",
                table: "Offers");

            migrationBuilder.DropColumn(
                name: "ReminderDate",
                table: "Meetings");

            migrationBuilder.DropColumn(
                name: "ReminderNote",
                table: "Meetings");

            migrationBuilder.AddColumn<int>(
                name: "ReminderId",
                table: "PromisesToPay",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ReminderId",
                table: "Payments",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ReminderId",
                table: "Offers",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ReminderId",
                table: "Meetings",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_PromisesToPay_ReminderId",
                table: "PromisesToPay",
                column: "ReminderId");

            migrationBuilder.CreateIndex(
                name: "IX_Payments_ReminderId",
                table: "Payments",
                column: "ReminderId");

            migrationBuilder.CreateIndex(
                name: "IX_Offers_ReminderId",
                table: "Offers",
                column: "ReminderId",
                unique: true,
                filter: "[ReminderId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Meetings_ReminderId",
                table: "Meetings",
                column: "ReminderId",
                unique: true,
                filter: "[ReminderId] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_Meetings_Reminders_ReminderId",
                table: "Meetings",
                column: "ReminderId",
                principalTable: "Reminders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Offers_Reminders_ReminderId",
                table: "Offers",
                column: "ReminderId",
                principalTable: "Reminders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Payments_Reminders_ReminderId",
                table: "Payments",
                column: "ReminderId",
                principalTable: "Reminders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PromisesToPay_Reminders_ReminderId",
                table: "PromisesToPay",
                column: "ReminderId",
                principalTable: "Reminders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
