﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LMS.Migrations
{
    public partial class PoliceCaseStatus : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PoliceCaseStatus",
                table: "Cases");

            migrationBuilder.AddColumn<int>(
                name: "PoliceCaseStatusId",
                table: "Cases",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "PoliceCaseStatuses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(maxLength: 100, nullable: true),
                    Order = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PoliceCaseStatuses", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Cases_PoliceCaseStatusId",
                table: "Cases",
                column: "PoliceCaseStatusId");

            migrationBuilder.AddForeignKey(
                name: "FK_Cases_PoliceCaseStatuses_PoliceCaseStatusId",
                table: "Cases",
                column: "PoliceCaseStatusId",
                principalTable: "PoliceCaseStatuses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cases_PoliceCaseStatuses_PoliceCaseStatusId",
                table: "Cases");

            migrationBuilder.DropTable(
                name: "PoliceCaseStatuses");

            migrationBuilder.DropIndex(
                name: "IX_Cases_PoliceCaseStatusId",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "PoliceCaseStatusId",
                table: "Cases");

            migrationBuilder.AddColumn<int>(
                name: "PoliceCaseStatus",
                table: "Cases",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
