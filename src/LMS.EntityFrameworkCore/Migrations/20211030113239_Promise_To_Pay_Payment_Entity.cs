﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LMS.Migrations
{
    public partial class Promise_To_Pay_Payment_Entity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PromiseToPayPayment_PromisesToPay_PromiseToPayId",
                table: "PromiseToPayPayment");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PromiseToPayPayment",
                table: "PromiseToPayPayment");

            migrationBuilder.RenameTable(
                name: "PromiseToPayPayment",
                newName: "PromisesToPayPayments");

            migrationBuilder.RenameIndex(
                name: "IX_PromiseToPayPayment_PromiseToPayId",
                table: "PromisesToPayPayments",
                newName: "IX_PromisesToPayPayments_PromiseToPayId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_PromisesToPayPayments",
                table: "PromisesToPayPayments",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_PromisesToPayPayments_PromisesToPay_PromiseToPayId",
                table: "PromisesToPayPayments",
                column: "PromiseToPayId",
                principalTable: "PromisesToPay",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PromisesToPayPayments_PromisesToPay_PromiseToPayId",
                table: "PromisesToPayPayments");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PromisesToPayPayments",
                table: "PromisesToPayPayments");

            migrationBuilder.RenameTable(
                name: "PromisesToPayPayments",
                newName: "PromiseToPayPayment");

            migrationBuilder.RenameIndex(
                name: "IX_PromisesToPayPayments_PromiseToPayId",
                table: "PromiseToPayPayment",
                newName: "IX_PromiseToPayPayment_PromiseToPayId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_PromiseToPayPayment",
                table: "PromiseToPayPayment",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_PromiseToPayPayment_PromisesToPay_PromiseToPayId",
                table: "PromiseToPayPayment",
                column: "PromiseToPayId",
                principalTable: "PromisesToPay",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
