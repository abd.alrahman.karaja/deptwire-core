﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LMS.Migrations
{
    public partial class Remove_Bank_Reference_From_Case : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cases_Banks_BankId",
                table: "Cases");

            migrationBuilder.DropIndex(
                name: "IX_Cases_BankId",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "BankId",
                table: "Cases");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "BankId",
                table: "Cases",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Cases_BankId",
                table: "Cases",
                column: "BankId");

            migrationBuilder.AddForeignKey(
                name: "FK_Cases_Banks_BankId",
                table: "Cases",
                column: "BankId",
                principalTable: "Banks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
