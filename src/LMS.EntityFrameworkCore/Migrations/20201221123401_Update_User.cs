﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LMS.Migrations
{
    public partial class Update_User : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "TeamLeaderId",
                table: "AbpUsers",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserType",
                table: "AbpUsers",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_AbpUsers_TeamLeaderId",
                table: "AbpUsers",
                column: "TeamLeaderId");

            migrationBuilder.AddForeignKey(
                name: "FK_AbpUsers_AbpUsers_TeamLeaderId",
                table: "AbpUsers",
                column: "TeamLeaderId",
                principalTable: "AbpUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AbpUsers_AbpUsers_TeamLeaderId",
                table: "AbpUsers");

            migrationBuilder.DropIndex(
                name: "IX_AbpUsers_TeamLeaderId",
                table: "AbpUsers");

            migrationBuilder.DropColumn(
                name: "TeamLeaderId",
                table: "AbpUsers");

            migrationBuilder.DropColumn(
                name: "UserType",
                table: "AbpUsers");
        }
    }
}
