﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LMS.Migrations
{
    public partial class Add_Indexes_Entity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cases_LegalStatus_LegalStatusId",
                table: "Cases");

            migrationBuilder.DropPrimaryKey(
                name: "PK_LegalStatus",
                table: "LegalStatus");

            migrationBuilder.RenameTable(
                name: "LegalStatus",
                newName: "LegalStatuses");

            migrationBuilder.AddPrimaryKey(
                name: "PK_LegalStatuses",
                table: "LegalStatuses",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "FileCategories",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(maxLength: 100, nullable: true),
                    Order = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FileCategories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TempCustomers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(maxLength: 150, nullable: true),
                    ArabicName = table.Column<string>(maxLength: 150, nullable: true),
                    MotherName = table.Column<string>(maxLength: 150, nullable: true),
                    Gender = table.Column<string>(nullable: true),
                    Age = table.Column<int>(nullable: false),
                    CompanyName = table.Column<string>(nullable: true),
                    DOB = table.Column<DateTime>(nullable: true),
                    CustomerMobileByBank = table.Column<string>(nullable: true),
                    CustomerEmailByBank = table.Column<string>(nullable: true),
                    ReferenceNameByBank = table.Column<string>(nullable: true),
                    ReferenceMobileByBank = table.Column<string>(nullable: true),
                    PassportNo = table.Column<string>(nullable: true),
                    PassportExpiry = table.Column<DateTime>(nullable: true),
                    ResidenceNo = table.Column<string>(nullable: true),
                    VisaNo = table.Column<string>(nullable: true),
                    InsideOrOutsideBankCountry = table.Column<string>(nullable: true),
                    InsideOrOutsideSyria = table.Column<string>(nullable: true),
                    CurrentLocation = table.Column<string>(nullable: true),
                    CustomerCareer = table.Column<string>(nullable: true),
                    CurrentJob = table.Column<string>(nullable: true),
                    HomeAddress = table.Column<string>(nullable: true),
                    CmMobileNO = table.Column<string>(nullable: true),
                    CmOtherMobileNO = table.Column<string>(nullable: true),
                    HomePhoneNumber = table.Column<string>(nullable: true),
                    CmOtherPhone = table.Column<string>(nullable: true),
                    CmOfficePhone = table.Column<string>(nullable: true),
                    CmReferncePhone = table.Column<string>(nullable: true),
                    CmOtherReferncePhone = table.Column<string>(nullable: true),
                    CmEmail = table.Column<string>(nullable: true),
                    CmOtherEmail = table.Column<string>(nullable: true),
                    CmSites = table.Column<string>(nullable: true),
                    NationalityId = table.Column<int>(nullable: false),
                    CustomerId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TempCustomers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TempCustomers_Customers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TempCustomers_Nationalities_NationalityId",
                        column: x => x.NationalityId,
                        principalTable: "Nationalities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "CaseUploadFiles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Path = table.Column<string>(nullable: true),
                    FileCategoryId = table.Column<int>(nullable: true),
                    CaseId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CaseUploadFiles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CaseUploadFiles_Cases_CaseId",
                        column: x => x.CaseId,
                        principalTable: "Cases",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CaseUploadFiles_FileCategories_FileCategoryId",
                        column: x => x.FileCategoryId,
                        principalTable: "FileCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CaseUploadFiles_CaseId",
                table: "CaseUploadFiles",
                column: "CaseId");

            migrationBuilder.CreateIndex(
                name: "IX_CaseUploadFiles_FileCategoryId",
                table: "CaseUploadFiles",
                column: "FileCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_TempCustomers_CustomerId",
                table: "TempCustomers",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_TempCustomers_NationalityId",
                table: "TempCustomers",
                column: "NationalityId");

            migrationBuilder.AddForeignKey(
                name: "FK_Cases_LegalStatuses_LegalStatusId",
                table: "Cases",
                column: "LegalStatusId",
                principalTable: "LegalStatuses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cases_LegalStatuses_LegalStatusId",
                table: "Cases");

            migrationBuilder.DropTable(
                name: "CaseUploadFiles");

            migrationBuilder.DropTable(
                name: "TempCustomers");

            migrationBuilder.DropTable(
                name: "FileCategories");

            migrationBuilder.DropPrimaryKey(
                name: "PK_LegalStatuses",
                table: "LegalStatuses");

            migrationBuilder.RenameTable(
                name: "LegalStatuses",
                newName: "LegalStatus");

            migrationBuilder.AddPrimaryKey(
                name: "PK_LegalStatus",
                table: "LegalStatus",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Cases_LegalStatus_LegalStatusId",
                table: "Cases",
                column: "LegalStatusId",
                principalTable: "LegalStatus",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
