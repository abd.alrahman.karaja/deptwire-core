﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LMS.Migrations
{
    public partial class Add_CustomerId_To_Social_Media_Entity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CustomerOfficePhones_Customers_CustomerId",
                table: "CustomerOfficePhones");

            migrationBuilder.DropForeignKey(
                name: "FK_CustomerSocialMediaAccounts_Customers_CustomerId",
                table: "CustomerSocialMediaAccounts");

            migrationBuilder.AlterColumn<int>(
                name: "CustomerId",
                table: "CustomerSocialMediaAccounts",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "CustomerId",
                table: "CustomerOfficePhones",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_CustomerOfficePhones_Customers_CustomerId",
                table: "CustomerOfficePhones",
                column: "CustomerId",
                principalTable: "Customers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CustomerSocialMediaAccounts_Customers_CustomerId",
                table: "CustomerSocialMediaAccounts",
                column: "CustomerId",
                principalTable: "Customers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CustomerOfficePhones_Customers_CustomerId",
                table: "CustomerOfficePhones");

            migrationBuilder.DropForeignKey(
                name: "FK_CustomerSocialMediaAccounts_Customers_CustomerId",
                table: "CustomerSocialMediaAccounts");

            migrationBuilder.AlterColumn<int>(
                name: "CustomerId",
                table: "CustomerSocialMediaAccounts",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "CustomerId",
                table: "CustomerOfficePhones",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_CustomerOfficePhones_Customers_CustomerId",
                table: "CustomerOfficePhones",
                column: "CustomerId",
                principalTable: "Customers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_CustomerSocialMediaAccounts_Customers_CustomerId",
                table: "CustomerSocialMediaAccounts",
                column: "CustomerId",
                principalTable: "Customers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
