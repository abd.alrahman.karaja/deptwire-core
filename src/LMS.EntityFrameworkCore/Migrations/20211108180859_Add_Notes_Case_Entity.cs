﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LMS.Migrations
{
    public partial class Add_Notes_Case_Entity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "LegalNote",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NegotiatorNote",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SupervisorNote",
                table: "Cases",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LegalNote",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "NegotiatorNote",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "SupervisorNote",
                table: "Cases");
        }
    }
}
