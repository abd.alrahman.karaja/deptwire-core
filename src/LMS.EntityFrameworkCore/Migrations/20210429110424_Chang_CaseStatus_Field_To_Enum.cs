﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LMS.Migrations
{
    public partial class Chang_CaseStatus_Field_To_Enum : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BankCodes_CaseStatuses_CaseStatusId",
                table: "BankCodes");

            migrationBuilder.DropForeignKey(
                name: "FK_Cases_CaseStatuses_BankStatusId",
                table: "Cases");

            migrationBuilder.DropForeignKey(
                name: "FK_Cases_CaseStatuses_WaedStatusId",
                table: "Cases");

            migrationBuilder.DropForeignKey(
                name: "FK_WorkflowSettings_CaseStatuses_CaseStatusId",
                table: "WorkflowSettings");

            migrationBuilder.DropTable(
                name: "CaseStatuses");

            migrationBuilder.DropIndex(
                name: "IX_WorkflowSettings_CaseStatusId",
                table: "WorkflowSettings");

            migrationBuilder.DropIndex(
                name: "IX_Cases_BankStatusId",
                table: "Cases");

            migrationBuilder.DropIndex(
                name: "IX_Cases_WaedStatusId",
                table: "Cases");

            migrationBuilder.DropIndex(
                name: "IX_BankCodes_CaseStatusId",
                table: "BankCodes");

            migrationBuilder.DropColumn(
                name: "CaseStatusId",
                table: "WorkflowSettings");

            migrationBuilder.DropColumn(
                name: "BankStatusId",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "WaedStatusId",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "CaseStatusId",
                table: "BankCodes");

            migrationBuilder.AddColumn<int>(
                name: "CaseStatus",
                table: "WorkflowSettings",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "BankStatus",
                table: "Cases",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "WaedStatus",
                table: "Cases",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "CaseStatus",
                table: "BankCodes",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CaseStatus",
                table: "WorkflowSettings");

            migrationBuilder.DropColumn(
                name: "BankStatus",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "WaedStatus",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "CaseStatus",
                table: "BankCodes");

            migrationBuilder.AddColumn<int>(
                name: "CaseStatusId",
                table: "WorkflowSettings",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "BankStatusId",
                table: "Cases",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "WaedStatusId",
                table: "Cases",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CaseStatusId",
                table: "BankCodes",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "CaseStatuses",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Order = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CaseStatuses", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WorkflowSettings_CaseStatusId",
                table: "WorkflowSettings",
                column: "CaseStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_Cases_BankStatusId",
                table: "Cases",
                column: "BankStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_Cases_WaedStatusId",
                table: "Cases",
                column: "WaedStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_BankCodes_CaseStatusId",
                table: "BankCodes",
                column: "CaseStatusId");

            migrationBuilder.AddForeignKey(
                name: "FK_BankCodes_CaseStatuses_CaseStatusId",
                table: "BankCodes",
                column: "CaseStatusId",
                principalTable: "CaseStatuses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Cases_CaseStatuses_BankStatusId",
                table: "Cases",
                column: "BankStatusId",
                principalTable: "CaseStatuses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Cases_CaseStatuses_WaedStatusId",
                table: "Cases",
                column: "WaedStatusId",
                principalTable: "CaseStatuses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_WorkflowSettings_CaseStatuses_CaseStatusId",
                table: "WorkflowSettings",
                column: "CaseStatusId",
                principalTable: "CaseStatuses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
