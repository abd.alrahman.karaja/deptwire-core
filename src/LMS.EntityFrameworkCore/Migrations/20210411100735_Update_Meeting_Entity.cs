﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LMS.Migrations
{
    public partial class Update_Meeting_Entity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ManagerTeamName",
                table: "Meetings");

            migrationBuilder.DropColumn(
                name: "MeetingManagerName",
                table: "Meetings");

            migrationBuilder.AddColumn<long>(
                name: "ManagerTeamNameId",
                table: "Meetings",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "MeetingManagerNameId",
                table: "Meetings",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Meetings_ManagerTeamNameId",
                table: "Meetings",
                column: "ManagerTeamNameId");

            migrationBuilder.CreateIndex(
                name: "IX_Meetings_MeetingManagerNameId",
                table: "Meetings",
                column: "MeetingManagerNameId");

            migrationBuilder.AddForeignKey(
                name: "FK_Meetings_AbpUsers_ManagerTeamNameId",
                table: "Meetings",
                column: "ManagerTeamNameId",
                principalTable: "AbpUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Meetings_AbpUsers_MeetingManagerNameId",
                table: "Meetings",
                column: "MeetingManagerNameId",
                principalTable: "AbpUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Meetings_AbpUsers_ManagerTeamNameId",
                table: "Meetings");

            migrationBuilder.DropForeignKey(
                name: "FK_Meetings_AbpUsers_MeetingManagerNameId",
                table: "Meetings");

            migrationBuilder.DropIndex(
                name: "IX_Meetings_ManagerTeamNameId",
                table: "Meetings");

            migrationBuilder.DropIndex(
                name: "IX_Meetings_MeetingManagerNameId",
                table: "Meetings");

            migrationBuilder.DropColumn(
                name: "ManagerTeamNameId",
                table: "Meetings");

            migrationBuilder.DropColumn(
                name: "MeetingManagerNameId",
                table: "Meetings");

            migrationBuilder.AddColumn<string>(
                name: "ManagerTeamName",
                table: "Meetings",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MeetingManagerName",
                table: "Meetings",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
